ALTER TABLE
  api
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  articles_event
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  contact
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  countries
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  devises
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  entree_type
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  esquerylog
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  event_flyers
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  event_lieu
ADD
  created_at DATETIME DEFAULT NULL;
ALTER TABLE
  event_local
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  event_type
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  event_videos
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  locality
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  newsletter_abonne
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  abonne_by_eventlieu DROP FOREIGN KEY abonne_by_eventlieu_ibfk_1;
ALTER TABLE
  abonne_by_eventlieu
ADD
  CONSTRAINT FK_3DF7C743C325A696 FOREIGN KEY (abonne_id) REFERENCES newsletter_abonne (id);
ALTER TABLE
  abonne_by_countries DROP FOREIGN KEY abonne_by_countries_ibfk_1;
ALTER TABLE
  abonne_by_countries
ADD
  CONSTRAINT FK_13628D9EC325A696 FOREIGN KEY (abonne_id) REFERENCES newsletter_abonne (id);
ALTER TABLE
  quartier
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  region
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL;
ALTER TABLE
  fos_user
ADD
  created_at DATETIME DEFAULT NULL,
ADD
  updated_at DATETIME DEFAULT NULL,
  DROP locked,
  DROP expired,
  DROP expires_at,
  DROP credentials_expired,
  DROP credentials_expire_at,
  CHANGE salt salt VARCHAR(255) DEFAULT NULL;