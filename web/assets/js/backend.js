addEvent( document , 'DOMContentLoaded', function() {

    /**
     * jQuery Selector
     */

    //Select generique
    if ($(".select2").length) {
        $(".select2").select2();
    }

    //Datemask dd/mm/yyyy
    if (jQuery(".datamask").length) {
        jQuery(".datamask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
    }

    //Tél
    if (selector_Telinput.length) {
        selector_Telinput.intlTelInput({
            utilsScript: "/bower_components/intl-tel-input/lib/libphonenumber/build/utils.js?8"
        });
    }

    
    if ( selector_Typeahead.length ) {
        var videosearch = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
            ,limit:10
            //,remote: '/rest/event?name=%QUERY'
            ,remote: {
                url: ajax_url_Event+'?name=%QUERY',
                wildcard: '%QUERY'
            }
        });

        videosearch.initialize();

        var templatehandle = '<table style="width:100%;border-collapse: collapse;"><tr style="border-top:1px solid #ccc;"><td style="padding:10px;" rowspan="2"><img alt="" height="70" width="70" src="{{flyer}}"></td><td style="vertical-align:top;padding:10px;"><strong>{{name}}</strong></td></tr><tr><td style="vertical-align:top;padding:10px;"><strong>{{dateformat}}</strong></td></tr></table>';
        Handlebars.registerHelper('dateformat', function() {
            var dateUnique = Handlebars.escapeExpression(this.dateUnique);
            dateUnique = getDateByObject(this);

            return new Handlebars.SafeString(
              dateUnique
            );
          });
        selector_Typeahead.typeahead(null, {
            name: 'videosearch',
            limit: 50,
            displayKey: 'name',
            source: videosearch.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message">Non trouvé.</div>'
                ].join('\n')
                ,suggestion: Handlebars.compile(templatehandle)
                ,pending: function(data) {
                    return '<div class="empty-message">Recherche en cours de '+data.query+'...</div>'
                }

            }
        });

        selector_Typeahead.bind('typeahead:selected', function(obj, datum, name) {
            //alert( datum.videoytid );

        });
    }

    //Atwho
    if (selector_ckeditor ) {
        CKEDITOR.replace( 'name_ck', {
            language: 'fr',
            removePlugins: 'toolbar,elementspath' ,
            resize_enabled : false ,
            height : '2em',
            contentsCss: '/assets/css/ckeditor.css',
            tabIndex: 1,
            blockedKeystrokes : [13]
        } );

        CKEDITOR.instances['name_ck'].on('contentDom', function() {

            CKEDITOR.instances['name_ck'].on('key', function(e){
                //console.log(e.data.keyCode);
                if(e.data.keyCode == 13){ // Do this code when ENTER is pressed
                    //document.getElementById('editor2').focus();
                    return false;
                }
                //console.log(CKEDITOR.instances['name_ck'].document.getBody().getText() );
                fillFieldEventName();
            });

            CKEDITOR.instances['name_ck'].on('blur', function(e){

                fillFieldEventName();
                //$('.collapse').collapse('hide');


                console.log(CKEDITOR.instances['name_ck'].getData() );
                var dataeval = CKEDITOR.instances['name_ck'].getData();
                var results = new Array();
                var resultsLieux = new Array();
                $('.dataeval', dataeval).each(function(){
                    console.log( $(this).text()+"-"+$(this).data('type')+"-"+$(this).data('id') );
                    var artiste = {
                        "name" : $(this).text(),
                        "id" : $(this).data('id'),
                        "photo" : $(this).data('photo')
                    };

                    if ($(this).data('country')!='') {
                        artiste.country = {};
                        artiste.country.id = "";
                        artiste.country.name = $(this).data('country');
                    }

                    if ($(this).data('region')!='') {
                        artiste.region = {};
                        artiste.region.id = "";
                        artiste.region.name = $(this).data('region');
                    }

                    if ($(this).data('locality')!='') {
                        artiste.locality = {};
                        artiste.locality.id = "";
                        artiste.locality.name = $(this).data('locality');
                    }

                    if ($(this).data('quartier')!='') {
                        artiste.quartier = {};
                        artiste.quartier.id = "";
                        artiste.quartier.name = $(this).data('quartier');
                    }

                    if('artiste' === $(this).data('type') && jQuery.inArray($(this).data('id'), artistsIDinOptions )<0 ) {
                        results.push(artiste);
                        artistsIDinOptions.push($(this).data('id'));
                    }

                    if('lieux' === $(this).data('type') && jQuery.inArray($(this).data('id'), lieuxIDinOptions )<0  ) {
                        resultsLieux.push(artiste);
                        lieuxIDinOptions.push($(this).data('id'));
                    }
                });

                createListLieuxAndTriggerSelect2(resultsLieux);
                createListArtisteAndTriggerSelect2(results);
            });


            this.document.on('mode', function(e) {
                load_atwho(this, at_config, at_conf_lieux);
            });

            // First load
            load_atwho(CKEDITOR.instances['name_ck'], at_config, at_conf_lieux);
        });
    }

    /**
     * -------  Init Select2 ------------
     */
    
    //Event in Artiste  Select2
    if ( selector_Event_Inarticle.length>0 ) {
        initSelect2Event(selector_Event_Inarticle);
    }

    //Event Select2
    if ( selector_Event.length>0) {
        initSelect2Event(selector_Event);
    }

    //Lieu Select2
    if (selector_lieu.length>0) {
        initSelect2Lieu(selector_lieu, '.select2_lieux');
    }
    
    //Fete Select2
    if (selector_Fete.length>0) {
        initSelect2Fete();
    }
    
    //Artistes
    if (selector_Artiste.length>0) {
        initSelect2Artiste();
    }
    
    //Related event
    if (selector_RelatedEvent.length>0) {
        initSelect2Event(selector_RelatedEvent);
    }

    // Parent Event local
    if (selector_ParentEventLocal.length>0) {
        selector_ParentEventLocal.select2();

    }

    //Event in Event by member  Select2
    if ( selector_Event_Ineventbymember.length>0 ) {
        initSelect2EventByMember(selector_Event_Ineventbymember);
    }

    //Parent Artistes Select2
    if (selector_parentArtistes.length>0) {
        initSelect2ParentArtiste();
    }

    // Enfant Artistes Select2
    if (selector_enfantArtistes.length>0) {
        initSelect2EnfantArtiste();
    }
     
    /**
     * -------  Fill Select2 field ------------
     */ 
    

    if ( selector_artistesdj && parseInt(selector_artistesdj.value) > 0 ) {
        
        let idparam = selector_artistesdj.value;
        fillSelect2ById(idparam, ajax_url_Artiste, selector_Artiste, createListArtisteAndTriggerSelect2, createListArtistes );
    } 
     

    if ( selector_eventmultilieu && parseInt(selector_eventmultilieu.value) > 0 ) {
        
        let idparam = selector_eventmultilieu.value;
        let fnCB = function(){};
        fillSelect2ById(idparam, ajax_url_lieu, selector_lieu, createListLieuxAndTriggerSelect2,fnCB,parseDataGenerique  );
    }

    

    if ( selector_eventlocal && parseInt(selector_eventlocal.value) > 0 ) {
        
        let idparam = selector_eventlocal.value;
        let fnCB = function(){};
        fillSelect2ById(idparam, ajax_url_Fete, selector_Fete,createListLocalAndTriggerSelect2,fnCB,parseDataGenerique    );
    }

     

    if ( selector_related_event && parseInt(selector_related_event.value) > 0 ) {
        
        let idparam = selector_related_event.value;
        let fnCB = function(){};
        fillSelect2ById(idparam, ajax_url_RelatedEvent, selector_RelatedEvent, createListEventsRelatedAndTriggerSelect2, fnCB, parseDataGenerique  );
    }
    
    /**
     * Event in articles
     */    
    if ( document.getElementById('mdts_homebundle_articlesevent_event') &&  document.getElementById('mdts_homebundle_articlesevent_event').value != '' ) {
        
        var idparam = document.getElementById('mdts_homebundle_articlesevent_event').value;
        var fnCB = function(){};
        fillSelect2ById(idparam, ajax_url_Event, selector_Event_Inarticle, createListEventsRelatedAndTriggerSelect2, fnCB, parseDataGenerique  );
    }
    
    if ( document.getElementById('mdts_homebundle_articlesevent_artiste') &&  document.getElementById('mdts_homebundle_articlesevent_artiste').value != '' ) {
        
        var idparam = document.getElementById('mdts_homebundle_articlesevent_artiste').value;
        var fnCB = function (){};
        fillSelect2ById(idparam, ajax_url_Artiste, selector_Event_Inarticle, createListArtisteAndTriggerSelect2, fnCB );
    }
    
    /**
     * Articles event
     */
    if ( document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_event') &&  document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_event').value != '' ) {
      
        var idparam = document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_event').value;
        var fnCB = function (){};
        fillSelect2ById(idparam, ajax_url_Event, selector_Event, createListEventsAndTriggerSelect2, fnCB, parseDataGenerique );
    }

    // Parent artiste
    if ( document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_parentartisteid') && eval(document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_parentartisteid').value) > 0 ) {

        let idparam = document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_parentartisteid').value;
        let fnCB = function (){};
        fillSelect2ById(idparam, ajax_url_Artiste, selector_parentArtistes, createListParentArtisteAndTriggerSelect2, fnCB );
    }

    // Enfant artiste
    if ( document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_enfantartisteid') && eval(document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_enfantartisteid').value) > 0 ) {

        let idparam = document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_enfantartisteid').value;
        let fnCB = function (){};
        fillSelect2ById(idparam, ajax_url_Artiste, selector_enfantArtistes, createListEnfantsArtisteAndTriggerSelect2, fnCB );
    }

    // Event by member
    if ( document.getElementById('mdts_frontendbundle_eventbymember_eventunmapped') &&  document.getElementById('mdts_frontendbundle_eventbymember_eventunmapped').value != '' ) {

        var idparam = document.getElementById('mdts_frontendbundle_eventbymember_eventunmapped').value;
        var fnCB = function (){};
        fillSelect2ById(idparam, ajax_url_Event, selector_Event_Ineventbymember, createOptionsAndTriggerSelect2, fnCB, parseDataGenerique  );
    }

    /**
     * -------  onClick Event ------------
     */
    //Click mainflyer
    if ( selector_mainFlyer.length>0 ) {
        setOnClickMainFlyer();
    }
    
    //Click delete flyer
    if ( selector_deleteFlyer.length>0 ) {
        setOnClickDeleteFlyer();
    }
    
    //Click sur bouton Delete
    if (selector_btndeleteitem.length>0) {
        setOnClickBtndeleteitem();
    }

    
    //Click checkbox enddate
    if ( checkbox_Enddate ) {
        setOnClickCheckboxEnddate();
    }
    
    if ( checkbox_All ) {
        setOnClickCheckboxAll();
    }
    
    if ( selector_button_All.length>0 ) {
        setOnClickButtonAll();
    }
    
    if ( selector_button_Add.length>0) {
        setOnClickButtonAdd();
    }
 
    
    /**
     * Prices
     */
    if ('undefined' !== typeof jsonprice && jsonprice.length ) {
    selector_fieldPrice.remove();

    var eld = divContainer_Price ;
    var dataprototype_html = eld.getAttribute('data-prototype');

    if ( jsonprice.length > 0 ) {
      for( var i=0; i<jsonprice.length; i++ ) {
        dataprototype_html = dataprototype_html.replace(/__name__/g, i);

        dataprototype_html = dataprototype_html.replace(/class="price form-control"/g, 'class="price form-control" value="'+jsonprice[i].price+'"');
        dataprototype_html = dataprototype_html.replace(/class="detailprice form-control"/g, 'class="detailprice form-control" value="'+jsonprice[i].detailprice+'"');

        var find = 'option value="'+jsonprice[i].devise+'"';
        var re = new RegExp(find,'g');
        var replace = 'option value="'+jsonprice[i].devise+'" selected="selected"';
        dataprototype_html = dataprototype_html.replace(re,replace);

        eld.innerHTML += dataprototype_html;
      }
    }

    

    var i = 0;
    [].forEach.call(eld.querySelectorAll(".blocclone"), function(el) {

      [].forEach.call(el.querySelectorAll("a"), function(el) {

          //Si on n'est pas à la fin
          if ( eval(i+1) < eld.querySelectorAll(".blocclone").length ) {

              if ( el.classList.contains('btnadd')) {
                el.classList.add('hidden');
              } 

              el.classList.remove('hidden');
              
          }
          
      });
       
      var labelelement = el.getElementsByTagName('label');
      var inputelement = el.getElementsByTagName('input');
      var selectelement = el.getElementsByTagName('select');

      for( var j=0; j<labelelement.length; j++ ) {
        labelelement[j].setAttribute('for', labelelement[j].getAttribute('for').replace(new RegExp('0','g'), i) );        
      }

      for( var j=0; j<inputelement.length; j++ ) {
        inputelement[j].setAttribute('name', inputelement[j].getAttribute('name').replace(new RegExp('0','g'), i) );        
        inputelement[j].setAttribute('id', inputelement[j].getAttribute('id').replace(new RegExp('0','g'), i) );        
      }

      for( var j=0; j<selectelement.length; j++ ) {
        selectelement[j].setAttribute('name', selectelement[j].getAttribute('name').replace(new RegExp('0','g'), i) );        
        selectelement[j].setAttribute('id', selectelement[j].getAttribute('id').replace(new RegExp('0','g'), i) );        
      }
       

      i++;

    });   

    
  }
  if (field_optionEntreetype && field_optionEntreetype.options[field_optionEntreetype.selectedIndex].value == 2 ) {
    field_price.classList.remove('hidden');
   }
   
   
    /**
     * -------  clone  ------------
     */
    
    
   /**
    * Clone Videos
    */
   if ('undefined' !== typeof jsoneventvideos &&  jsoneventvideos.length ) {
       createCloneVideo();
    }
    /**
     * Clone articles in artistes
     */    
    if (document.querySelectorAll('.blocclone_articles_artiste').length && 'undefined' !== typeof jsonarticlesevent &&   jsonarticlesevent.length ) {
        createCloneArticlesInArtiste();
    }

    /**
     * Clone Articles
     */
    if (document.querySelectorAll('.blocclone_articles').length && 'undefined' !== typeof jsonarticlesevent && jsonarticlesevent.length ) {
        createCloneArticles();
    }
    
    /**
     * Clone Articles
     */
    if (document.querySelectorAll('.blocclone_tel').length && 'undefined' !== typeof jsontelslieu && jsontelslieu.length ) {
        createCloneTelsLieu();
    }
   
   
    /**
     * Image flyer
     */
    if ( 'undefined' !=  typeof imageByFlyer ) {

        divContainer_Image.innerHTML = '<img src="' + imageByFlyer + '" class="img-responsive">';
        enableCropper();
    }
    
    
    
    /**
     * -------  onSubmit Event ------------
     */

    //Validation du formulaire Lieu
    jQuery(document).on('submit','form[name=mdts_homebundle_eventlieu]',function(evt){
        setOnSubmitFormLieu();
    });
    
    
    /**
     * Submit form mdts_homebundle_articlesevent
     */
    if ( document.querySelectorAll(dom_formArticles).length>0 ) {
        setOnSubmitFormArticles();
    }
    // Submit form artistes
    if ( document.querySelectorAll(dom_formArtistes).length>0 ) {
        setOnSubmitFormArtistes();
    }

    // Submit form event
    if ( document.querySelectorAll(dom_formEvent).length>0 ) {
        setOnSubmitFormEvent();
    }
    if ( selector_formEventMultiple ) {
        setOnSubmitFormEventMultiple();
    }
    // Submit form event local
    if (document.querySelectorAll(dom_formLocal).length>0) {
        setOnSubmitFormLocal();
    }
    // Submit form event by member
    if ( document.querySelectorAll(dom_formEvent_member).length>0 ) {
        setOnSubmitFormEventMember();
    }

    // Submit form checkbox IDS
    if ( document.querySelectorAll(dom_form_ids).length>0 ) {
        setOnSubmitFormIds();
    }




     

    /**
     * -------  onChange Event ------------
     */
    if ( field_Country ) {
        setOnEventChange(field_Country, eventChangeCountry );
        setOnEventSelect2(jQuery('#mdts_homebundle_eventlieu_country'), eventChangeCountry );

    }
    
    if ( field_Region ) {
        setOnEventChange(field_Region, eventChangeRegion );
        setOnEventSelect2(jQuery('#mdts_homebundle_eventlieu_region'), eventChangeRegion );
    }
    
    if ( field_Locality ) {
        setOnEventChange(field_Locality, eventChangeLocality );
        setOnEventSelect2(jQuery('#mdts_homebundle_eventlieu_locality'), eventChangeLocality );
    }
    
    if ( field_Event_Entree ) {
        let fieldoption = addNewToOption();
        field_Event_Entree.appendChild(fieldoption);
        // setOnEventChange(field_Event_Entree, eventChangeEventEntree );
        setOnEventSelect2(jQuery("#mdts_homebundle_event_entreetype"), eventChangeEventEntree );
    }
    
    if ( field_Event_File ) {
        setOnEventChange(field_Event_File, eventChangeFile );
    }
    
    if ( field_Artiste_File ) {
        setOnEventChange(field_Artiste_File, eventChangeArtisteFile );
    }
    
    if ( field_filedrag ) {
        addEvent(field_filedrag,'drop', function(event) {
            event.preventDefault();
            event.target.className = '';


            var i = 0,
            files = event.dataTransfer.files;
            
            changeFiles(files);
        });
        
        addEvent(field_filedrag,'dragover', function(event) {
            //console.log('addEventListener dragover - event.type: '+event.type);
            event.stopPropagation();
            event.preventDefault();
            event.target.className = (event.type == "dragover" ? "hover" : "");
        });
        
        addEvent(field_filedrag,'paste', function(event) {
            // use event.originalEvent.clipboard for newer chrome versions
            var items = (event.clipboardData  || event.originalEvent.clipboardData).items;
             

            if( (field_Event_filenameajax && field_Event_filenameajax.value != '') || field_mainflyerId != null ) {
                flyerexiste = true;
            }

            if ( document.querySelectorAll('div[data-context]').length > 0 ) {
                flyerexiste = false;
            }


             
            console.log("flyerexiste : "+flyerexiste+" - loadedimge length : "+selector_loadedimge.length);

            // find pasted image among pasted items
            var blob = null;
            for (var i = 0; i < items.length; i++) {
                if (items[i].type.indexOf("image") === 0) {
                    blob = items[i].getAsFile();
                }
            }
            // load image if there is a pasted image
            if (blob !== null) {
                
                var reader = new FileReader();
                reader.onload = function(event) {
                     
                    divContainer_Resultpic.classList.remove('hidden');
                    document.getElementById(divimage).innerHTML = '<div style="position:relative;"><span style="position:absolute;z-index:1;margin-top:80px;width:100%;background: rgba(255,255,255,0.5);padding:20px;font-weight:bold;font-size:1.3em;" class="text-center center-block">Upload en cours...</span></span><img class="img-responsive" style="opacity:0.2" src="'+event.target.result+'" /></div>';
                    document.getElementById(divimage).scrollIntoView();
                    
                    saveFlyerToServer(event.target.result);
                     
                };
                reader.readAsDataURL(blob);
            }
        });


        
        
    }

    if ( selector_option_addNew.length>0 ) {
        [].forEach.call(selector_option_addNew, function(el) {
            let fieldoption = addNewToOption();
            el.appendChild(fieldoption);
            setOnEventSelect2(jQuery('#'+el.id), eventChangeGeneric );
        });
    }



    
    
    /**
     * -------  Duplicate action ------------
     */

    if ( document.getElementById('collectionContainer')) {
        duplicatePrice();
    }

    if ( document.getElementById('collectionvideoContainer')) {
        duplicateVideo();
    }

    if ( document.getElementById('collectionflyerContainer')) {
        duplicateFlyer();
    }

    if ( document.getElementById('collectiontelContainer')) {
        duplicateLieuTel();
    }

    if ( document.getElementById('collectionarticlesContainer')) {
        duplicateArticles();
    }

    if ( document.getElementById('collectionarticlesartisteContainer')) {
        duplicateArticlesArtiste();
    }
    
    if(divContainer_Resultpic && divContainer_Resultpic.querySelectorAll("input[type=hidden]").length > 0 ) {
        [].forEach.call(divContainer_Resultpic.querySelectorAll("input[type=hidden]"), function(el) {
            tab_hidden.push( el.getAttribute("id"));
        });
    }
    
    if (divContainer_Resultpic && typeof url !='undefined' && url !== '' ) {
        divContainer_Resultpic.classList.remove('hidden');
        document.getElementById(divimage).innerHTML = 'Patientez...';

        saveFlyerToServer(url);
    }

     

    //Keyup sur .select2-search__field

    var search__field = document.querySelectorAll(".select2-search__field");
    [].forEach.call(search__field, function(el) {
        el.addEventListener('keyup', function(evt) {
            console.log('Value: #'+el.value+'#');
            sessionStorage.setItem("search__field",el.value);
        });
    });



    //Click sur bouton getImageEvent

    if ( document.getElementById('getImageEvent') ) {
        getImageInEvent();
    }

     

    if (document.querySelectorAll('div[data-toolcrop]').length > 0)
        clickToolbarCrop();


    

    if ( document.getElementById('successEditArtiste')) {
        reFreshParentIframe();
    }

    addEvent( window , 'message', function(e) {
        getMessage(e);
    });
    
    //Google MAP
    if ( document.querySelectorAll(pathQuery_BtnCarteGoogle).length > 0 ) {
        addEvent( document.querySelector(pathQuery_BtnCarteGoogle) , 'click', function(e) {
            openMapAndSearch(8);
        });
        
        addEvent( document.getElementById('pac-input') , 'keypress', function(event) {
          // console.log("event.keyCode = "+event.keyCode);
          if (event.keyCode == 13) {
            event.preventDefault();
          }
        });
    }
    
    //Clic close iframe ou retour
    if ( btn_closeparentiframe && 'function' === typeof callCloseIframe ) {
        addEvent( btn_closeparentiframe , 'click', function(event) {
            console.log("Clic btn_closeparentiframe " );
            callCloseIframe();
        });
    }

    // Clic rangedate
    if ( selector_dataShowrangedate.length>0 && 'function' === typeof callClickShowrangedate ) {

        [].forEach.call(selector_dataShowrangedate, function(el) {
            addEvent( el , 'click', function(event) {
                // console.log("Clic btn_closeparentiframe " );
                callClickShowrangedate();
            });
        });
    }

    // Clic resetdate
    if ( selector_dataResetdate.length>0 && 'function' === typeof callClickShowresetdate ) {

        [].forEach.call(selector_dataResetdate, function(el) {
            addEvent( el , 'click', function(event) {
                callClickShowresetdate();
            });
        });
    }

    // Clic resetdate
    if ( selector_dataShowmultiplesdate.length>0 && 'function' === typeof callClickShowmultiplesdate ) {

        [].forEach.call(selector_dataShowmultiplesdate, function(el) {
            addEvent( el , 'click', function(event) {
                callClickShowmultiplesdate();
            });
        });
    }

    // Autocomplete recherche pays
    if ( document.getElementById('remote_search_paysregioncommunequartier')!=null ) {
        setRemoteSearchPaysRegionCommuneQuartier();
    }




    
    // Si flyer_eventbymember
    if( 'undefined' !== typeof flyer_eventbymember ) {
        callbackUploadFlyer(flyer_eventbymember);
    }

    // Event on change Select pagination
    if ( selector_dataSelectpagination.length>0 && 'function' === typeof callEventChangeSelectpagination ) {

        [].forEach.call(selector_dataSelectpagination, function(el) {
            addEvent( el , 'change', function(event) {
                callEventChangeSelectpagination(event);
            });
        });
    }

    // Event on click btn upload
    if ( selector_btnUpload.length>0 && 'function' === typeof callEventClickBtnupload ) {

        [].forEach.call(selector_btnUpload, function(el) {
            addEvent( el , 'click', function(event) {
                callEventClickBtnupload(event);
            });
        });
    }
   

});
 

(function() {
    //function alterClass(el, className, enable) {
    function alterClass(el, className ) {
        if (el) {
            //el.className = el.className.replace(RegExp('(^|\\s)' + className + '(\\s|$)'), '$2') + (enable ? ' ' + className : '');
            el.className = el.className.replace(RegExp('(^|\\s)' + className + '(\\s|$)'), '$2') ;
        }
    }

    function trCheck(el) {

        var tr = parentTag(el, 'tr');

        //alterClass(tr, 'checked', el.checked);
        alterClass(tr, 'checked');
        //if (el.form && el.form['all'] && el.form['all'].onclick) { el.form['all'].onclick(); }
    }


    function isTag(el, tag) {
        var re = new
            RegExp('^(' + tag + ')$', 'i');
        return re.test(el.tagName);
    }

    function getTarget(event) {
        return event.target || event.srcElement;
    }

    function parentTag(el, tag) {
        while (el && !isTag(el, tag)) {
            el = el.parentNode;
        }
        return el;
    }

    function checkboxClick(event, el) {
        if (!el.name) {
            return;
        }
        if (event.shiftKey && (!lastChecked || lastChecked.name == el.name)) {
            var
                checked = (lastChecked ? lastChecked.checked : true);
            var
                inputs = parentTag(el, 'table').getElementsByTagName('input');
            var
                checking = !lastChecked;
            for (var
                     i = 0; i < inputs.length; i++) {
                var
                    input = inputs[i];
                if (input.name === el.name) {
                    if (checking) {
                        input.checked = checked;
                        trCheck(input);
                    }
                    if (input === el || input === lastChecked) {
                        if (checking) {
                            break;
                        }
                        checking = true;
                    }
                }
            }
        } else { lastChecked = el; }
    }

    function tableClick(event, click) {
        click = (click || !window.getSelection || getSelection().isCollapsed);
        var el = getTarget(event);
        while (!isTag(el, 'tr')) {
            if (isTag(el, 'table|a|input|textarea')) {
                console.log(el.type);
                if (el.type != 'checkbox') {
                    return;
                }
                checkboxClick(event, el);
                click = false;
            }
            el = el.parentNode;
            if (!el) {
                return;
            }
        }
        el = el.firstChild.firstChild;
        if (click) {
            //el.checked = !el.checked;
            //el.onclick && el.onclick();
        }
        trCheck(el);
    }
    
    if ( document.querySelectorAll('table.table').length>0 ) {
        addEvent( document.querySelector('table.table') , 'click', function(e) {
            console.log(e);
            tableClick(e);
        } );
    }

     

})();


