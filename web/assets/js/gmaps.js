function fillTxtField(lat,lng)
{
    document.getElementById('mdts_homebundle_eventlieu_gps').value = lat +","+lng;
}

document.addEventListener('DOMContentLoaded', function() {
    var map;
    var markers = [];


     
    
     
    function initMap() {

        var tab_gps_value = document.getElementById('mdts_homebundle_eventlieu_gps').value.split(',');
        var latvalue = (tab_gps_value.length==2 && tab_gps_value[0]!=''?eval(tab_gps_value[0]):-18.91554305193574);
        var lngvalue = (tab_gps_value.length==2 && tab_gps_value[1]!=''?eval(tab_gps_value[1]):47.52160906791687);

        //alert( tab_gps_value.length+'*'+typeof tab_gps_value+' / '+ typeof latvalue+' - '+typeof lngvalue);

        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: latvalue, lng: lngvalue},
            zoom: 8
        });

        addMarker({lat: latvalue, lng: lngvalue} ,latvalue,lngvalue);
      

     
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
          var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            anchorPoint: new google.maps.Point(0, -29)
        });

        google.maps.event.addListener(marker,'dragstart',function(event) 
        {
            console.log('dragstart');
            infowindow.close();
        });
        google.maps.event.addListener(marker,'dragend',function(event) 
        {
            //document.getElementById('lat').value =event.latLng.lat();
            //document.getElementById('lng').value =event.latLng.lng();
             alert(event.latLng.lat()+","+event.latLng.lng());
        });

         
        /*google.maps.event.addListener(map, 'click', function(event) {
            var myLatLng = event.latLng;
            var lat = myLatLng.lat();
            var lng = myLatLng.lng();
            alert(lat+","+lng);

            placeMarker(myLatLng, map);
        });*/ 

        map.addListener('click', function(event) {
            var myLatLng = event.latLng;
            var lat = myLatLng.lat();
            var lng = myLatLng.lng();
            //alert(lat+","+lng);
            deleteMarkers();
            addMarker(event.latLng,lat,lng);
        });
        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
        }
        function clearMarkers() {
          setMapOnAll(null);
        }
        function deleteMarkers() {
          clearMarkers();
          markers = [];
        }
        function addMarker(location,lat,lng) {
          var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true/*,
            animation: google.maps.Animation.DROP,
            anchorPoint: new google.maps.Point(0, -29)*/
          });
          fillTxtField( lat,lng );

          var infowindow = new google.maps.InfoWindow();
          infowindow.setContent('<div><strong>' + lat +","+lng+ '</strong><p style="text-align:center;"><a class="btn btn-default" onclick="fillTxtField('+lat+','+lng+')">Recupérer ces coordonnées</a></p>' );
          infowindow.open(map, marker);
          google.maps.event.addListener(marker,'dragstart',function(event) 
            {
                console.log('dragstart');
                infowindow.close();
            });
          google.maps.event.addListener(marker,'dragend',function(event) 
            {

                //document.getElementById('lat').value =event.latLng.lat();
                //document.getElementById('lng').value =event.latLng.lng();
                 //alert(event.latLng.lat()+","+event.latLng.lng());
                 infowindow.setContent('<div><strong>' + event.latLng.lat()+","+event.latLng.lng() + '</strong><p style="text-align:center;"><a class="btn btn-default" onclick="fillTxtField('+event.latLng.lat()+','+event.latLng.lng()+')">Recupérer ces coordonnées</a></p>' );
                infowindow.open(map, marker);
                //fillTxtField( event.latLng.lat(),event.latLng.lng() );
            });
          google.maps.event.addListener(marker,'click',function(event) 
            {
                
                infowindow.setContent('<div><strong>' + event.latLng.lat()+","+event.latLng.lng()  + '</strong><p style="text-align:center;"><a class="btn btn-default" onclick="fillTxtField('+event.latLng.lat()+','+event.latLng.lng()+')">Recupérer ces coordonnées</a></p>' );
                infowindow.open(map, marker);
                //fillTxtField( event.latLng.lat(),event.latLng.lng() );  
            });

          markers.push(marker);
        }

        function placeMarker(position, map) {
            var marker = new google.maps.Marker({
              position: position,
              map: map
            });  
            map.panTo(position);
        }
         

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            //marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
              window.alert("Autocomplete's returned place contains no geometry");
              return;
            }

            //alert(place.geometry.location.lat()+","+place.geometry.location.lng());

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);  // Why 17? Because it looks good.
            }
            /*marker.setIcon(({
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(35, 35)
            }));*/
            /*marker.setPosition(place.geometry.location);
            marker.setVisible(true);*/

            addMarker(place.geometry.location,place.geometry.location.lat(),place.geometry.location.lng());


            var address = '';
            var componentForm = {
              street_number: 'short_name',
              route: 'long_name',
              locality: 'long_name',
              administrative_area_level_1: 'short_name',
              country: 'long_name',
              postal_code: 'short_name'
            };
            if (place.address_components) {
              address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
              ].join(' ');
            }
            var 
            street_number
            ,route 
            ,locality
            ,administrative_area_level_1 
            ,country
            ,postal_code 
            ,administrative_area_level_2
            ,administrative_area_level_3
            ,administrative_area_level_4
            ,administrative_area_level_5
            ,colloquial_area
            ,ward
            ,sublocality
            ,neighborhood
            ,premise
            ,subpremise
            ,natural_feature
            ,point_of_interest
             = '';

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    //document.getElementById(addressType).value = val;
                    console.log(addressType+' = '+val);
                    
                    if ( addressType == 'street_number' )
                        street_number = (val!='undefined'? val : '');

                    if ( addressType == 'route' )
                        route = (val!='undefined'? val : '');

                    if ( addressType == 'locality' )
                        locality = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_1' )
                        administrative_area_level_1 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_2' )
                        administrative_area_level_2 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_3' )
                        administrative_area_level_3 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_4' )
                        administrative_area_level_2 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_5' )
                        administrative_area_level_5 = (val!='undefined'? val : '');

                    if ( addressType == 'country' )
                        country = (val!='undefined'? val : '');

                    if ( addressType == 'postal_code' )
                        postal_code = (val!='undefined'? val : '');



                }
              }
            //alert(document.getElementById('mdts_homebundle_eventlieu_address').value+" - "+address);
            if ( address != '' ) {
                address = (street_number>0?street_number:'')+(street_number>0?' ':'')+(route!='undefined'?route:'')+(route!=''?' ':'')+(postal_code!='undefined'?postal_code:'')+(postal_code!=''?' ':'')+(locality!='undefined'?locality:'')+(locality!=''?' - ':'')+(administrative_area_level_1!='undefined'?administrative_area_level_1:'')+(administrative_area_level_1!=''?' - ':'')+(country!='undefined'?country:'');
                document.getElementById('mdts_homebundle_eventlieu_address').value = address;

                //ajax save
                var params = "country="+country+"&region="+administrative_area_level_1+"&locality="+locality;
                var r = new XMLHttpRequest(); 
                r.open("POST", "/rest/savealls", true);

                //POST Request
                r.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                r.setRequestHeader("Content-length", params.length);
                r.setRequestHeader("Connection", "close");

                r.onreadystatechange = function () {
                    if (r.readyState != 4 || r.status != 200) return; 
                    var jsonObj = JSON.parse(r.responseText);
                    if (  typeof jsonObj.success == 'string' ) {
                         
                        if ( document.getElementById('mdts_homebundle_eventlieu_country') ) {
                          for(var i=0; i<document.getElementById('mdts_homebundle_eventlieu_country').options.length; i++) {
                            if ( document.getElementById('mdts_homebundle_eventlieu_country').options[i].value == jsonObj.country_id ) {
                              document.getElementById('mdts_homebundle_eventlieu_country').selectedIndex = i;
                              break;
                            }
                          }
                          if (document.getElementById('mdts_homebundle_eventlieu_country').selectedIndex<=0) 
                            document.getElementById('mdts_homebundle_eventlieu_country').innerHTML += '<option selected="selected" value="'+jsonObj.country_id+'">'+jsonObj.country_name+'</option>';
                        }

                        if ( document.getElementById('mdts_homebundle_eventlieu_region') ) {
                          for(var i=0; i<document.getElementById('mdts_homebundle_eventlieu_region').options.length; i++) {
                            if ( document.getElementById('mdts_homebundle_eventlieu_region').options[i].value == jsonObj.region_id ) {
                              document.getElementById('mdts_homebundle_eventlieu_region').selectedIndex = i;
                              break;
                            }
                          }
                          if (document.getElementById('mdts_homebundle_eventlieu_region').selectedIndex<=0) 
                            document.getElementById('mdts_homebundle_eventlieu_region').innerHTML += '<option selected="selected" value="'+jsonObj.region_id+'">'+jsonObj.region_name+'</option>';
                        }
                        if ( document.getElementById('mdts_homebundle_eventlieu_locality') ) {
                            for(var i=0; i<document.getElementById('mdts_homebundle_eventlieu_locality').options.length; i++) {
                              if ( document.getElementById('mdts_homebundle_eventlieu_locality').options[i].value == jsonObj.locality_id ) {
                                document.getElementById('mdts_homebundle_eventlieu_locality').selectedIndex = i;
                                break;
                              }
                            }
                            if (document.getElementById('mdts_homebundle_eventlieu_locality').selectedIndex<=0)
                              document.getElementById('mdts_homebundle_eventlieu_locality').innerHTML += '<option selected="selected" value="'+jsonObj.locality_id+'">'+jsonObj.locality_name+'</option>';
                        }


                    }
                    
                };
                r.send(params);

                
                
            }

            /*infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);*/
        });

    }

//initMap();
 
});