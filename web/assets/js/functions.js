/**
 * ----------------- Fonctions -----------------
 */
function getFilePathExtension(path) {
    var filename = path.split('\\').pop().split('/').pop();
    var lastIndex = filename.lastIndexOf(".");
    if (lastIndex < 1) return "";
    return filename.substr(lastIndex + 1);
}
function getFileName(path) {
    var filename = path.split('\\').pop().split('/').pop();
    return filename;
}
function addEvent(obj, evt, fn) {
    if (obj.addEventListener ){
        obj.addEventListener(evt, fn, false);
    }
    else if (obj.attachEvent) {
        obj.attachEvent("on" + evt, fn);
    }
}
function getDateByObject(repo)
{
    var date_str = '';
    
    moment.locale('fr');
    var moment_dateUnique = moment(repo.dateUnique, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('x');
    var moment_heureDebut = moment(repo.heureDebut, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('x');
    var moment_heureFin = moment(repo.heureFin, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('x');
    var moment_dateFin = moment(repo.dateFin, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('x');
    var moment_dateDebut = moment(repo.dateDebut, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('x');
    
    if ( moment_dateUnique>0 ) {
        date_str = moment(repo.dateUnique, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('DD MMMM YYYY');
        if ( moment_heureDebut>0 ) {
            if ( moment_heureFin>0 )
                date_str += ' de ';
            else
                date_str += ' à ';

            date_str += moment(repo.heureDebut, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('HH:mm');
        }
        if ( moment_heureFin>0 ) {
            date_str += ' à '+moment(repo.heureFin, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('HH:mm');
        }
    } else if(moment_dateDebut>0 && moment_dateFin>0) {
        date_str = moment(repo.dateDebut, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('DD MMMM YYYY')+' - '+moment(repo.dateFin, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('DD MMMM YYYY');

    } else if(moment_dateDebut>0 && moment_dateFin<0) {
        date_str = moment(repo.dateDebut, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('DD MMMM YYYY') ;

    } else if(moment_dateDebut<0 && moment_dateFin>0) {
        date_str = moment(repo.dateFin, 'YYYY-MM-DD[T]HH:mm:ss[Z]').format('DD MMMM YYYY') ;

    }
    
    return date_str;
}
 function formatSelect2TemplateResultEventInArtiste(repo)
 {
    if (repo.loading) return repo.text;
    
    var date_str = getDateByObject(repo);
    var markup = '<table><tr><td style="padding:5px;" rowspan="2"><img width="50" height="50" src="'+repo.flyer+'" /></td><td style="padding:5px;" valign="top">'+repo.name+'<td></tr><tr><td style="padding:5px;" valign="top">'+date_str+'</td></tr></table>';



    return markup;
 }
  

/**
 * Generique pour afficher le resultat à la recherche d'une selection
 *
 * @param repo
 * @returns {*}
 */
function formatSelect2TemplateResultGenerique(repo)
{
    // console.log('formatSelect2TemplateResultGenerique : '+repo);
    if (repo.loading) return repo.text;
    var markup = repo.name;
    return markup;
}
/**
 * Generique pour afficher le resultat apres clic d'une selection
 *
 * @param repo
 * @returns {*}
 */
function formatSelect2TemplateResultSelection  (repo) {
    console.log('formatSelect2TemplateResultSelection : '+repo);
    return repo.name || repo.text;
}
function formatSelect2TemplateResultLieu(repo) {
    if (repo.loading) return repo.text;

    var markup = repo.name;
    var photo = repo.logo;

    if ( 'string' === typeof photo ) {
        photo = '/media/cache/crop_100_100/assets/flyers/'+photo;
    } else {
        photo = '/assets/img/people.png';
    }

    markup = '<table><tr><td valign="top" width="40" style="padding:3px;"><img src="'+photo+'" width="40" height="40" /></td><td valign="middle" style="padding-left:3px;">'+repo.name+'</td></table>';
    return markup;
}
function formatSelect2TemplateResultSelectionLieu(repo) {
    //var markup = repo.name || repo.text;

    var name = (repo.name || repo.text);
    var id = repo.id;
    var image = '/assets/img/people.png';

    if ('string' === typeof repo.logo ) {
        image = '/media/cache/crop_100_100/assets/flyers/'+repo.logo;
    }
    if ('string' === typeof repo.element.getAttribute('data-logo') ) {
        image = '/media/cache/crop_100_100/assets/flyers/'+repo.element.getAttribute('data-logo');
    }

    var markup = '<table><tr><td valign="top" width="50" style="padding:3px;" class="photocontainer_'+id+'"><img src="'+image+'" width="40" /></td><td valign="middle">'+name+'</td></table>';
    return markup;
}
function formatSelect2TemplateResultArtiste (repo) {
    if (repo.loading) return repo.text;

    var markup = repo.name;
    var photo = repo.photo;

    if ( 'string' === typeof photo ) {
        photo = '/assets/artistes/'+photo.replace(/\.jpg/,'.png');
    } else {
        photo = '/assets/img/people.png';
    }

    markup = '<table data-func="formatSelect2TemplateResultArtiste"><tr><td valign="top" width="40" style="padding:3px;"><img src="'+photo+'" width="40" height="40" /></td><td valign="middle" style="padding-left:3px;">'+repo.name+'</td></table>';
    return markup;
}


function formatSelect2TemplateResultSelectionArtiste (repo) {

    // alert(repo.name);
     //var markup = repo.name || repo.text;
    var markup = '<table data-func="formatSelect2TemplateResultSelectionArtiste146"><tr><td valign="top" width="50" style="padding:3px;" class="photocontainer_'+repo.id+'"><img src="/assets/img/people.png" width="40" /></td><td valign="middle">'+(repo.name || repo.text)+'</td></tr></table>';

    if ('string' === typeof repo.photo ) {
        markup = '<table data-func="formatSelect2TemplateResultSelectionArtiste149"><tr><td valign="top" width="50" style="padding:3px;" class="photocontainer_'+repo.id+'"><img class="circleimage_'+repo.id+'" src="/assets/artistes/'+repo.photo.replace(/\.jpg/,'.png')+'" height="40" width="40" /></td><td valign="middle" class="artistename_'+repo.id+'">'+(repo.name || repo.text)+'</td></tr></table>';
    } else if ('string' === typeof repo.element.getAttribute('data-photo') ) {
        markup = '<table data-func="formatSelect2TemplateResultSelectionArtiste151"><tr><td valign="top" width="50" style="padding:3px;" class="photocontainer_'+repo.id+'"><img class="circleimage_'+repo.id+'" src="/assets/artistes/'+repo.element.getAttribute('data-photo').replace(/\.jpg/,'.png')+'" height="40" width="40"  /></td><td valign="middle" class="artistename_'+repo.id+'">'+repo.text+'</td></tr></table>';
    }

    // console.log(markup);
    return markup;
}

function formatSelect2TemplateResultSelectionEvent(repo)
{
    

    var flyer = repo.flyer;
    if ('string' === typeof repo.element.getAttribute('data-flyer')) {
        flyer = '/media/cache/my_thumb_70/assets/flyers/'+ repo.element.getAttribute('data-flyer');
        repo = {
            'dateUnique' : repo.element.getAttribute('data-date_unique'),
            'heureDebut' : repo.element.getAttribute('data-heure_debut'),
            'heureFin' : repo.element.getAttribute('data-heure_fin'),
            'dateFin' : repo.element.getAttribute('data-date_fin'),
            'dateDebut' : repo.element.getAttribute('data-date_debut'),
            'name' : (repo.name || repo.text)
        };
    }
    
    var date_str = getDateByObject(repo);
    var markup = '<table><tr><td style="padding:5px;" rowspan="2"><img width="50" height="50" src="'+flyer+'" /></td><td style="padding:5px;" valign="top">'+(repo.name || repo.text)+'<td></tr><tr><td style="padding:5px;" valign="top">'+date_str+'</td></tr></table>';

    return markup;
}


function formatSelect2TemplateResultFete (repo) {
    // console.log( repo.element );
    if (repo.loading) return repo.text;


    let markup = repo.name;
    let dateDebut = repo.startdate;
    let dateFin = repo.enddate;

    if ( 'function' !== typeof moment )
        return markup;

    if ( 'undefined' == typeof markup  )
        return repo.text;

    if ('string' !== typeof dateDebut && 'string' == typeof markup )
        return markup;

    let momentDateDebut = moment(dateDebut,'YYYY-MM-DD');
    let momentDateFin = moment(dateFin,'YYYY-MM-DD');

    dateDebut = momentDateDebut.format('DD/MM/YYYY');

    if ( momentDateDebut.isSame(momentDateFin) ) {
        markup = '<table ><tr><td valign="middle" style="padding-left:3px;"><strong>'+markup+'</strong></td><td valign="middle" style="padding-left:3px;"> - <em>'+momentDateDebut.format('DD MMMM YYYY')+'</em></td></tr></table>';
        return markup;
    }

    dateFin =  momentDateFin.format('DD/MM/YYYY');

    markup = '<table ><tr><td valign="middle" style="padding-left:3px;"><strong>'+markup+'</strong></td><td valign="middle" style="padding-left:3px;"> - '+dateDebut+' - </td><td valign="middle" style="padding-left:3px;">'+dateFin+'</td></tr></table>';
    return markup;
}
function formatSelect2TemplateSelectionFete (repo) {
    // console.log( repo.element );
    if (repo.loading) return repo.text;

    // console.log(typeof repo.element );

    if (null != repo.element.getAttribute('data-startdate') && 'string' === typeof repo.element.getAttribute('data-startdate')) {
        repo = {
            'startdate' : repo.element.getAttribute('data-enddate'),
            'enddate' : repo.element.getAttribute('data-enddate') ,
            'name' : (repo.name || repo.text)
        };
    }

    let markup = repo.name;
    let dateDebut = repo.startdate;
    let dateFin = repo.enddate;



    if ( 'function' !== typeof moment )
        return markup;

    if ( 'undefined' == typeof markup  )
        return repo.text;

    if ('string' !== typeof dateDebut && 'string' == typeof markup )
        return markup;

    let momentDateDebut = moment(dateDebut,'YYYY-MM-DD');
    let momentDateFin = moment(dateFin,'YYYY-MM-DD');

    dateDebut = momentDateDebut.format('DD/MM/YYYY');

    if ( momentDateDebut.isSame(momentDateFin) ) {
        markup = '<table ><tr><td valign="middle" style="padding-left:3px;"><strong>'+markup+'</strong></td><td valign="middle" style="padding-left:3px;"> - <em>'+momentDateDebut.format('DD MMMM YYYY')+'</em></td></tr></table>';
        return markup;
    }

    dateFin =  momentDateFin.format('DD/MM/YYYY');

    markup = '<table ><tr><td valign="middle" style="padding-left:3px;"><strong>'+markup+'</strong></td><td valign="middle" style="padding-left:3px;"> - '+dateDebut+' - </td><td valign="middle" style="padding-left:3px;">'+dateFin+'</td></tr></table>';
    return markup;
}

function addselect(el)
{
    //alert( $(el).closest('.select2-dropdown').find('.select2-search__field').val() );
    promptGenerique( "Ajouter", $(el).closest('.select2-dropdown').find('.select2-search__field').val(), function(result) {
            if (result !== null) {
                //bootbox.alert("Hi <b>"+result+"</b>");
                //Ajax add
                var params = "name="+result;
                genericAjax(ajax_url_saveCountries,"POST",params);

            }
        } );
    //bootbox.alert($(el).closest('.select2-dropdown').find('.select2-search__field').val());
}

/**
 *
 * @param editor
 * @param at_config
 * @param at_conf_lieux
 */
function load_atwho(editor, at_config, at_conf_lieux) {

    // WYSIWYG mode when switching from source mode
    if (editor.mode != 'source') {

        editor.document.getBody().$.contentEditable = true;

        $(editor.document.getBody().$)
            .atwho('setIframe', editor.window.getFrame().$)
            .atwho(at_config)
            .atwho(at_conf_lieux);

    }
    // Source mode when switching from WYSIWYG
    else {
        $(editor.container.$).find(".cke_source").atwho(at_config).atwho(at_conf_lieux);
    }

}

function fillFieldEventName(){
    var plainTxt = CKEDITOR.instances['name_ck'].document.getBody().getText();
    plainTxt = plainTxt.replace(/@/,'');
    document.getElementById('mdts_homebundle_event_name').value = plainTxt;

}

//Flyer
function removeMultiFlyer(el) {

    confirmGenerique("Voulez-vous supprimer ?", function(result) {
        if ( result ) {
            el.closest('.pull-left').remove();
        }
    });
}
function removelineFlyer(el)
{
    var error = false;
    var copy = el.closest('.blocclone_flyer');
    copy.remove();

}

function removelinePrice(el)
{
    var error = false;
    var copy = el.closest('.blocclone');
    var inputstext = copy.querySelectorAll('input[type=text]');
    [].forEach.call(inputstext, function(el) {
        if (el.value != '' ) {
            error = true;
        }
    });


    if( error ) {
        confirmGenerique("Des champs sont remplis sur cette ligne. Voulez-vous tout de même supprimer ?", function(result) {
            if ( result )
                copy.remove();
        });
        return;
    }

    copy.remove();

}

//Video
function removelineVideo(el)
{
    var error = false;
    var copy = el.closest('.blocclone_video');
    var inputstext = copy.querySelectorAll('input[type=text]');
    [].forEach.call(inputstext, function(el) {
        if (el.value != '' ) {
            error = true;
        }
    });


    if( error ) {
        confirmGenerique("Des champs sont remplis sur cette ligne. Voulez-vous tout de même supprimer ?", function(result) {
            if ( result )
                copy.remove();
        });
        return;
    }

    copy.remove();

}
function duplicatePrice()
{

    // var blocclone = selector_fieldPrice;
    var blocclone = document.querySelectorAll('.blocclone');

    if ( typeof arguments[0] != 'undefined' ) {
        var nodel = arguments[0];

        var copy = nodel.closest('.blocclone').cloneNode(true);

        //Modif Attributes
        copy.querySelector('.price').setAttribute('name', copy.querySelector('.price').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('.detailprice').setAttribute('name', copy.querySelector('.detailprice').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('select').setAttribute('name', copy.querySelector('select').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );

        copy.querySelector('.price').value = '';
        copy.querySelector('.detailprice').value = '';

        divContainer_Price.appendChild(copy);

        nodel.classList.add('hidden');
        nodel.nextElementSibling.classList.remove('hidden');

        return;
    }
 

    var eld = divContainer_Price ;
    var dataprototype_html = eld.getAttribute('data-prototype');

    dataprototype_html = dataprototype_html.replace(/__name__/g,blocclone.length);

    eld.innerHTML += dataprototype_html;

    var i = 0;
    [].forEach.call(blocclone, function(el) {

        //Modif Attributes
        el.querySelector('.price').setAttribute('name', el.querySelector('.price').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );
        el.querySelector('.detailprice').setAttribute('name', el.querySelector('.detailprice').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );
        el.querySelector('select').setAttribute('name', el.querySelector('select').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );


        i++;

    });

    //el.classList.add('hidden');
    //el.nextElementSibling.classList.remove('hidden');
}
function duplicateVideo()
{

    // var blocclone = selector_fieldVideo;
    var blocclone = document.querySelectorAll('.blocclone_video');
    if ( typeof arguments[0] != 'undefined' ) {
        var nodel = arguments[0];

        var copy = nodel.closest('.blocclone_video').cloneNode(true);

        //Modif Attributes
        copy.querySelector('input[type=text]').setAttribute('name', copy.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('input[type=text]').value = '';

        divContainer_Video.appendChild(copy);

        nodel.classList.add('hidden');
        nodel.nextElementSibling.classList.remove('hidden');

        return;
    }



    /*var copy = el.closest('.blocclone').cloneNode(true);
     document.getElementById('fieldprice').appendChild(copy);*/

    var eld = divContainer_Video ;
    var dataprototype_html = eld.getAttribute('data-prototype');

    dataprototype_html = dataprototype_html.replace(/__name__/g, blocclone.length );

    eld.innerHTML += dataprototype_html;

    var i = 0;
    [].forEach.call(blocclone, function(el) {



        //Modif Attributes
        el.querySelector('input[type=text]').setAttribute('name', el.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );



        i++;

    });

    //el.classList.add('hidden');
    //el.nextElementSibling.classList.remove('hidden');
}

//Tel
function removelineTel(el)
{
    var error = false;
    var copy = el.closest('.blocclone_tel');
    var inputstext = copy.querySelectorAll('input[type=text]');
    [].forEach.call(inputstext, function(el) {
        if ( el.value != '' ) {
            error = true;
        }
    });


    if( error ) {
        confirmGenerique("Des champs sont remplis sur cette ligne. Voulez-vous tout de même supprimer ?", function(result) {
            if ( result )
                copy.remove();
        });
        return;
    }

    copy.remove();

}

//Articles
function removelineArticles(el)
{
    var error = false;
    var copy = el.closest('.blocclone_articles');
    var inputstext = copy.querySelectorAll('input[type=text]');
    [].forEach.call(inputstext, function(el) {
        if ( el.value != '' ) {
            error = true;
        }
    });


    if( error ) {
        confirmGenerique("Des champs sont remplis sur cette ligne. Voulez-vous tout de même supprimer ?", function(result) {
            if ( result )
                copy.remove();
        });
        return;
    }

    copy.remove();

}
function duplicateArticles()
{
    // var blocclone = selector_fieldArticles;
    var blocclone = document.querySelectorAll('.blocclone_articles');
     //alert(blocclone.length);

    if ( typeof arguments[0] != 'undefined' ) {
        var nodel = arguments[0];

        var copy = nodel.closest('.blocclone_articles').cloneNode(true);

        //Modif Attributes
        copy.querySelector('input[type=text]').setAttribute('name', copy.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('input[type=checkbox]').setAttribute('name', copy.querySelector('input[type=checkbox]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('input[type=text]').value = '';

        divContainer_Articles.appendChild(copy);

        nodel.classList.add('hidden');
        nodel.nextElementSibling.classList.remove('hidden');


        var i = 0;
        [].forEach.call(blocclone, function(el) {

            //Modif Attributes
            el.querySelector('input[type=text]').setAttribute('name', el.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );
            el.querySelector('input[type=checkbox]').setAttribute('name', el.querySelector('input[type=checkbox]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );



            i++;

        });




        return;
    }


    delete eld;
    var eld = divContainer_Articles ;
    var dataprototype_html = eld.getAttribute('data-prototype');
    //alert(blocclone.length);


    dataprototype_html = dataprototype_html.replace(/__name__/g, blocclone.length );

    eld.innerHTML += dataprototype_html;


}
//Article artistes
function removelineArticlesArtiste(el)
{
    var error = false;
    var copy = el.closest('.blocclone_articles_artiste');
    var inputstext = copy.querySelectorAll('input[type=text]');
    [].forEach.call(inputstext, function(el) {
        if ( el.value != '' ) {
            error = true;
        }
    });


    if( error ) {
        confirmGenerique("Des champs sont remplis sur cette ligne. Voulez-vous tout de même supprimer ?", function(result) {
            if ( result )
                copy.remove();
        });
        return;
    }

    copy.remove();

}

//Flyer
function duplicateFlyer()
{
    var blocclone = document.querySelectorAll('.blocclone_flyer');
    //alert(blocclone.length);

    if ( typeof arguments[0] != 'undefined' ) {
        var nodel = arguments[0];

        var copy = nodel.closest('.blocclone_flyer').cloneNode(true);

        //Modif Attributes
        copy.querySelector('input[type=file]').setAttribute('name', copy.querySelector('input[type=file]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('select').setAttribute('name', copy.querySelector('select').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );

        document.getElementById('collectionflyerContainer').appendChild(copy);

        nodel.classList.add('hidden');
        nodel.nextElementSibling.classList.remove('hidden');


        var i = 0;
        [].forEach.call(blocclone, function(el) {

            //Modif Attributes
            el.querySelector('input[type=file]').setAttribute('name', el.querySelector('input[type=file]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );
            el.querySelector('select').setAttribute('name', el.querySelector('select').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );


            i++;

        });

        document.querySelectorAll('.blocclone_flyer')[eval(document.querySelectorAll('.blocclone_flyer').length-1)].querySelector('.blocimage').remove();


        return;
    }



    /*var copy = el.closest('.blocclone').cloneNode(true);
     document.getElementById('fieldprice').appendChild(copy);*/
    delete eld;
    var eld = document.getElementById('collectionflyerContainer') ;
    var dataprototype_html = eld.getAttribute('data-prototype');
    //alert(blocclone.length);


    dataprototype_html = dataprototype_html.replace(/__name__/g, blocclone.length );

    eld.innerHTML += dataprototype_html;

    //el.classList.add('hidden');
    //el.nextElementSibling.classList.remove('hidden');
}

//Lieu Tél
function duplicateLieuTel()
{
    var blocclone = selector_fieldTel;
    //alert(blocclone.length);

    if ( typeof arguments[0] != 'undefined' ) {
        var nodel = arguments[0];

        var copy = nodel.closest('.blocclone_tel').cloneNode(true);

        //Modif Attributes
        copy.querySelector('input[type=text]').setAttribute('name', copy.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('input[type=text]').value = '';

        divContainer_Tel.appendChild(copy);

        nodel.classList.add('hidden');
        nodel.nextElementSibling.classList.remove('hidden');


        var i = 0;
        [].forEach.call(blocclone, function(el) {

            //Modif Attributes
            el.querySelector('input[type=text]').setAttribute('name', el.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );


            i++;

        });

        selector_Telinput.intlTelInput({
            utilsScript: "/bower_components/intl-tel-input/lib/libphonenumber/build/utils.js?8"
        });


        return;
    }


    delete eld;
    var eld = divContainer_Tel ;
    var dataprototype_html = eld.getAttribute('data-prototype');
    //alert(blocclone.length);


    dataprototype_html = dataprototype_html.replace(/__name__/g, blocclone.length );

    eld.innerHTML += dataprototype_html;

    selector_Telinput.intlTelInput({
        utilsScript: "/bower_components/intl-tel-input/lib/libphonenumber/build/utils.js?8"
    });


}


 
function createIframeId() {
    if ( document.querySelectorAll('.box-body').length<0 )
        return false;

    let divparent = document.querySelector('.box-body') ;

    //creer div
    let divTag = createElement('div', {class:'well hidden', id: 'blociframeartiste'});

    let h4Tag = createElement('h4',{}, 'Modification de l\'artiste <em class="fa fa-arrow-down"></em>');

    let  iframeTag = createElement('iframe', {width: '100%', src: 'about:blank', frameborder: '0', height: '0', id: 'iframe'});

    divTag.appendChild(h4Tag);
    divTag.appendChild(iframeTag)

    divparent.parentNode.insertBefore(divTag, null);
    return divTag;
}
function edit_lieu(id) {
    var urlplus = '';


    var blociframeartiste = document.getElementById('blociframeartiste');

    if (!blociframeartiste) {
        blociframeartiste = createIframeId();


    }

    iframeid = document.getElementById('iframe');

    if (id>0 && iframeid) {
        blociframeartiste.classList.remove('hidden');
        var url = ('string' === typeof urlpath ? urlpath : '')+'/admin/eventlieu/'+id+'/edit?iframe=1'+urlplus;
        iframeid.setAttribute('src',url);
        iframeid.setAttribute('height',400);
        setTimeout(function(){
            iframeid.scrollIntoView();
        },2000 );
    }
}
function edit_artiste(artisteid)
{
    var urlplus = '';
    if ( typeof arguments[1] != 'undefined' && document.getElementById('image') && document.getElementById('image').innerHTML.trim() != ''  ) {
        urlplus = '&img='+encodeURIComponent( document.getElementById('image').querySelector('img').getAttribute('src') );
    }
    if ( typeof arguments[1] != 'undefined' && document.getElementById('mainflyer') && document.getElementById('mainflyer').getAttribute('src').trim() != ''  ) {
        urlplus = '&img='+encodeURIComponent( document.getElementById('mainflyer').getAttribute('src') );
    }
    //alert(artisteid);

    var blociframeartiste = document.getElementById('blociframeartiste');

    if (!blociframeartiste) {
        blociframeartiste = createIframeId();


    }

    iframeid = document.getElementById('iframe');

    if (artisteid>0 && iframeid) {
        blociframeartiste.classList.remove('hidden');
        var url = ('string' === typeof urlpath ? urlpath : '')+'/admin/artistesdjorganisateurs/'+artisteid+'/edit?iframe=1'+urlplus;
        iframeid.setAttribute('src',url);
        iframeid.setAttribute('height',400);
        setTimeout(function(){
            iframeid.scrollIntoView();
        },2000 );
    }
}

var createDivContainerList = function(idName, selectorName, dom)
{
    if ( document.querySelectorAll(selectorName).length<0 )
        return false;

    let divparent = dom.closest('.form-group');

    //creer div
    let hrTag = createElement('hr',{});
    let divTag = createElement('div',{});
    divTag.id = idName;

    //divparent.parentNode.insertBefore(div, null);
    divparent.after(hrTag);
    divparent.after(divTag);

    return divTag;
}

var createDivResultArtistes = function () {
    if ( document.querySelectorAll('.select2_artiste').length<=0 )
        return false;

    let divparent = document.querySelector('.select2_artiste').closest('.form-group');

    //creer div
    let hrTag = createElement('hr',{});
    let divTag = createElement('div',{});
    divTag.id = 'resultArtistes';

    //divparent.parentNode.insertBefore(div, null);
    divparent.insertBefore(hrTag, null);
    divparent.insertBefore(divTag, null);
    return divTag;
};
function bootbox_Image(data) {

    /*if ( typeof arguments[1] != 'undefined' )
        alert( arguments[1].getAttribute('data-name') );*/

    /*bootbox.dialog({
        title: "Artiste - "+data.getAttribute('data-name'),
        message: '<div class="center-block"><img src="/assets/artistes_orig/'+data.getAttribute('data-photo')+'" class="img-responsive" /></div>',
        buttons: {
            success: {
                label: "Fermer",
                className: "btn-success"
            }
        }
    });*/

    dialogGenerique(
        "Artiste - "+data.getAttribute('data-name'),
        '<div class="center-block"><img src="/assets/artistes_orig/'+data.getAttribute('data-photo')+'" class="img-responsive" /></div>',
        'Fermer',
        function (result) {
             return true;
        },
        function (resolve) {
            resolve([])
        }
    );
}
function createListHTML(data, idName, selectorName, dom, fnEdit)
{
    var fieldName = '';
    var containerList =  document.getElementById(idName) ;
    if ( !containerList ) {
        containerList = createDivContainerList(idName,selectorName, dom);
    }



    if ( containerList.innerHTML.trim() === '' ) {
        containerList.innerHTML = '<div class="clearfix" style="background:#ccc;margin-bottom:10px;"><div class="pull-left" style="padding:10px;width:70px;">Image/logo</div><div class="pull-left"  style="margin-left:10px;padding:10px;" >Nom</div></div>';
    }


    let divrow = createElement('DIV', {class: 'clearfix datarow'});

    divrow.id = idName+'_'+data.id;
    divrow.setAttribute('data-'+idName,data.id ) ;

    var divpulleft = document.createElement('DIV');
    divpulleft.className = 'pull-left photo_'+idName+'_'+data.id;

    if (data.logo!=='' && typeof data.logo != 'undefined') {
        var imageTag = document.createElement('IMG');
        imageTag.setAttribute('src','/media/cache/crop_100_100/assets/flyers/'+data.logo );
        imageTag.setAttribute('width','40' );
        imageTag.setAttribute('style','cursor: pointer' );
        imageTag.setAttribute('class','leftimage_'+idName+'_'+data.id );
        imageTag.setAttribute('data-name',data.name );
        imageTag.setAttribute('data-logo',data.logo );
        imageTag.onclick = function() {
            bootbox_Image( imageTag);
        }
        divpulleft.appendChild(imageTag);
    }

    var divpulleft2 = document.createElement('DIV');
    divpulleft2.className = 'pull-left name_'+idName+'_'+data.id;
    if ( data.logo!=='' ) {
        divpulleft2.setAttribute('style','margin-left:10px;');
    }

    //console.log( typeof data.country.name+" ---- "+data.quartier.name );
    fieldName = data.name;
    
    //Si country existe
    if ('undefined'!==typeof data.country && 'string'===typeof data.country.name && ''!==data.country.name)
        fieldName += ' - Pays: '+data.country.name;
    //Si region existe
    if ('undefined'!==typeof data.region && 'string'===typeof data.region.name && ''!==data.region.name)
        fieldName += ' - Région: '+data.region.name;
    //Si locality existe
    if ('undefined'!==typeof data.locality && 'string'===typeof data.locality.name && ''!==data.locality.name)
        fieldName += ' - Ville : '+data.locality.name;
    //Si quartier existe
    if ('undefined'!==typeof data.quartier && 'string'===typeof data.quartier.name && ''!==data.quartier.name)
        fieldName += ' - Quartier : '+data.quartier.name;
    
    divpulleft2.textContent = fieldName;

    var divpulright = document.createElement('DIV');
    divpulright.className = 'pull-right';

    var divdropdown = document.createElement('DIV');
    divdropdown.className = "dropdown";
    var button = document.createElement("BUTTON");
    button.className = "btn btn-default dropdown-toggle";
    button.setAttribute("type","button");
    button.setAttribute("data-toggle","dropdown");
    button.setAttribute("aria-haspopup","true");
    button.setAttribute("aria-expanded","true");
    button.innerHTML = '<span class="caret"></span>';
    divdropdown.appendChild(button);

    var ul = document.createElement("UL");
    ul.className = "dropdown-menu dropdown-menu-right";

    var li1 = document.createElement("LI");
    var a =  document.createElement("A");
    a.href = "javascript:;";
    a.textContent = "Modifier les infos";
    a.onclick = function(){
        fnEdit(data.id);
    }


    li1.appendChild(a);
    ul.appendChild(li1);


    divdropdown.appendChild(ul);
    divpulright.appendChild(divdropdown);

    divrow.appendChild(divpulleft);
    divrow.appendChild(divpulleft2);
    divrow.appendChild(divpulright);

    var hr = document.createElement('HR');

    containerList.appendChild(divrow);
    containerList.appendChild(hr);


}
function createListArtistes(data) {
    //console.log( data   );
    /*var data = e.params.data;*/
    //console.log( typeof data.photo   );
    //document.getElementById('resultArtistes').classList.remove('hidden');
    var resultArtistes = selector_resultArtistes ;
    if ( !resultArtistes ) {
        resultArtistes = createDivResultArtistes();

        if ( ! resultArtistes )
            return false;

    }


    if ( resultArtistes.innerHTML.trim() === '' ) {
        resultArtistes.innerHTML = '<div class="clearfix" style="background:#ccc;margin-bottom:10px;"><div class="pull-left" style="padding:10px;width:40px;">Photo</div><div class="pull-left"  style="margin-left:10px;padding:10px;" >Artiste</div></div>';
    }
    var divrow = document.createElement('DIV');
    divrow.className = 'clearfix datarow';
    divrow.id = 'artiste_'+data.id;
    divrow.setAttribute('data-artisteid',data.id ) ;
    var divpulleft = document.createElement('DIV');
    divpulleft.className = 'pull-left photocontainer_'+data.id;
    //divpulleft.innerHTML = (data.photo!=='' && typeof data.photo != 'undefined'?'<img src="/assets/artistes/'+data.photo.replace(/\.jpg/,'.png')+'" width="40">':'');
    if (data.photo!=='' && typeof data.photo != 'undefined') {
        var imageTag = document.createElement('IMG');
        imageTag.setAttribute('src','/assets/artistes/'+data.photo.replace(/\.jpg/,'.png'));
        imageTag.setAttribute('width','40' );
        imageTag.setAttribute('style','cursor: pointer' );
        imageTag.setAttribute('class','circleimage_'+data.id );
        imageTag.setAttribute('data-name',data.name );
        imageTag.setAttribute('data-photo',data.photo );
        imageTag.onclick = function() {
            bootbox_Image( imageTag);
        }
        divpulleft.appendChild(imageTag);
    }

    var divpulleft2 = document.createElement('DIV');
    divpulleft2.className = 'pull-left artistename_'+data.id;
    if ( data.photo!=='' ) {
        divpulleft2.setAttribute('style','margin-left:10px;');
    }


    divpulleft2.textContent = data.name;

    var divpulright = document.createElement('DIV');
    divpulright.className = 'pull-right';

    var divdropdown = document.createElement('DIV');
    divdropdown.className = "dropdown";
    var button = document.createElement("BUTTON");
    button.className = "btn btn-default dropdown-toggle";
    button.setAttribute("type","button");
    button.setAttribute("data-toggle","dropdown");
    button.setAttribute("aria-haspopup","true");
    button.setAttribute("aria-expanded","true");
    button.innerHTML = '<span class="caret"></span>';
    divdropdown.appendChild(button);

    var ul = document.createElement("UL");
    ul.className = "dropdown-menu dropdown-menu-right";

    var li1 = document.createElement("LI");
    var a =  document.createElement("A");
    a.href = "javascript:;";
    a.textContent = "Modifier les infos de cet artiste";
    a.onclick = function(){
        edit_artiste(data.id);
    }
    li1.appendChild(a);
    if (document.getElementById('image') && document.getElementById('image').innerHTML.trim() != '') {
        var li2 = document.createElement("LI");
        var a =  document.createElement("A");
        a.href = "javascript:;";
        a.textContent = "Modifier les infos + flyer actuel";
        a.onclick = function(){
            edit_artiste(data.id, true );
        }
    }
    if (document.getElementById('mainflyer') && document.getElementById('mainflyer').getAttribute('src').trim() != '') {
        var li2 = document.createElement("LI");
        var a =  document.createElement("A");
        a.href = "javascript:;";
        a.textContent = "Modifier les infos + flyer actuel";
        a.onclick = function(){
            edit_artiste(data.id, true );
        }
    }
    li1.appendChild(a);
    ul.appendChild(li1);
    if (document.getElementById('image') && document.getElementById('image').innerHTML.trim() != '')
        ul.appendChild(li2);

    divdropdown.appendChild(ul);
    divpulright.appendChild(divdropdown);

    divrow.appendChild(divpulleft);
    divrow.appendChild(divpulleft2);
    divrow.appendChild(divpulright);

    var hr = document.createElement('HR');

    resultArtistes.appendChild(divrow);
    resultArtistes.appendChild(hr);
}
function callbackAddEntree(responseText) {
    let selectall = document.querySelector('#mdts_homebundle_event_entreetype');
    let jsonObj = JSON.parse(responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);



        let child = document.createElement('option');
        child.innerHTML = jsonObj.name;
        child.setAttribute('value',jsonObj.id);
        child.setAttribute('selected','selected');

        selectall.appendChild(child) ;

        optnew.remove();

        child.innerHTML = 'Ajouter une option';
        child.setAttribute('value','new');
        selectall.appendChild(child);

        $.notify({ message: msg },{ type: 'info' });

        jQuery('#mdts_homebundle_event_entreetype').trigger('change');
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
    }
}
//test
function addentreetype(url)
{
    let selectall = document.querySelector('#mdts_homebundle_event_entreetype');
    let optnew = selectall.querySelector('option[value=new]');
    //alert( optnew.innerHTML );

    promptGenerique( "Saisir le nom de l'entrée", sessionStorage.getItem("search__field"), function(result) {
        if (result === null) {
            //Press on cancel
            sessionStorage.clear();
        }
        if (result !== null) {
            
                let params = "name="+result;
                genericAjax(url,'POST',params,'callbackAddEntree');
                                          
          } else {
            selectall.selectedIndex = 0;
          }
        } );
}
function addGenericEntity(url, target)
{
    let id = target.id;
    var selectall = document.querySelector('#'+id);
    if (null==selectall)
        return;

    let titleDialog = target.getAttribute('data-titledialog');
    if (null==titleDialog)
        titleDialog = 'Ajouter une option';

    let fnCallback = target.getAttribute('data-fnCallback');
    if (null==fnCallback)
        fnCallback = 'callbackAddGenericOption';

    console.log(titleDialog);
    console.log(id);
    console.log(fnCallback);

    promptGenerique( titleDialog, '', function(result) {

        console.log(fnCallback);

        if (result !== null) {

            let params = "name="+result;
            genericAjax(url,'POST',params, fnCallback, { domElement:target });

        } else {
            selectall.selectedIndex = 0;
        }
    } );
}
function callbackAddGenericOption(responseText) {

    console.log(arguments);

    let idDom = null;
    if ('undefined' !== typeof arguments[1]  ) {
        objetparamplus = arguments[1];
        bFnparseDataExists = true;
        if (typeof objetparamplus.domElement != 'undefined') {
            domElement = objetparamplus.domElement;
            idDom = domElement.id;
            console.log(idDom);
        }
    }

    let selectall = document.querySelector('#'+idDom);
    let jsonObj = JSON.parse(responseText);
    let optnew = selectall.querySelector('option[value=new]');

    if (  typeof jsonObj.success == 'string' ) {

        optnew.remove();

        msg = ( jsonObj.success );
        console.log(msg);



        let child = createElement('option', {value: jsonObj.id, 'selected':'selected'}, jsonObj.name);
        selectall.appendChild(child) ;

        let childNewOpt = addNewToOption();
        selectall.appendChild(childNewOpt);

        $.notify({ message: msg },{ type: 'info' });

        jQuery('#'+idDom).trigger('change');
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
    }



}
function callbackAddLieu(responseText) {

    let jsonObj = JSON.parse(responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);

        $.notify({ message: msg },{ type: 'info' });

        if ( jsonObj.id > 0 && jsonObj.name != '' ) {
            var option = new Option(jsonObj.name,jsonObj.id, true, true);
            $("#select_mdts_homebundle_event_eventlieu").append(option);
            $("#select_mdts_homebundle_event_eventlieu").trigger('change');

            createListHTML(jsonObj, 'resultLieux', '.select2_lieux', selector_lieu, edit_lieu);


        }

        // swal( msg, "", "success");
        // alertGenerique(msg,"","success");
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
        // swal( msg, "", "error");
        // alertGenerique(msg,"","error");
    }
}
function addlieu(url)
{

    promptGenerique( "Saisir le nom du lieu", sessionStorage.getItem("search__field"), function(result) {
            if (result === false) {
                //Press on cancel
                sessionStorage.clear();
                return false;
            }
            if (result == "") {
                swal.showInputError("Champs obligatoire.");
                return false;
            }

            let params = "data[name]="+result;
            genericAjax(url,'POST',params,'callbackAddLieu');
        } );

}

function callbackAddArtiste(responseText) {

    let jsonObj = JSON.parse(responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);

        $.notify({ message: msg },{ type: 'info' });
        // alertGenerique(msg,"","success",2000);

        if('function' === typeof createListArtistes)
            createListArtistes(jsonObj);

        if ( jsonObj.id > 0 && jsonObj.name != '' ) {
            let option = new Option(jsonObj.name,jsonObj.id, true, true);
            $("#select_mdts_homebundle_event_artistesdj").append(option);
            $("#select_mdts_homebundle_event_artistesdj").trigger('change');
        }
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
        // alertGenerique(msg,"","error");
    }
}
function saveArtist(url)
{
    let name = $('#nameartiste').val();
    let typeselect = $('#addtypeselect').val();
    //bootbox.alert("Hello " + name + ". You've chosen <b>" + paysselect + "</b>");

    let params = "data[name]="+name+"&data[type]="+typeselect;
    genericAjax(url,'POST',params,'callbackAddArtiste');
}
function saveVille(url)
{
    let name = $('#nameville').val();
    let paysselect = $('#addpaysregion').val();
    let params = "name="+name+"&region_id="+paysselect;
    genericAjax(url,"POST",params,'callbackAddVille');
}

function saveRegion(url)
{
    let name = $('#nameregion').val();
    let paysselect = $('#addpaysselect').val();
    let params = "name="+name+"&country_id="+paysselect;
    genericAjax(url,"POST",params,'callbackAddRegion');
}

function saveQuartier(url)
{
    let name = $('#namequartier').val();
    let paysselect = $('#addpaysville').val();
    let params = "name="+name+"&ville_id="+paysselect;
    genericAjax(url,"POST",params,'callbackAddQuartier');
}
function addartistes(url)
{
    let html = '<div class="row">  ' +
        '<div class="col-md-12"> ' +
        '<form class="form-horizontal" onsubmit="return saveArtistFormDialog(\''+url+'\');"> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="name">Nom </label> ' +
        '<div class="col-md-4"> ' +
        '<input id="nameartiste"  name="name" type="text" class="form-control input-md" value="'+sessionStorage.getItem("search__field")+'"> ' +
        '</div> ' +
        '</div> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="awesomeness">Type</label> ' +
        '<div class="col-md-4"> <select id="addtypeselect"><option value="artiste">Artiste</option><option value="dj">DJ</option><option value="organisateur">Organisateur</option></select>' +
        '</div> </div>' +
        '</form> </div>  </div>';

    dialogGenerique(
        "Saisir le nom de l'artiste ou du DJ",
        html,
        'Enregistrer',
        function (result) {
            saveArtist(url);
        },
        function (resolve) {
            resolve([
                $('#nameartiste').val(),
                 $('#addtypeselect').val()
            ])
        },
        function () {
            $('#nameartiste').focus();
        }
    );
     
}
function saveArtistFormDialog(url)
{
    if (''!==$('#nameartiste').val() ) {
        saveArtist(url);
    }
    return false;
}

function saveVilleFormDialog(url)
{
    if (''!==$('#nameville').val() ) {
        saveVille(url);
    }
    return false;
}

function saveRegionFormDialog(url)
{
    if (''!==$('#nameregion').val() ) {
        saveRegion(url);
    }
    return false;
}

function saveQuartierFormDialog(url)
{
    if (''!==$('#namequartier').val() ) {
        saveQuartier(url);
    }
    return false;
}

function callbackAddLocal(responseText)
{
    var jsonObj = JSON.parse( responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);

        $.notify({ message: msg },{ type: 'info' });

        if ( jsonObj.id > 0 && jsonObj.name != '' ) {
            var option = new Option(jsonObj.name,jsonObj.id, true, true);
            $("#select_mdts_homebundle_event_eventlocal").append(option);
            $("#select_mdts_homebundle_event_eventlocal").trigger('change');
        }

    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
    }
}
function addlocal(url)
{
     

    promptGenerique( "Saisir le nom de la fête", sessionStorage.getItem("search__field"), function(result) {
            if (result === null) {
                //Press on cancel
                sessionStorage.clear();
            }
          if (result !== null) {                                             
            
                var params = "name="+result;
                genericAjax(url,'post',params,'callbackAddLocal');

          } 
        } );
}
function callbackAddCountry(responseText)
{
    var jsonObj = JSON.parse( responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);

        var child = document.createElement('option');
        child.innerHTML = jsonObj.name;
        child.setAttribute('value',jsonObj.id);
        child.setAttribute('selected','selected');
        document.querySelector('#mdts_homebundle_eventlieu_country').appendChild(child);

        $.notify({ message: msg },{ type: 'info' });

        $('#mdts_homebundle_eventlieu_country').trigger('change');

        // alertGenerique( result+" enregistré correctement", "", "success");
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
        // alertGenerique( msg, "", "error");
    }
}
function addcountry(url)
{
    promptGenerique( "Saisir le nom du pays", sessionStorage.getItem("search__field"), function(result) {
        if (result === false) {
            //Press on cancel
            sessionStorage.clear();
            return false;
        }
        if (result === "") {
            swal.showInputError("Champs obligatoire.");
            return false
        }

            
                var params = "name="+result;
                genericAjax(url,'POST',params,'callbackAddCountry');

        } );
}
function callbackAddRegion(responseText)
{
    var jsonObj = JSON.parse( responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);

        var child = document.createElement('option');
        child.innerHTML = jsonObj.name;
        child.setAttribute('value',jsonObj.id);
        child.setAttribute('selected','selected');
        document.querySelector('#mdts_homebundle_eventlieu_region').appendChild(child);

        $.notify({ message: msg },{ type: 'info' });

        $('#mdts_homebundle_eventlieu_region').trigger('change');
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
    }
}
function addregion(url)
{
    let html = '<div class="row">  ' +
        '<div class="col-md-12"> ' +
        '<form class="form-horizontal" onsubmit="return saveRegionFormDialog(\''+url+'\');"> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="name">Nom de la région</label> ' +
        '<div class="col-md-4"> ' +
        '<input id="nameregion" name="name" type="text" placeholder="ex: Analamanga, Analanjirofo" class="form-control input-md"> ' +
        '</div> ' +
        '</div> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="awesomeness">Pays</label> ' +
        '<div class="col-md-4"> <select id="addpaysselect">'+document.querySelector('#mdts_homebundle_eventlieu_country').innerHTML+'</select>' +
        '</div> </div>' +
        '</form> </div>  </div>';

    dialogGenerique(
        "Saisir le nom de la région.",
        html,
        'Enregistrer',
        function (result) {
            saveRegion(url);
        },
        function (resolve) {
            resolve([
                $('#nameregion').val(),
                $('#addpaysselect').val()
            ])
        },
        function () {
            $('#nameregion').focus();
        }
    );
     /*bootbox.dialog({
        title: "Saisir le nom de la région.",
        message: '<div class="row">  ' +
            '<div class="col-md-12"> ' +
            '<form class="form-horizontal"> ' +
            '<div class="form-group"> ' +
            '<label class="col-md-4 control-label" for="name">Nom de la région</label> ' +
            '<div class="col-md-4"> ' +
            '<input id="nameregion" name="name" type="text" placeholder="ex: Analamanga, Analanjirofo" class="form-control input-md"> ' +
            '</div> ' +
            '</div> ' +
            '<div class="form-group"> ' +
            '<label class="col-md-4 control-label" for="awesomeness">Pays</label> ' +
            '<div class="col-md-4"> <select id="addpaysselect">'+document.querySelector('#mdts_homebundle_eventlieu_country').innerHTML+'</select>' +
            '</div> </div>' +
            '</form> </div>  </div>',
        buttons: {
            success: {
                label: "Créer",
                className: "btn-success",
                callback: function () {
                    var msg = '';
                    var name = $('#nameregion').val();
                    var paysselect = $('#addpaysselect').val(); 
                    //bootbox.alert("Hello " + name + ". You've chosen <b>" + paysselect + "</b>");

                    var params = "name="+name+"&country_id="+paysselect;
                    genericAjax(url,"POST",params,'callbackAddRegion');
                }
            }
        }
    });*/
}
function callbackAddQuartier(responseText)
{
    var jsonObj = JSON.parse( responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);

        var child = document.createElement('option');
        child.innerHTML = jsonObj.name;
        child.setAttribute('value',jsonObj.id);
        child.setAttribute('selected','selected');
        document.querySelector('#mdts_homebundle_eventlieu_quartier').appendChild(child);

        $.notify({ message: msg },{ type: 'info' });
        $('#mdts_homebundle_eventlieu_quartier').trigger('change');
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
    }
}
function addquartier(url)
{
    let html = '<div class="row">  ' +
        '<div class="col-md-12"> ' +
        '<form class="form-horizontal" onsubmit="return saveQuartierFormDialog(\''+url+'\');"> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="name">Nom du quartier</label> ' +
        '<div class="col-md-4"> ' +
        '<input id="namequartier" name="name" type="text" placeholder="ex: Analakely, Behoririka" class="form-control input-md"> ' +
        '</div> ' +
        '</div> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="awesomeness">Ville</label> ' +
        '<div class="col-md-4"> <select id="addpaysville">'+document.querySelector('#mdts_homebundle_eventlieu_locality').innerHTML+'</select>' +
        '</div> </div>' +
        '</form> </div>  </div>';

    dialogGenerique(
        "Saisir le nom du quartier.",
        html,
        'Enregistrer',
        function (result) {
            saveQuartier(url);
        },
        function (resolve) {
            resolve([
                $('#namequartier').val(),
                $('#addpaysville').val()
            ])
        },
        function () {
            $('#namequartier').focus();
        }
    );
     /*bootbox.dialog({
        title: "Saisir le nom du quartier.",
        message: '<div class="row">  ' +
            '<div class="col-md-12"> ' +
            '<form class="form-horizontal"> ' +
            '<div class="form-group"> ' +
            '<label class="col-md-4 control-label" for="name">Nom du quartier</label> ' +
            '<div class="col-md-4"> ' +
            '<input id="namequartier" name="name" type="text" placeholder="ex: Analakely, Behoririka" class="form-control input-md"> ' +
            '</div> ' +
            '</div> ' +
            '<div class="form-group"> ' +
            '<label class="col-md-4 control-label" for="awesomeness">Ville</label> ' +
            '<div class="col-md-4"> <select id="addpaysville">'+document.querySelector('#mdts_homebundle_eventlieu_locality').innerHTML+'</select>' +
            '</div> </div>' +
            '</form> </div>  </div>',
        buttons: {
            success: {
                label: "Créer",
                className: "btn-success",
                callback: function () {
                    var name = $('#namequartier').val();
                    var paysselect = $('#addpaysville').val(); 
                    

                    var params = "name="+name+"&ville_id="+paysselect;
                    genericAjax(url,"POST",params,'callbackAddQuartier');
                }
            }
        }
    });*/
}

function callbackAddVille(responseText)
{
    var jsonObj = JSON.parse( responseText);
    if (  typeof jsonObj.success == 'string' ) {

        msg = ( jsonObj.success );
        console.log(msg);

        let child = createElement('option', {value:jsonObj.id, selected:'selected'},jsonObj.name);
        document.querySelector('#mdts_homebundle_eventlieu_locality').appendChild(child);

        $.notify({ message: msg },{ type: 'info' });

        $('#mdts_homebundle_eventlieu_locality').trigger('change');
    }
    else {
        msg = jsonObj.error;
        $.notify({ message: msg },{ type: 'error' });
    }
}
function addville(url)
{
    let html = '<div class="row">  ' +
        '<div class="col-md-12"> ' +
        '<form class="form-horizontal" onsubmit="return saveVilleFormDialog(\''+url+'\');"> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="name">Nom de la ville</label> ' +
        '<div class="col-md-4"> ' +
        '<input id="nameville" name="name" type="text" placeholder="ex: Antananarivo, Ankadikely-Ilafy" class="form-control input-md"> ' +
        '</div> ' +
        '</div> ' +
        '<div class="form-group"> ' +
        '<label class="col-md-4 control-label" for="awesomeness">Région</label> ' +
        '<div class="col-md-4"> <select id="addpaysregion">'+document.querySelector('#mdts_homebundle_eventlieu_region').innerHTML+'</select>' +
        '</div> </div>' +
        '</form> </div>  </div>';
    dialogGenerique(
        "Saisir le nom de la ville.",
        html,
        'Enregistrer',
        function (result) {
            saveVille(url);
        },
        function (resolve) {
            resolve([
                $('#nameville').val(),
                $('#addpaysregion').val()
            ])
        },
        function () {
            $('#nameville').focus();
        }
    );

     /*bootbox.dialog({
        title: "Saisir le nom de la ville.",
        message: '<div class="row">  ' +
            '<div class="col-md-12"> ' +
            '<form class="form-horizontal"> ' +
            '<div class="form-group"> ' +
            '<label class="col-md-4 control-label" for="name">Nom de la ville</label> ' +
            '<div class="col-md-4"> ' +
            '<input id="nameville" name="name" type="text" placeholder="ex: Antananarivo, Ankadikely-Ilafy" class="form-control input-md"> ' +
            '</div> ' +
            '</div> ' +
            '<div class="form-group"> ' +
            '<label class="col-md-4 control-label" for="awesomeness">Région</label> ' +
            '<div class="col-md-4"> <select id="addpaysregion">'+document.querySelector('#mdts_homebundle_eventlieu_region').innerHTML+'</select>' +
            '</div> </div>' +
            '</form> </div>  </div>',
        buttons: {
            success: {
                label: "Créer",
                className: "btn-success",
                callback: function () {
                    var name = $('#nameville').val();
                    var paysselect = $('#addpaysregion').val(); 
                    

                    var params = "name="+name+"&region_id="+paysselect;
                    genericAjax(url,"POST",params,'callbackAddVille');

                }
            }
        }
    });*/
}

function setCropper(elem) {
    //alert(typeof elem.src);
    if ('string' !== typeof elem.src || !document.getElementById('imagecontainer'))
        return false;

    document.getElementById('imagecontainer').innerHTML = '<img src="/assets/flyers/'+getFileName(elem.src)+'" class="img-responsive" />';
    enableCropper();
}
//Au chargement du DOM
function getImageInEvent() {

    //document.getElementById('eventsearch').classList.toggle('hidden');

    //Typehead
     if ( selector_Typeahead.length ) {

         var videosearch = new Bloodhound({
             datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
             queryTokenizer: Bloodhound.tokenizers.whitespace
             ,limit:100
             //,remote: '/rest/event?name=%QUERY'
             ,remote: {
                url: ajax_url_Event+'?name=%QUERY',
                wildcard: '%QUERY'
              }
         });

         videosearch.initialize();

         var templatehandle = '<div class="clearfix" style="border-top:1px solid #ccc;"><div class="pull-left"><img alt="" height="70" width="70" src="{{flyer}}"></div><div class="pull-left" style="margin-left:5px;"><strong>{{name}}</strong></div></div>';
         selector_Typeahead.typeahead(null, {
             name: 'videosearch',
             displayKey: 'name',
             source: videosearch.ttAdapter(),
             templates: {
                 empty: [
                     '<div class="empty-message">Non trouvé.</div>'
                 ].join('\n')
                 ,suggestion: Handlebars.compile(templatehandle)
                 ,pending: function(data) {
                     return '<div class="empty-message">Recherche en cours de '+data.query+'...</div>'
                 }
                  
             }
         });

         selector_Typeahead.bind('typeahead:selected', function(obj, datum, name) {
              if('' !== getFileName(datum.flyer)  ) {
                 document.getElementById('imagecontainer').innerHTML = '<img src="/assets/flyers/'+getFileName(datum.flyer)+'" class="img-responsive" />';
                
                 enableCropper();
                 
              }

         });
     }




     
}
function enableCropper()
{
    //alert(document.querySelectorAll('.tools').length)
    document.querySelector('.tools').classList.remove('hidden');
    document.getElementById('resultpic').classList.remove('hidden');
    cropper = new Cropper(document.getElementById('imagecontainer').querySelector('img'),{ aspectRatio: 1 / 1} );
    cropper.crop();
}

function clickToolbarCrop()
{
    [].forEach.call(document.querySelectorAll('.tool_check'), function(el) {
        el.addEventListener( 'click', function(evt){
            if(typeof cropper == 'undefined')
                enableCropper();
            else
                cropper.crop();

        });
    });

    [].forEach.call(document.querySelectorAll('.tool_clear'), function(el) {
        el.addEventListener( 'click', function(evt){
            if(typeof cropper == 'undefined')
                enableCropper();
            cropper.clear();
        });
    });

    [].forEach.call(document.querySelectorAll('.tool_move'), function(el) {
        el.addEventListener( 'click', function(evt){
            if(typeof cropper == 'undefined')
                enableCropper();
            cropper.setDragMode('move');
        });
    });

    [].forEach.call(document.querySelectorAll('.tool_aspectratiofree'), function(el) {
        el.addEventListener( 'click', function(evt){
            if(typeof cropper == 'undefined')
                enableCropper();
            cropper.setAspectRatio(0);
        });
    });

    [].forEach.call(document.querySelectorAll('.tool_getcropped'), function(el) {
        el.addEventListener( 'click', function(evt){
            if(typeof cropper == 'undefined')
                enableCropper();

            /*bootbox.dialog({
             title: "Image recadrée",
             message: '<img src="'+cropper.getCroppedCanvas().toDataURL('image/jpeg')+'" class="img-responsive" />',
             buttons: {
             success: {
             label: "Valider et enregistrer",
             className: "btn-success",
             callback: function () {
             saveFlyerToServer(cropper.getCroppedCanvas().toDataURL('image/jpeg'));
             setTimeout(function(){
             cropper.destroy();
             //cacher les tools
             document.getElementById('toolcrop').classList.add('hidden');
             },1000);
             }
             }
             }
             });*/

            dialogGenerique(
                "Image recadrée",
                '<img src="'+cropper.getCroppedCanvas().toDataURL('image/jpeg')+'" class="img-responsive" />',
                "Valider et enregistrer",
                function (result) {
                    saveFlyerToServer(cropper.getCroppedCanvas().toDataURL('image/jpeg'));
                    setTimeout(function(){
                        cropper.destroy();
                        //cacher les tools
                        document.querySelector('div[data-toolcrop]').classList.add('hidden');
                    },1000);
                },
                function (resolve) {
                    resolve([])
                }
            );
        });
    });


}
function callbackUploadFlyer(responseText)
{
    divContainer_Resultpic.classList.remove('hidden');
    var jsonObj = JSON.parse( responseText);
    traitementXHR(jsonObj);
}
function saveFlyerToServer(objet)
{
    //console.log("saveFlyerToServer typeof objet : "+ typeof objet+" - objet : " +objet);
    //Submit ajax
    var form = new FormData();
    if ( 'string' === typeof objet )
        form.append("flyerurl", objet);
    else {
        form.append("flyer", objet);
        form.append("flyerurl", '');
    }


    if ( tab_hidden.length>0)
        form.append("oldfilename", "" );

    genericAjax(ajax_url_uploadFlyer, "FILE", form, 'callbackUploadFlyer');
}
var getSiblings = function ( elem ) {
    var siblings = [];
    var sibling = elem.parentNode.firstChild;
    for ( ; sibling; sibling = sibling.nextSibling ) {
        if ( sibling.nodeType === 1 && sibling !== elem ) {
            siblings.push( sibling );
        }
    }
    return siblings;
};
function setMainFlyer(img) {
    var attr_mainflyer = img.getAttribute('data-mainflyer');

    if ( 'string' !== typeof attr_mainflyer )
        return false;

    //alert(img.className);

    $(img).closest('.pull-left').toggleClass('setmain').siblings().removeClass('setmain');



}
function traitementXHR(jsonObj)
{
    //bootbox.alert('Fichier enregistré sous le nom <strong>'+jsonObj.filename+'</strong>');
        $.notify({ message:  jsonObj.success  },{ type: 'info' });

        if (flyerexiste ) {
            //Append carouselimages

            let divp = createElement('div', {class:'pull-left border4px', style: 'margin-left:5px;'});


            let img = createElement('img', {class: 'imgliste', src: "/tmp/"+jsonObj.filename, 'data-filename': jsonObj.filename, 'data-mainflyer': 'mf', height: 70});
            img.setAttribute("onclick", "setMainFlyer(this);" );

            divp.appendChild(img);

            let hr = createElement("hr", {});
            divp.appendChild(hr);

            //Bouton Suppr
            let btnsuppr = createElement('a', {class:'btn btn-default', title: 'Supprimer'});
            btnsuppr.setAttribute("onclick","removeMultiFlyer(this);");

            //fatrash
            let emfatrash = createElement('em', {class: 'fa fa-trash'});
            btnsuppr.appendChild(emfatrash);

            divp.appendChild(btnsuppr);




            document.getElementById('carouselimages').appendChild(divp);

            //document.getElementById('image').innerHTML = '';
            if ( document.querySelectorAll('.loadedimge').length <= 0 )
                document.getElementById('image').innerHTML = '';
            else
                document.getElementById('imageplus').innerHTML = '';

        } else {
            document.getElementById(divimage).innerHTML = '<a class="loadedimge" href="/tmp/' + jsonObj.filename + '" target="_blank"><img src="/tmp/' + jsonObj.filename + '" class="img-responsive"></a>';
            if ( document.getElementById('imagecontainer') ) {
                document.getElementById(divimage).innerHTML = '<img src="/tmp/' + jsonObj.filename + '" class="img-responsive">';
                enableCropper();
            }

            console.log('traitementXHR - tab_hidden.length : '+tab_hidden.length+' - '+tab_hidden ); 

            if ( tab_hidden.length>=2) {
                document.getElementById(tab_hidden[0]).value = jsonObj.filename;
                document.getElementById(tab_hidden[1]).value = '1';
            }
        }
}
function duplicateArticlesArtiste()
{
    var blocclone = selector_fieldArticlesArtists;
    // alert(blocclone.length);

    if ( typeof arguments[0] != 'undefined' ) {
        var nodel = arguments[0];

        var copy = nodel.closest('.blocclone_articles_artiste').cloneNode(true);

        //Modif Attributes
        copy.querySelector('input[type=text]').setAttribute('name', copy.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+eval(blocclone.length)+"]" ) );
        copy.querySelector('input[type=text]').value = '';

        divContainer_Artistes.appendChild(copy);

        nodel.classList.add('hidden');
        nodel.nextElementSibling.classList.remove('hidden');


            var i = 0;
            [].forEach.call(blocclone, function(el) {

                //Modif Attributes
                el.querySelector('input[type=text]').setAttribute('name', el.querySelector('input[type=text]').getAttribute('name').replace(/\[[0-9]*\]/g, "["+i+"]" ) );



                i++;

            });




        return;
    }


    delete eld;
    var eld = divContainer_Artistes ;
    var dataprototype_html = eld.getAttribute('data-prototype');
    //alert(blocclone.length);


    dataprototype_html = dataprototype_html.replace(/__name__/g, blocclone.length );

    eld.innerHTML += dataprototype_html;


}
function callbackFillSelect2Generic(responseText)
{
    let objetparamplus = null;
    bFnparseDataExists = false;

    if ('undefined' !== typeof arguments[1]  ) {
        objetparamplus = arguments[1];
        bFnparseDataExists = true;
        if (typeof objetparamplus.domElement != 'undefined') {
            domElement = objetparamplus.domElement;
        }

        if (typeof objetparamplus.fnCallback != 'undefined') {
            fnCallback = objetparamplus.fnCallback;
        }

        if (typeof objetparamplus.fnTriggerSelect2 != 'undefined') {
            fnTriggerSelect2 = objetparamplus.fnTriggerSelect2;
        }

        if (typeof objetparamplus.fnParseData != 'undefined') {
            fnParseData = objetparamplus.fnParseData;
        }


    }

    var jsonObjlocal = JSON.parse( responseText);
    console.log('xhr.responseText '+ jsonObjlocal);
    if (bFnparseDataExists) {
        var results = fnParseData(jsonObjlocal);
        fnTriggerSelect2(results, domElement, fnCallback);
    }
}
function fillSelect2ById(id,url,domElement) {

    var fnTriggerSelect2 = createOptionsAndTriggerSelect2;
    var fnCallback = function(){};
    var fnParseData = parseDataResultsGenerique;

    if ('function' === typeof arguments[3]  ) {
        fnTriggerSelect2 = arguments[3];
    }
    if ('function' === typeof arguments[4]  ) {
        fnCallback = arguments[4];
    }
    if ('function' === typeof arguments[5]  ) {
        fnParseData = arguments[5];
    }

    console.log(typeof fnTriggerSelect2+' - '+fnTriggerSelect2.name );
    console.log(typeof fnCallback+' - '+fnCallback.name );
    console.log('id : '+id+' - url: '+ url);

    genericAjax(url+'?id='+id,"GET",null,'callbackFillSelect2Generic',{fnParseData:fnParseData, domElement:domElement,fnCallback:fnCallback, fnTriggerSelect2:fnTriggerSelect2});

}
// *********** Create List ****************
function createOptionsByData(data)
{
    console.log( 'createOptionsByData '  );
    console.log( data  );
    console.log( 'createOptionsByData - data.dateUnique : '+data.id+"-"+data.name );
    //var optionlocal = new Option(results[i].name,results[i].id, true, true);
    var optHTML = document.createElement('option');
    optHTML.value = data.id;
    optHTML.textContent = data.name;
    optHTML.setAttribute('selected','selected');
    if (typeof data.photo != 'undefined' && '' !== data.photo )
        optHTML.setAttribute('data-photo',data.photo);
    if (typeof data.flyer != 'undefined' && '' !== data.flyer )
        optHTML.setAttribute('data-flyer',data.flyer);
    if (typeof data.date_unique != 'undefined' && '' !== data.date_unique )
        optHTML.setAttribute('data-date_unique',data.date_unique);
    if (typeof data.heure_debut != 'undefined' && '' !== data.heure_debut )
        optHTML.setAttribute('data-heure_debut',data.heure_debut);
    if (typeof data.heure_fin != 'undefined' && '' !== data.heure_fin )
        optHTML.setAttribute('data-heure_fin',data.heure_fin);
    if (typeof data.date_debut != 'undefined' && '' !== data.date_debut )
        optHTML.setAttribute('data-date_debut',data.date_debut);
    if (typeof data.date_fin != 'undefined' && '' !== data.date_fin )
        optHTML.setAttribute('data-date_fin',data.date_fin);
    if (typeof data.logo != 'undefined' && '' !== data.logo )
        optHTML.setAttribute('data-logo',data.logo);
    if (typeof data.startdate != 'undefined' && '' !== data.startdate )
        optHTML.setAttribute('data-startdate',data.startdate);
    if (typeof data.enddate != 'undefined' && '' !== data.enddate )
        optHTML.setAttribute('data-enddate',data.enddate);


    return optHTML;
}


function createOptionsAndTriggerSelect2(results, domElement)
{
    console.log('results : '+results  );
    console.log('results.length : '+results.length);
    if(results.length<=0)
        return false;
    
    var fnOption = function(e){};
    if ('function' === typeof arguments[2]  ) {
        fnOption = arguments[2];
    }
    
    if(results.length>0) {
        for(var i = 0; i < results.length; i++ ) {


            domElement.append(createOptionsByData(results[i]));
            
            fnOption(results[i]);
        }
        domElement.trigger('change');
    }
}

function createListLocalAndTriggerSelect2(results)
{
    console.log('createListLocalAndTriggerSelect2');
    console.log(results);
    if(results.length<=0)
        return false;

    if(results.length>0) {
        for(var i = 0; i < results.length; i++ ) {

            selector_Fete.append(createOptionsByData(results[i]));
        }
        selector_Fete.trigger('change');
    }


}

function createListLieuxAndTriggerSelect2(results)
{
    console.log('createListLieuxAndTriggerSelect2');
    console.log(results);
    if(results.length<=0)
        return false;
    
    if(results.length>0) {
        for(var i = 0; i < results.length; i++ ) {

            selector_lieu.append(createOptionsByData(results[i]));
            createListHTML(results[i],'resultLieux', '.select2_lieux', selector_lieu, edit_lieu);
        }
        selector_lieu.trigger('change');
    }
}
function createListArtisteAndTriggerSelect2(results)
{
    if(results.length<=0)
        return false;
    
   ;
    
    if(results.length>0) {
        for(var i = 0; i < results.length; i++ ) {

            selector_Artiste.append(createOptionsByData(results[i]));
            createListArtistes(results[i]);
        }
        selector_Artiste.trigger('change');
    }
}


function createListEventsRelatedAndTriggerSelect2(results, domElement) {
    console.log('createListEventsRelatedAndTriggerSelect2 : ' + results);
    if (results.length <= 0)
        return false;


    if (results.length > 0) {
        var tab_html = [];

        for (var i = 0; i < results.length; i++) {

            domElement.append(createOptionsByData(results[i]));
        }
        domElement.trigger('change');
    }

}


function createListEventsAndTriggerSelect2(results)
{
    console.log('createListEventsAndTriggerSelect2 : '+results);
    if(results.length<=0)
        return false;
    

    
    if(results.length>0) {
        var tab_html = [];
        
        for(var i = 0; i < results.length; i++ ) {

            selector_Event.append(createOptionsByData(results[i]));
            
            tab_html.push('<div class="col-sm-1" style="margin-top: 4px;" ><img style="cursor: pointer;" data-toggle="tooltip" title="Choisir cette image pour recadrer une photo de cet artiste" onclick="setCropper(this);" src="/media/cache/my_thumb_70/assets/flyers/'+results[i].flyer +'" /></div>');
        }
        selector_Event.trigger('change');
        
        if (document.getElementById("carouselevent") && tab_html.length>0) {
            document.getElementById("carouselevent").innerHTML = tab_html.join(" ");
            //$('#carouselevent').slick();
        }
    }
}
function createListParentArtisteAndTriggerSelect2(results)
{
    if(results.length<=0)
        return false;

    ;

    if(results.length>0) {
        for(var i = 0; i < results.length; i++ ) {

            selector_parentArtistes.append(createOptionsByData(results[i]));
            createListArtistes(results[i]);
        }
        selector_parentArtistes.trigger('change');
    }
}

function createListEnfantsArtisteAndTriggerSelect2(results)
{
    if(results.length<=0)
        return false;

    ;

    if(results.length>0) {
        for(var i = 0; i < results.length; i++ ) {

            selector_enfantArtistes.append(createOptionsByData(results[i]));
            createListArtistes(results[i]);
        }
        selector_enfantArtistes.trigger('change');
    }
}

// *********** Submit event ****************
function setOnSubmitFormLieu()
{
    selector_field_Eventlieu_Tel.val( selector_field_Eventlieu_Tel.intlTelInput("getNumber") );

    selector_Telinput.each( function(){
        $(this).val( $(this).intlTelInput("getNumber") );
    });
}

/**
 * 
 * @param DOM selector_Event_Inarticle
 * @param STRING ajax_url_Event
 * @param function formatSelect2TemplateResultEventInArtiste
 * @param function formatSelect2TemplateResultSelection
 * @returns voir
 */
function initSelect2(domElement,url) 
{
    let fnData = parseDataGenerique;
    let fnResult = formatSelect2TemplateResultGenerique;
    let fnSelection = formatSelect2TemplateResultSelection;
    let maximumSelectionLength = 0;
    
    if ('function' === typeof arguments[2]  ) {
        fnResult = arguments[2];
    }
    if ('function' === typeof arguments[3]  ) {
        fnSelection = arguments[3];
    }
    if ('function' === typeof arguments[4]  ) {
        fnData = arguments[4];
    }

    if ('number' === typeof arguments[5]  ) {
        maximumSelectionLength = arguments[5];
    }
    


    let confSelect = {
        language: "fr",
        ajax: {
            //url: "https://api.github.com/search/repositories",
            url: url,
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    name: params.term, // search term
                    //page: params.page
                };
            },
            processResults: function (data, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: fnData(data),
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function (markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: fnResult,
        templateSelection: fnSelection

    };


    // Si url vide
    if ('' == url ) {
        delete confSelect.ajax;
    }

    // Si maximumSelectionLength
    if (maximumSelectionLength>0) {
        confSelect.maximumSelectionLength = 1;
    }

    domElement.select2(confSelect);
}

function initSelect2Event(dom)
{
    initSelect2(dom,ajax_url_Event,formatSelect2TemplateResultEventInArtiste,formatSelect2TemplateResultSelectionEvent  );
}


function initSelect2Lieu(dom, selectorName)
{
    initSelect2(dom,ajax_url_lieu,formatSelect2TemplateResultLieu,formatSelect2TemplateResultSelectionLieu,parseDataLieu);

    dom
        .on("select2:select", function (e) {
             console.log('select2:select '+e.params.data);
            createListHTML(e.params.data, 'resultLieux', selectorName, dom, edit_lieu );
        })
        .on("select2:unselecting", function (e) {
             if(1 == dom.val().length) {
                document.getElementById('resultLieux').innerHTML = '';
            }
        })
        .on("select2:unselect", function (e) {
            console.log( e.params.data   );
            var data = e.params.data;
             if (document.getElementById('resultLieux_'+data.id ))
                document.getElementById('resultLieux_'+data.id ).remove();

        });
}

function initSelect2Artiste()
{
    initSelect2(selector_Artiste,ajax_url_Artiste,formatSelect2TemplateResultArtiste,formatSelect2TemplateResultSelectionArtiste,parseDataResultsGenerique );

    selector_Artiste
    .on("select2:select", function (e) {
        createListArtistes(e.params.data);
    })
    .on("select2:unselecting", function (e) {
        if(1 == selector_Artiste.val().length) {
            document.getElementById('resultArtistes').innerHTML = '';
        }
    })
    .on("select2:unselect", function (e) {
        console.log( e.params.data   );
        var data = e.params.data;
        if (document.getElementById('artiste_'+data.id ))
            document.getElementById('artiste_'+data.id ).remove();

    });
}

function initSelect2Fete()
{
    initSelect2(selector_Fete,ajax_url_Fete,formatSelect2TemplateResultFete,formatSelect2TemplateSelectionFete );
}

function initSelect2EventByMember(dom)
{
    initSelect2(dom,ajax_url_Event,formatSelect2TemplateResultEventInArtiste,formatSelect2TemplateResultSelectionEvent,parseDataGenerique,1  );
}

function initSelect2ParentArtiste()
{
    // initSelect2(selector_parentArtistes,ajax_url_Artiste,formatSelect2TemplateResultArtiste,formatSelect2TemplateResultSelectionArtiste,parseDataResultsGenerique,1 );
    initSelect2(selector_parentArtistes,ajax_url_Artiste,formatSelect2TemplateResultArtiste,formatSelect2TemplateResultSelectionArtiste,parseDataResultsGenerique );

}

function initSelect2EnfantArtiste()
{
    initSelect2(selector_enfantArtistes,ajax_url_Artiste,formatSelect2TemplateResultArtiste,formatSelect2TemplateResultSelectionArtiste,parseDataResultsGenerique );
}

function parseDataGenerique(data)
{
    return data;
}
function parseDataResultsGenerique(data)
{
    return data.results;
}

function parseDataLieu(data)
{
    return data.lieu;
}
function closeIframeArtisteEdition() {
    var blociframeartiste = document.getElementById('blociframeartiste');

    if (!blociframeartiste)
        return;

    iframeid = document.getElementById('iframe');

    blociframeartiste.classList.add('hidden');
    iframeid.setAttribute('src','about:blank');
    iframeid.setAttribute('height',0);
}
function getMessage(e) {
    var data = e.data;
    if('string' === typeof data.classimg && 'string' === typeof data.action && 'refreshimageartiste' === data.action ) {
        var classimg = '.'+data.classimg;
        var src = data.src;
        var data_photo = data.data_photo;
        var data_name = data.data_name;
        var class_artistename = '.artistename_'+data.idimg;
        var class_photocontainer = '.photocontainer_'+data.idimg;
        if( document.querySelectorAll(classimg).length > 0 ) {
            [].forEach.call(document.querySelectorAll(classimg), function(el) {
                el.setAttribute('src',src);
                el.setAttribute('data-photo', data_photo)
                el.setAttribute('data-name', data_name);
            });
        } else {
            [].forEach.call(document.querySelectorAll(class_photocontainer), function(el) {
                el.innerHTML = '<img onclick="bootbox_Image(this);" style="cursor:pointer;" src="'+src+'" class="'+classimg+'" data-name="'+data_name+'" data-photo="'+data_photo+'" alt="" width="40" >';
            });
        }
        if( document.querySelectorAll(class_artistename).length > 0 ) {
            [].forEach.call(document.querySelectorAll(class_artistename), function(el) {
                el.textContent = data_name;
            });
        }
        closeIframeArtisteEdition();
    }
    if( 'string' === typeof data.action && 'closeiframe' === data.action ) {
        closeIframeArtisteEdition();
    }
}
function callPostMessage(json)
{
    var argument2postmessage = document.location.protocol+'//'+document.location.hostname;
    window.parent.postMessage( json ,argument2postmessage );
}
function callCloseIframe()
{
    var json = {
            "action" : "closeiframe" 
        };
    callPostMessage(json);
    
}

function reFreshParentIframe()
{
    var circleimage = document.getElementById("circleimage");
    if (circleimage) {
        var idimg = circleimage.getAttribute("data-id");
        var classimg = "circleimage_"+idimg;
        var src = circleimage.getAttribute("src");
        var data_photo = circleimage.getAttribute("data-photo");
        var data_name = circleimage.getAttribute("data-name");

        var json = {
            "classimg" : classimg,
            "action" : "refreshimageartiste",
            "src" : src,
            "data_photo" : data_photo,
            "data_name" : data_name,
            "idimg" : idimg
        };

        callPostMessage(json);
    }
}



function createCloneVideo()
{
    selector_fieldVideo.remove();

    var eld = divContainer_Video ;
    var dataprototype_html = eld.getAttribute('data-prototype');

    for( var i=0; i<jsoneventvideos.length; i++ ) {
        dataprototype_html = dataprototype_html.replace(/__name__/g, i);
        dataprototype_html = dataprototype_html.replace(/class="form-control"/g, 'class="form-control"  value="'+jsoneventvideos[i].url+'"'); 

        //dataprototype_html = dataprototype_html.replace(new RegExp('data-index','g'), i);
        eld.innerHTML += dataprototype_html;
    }

    var i = 0;
    [].forEach.call(eld.querySelectorAll(".blocclone_video"), function(el) {

      [].forEach.call(el.querySelectorAll("a"), function(el) {

          //Si on n'est pas à la fin
          if (eld.querySelectorAll(".blocclone_video").length>1 && eval(i+1) < eld.querySelectorAll(".blocclone_video").length ) {

              if ( el.classList.contains('btnadd')) {
                el.classList.add('hidden');
              } else {
                el.classList.remove('hidden');
              }
              
          } 
          
      });

      var element = el.querySelector('.form-control');
      var labelelement = el.getElementsByTagName('label');

      var nameattr_element = element.getAttribute('name');
      var id_element = element.getAttribute('id');
      var forattr_labelelement = labelelement[0].getAttribute('for');

      labelelement[0].setAttribute('for', forattr_labelelement.replace(new RegExp('0','g'), i));
      element.setAttribute('name', nameattr_element.replace(new RegExp('0','g'), i));
      element.setAttribute('id', id_element.replace(new RegExp('0','g'), i));

       

      i++;

    }); 
}

function createCloneArticlesInArtiste()
{
    document.querySelector('.blocclone_articles_artiste').remove();

        var eld = divContainer_Artistes ;
        var dataprototype_html = eld.getAttribute('data-prototype');

        for( var i=0; i<jsonarticlesevent.length; i++ ) {
            dataprototype_html = dataprototype_html.replace(/__name__/g, i);
            dataprototype_html = dataprototype_html.replace(/class="form-control"/g, 'class="form-control"  value="'+jsonarticlesevent[i].url+'"');


            eld.innerHTML += dataprototype_html;
        }

        var i = 0;
        [].forEach.call(eld.querySelectorAll(".blocclone_articles_artiste"), function(el) {

            [].forEach.call(el.querySelectorAll("a"), function(el) {

                //Si on n'est pas à la fin
                if (eld.querySelectorAll(".blocclone_articles_artiste").length>1 && eval(i+1) < eld.querySelectorAll(".blocclone_articles_artiste").length ) {

                    if ( el.classList.contains('btnadd')) {
                        el.classList.add('hidden');
                    } else {
                        el.classList.remove('hidden');
                    }

                }

            });

            var element = el.querySelector('.form-control');
            var labelelement = el.getElementsByTagName('label');

            var nameattr_element = element.getAttribute('name');
            var id_element = element.getAttribute('id');
            var forattr_labelelement = labelelement[0].getAttribute('for');

            labelelement[0].setAttribute('for', forattr_labelelement.replace(new RegExp('0','g'), i));
            element.setAttribute('name', nameattr_element.replace(new RegExp('0','g'), i));
            element.setAttribute('id', id_element.replace(new RegExp('0','g'), i));



            i++;

        });
}
function createCloneArticles()
{
    document.querySelector('.blocclone_articles').remove();

    var eld = divContainer_Articles ;
    var dataprototype_html = eld.getAttribute('data-prototype');

    for( var i=0; i<jsonarticlesevent.length; i++ ) {
        dataprototype_html = dataprototype_html.replace(/__name__/g, i);
        dataprototype_html = dataprototype_html.replace(/class="form-control"/g, 'class="form-control"  value="'+jsonarticlesevent[i].url+'"');


        eld.innerHTML += dataprototype_html;
    }

    var i = 0;
    [].forEach.call(eld.querySelectorAll(".blocclone_articles"), function(el) {

        [].forEach.call(el.querySelectorAll("a"), function(el) {

            //Si on n'est pas à la fin
            if (eld.querySelectorAll(".blocclone_articles").length>1 && eval(i+1) < eld.querySelectorAll(".blocclone_articles").length ) {

                if ( el.classList.contains('btnadd')) {
                    el.classList.add('hidden');
                } else {
                    el.classList.remove('hidden');
                }

            }

        });

        var element = el.querySelector('.form-control');
        var labelelement = el.getElementsByTagName('label');

        var nameattr_element = element.getAttribute('name');
        var id_element = element.getAttribute('id');
        var forattr_labelelement = labelelement[0].getAttribute('for');

        labelelement[0].setAttribute('for', forattr_labelelement.replace(new RegExp('0','g'), i));
        element.setAttribute('name', nameattr_element.replace(new RegExp('0','g'), i));
        element.setAttribute('id', id_element.replace(new RegExp('0','g'), i));



        i++;

    });
}


function createCloneTelsLieu()
{
    document.querySelector('.blocclone_tel').remove();

    var eld = document.getElementById('collectiontelContainer') ;
    var dataprototype_html = eld.getAttribute('data-prototype');

    for( var i=0; i<jsontelslieu.length; i++ ) {

        dataprototype_html = dataprototype_html.replace(/__name__/g, i);
        dataprototype_html = dataprototype_html.replace(/form-control"/g, 'form-control"  value="'+jsontelslieu[i].tel+'"'); 

        //dataprototype_html = dataprototype_html.replace(new RegExp('data-index','g'), i);
        eld.innerHTML += dataprototype_html;
    }

    var i = 0;
    [].forEach.call(eld.querySelectorAll(".blocclone_tel"), function(el) {

      [].forEach.call(el.querySelectorAll("a"), function(el) {

          //Si on n'est pas à la fin
          if (eld.querySelectorAll(".blocclone_tel").length>1 && eval(i+1) < eld.querySelectorAll(".blocclone_tel").length ) {

              if ( el.classList.contains('btnadd')) {
                el.classList.add('hidden');
              } else {
                el.classList.remove('hidden');
              }

          } 

      });

      var element = el.querySelector('.form-control');
      var labelelement = el.getElementsByTagName('label');

      var nameattr_element = element.getAttribute('name');
      var id_element = element.getAttribute('id');


      element.setAttribute('name', nameattr_element.replace(new RegExp('0','g'), i));
      element.setAttribute('id', id_element.replace(new RegExp('0','g'), i));



      i++;

    }); 
 
}
function callbackSetMainFlyer(responseText)
{
    if ('undefined' !== typeof arguments[1]  ) {
        objetparamplus = arguments[1];
        if (typeof objetparamplus.el != 'undefined') {
            el = objetparamplus.el;
        }
    }

    var jsonObj = JSON.parse( responseText);
    var optionhtml = [];
    if ('undefined'!=typeof el && jsonObj.success != '' ) {
        var mainflyer = field_mainflyerId;
        var mainflyer_src = mainflyer.getAttribute('src');

        mainflyer.setAttribute('src',el.querySelector('img').getAttribute('src'));
        el.querySelector('img').setAttribute('src',mainflyer_src);
        el.setAttribute('data-flyerid', jsonObj.id );

        $.notify({ message: jsonObj.success  },{ type: 'info' });
    } else {
        $.notify({ message: jsonObj.error },{ type: 'error' });
    }
}
function setOnClickMainFlyer()
{
    [].forEach.call(selector_mainFlyer, function(el) {

       
       el.addEventListener( 'click', function(evt){
           
            confirmGenerique("Modifier le flyer principal par celui-ci ?", function(result) {
              if ( result ) {

                  let url = ajax_url_setFlyer+'?eventid='+el.getAttribute('data-eventid')+'&id='+el.getAttribute('data-flyerid');
                  genericAjax(url ,"GET",null,'callbackSetMainFlyer', {el:el} );

                
              }
            });

               evt.preventDefault();
            

       });
    });
}
function callbackDeleteFlyer(responseText)
{

    if ('undefined' !== typeof arguments[1]  ) {
        objetparamplus = arguments[1];
        if (typeof objetparamplus.el != 'undefined') {
            el = objetparamplus.el;
        }
    }

    var jsonObj = JSON.parse( responseText);
    var optionhtml = [];
    if ('undefined'!=typeof el && jsonObj.success != '' ) {
        el.closest('.pull-left').remove();
        $.notify({ message: jsonObj.success },{ type: 'info' });
    } else {
        $.notify({ message: jsonObj.error },{ type: 'error' });
    }
}
function setOnClickDeleteFlyer()
{
    [].forEach.call(selector_deleteFlyer, function(el) {

       
       el.addEventListener( 'click', function(evt){
           
            confirmGenerique("Supprimer ce flyer ?", function(result) {
              if ( result ) {

                genericAjax(ajax_url_deleteFlyer+'?id='+el.getAttribute('data-flyerid') ,"GET",null,'callbackDeleteFlyer', {el:el} );

                
              }
            });

               evt.preventDefault();
            

       });
       

    });
}
function setOnClickBtndeleteitem()
{
    [].forEach.call(selector_btndeleteitem, function(el) {
          el.addEventListener('click', function(evt) {
                
                confirmGenerique("Êtes-vous sur de vouloir supprimer cet item ?", function(result) {
                    if( result ) {
                            window.location.href = el.getAttribute('href');                            
                    }
                }); 
                evt.preventDefault();
          });
    });
}

function setOnClickCheckboxEnddate()
{
    checkbox_Enddate.addEventListener('click', function(evt) {
        document.querySelector(".blockdatefin").classList.toggle('hidden');
        document.querySelector(".lbl_debut").classList.toggle('hidden');
        document.querySelector(".lbl_unique").classList.toggle('hidden');

    });
}

function setOnClickCheckboxAll()
{
    checkbox_All.addEventListener('click', function(evt) {
            
        [].forEach.call(selector_td_classStyled, function(el) {
           if ( checkbox_All.checked ) {
               el.setAttribute('checked','checked');
           } else {
               el.removeAttribute('checked');
           }

        });

   });
}

function setOnClickButtonAll()
{
    [].forEach.call(selector_button_All, function(el) {
          el.addEventListener('click', function(evt) {
                var nbchecked = 0;   
                 [].forEach.call(selector_td_classStyled, function(el) {
                    if ( el.checked ) {
                        nbchecked++;
                    } 
                     
                 });
                //alert( el.classList.contains('btndelete') );

                if (nbchecked<=0) {
                    alertGenerique("Veuillez cocher au moins une entité avant de cliquer sur ce bouton.");
                    evt.preventDefault();  
                } 

                if (nbchecked>0 && el.classList.contains('btndelete') ) {
                    confirmGenerique("Êtes-vous sur de vouloir supprimer les entités sélectionnés ?", function(result) {
                        if( result ) {
                            // Remplir le champ hidden mdts_homebundle_all[ids] par les ID cochés
                            checkIdsForm();
                            document.querySelector("form[name=mdts_homebundle_all]").submit();
                        }
                    });
                } else if (nbchecked>0) {
                    if ( !el.classList.contains('btncopy') && select_action.options[select_action.selectedIndex].value == '' ) {
                        alertGenerique("Veuillez choisir au moins une option dans la liste avant de cliquer sur ce bouton.","","info");
                        evt.preventDefault();
                    } else {
                        //alert( document.getElementById('select_action').options[document.getElementById('select_action').selectedIndex].value );
                        // Remplir le champ hidden mdts_homebundle_all[ids] par les ID cochés
                        checkIdsForm();
                        document.querySelector("input[name='_method']").value = 'POST';
                        document.querySelector("form[name=mdts_homebundle_all]").submit();
                    }

                }

                evt.preventDefault(); 
          });

        }); 
}

function setOnClickButtonAdd()
{
    [].forEach.call(selector_button_Add, function(el) {
          el.addEventListener('click', function(evt) {
            var  url;
            
            if ( el.classList.contains('btnaddcountry') ) {
                addcountry(ajax_url_saveCountries);
            } else if ( el.classList.contains('btnaddregion') ) { 
                addregion(ajax_url_saveRegions);
            } else if ( el.classList.contains('btnaddville') ) { 
                addville(ajax_url_saveLocalities);
            } else if ( el.classList.contains('btnaddquartier') )  {
                addquartier(ajax_url_saveQuartiers);
            } else if ( el.classList.contains('btnaddlieu') )  {                
                addlieu(ajax_url_saveLieux);
            } else if ( el.classList.contains('btnaddlocal') )  {                
                addlocal(ajax_url_saveLocal);
            }  else if ( el.classList.contains('btnaddartistes') )  {                
                addartistes(ajax_url_saveArtists);
            }
             
         
            evt.preventDefault();
            
        });
    });
}

function setOnSubmitFormArticles()
{
    addEvent( document.querySelector(dom_formArticles), 'submit', function(evt) {
        document.getElementById('mdts_homebundle_articlesevent_event').value =  $('#select_mdts_homebundle_event').val() ;
        document.getElementById('mdts_homebundle_articlesevent_artiste').value =  $('#select_mdts_homebundle_artiste').val() ;
    });
}

function setOnSubmitFormArtistes()
{
    addEvent( document.querySelector(dom_formArtistes), 'submit', function(evt) {
        document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_event').value =  $('#select_mdts_homebundle_event').val() ;


        // Remplir parentartisteid
        document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_parentartisteid').value =  selector_parentArtistes.val() ;

        // Remplir enfantid
        document.getElementById('mdts_homebundle_eventartistesdjorganisateurs_enfantartisteid').value =  selector_enfantArtistes.val() ;

    });
}

function setOnSubmitFormEvent()
{
    addEvent( document.querySelector(dom_formEvent), 'submit', function(evt) {
        document.getElementById('mdts_homebundle_event_eventlocal').value =  $('#select_mdts_homebundle_event_eventlocal').val() ;
        document.getElementById('mdts_homebundle_event_artistesdj').value =  $('#select_mdts_homebundle_event_artistesdj').val() ;
        document.getElementById('mdts_homebundle_event_eventmultilieu').value =  $('#select_mdts_homebundle_event_eventlieu').val() ;
        document.getElementById('mdts_homebundle_event_related_event').value =  $('#select_mdts_homebundle_related_event').val() ;
        document.getElementById('mdts_homebundle_event_eventlocal').value =  $('#select_mdts_homebundle_event_eventlocal').val() ;


        //Multiflyer
        if ( document.querySelectorAll('.imgliste').length > 0 ) {
            var strimage = '';
            var ismain = false;
            var arrayimage = new Array();
            [].forEach.call(document.querySelectorAll('.imgliste'), function(el) {
                strimage = el.getAttribute('data-filename');
                ismain = el.closest('.pull-left').classList.contains('setmain');

                var obj = {
                    'image' : strimage,
                    'main' : ismain,
                };

                arrayimage.push(obj);
            });

            //alert( JSON.stringify(arrayimage));
            //document.getElementById('mdts_homebundle_event_multiflyer').value = arrayimage.join(";");
            document.getElementById('mdts_homebundle_event_multiflyer').value = JSON.stringify(arrayimage);
        }


        // console.log( 'FORM : '+$(dom_formEvent).serialize() );

        // Remplir mdts_homebundle_event[dateclair]
        fillDateclairField();

        return false;

    });
}

/**
 * Remplir le champ hidden mdts_homebundle_event[dateclair] en fonction du type dans mdts_homebundle_event[opt_dateclair]
 *
 * @return String
 */
function fillDateclairField()
{
    // Valeur de mdts_homebundle_event[opt_dateclair]
    var valOpt = opt_dateclair.value;

    var dValue = '';

    if ('rangedate' == valOpt && document.querySelector(selector_DateRange).getAttribute('value')!=='' ) {
        // Si date a intervalles.
        dValue = document.querySelector(selector_DateRange).getAttribute('value');

    } else if ('multiplesdate' == valOpt && document.querySelector(selector_DateMultiple).getAttribute('value')!==''  ) {
        // Si date a intervalles.
        dValue = document.querySelector(selector_DateMultiple).getAttribute('value');
    } else {
        // Si date unique
        dValue = document.querySelector(selector_Date).getAttribute('value')+'---'+document.querySelector(selector_Date2).getAttribute('value');
    }

    // Remplir le champ
    dateclair.value = dValue;
    // console.log(dValue);
    return dValue;

}

function testDateR(selectedDates, dateStr, instance)
{
    alert(selectedDates);
    alert(dateStr);
}

function setOnSubmitFormEventMultiple()
{
    addEvent( selector_formEventMultiple, 'submit', function(evt) {
        document.getElementById('form_eventlocal').value =  $('#select_mdts_homebundle_event_eventlocal').val() ;
        document.getElementById('form_artistesdj').value =  $('#select_mdts_homebundle_event_artistesdj').val() ;
        document.getElementById('form_eventmultilieu').value =  $('#select_mdts_homebundle_event_eventlieu').val() ;
        document.getElementById('form_related_event').value =  $('#select_mdts_homebundle_related_event').val() ;
    });
}

function setOnSubmitFormLocal()
{
    addEvent( document.querySelector(dom_formLocal), 'submit', function(evt) {
        var txtContent = document.querySelector("#rfc-output a").textContent;         
        if ( txtContent != '' ) 
           document.getElementById('mdts_homebundle_eventlocal_recurrence').value = txtContent;
       
    });
}
/**
 * Action event generique
 *
 * @param dom
 * @param event - Nom de l'action
 * @param fn - Fonction appelée
 */
function setOnEvent(dom,event,fn)
{
    if ('string' !== typeof event)
        return;

    if ('function'!==fn)
        return;

    addEvent(dom,event,fn);
}
function setOnEventChange(dom,fn)
{
    // setOnEvent(dom,'change',fn);
    addEvent(dom,'change',fn);
}

function setOnEventSelect2(dom,fn)
{
    dom.on("select2:select", fn);
}
function setOnEventUnselecting(dom,fn)
{
    dom.on("select2:unselecting", fn);
}
function setOnEventUnselect(dom,fn)
{
    dom.on("select2:unselect", fn);
}
function callbackFillSelect2Region(responseText)
{
    var jsonObj = JSON.parse( responseText);
    var optionhtml = [];
    if ( jsonObj.length > 0 ) {
        for( var i =0; i < jsonObj.length; i++) {
            optionhtml.push('<option value="'+jsonObj[i].id+'">'+jsonObj[i].name+'</option>');
        }
        field_Region.innerHTML += optionhtml.join("\n");
    }

    jQuery('#mdts_homebundle_eventlieu_region').trigger('change');
}
function eventChangeCountry(evt)
{
    var value = field_Country.options[field_Country.selectedIndex].value;
    var text = field_Country.options[field_Country.selectedIndex].text;
    //alert( text+" - "+value );
    //alert(field_Quartier.innerHTML);
    if ( value != '' ) {
        field_Region.innerHTML = '<option>Choisir la région</option>';
        field_Locality.innerHTML = '<option value="">Choisir la ville</option>';
        field_Quartier.innerHTML = '<option value="">Choisir le quartier</option>';

        //remplir mdts_homebundle_eventlieu_region
        genericAjax(ajax_url_Regions+'?country_id='+value, "GET", null, 'callbackFillSelect2Region');
    }
}
function callbackFillSelect2Locality(responseText)
{
    var jsonObj = JSON.parse( responseText);
    var optionhtml = [];
    if ( jsonObj.length > 0 ) {
        for( var i =0; i < jsonObj.length; i++) {
            optionhtml.push('<option value="'+jsonObj[i].id+'">'+jsonObj[i].name+'</option>');
        }
        field_Locality.innerHTML += optionhtml.join("\n");
        jQuery('#mdts_homebundle_eventlieu_locality').trigger('change');
    }
}
function eventChangeRegion(evt)
{
    var value = field_Region.options[field_Region.selectedIndex].value;
    var text = field_Region.options[field_Region.selectedIndex].text;
    //alert( text+" - "+value );
    if ( value != '' ) {

        field_Locality.innerHTML = '<option value="">Choisir la ville</option>';
        field_Quartier.innerHTML = '<option value="">Choisir le quartier</option>';

        genericAjax(ajax_url_Localities+'?region_id='+value, "GET", null, 'callbackFillSelect2Locality');
    }
}
function callbackFillSelect2Quartier(responseText)
{
    var jsonObj = JSON.parse(responseText);
    var optionhtml = [];
    if ( jsonObj.length > 0 ) {
        for( var i =0; i < jsonObj.length; i++) {
            optionhtml.push('<option value="'+jsonObj[i].id+'">'+jsonObj[i].name+'</option>');
        }
        field_Quartier.innerHTML += optionhtml.join("\n");
        jQuery('#mdts_homebundle_eventlieu_quartier').trigger('change');
    }
}
function eventChangeLocality(evt)
{
    var value = field_Locality.options[field_Locality.selectedIndex].value;
    var text = field_Locality.options[field_Locality.selectedIndex].text;
    //alert( text+" - "+value );
    if ( value != '' ) {

        field_Quartier.innerHTML = '<option value="">Choisir le quartier</option>';

        //remplir mdts_homebundle_eventlieu_region
        genericAjax(ajax_url_Quartiers+'?ville_id='+value, "GET", null, 'callbackFillSelect2Quartier');
    }
}

function eventChangeEventEntree(evt)
{
    var value = field_optionEntreetype.options[field_optionEntreetype.selectedIndex].value;
    var text = field_optionEntreetype.options[field_optionEntreetype.selectedIndex].text;

    if ( value == 'new' ) { 
        addentreetype(ajax_url_saveEntreetypes);
    } else if ( value == '2' ) {
        //Payant
        field_price.classList.remove('hidden');
    } else  {
        field_price.classList.add('hidden');
    }
}

function eventChangeArtisteFile(event)
{
    var i = 0,
        files = field_Artiste_File.files,
        len = files.length;

    changeFiles(files);
}
function eventChangeFile(event)
{
    var i = 0,
        files = field_Event_File.files,
        len = files.length;

    changeFiles(files);
 
}
function eventChangeGeneric(evt)
{


    let target = evt.target;
    let value = target.value;
    let text = target.options[target.selectedIndex].text;
    let url = urlprefixe+target.getAttribute('data-ajaxurl');

    /*console.log(value);
    console.log(text);
    console.log(url);*/

    if ( value == 'new' ) {
        addGenericEntity(url, target);
    }
}
function changeFiles(files)
{
    var len = files.length;
    if (len > 1)
        return;

    for (let i=0; i < len; i++) {
        // alert("Filename: " + files[i].name);
        //alert("Type: " + files[i].type);
        //alert("Size: " + files[i].size + " bytes");

        //Si c'est u fichier image
        if (files[i].type == 'image/png' || files[i].type == 'image/jpeg' || files[i].type == 'image/jpg') {


            
            if ( document.getElementById(divimage) )
                document.getElementById(divimage).innerHTML = 'Patientez...'; 

            saveFlyerToServer(files[i]);


        } else {
            alertGenerique('Le fichier choisi doit avoir l\'extension <strong>jpg, jpeg</strong> ou <strong>png</strong>','','info');
        }
    }
}
/**
 * Google MAP
 */

function fillTxtField(lat,lng)
{
    let domFieldGps = document.getElementById(field_GpsID);
    if (domFieldGps === null) {
        return false;
    }
    window.parent.postMessage('iframe_parent', '*');
    domFieldGps.value = lat +","+lng;
}

function openMapAndSearch(zoom)
{
    document.querySelector(pathQuery_CarteGoogle).classList.toggle('hidden'); 
    initMap(zoom);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(field_Map_Input);
}
function callbackSaveAdress(responseText)
{
    var jsonObj = JSON.parse(responseText);
    if (  typeof jsonObj.success == 'string' ) {

        if ( document.getElementById('mdts_homebundle_eventlieu_country') ) {
            for(var i=0; i<document.getElementById('mdts_homebundle_eventlieu_country').options.length; i++) {
                if ( document.getElementById('mdts_homebundle_eventlieu_country').options[i].value == jsonObj.country_id ) {
                    document.getElementById('mdts_homebundle_eventlieu_country').selectedIndex = i;
                    break;
                }
            }


            document.getElementById('mdts_homebundle_eventlieu_country').innerHTML += '<option selected="selected" value="'+jsonObj.country_id+'">'+jsonObj.country_name+'</option>';

            jQuery('#mdts_homebundle_eventlieu_country').trigger('change');
        }

        if ( document.getElementById('mdts_homebundle_eventlieu_region') ) {
            for(var i=0; i<document.getElementById('mdts_homebundle_eventlieu_region').options.length; i++) {
                if ( document.getElementById('mdts_homebundle_eventlieu_region').options[i].value == jsonObj.region_id ) {
                    document.getElementById('mdts_homebundle_eventlieu_region').selectedIndex = i;
                    break;
                }
            }
            document.getElementById('mdts_homebundle_eventlieu_region').innerHTML += '<option selected="selected" value="'+jsonObj.region_id+'">'+jsonObj.region_name+'</option>';

            jQuery('#mdts_homebundle_eventlieu_region').trigger('change');
        }
        if ( document.getElementById('mdts_homebundle_eventlieu_locality') ) {
            for(var i=0; i<document.getElementById('mdts_homebundle_eventlieu_locality').options.length; i++) {
                if ( document.getElementById('mdts_homebundle_eventlieu_locality').options[i].value == jsonObj.locality_id ) {
                    document.getElementById('mdts_homebundle_eventlieu_locality').selectedIndex = i;
                    break;
                }
            }
            // console.log(document.getElementById('mdts_homebundle_eventlieu_locality').selectedIndex);
            document.getElementById('mdts_homebundle_eventlieu_locality').innerHTML += '<option selected="selected" value="'+jsonObj.locality_id+'">'+jsonObj.locality_name+'</option>';

            jQuery('#mdts_homebundle_eventlieu_locality').trigger('change');
        }


    }
}
function initMap(zoom) 
{
        var map;
        var markers = [];
        console.log(typeof field_GpsID);
        console.log(field_GpsID);
        let domFieldGps = document.getElementById(field_GpsID);
        
        var tab_gps_value = (domFieldGps !== null ? domFieldGps.value.split(',') : []);
        var latvalue = (tab_gps_value.length==2 && tab_gps_value[0]!=''?eval(tab_gps_value[0]):-18.91554305193574);
        var lngvalue = (tab_gps_value.length==2 && tab_gps_value[1]!=''?eval(tab_gps_value[1]):47.52160906791687);

        //alert( tab_gps_value.length+'*'+typeof tab_gps_value+' / '+ typeof latvalue+' - '+typeof lngvalue);

        map = new google.maps.Map(document.getElementById(field_mapID), {
            center: {lat: latvalue, lng: lngvalue},
            zoom: zoom
        });

        addMarker({lat: latvalue, lng: lngvalue} ,latvalue,lngvalue);
      

     
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(field_Map_Input);

        var autocomplete = new google.maps.places.Autocomplete(field_Map_Input);
        autocomplete.bindTo('bounds', map);

        var infowindow = new google.maps.InfoWindow();
          var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            anchorPoint: new google.maps.Point(0, -29)
        });

        google.maps.event.addListener(marker,'dragstart',function(event) 
        {
            console.log('dragstart');
            infowindow.close();
        });
        google.maps.event.addListener(marker,'dragend',function(event) 
        {
            //document.getElementById('lat').value =event.latLng.lat();
            //document.getElementById('lng').value =event.latLng.lng();
             alert(event.latLng.lat()+","+event.latLng.lng());
        });

         
        /*google.maps.event.addListener(map, 'click', function(event) {
            var myLatLng = event.latLng;
            var lat = myLatLng.lat();
            var lng = myLatLng.lng();
            alert(lat+","+lng);

            placeMarker(myLatLng, map);
        });*/ 

        map.addListener('click', function(event) {
            var myLatLng = event.latLng;
            var lat = myLatLng.lat();
            var lng = myLatLng.lng();
            //alert(lat+","+lng);
            deleteMarkers();
            addMarker(event.latLng,lat,lng);
        });
        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
          for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
          }
        }
        function clearMarkers() {
          setMapOnAll(null);
        }
        function deleteMarkers() {
          clearMarkers();
          markers = [];
        }
        function addMarker(location,lat,lng) {
          var marker = new google.maps.Marker({
            position: location,
            map: map,
            draggable: true/*,
            animation: google.maps.Animation.DROP,
            anchorPoint: new google.maps.Point(0, -29)*/
          });
          fillTxtField( lat,lng );

          var infowindow = new google.maps.InfoWindow();
          infowindow.setContent('<div><strong>' + lat +","+lng+ '</strong><p style="text-align:center;"><a class="btn btn-default" onclick="fillTxtField('+lat+','+lng+')">Recupérer ces coordonnées</a></p>' );
          infowindow.open(map, marker);
          google.maps.event.addListener(marker,'dragstart',function(event) 
          {
                console.log('dragstart');
                infowindow.close();
          });
          google.maps.event.addListener(marker,'dragend',function(event) 
            {

                //document.getElementById('lat').value =event.latLng.lat();
                //document.getElementById('lng').value =event.latLng.lng();
                 //alert(event.latLng.lat()+","+event.latLng.lng());
                 infowindow.setContent('<div><strong>' + event.latLng.lat()+","+event.latLng.lng() + '</strong><p style="text-align:center;"><a class="btn btn-default" onclick="fillTxtField('+event.latLng.lat()+','+event.latLng.lng()+')">Recupérer ces coordonnées</a></p>' );
                infowindow.open(map, marker);
                //fillTxtField( event.latLng.lat(),event.latLng.lng() );
            });
          google.maps.event.addListener(marker,'click',function(event) 
            {
                
                infowindow.setContent('<div><strong>' + event.latLng.lat()+","+event.latLng.lng()  + '</strong><p style="text-align:center;"><a class="btn btn-default" onclick="fillTxtField('+event.latLng.lat()+','+event.latLng.lng()+')">Recupérer ces coordonnées</a></p>' );
                infowindow.open(map, marker);
                //fillTxtField( event.latLng.lat(),event.latLng.lng() );  
            });

          markers.push(marker);
        }

        function placeMarker(position, map) {
            var marker = new google.maps.Marker({
              position: position,
              map: map
            });  
            map.panTo(position);
        }


         

        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            //marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
              window.alert("Autocomplete's returned place contains no geometry");
              return;
            }

            //alert(place.geometry.location.lat()+","+place.geometry.location.lng());

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
              map.fitBounds(place.geometry.viewport);
            } else {
              map.setCenter(place.geometry.location);
              map.setZoom(17);  // Why 17? Because it looks good.
            }
            /*marker.setIcon(({
              url: place.icon,
              size: new google.maps.Size(71, 71),
              origin: new google.maps.Point(0, 0),
              anchor: new google.maps.Point(17, 34),
              scaledSize: new google.maps.Size(35, 35)
            }));*/
            /*marker.setPosition(place.geometry.location);
            marker.setVisible(true);*/

            addMarker(place.geometry.location,place.geometry.location.lat(),place.geometry.location.lng());


            var address = '';
            var componentForm = {
              street_number: 'short_name',
              route: 'long_name',
              locality: 'long_name',
              administrative_area_level_1: 'short_name',
              country: 'long_name',
              postal_code: 'short_name'
            };
            if (place.address_components) {
              address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
              ].join(' ');
            }
            var 
            street_number
            ,route 
            ,locality
            ,administrative_area_level_1 
            ,country
            ,postal_code 
            ,administrative_area_level_2
            ,administrative_area_level_3
            ,administrative_area_level_4
            ,administrative_area_level_5
            ,colloquial_area
            ,ward
            ,sublocality
            ,neighborhood
            ,premise
            ,subpremise
            ,natural_feature
            ,point_of_interest
             = '';

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    //document.getElementById(addressType).value = val;
                    console.log(addressType+' = '+val);
                    
                    if ( addressType == 'street_number' )
                        street_number = (val!='undefined'? val : '');

                    if ( addressType == 'route' )
                        route = (val!='undefined'? val : '');

                    if ( addressType == 'locality' )
                        locality = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_1' )
                        administrative_area_level_1 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_2' )
                        administrative_area_level_2 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_3' )
                        administrative_area_level_3 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_4' )
                        administrative_area_level_2 = (val!='undefined'? val : '');

                    if ( addressType == 'administrative_area_level_5' )
                        administrative_area_level_5 = (val!='undefined'? val : '');

                    if ( addressType == 'country' )
                        country = (val!='undefined'? val : '');

                    if ( addressType == 'postal_code' )
                        postal_code = (val!='undefined'? val : '');



                }
              }
            //alert(document.getElementById('mdts_homebundle_eventlieu_address').value+" - "+address);
            if ( address != '' ) {
                
                //Ne rien faire Si Mada
                if ('madagascar' === country.toLowerCase() )
                    return;

                if (document.getElementById('mdts_homebundle_eventlieu_address') === null) {
                    return;
                }
                
                address = (street_number>0?street_number:'')+(street_number>0?' ':'')+(route!='undefined'?route:'')+(route!=''?' ':'')+(postal_code!='undefined'?postal_code:'')+(postal_code!=''?' ':'')+(locality!='undefined'?locality:'')+(locality!=''?' - ':'')+(administrative_area_level_1!='undefined'?administrative_area_level_1:'')+(administrative_area_level_1!=''?' - ':'')+(country!='undefined'?country:'');
                document.getElementById('mdts_homebundle_eventlieu_address').value = address;

                //ajax save
                var params = "country="+country+"&region="+administrative_area_level_1+"&locality="+locality;
                genericAjax("/rest/savealls","POST",params,"callbackSaveAdress")


                
                
            }

            /*infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
            infowindow.open(map, marker);*/
        });

}

function hide(selector)
{
    if ( selector.length>0 ) {
        [].forEach.call(selector, function(el) {
            el.classList.add('hidden');
        });
    }
}

function show(selector)
{
    if ( selector.length>0 ) {
        [].forEach.call(selector, function(el) {
            el.classList.remove('hidden');
        });
    }
}

function callClickShowrangedate()
{
    hide(selector_divDataResetdate);
    hide(selector_divDataMultiplesdate);
    show(selector_divDataRangedate);

    setTimeout(function(){
        calRange.toggle();
    },300);

    if (null!==opt_dateclair) {
        opt_dateclair.value = 'rangedate';
    }


};

function callEventChangeSelectpagination(elt)
{
    if(parseInt(elt.target.value)<=0)
        return;

    if (null==document.getElementById('form_limit'))
        return;


    document.getElementById('form_limit').value = parseInt(elt.target.value);

    // Valider form contenant form_limit
    document.getElementById('form_limit').closest('form').submit();


}

function callEventClickBtnupload(elt)
{

    let blockinput = elt.target.closest('.file-input') ;
    if (null==blockinput)
        return;

    let fileinput = blockinput.querySelector('input[type=file]') ;
    if (null==fileinput)
        return;

    let fileinputDiv = fileinput.closest('div') ;
    if (null==fileinputDiv)
        return;

    let fileinputLabel = fileinputDiv.querySelector('label') ;
    if (null==fileinputLabel)
        return;

    fileinputLabel.click();

}

function callClickShowresetdate()
{
    hide(selector_divDataRangedate);
    hide(selector_divDataMultiplesdate);
    show(selector_divDataResetdate);

    setTimeout(function(){
        calReset.toggle();
    },300);

    if (null!==opt_dateclair) {
        opt_dateclair.value = '';
    }



};

function callClickShowmultiplesdate()
{
    hide(selector_divDataRangedate);
    hide(selector_divDataResetdate);
    show(selector_divDataMultiplesdate);

    setTimeout(function(){
        calMultiple.toggle();
    },300);

    if (null!==opt_dateclair) {
        opt_dateclair.value = 'multiplesdate';
    }

};


function triggerEvent(type,el){
    if ('createEvent' in document) {
        // modern browsers, IE9+
        var e = document.createEvent('HTMLEvents');
        e.initEvent(type, false, true);
        el.dispatchEvent(e);
    } else {
        // IE 8
        var e = document.createEventObject();
        e.eventType = type;
        el.fireEvent('on'+e.eventType, e);
    }
}

function fltriggerEvent(event, data) {
    if (self.config["on" + event]) {
        const hooks = Array.isArray(self.config["on" + event])
            ? self.config["on" + event]
            : [self.config["on" + event]];

        for (var i = 0; i < hooks.length; i++)
            hooks[i](self.selectedDates, self.input.value, self, data);
    }

    if (event === "Change") {
        if (typeof Event === "function" && Event.constructor) {
            self.input.dispatchEvent(new Event("change", { "bubbles": true }));

            // many front-end frameworks bind to the input event
            self.input.dispatchEvent(new Event("input", { "bubbles": true }));
        }

        else {
            if (window.document.createEvent !== undefined)
                return self.input.dispatchEvent(self.changeEvent);

            self.input.fireEvent("onchange");
        }
    }
}


function setRemoteSearchPaysRegionCommuneQuartier() {

    //document.getElementById('eventsearch').classList.toggle('hidden');

    //Typehead
    if ( selector_Typeahead_PRCQ.length ) {



        var bHound = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace
            ,limit:100
            //,remote: '/rest/event?name=%QUERY'
            ,remote: {
                url: ajax_url_lieu+'?nolieu=1&name=%QUERY',
                wildcard: '%QUERY'
            }
        });

        bHound.initialize();

        var templatehandle = '{{#list quartier}}{{name}} {{/list}}';
        selector_Typeahead_PRCQ.typeahead(null, {
            name: 'bHound',
            displayKey: 'name',
            source: bHound.ttAdapter(),
            templates: {
                empty: [
                    '<div class="empty-message">Non trouvé.</div>'
                ].join('\n')
                ,suggestion: function (data) {

                    return afficherResultat(data);

                    // return '<p><strong>' + data.name + '</strong> typeof region: '+typeof data.region+' - typeof locality: '+typeof data.locality+' - typeof country : '+typeof data.country+' </p>';
                }
                ,pending: function(data) {
                    return '<div class="empty-message">Recherche en cours de '+data.query+'...</div>'
                }

            }
        });

        selector_Typeahead_PRCQ.bind('typeahead:selected', function(obj, data, name) {
            fillSelectPaysRegionCommuneQuartier(data);
        });
    }





}
function afficherResultatTypeQuartier(data) {

    // return '<p>Quartier : <strong>' + data.name + '</strong> typeof region: '+typeof data.region+' - typeof locality: '+typeof data.locality+' - typeof country : '+typeof data.country+' </p>';
    let html = '';

    let commune = data.locality.name;
    let region = data.locality.region.name;
    let country = data.locality.region.country.name;

    html  = '<p style="padding:5px;cursor:pointer;">Quartier : <strong>'+data.name+'</strong><em> - Commune : <strong>'+commune+'</strong> - Region : <strong>'+region+'</strong> - Pays : <strong>'+country+'</strong></em></p>';

    return html;
}
function afficherResultatTypeCommune(data) {
    // return '<p>Commune : <strong>' + data.name + '</strong> typeof region: '+typeof data.region+' - typeof locality: '+typeof data.locality+' - typeof country : '+typeof data.country+' </p>';
    let html = '';
    let region = data.region.name;
    let country = data.region.country.name;

    html  = '<p style="padding:5px;cursor:pointer;">Commune : <strong>'+data.name+'</strong><em> - Region : <strong>'+region+'</strong> - Pays : <strong>'+country+'</strong></em></p>';

    return html;
}
function afficherResultatTypeRegion(data) {
    // return '<p>Région : <strong>' + data.name + '</strong> typeof region: '+typeof data.region+' - typeof locality: '+typeof data.locality+' - typeof country : '+typeof data.country+' </p>';
    let html = '';

    let country = data.country.name;

    html  = '<p style="padding:5px;cursor:pointer;">Region : <strong>'+data.name+'</strong><em> - Pays : <strong>'+country+'</strong></em></p>';

    return html;
}
function afficherResultatTypePays(data) {
    // return '<p>Pays : <strong>' + data.name + '</strong> typeof region: '+typeof data.region+' - typeof locality: '+typeof data.locality+' - typeof country : '+typeof data.country+' </p>';
    var html = '';

    html  = '<p style="padding:5px;cursor:pointer;">Pays : <strong>'+data.name+'</strong></p>';

    return html;
}
function afficherResultat(data)
{
    // return '<p><strong>' + data.name + '</strong> typeof region: '+typeof data.region+' - typeof locality: '+typeof data.locality+' - typeof country : '+typeof data.country+' </p>';
    // Si c'est un pays
    if ( isResultPaysType(data)) {
        return afficherResultatTypePays(data);
    }

    // Si c'est une région
    if ( isResultRegionType(data)) {
        return afficherResultatTypeRegion(data);
    }

    // Si c'est une commune
    if ( isResultCommuneType(data)) {
        return afficherResultatTypeCommune(data);
    }

    // Si c'est un quartier
    if ( isResultQuartierType(data)) {
        return afficherResultatTypeQuartier(data);
    }

    return false;
}

function isResultQuartierType(data)
{

    let typeofregion = typeof data.region;
    let typeoflocality = typeof data.locality;
    let typeofcountry = typeof data.country;


    // Le type est un Quartier
    if ('undefined' == typeofregion && 'undefined' !== typeoflocality && 'undefined' == typeofcountry ) {
        return true;
    }

    return false;
}

function isResultCommuneType(data)
{

    let typeofregion = typeof data.region;
    let typeoflocality = typeof data.locality;
    let typeofcountry = typeof data.country;


    // Le type est un Commune
    if ('undefined' !== typeofregion && 'undefined' == typeoflocality && 'undefined' == typeofcountry ) {
        return true;
    }

    return false;
}

function isResultRegionType(data)
{

    let typeofregion = typeof data.region;
    let typeoflocality = typeof data.locality;
    let typeofcountry = typeof data.country;


    // Le type est une Region
    if ('undefined' == typeofregion && 'undefined' == typeoflocality && 'undefined' !== typeofcountry ) {
        return true;
    }

    return false;
}

function isResultPaysType(data)
{

    let typeofregion = typeof data.region;
    let typeoflocality = typeof data.locality;
    let typeofcountry = typeof data.country;


    // Le type est un pays
    if ('undefined' == typeofregion && 'undefined' == typeoflocality && 'undefined' == typeofcountry ) {
        return true;
    }

    return false;
}
function fillSelectPaysRegionCommuneQuartier (data)
{
    // Vider le champ de recherche
    selector_Typeahead_PRCQ.val('');

    // Remplir le <select pays>
    if ( isResultPaysType(data)) {
        fillSelectPays(data);
        return;
    }

    // Remplir le <select region>
    if ( isResultRegionType(data)) {
        fillSelectRegion(data);
        return;
    }

    // Si c'est une commune
    if ( isResultCommuneType(data)) {
        fillSelectCommune(data);
        return;
    }

    // Si c'est un quartier
    if ( isResultQuartierType(data)) {
        fillSelectQuartier(data);
        return;
    }

    return ;
}
function fillSelectPays(data)
{
    removeChildById("mdts_homebundle_eventlieu_country");
    addOptionsToSelect("mdts_homebundle_eventlieu_country",data);

    removeChildByIdAndTriggerChange('mdts_homebundle_eventlieu_region');
    removeChildByIdAndTriggerChange('mdts_homebundle_eventlieu_locality');
    removeChildByIdAndTriggerChange('mdts_homebundle_eventlieu_quartier');
}

function fillSelectRegion(data)
{
    fillSelectPays(data.country);

    removeChildById('mdts_homebundle_eventlieu_region');
    addOptionsToSelect("mdts_homebundle_eventlieu_region",data);

    removeChildByIdAndTriggerChange('mdts_homebundle_eventlieu_locality');
    removeChildByIdAndTriggerChange('mdts_homebundle_eventlieu_quartier');
}
function fillSelectCommune(data)
{
    fillSelectRegion(data.region);

    removeChildById('mdts_homebundle_eventlieu_locality');
    addOptionsToSelect("mdts_homebundle_eventlieu_locality",data);

    removeChildByIdAndTriggerChange('mdts_homebundle_eventlieu_quartier');
}
function fillSelectQuartier(data)
{
    fillSelectCommune(data.locality);

    removeChildById('mdts_homebundle_eventlieu_quartier');
    addOptionsToSelect("mdts_homebundle_eventlieu_quartier",data);
}
function removeChildById(id)
{
    let myNode = document.getElementById(id);
    while (myNode.firstChild) {
        myNode.removeChild(myNode.firstChild);
    }
    return true;
}

function addOptionsToSelect(id,data)
{
    let child = createElement('option', {value: data.id, selected:'selected'},data.name);

    document.getElementById(id).appendChild(child);

    $('#'+id).trigger('change');
}

function removeChildByIdAndTriggerChange(id)
{
    removeChildById(id);
    $('#'+id).trigger('change');
}

/**
 * Alerte generique via bootbox
 *
 * @param string msg
 */
function alertGenerique(msg)
{
    let txt = '';
    let type = '';

    let elemSwal = {
        title: msg
    };

    if ( typeof arguments[1] != 'undefined' )
        elemSwal.txt = arguments[1];

    if ( typeof arguments[2] != 'undefined' )
        elemSwal.type = arguments[2];

    if ( typeof arguments[3] != 'undefined' )
        elemSwal.timer = arguments[3];


    swal( elemSwal );
}

/**
 * Confirm generique via bootbox
 *
 * @param string msg - Message de confirmation
 * @param function cbf - Fonction callback
 */
function confirmGenerique(msg, cbf)
{
    if ('function' !== typeof cbf)
        return;


    // bootbox.confirm(msg, cbf);
    swal({
        title: msg ,
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Oui",
        cancelButtonText: "Non",
    }).then(cbf);
}

/**
 * Prompt generique via bootbox
 *
 * @param string msg - Message de confirmation
 * @param function cbf - Fonction callback
 */
function promptGenerique(titlebox, valueTxt, cbf)
{
    /*console.log(typeof cbf);
    console.log(typeof valueTxt);
    console.log(typeof titlebox);*/


    if ('function' !== typeof cbf ||  'string'!==typeof titlebox)
        return;


    /*bootbox.prompt({
        title: titlebox,
        value: valueTxt,
        callback: cbf
    });*/

    swal({
        title: titlebox,
        input: "text",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        inputValue: valueTxt,
        confirmButtonText: "Enregistrer",
        cancelButtonText: "Annuler",
        showLoaderOnConfirm: true,
        allowOutsideClick: false
    }).then(cbf);
}

/**
 * Alerte generique via bootbox
 *
 * @param string msg - Message de confirmation
 * @param function cbf - Fonction callback
 */
function dialogGenerique(titlebox, htmlContent )
{
    if ( 'string' !== typeof htmlContent || 'string'!==typeof titlebox)
        return;

    let cbf = function(){};
    let cbfPromise = function(){};
    let cbfonOpen = function(){};
    let confirmButtonText = '';

    if ( typeof arguments[2] != 'undefined' ) {
        confirmButtonText = arguments[2];
    }

    if ( typeof arguments[3] != 'undefined' ) {
        cbf = arguments[3];
    }
    if ( typeof arguments[4] != 'undefined' ) {
        cbfPromise = arguments[4];
    }
    if ( typeof arguments[5] != 'undefined' ) {
        cbfonOpen = arguments[5];
    }

    /*bootbox.dialog({
        title: titlebox,
        message: htmlContent,
        buttons: button
    });*/

    var elemSwal = {
        title: titlebox,
        html:  htmlContent,
        preConfirm: function () {
            return new Promise(cbfPromise)
        },
        onOpen: cbfonOpen
    };

    if (''!=confirmButtonText) {
        elemSwal.confirmButtonText = confirmButtonText;
    }
    swal(elemSwal).then(cbf).catch(swal.noop)
}

/**
 * Appeler une URL Ajax
 *
 * @param string url - URL à appeler
 * @param string method - Methode à utiliser GET (par défaut), POST, FILE (fichier / POST)
 * @param string params - Paramètre en method POST ou FILE
 * @param function cbfReadyState - Fonction appelée apres readyState
 * @param object paramplus - Paramètres supplémentaires
 *
 * @return mixed
 *
 */
function genericAjax(url)
{
    let method = 'get';

    if ( typeof arguments[1] != 'undefined' ) {
        method = arguments[1];
    }

    method = method.toUpperCase();
    let origmethod = method;

    if ('FILE' == origmethod )
        method = 'POST';

    let cbfReadyState = '';
    let paramplus = null;
    let params = null;

    let r = new XMLHttpRequest();

    if ( typeof arguments[2] != 'undefined' ) {
        params = arguments[2];
    }

    if ( typeof arguments[3] != 'undefined' ) {
        cbfReadyState = arguments[3];
    }

    if ( typeof arguments[4] != 'undefined' ) {
        paramplus=arguments[4];
        //console.log(paramplus.domElement.parentNode.innerHTML);
        //alert(paramplus.domElement.parentNode.innerHTML);
    }

    r.open(method, url, true);

    // alert(typeof cbfReadyState);

    if ('string'==typeof cbfReadyState) {
        r.onreadystatechange = function () {
            console.log('ajax readyState : '+r.readyState+' - ajax status : '+r.status);
            if (r.readyState != 4 || r.status != 200) return;
            var fn = window[cbfReadyState];
            console.log('typeof fn : '+typeof fn+' - cbfReadyState: '+cbfReadyState);
            let arg = [r.responseText];
            let objparamplus = {};

            if (paramplus!=null && typeof paramplus.fnParseData!='undefined') {
                objparamplus.fnParseData = paramplus.fnParseData;
            }
            if (paramplus!=null && typeof paramplus.domElement!='undefined') {
                objparamplus.domElement = paramplus.domElement;
            }
            if (paramplus!=null && typeof paramplus.fnCallback!='undefined') {
                objparamplus.fnCallback = paramplus.fnCallback;
            }
            if (paramplus!=null && typeof paramplus.fnTriggerSelect2!='undefined') {
                objparamplus.fnTriggerSelect2 = paramplus.fnTriggerSelect2;
            }
            if (paramplus!=null && typeof paramplus.el!='undefined') {
                objparamplus.el = paramplus.el;
            }

            if (paramplus!=null) {
                arg.push(objparamplus);
            }

            if ('function' == typeof fn)
                fn.apply(null,arg);
        };
    }

    //POST Request
    if (origmethod == 'POST') {
        r.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // r.setRequestHeader("Content-length", params.length);
        // r.setRequestHeader("Connection", "close");
    }



    if (method=='GET') {
        params = null;
    }
    r.send(params);
}
function setOnSubmitFormEventMember()
{
    if ('function'!=typeof checkSendAuthor)
        return;

    addEvent( document.querySelector(dom_formEvent_member), 'submit', function(evt) {
        checkSendAuthor();
    });
}
function setOnSubmitFormIds()
{
    if ('function'!=typeof checkIdsForm)
        return;

    addEvent( document.querySelector(dom_form_ids), 'submit', function(evt) {
        checkIdsForm();
    });
}
function checkSendAuthor()
{

    let oldEventId = document.getElementById('mdts_frontendbundle_eventbymember_eventunmapped').value;
    let newEventId = $('#select_mdts_homebundle_event').val();

    if (newEventId==null)
        return false;

    if (oldEventId==newEventId)
        return false;

    document.getElementById('mdts_frontendbundle_eventbymember_eventunmapped').value =  newEventId ;



}

function checkIdsForm()
{
    let arrIds = [];

    // DOm du formulaire
     let formQuerySelector = document.querySelector("form[name=mdts_homebundle_all]");

     // Dom du champ hidden mdts_homebundle_all[ids]
     let hiddenfieldAllids = document.getElementById("mdts_homebundle_all_ids");

     if (null==formQuerySelector)
         return;

     // DOm de tous les checkbox
     let checkboxInForm = formQuerySelector.querySelectorAll('input[data-chkid]');

     if (null==checkboxInForm)
         return;

     // Parcourir les checkbox, si cochés, remplir le champ hidden par la valeur de la case cochée
    [].forEach.call(checkboxInForm, function(el) {


         let idval = el.getAttribute('data-chkid');
         // alert(idval+' - '+el.checked);

         if (idval!='' && parseInt(idval) && el.checked)
             arrIds.push(idval);
    });

    if (null==hiddenfieldAllids)
        return;

    if (arrIds.length>0)
        hiddenfieldAllids.value = arrIds.join(',');

    // alert(arrIds.join(', '));

    // alert(hiddenfieldAllids.value);
}

function cbAtRemoteLieu(query, callback)
{
    if(query.length>=2) {
        $.getJSON(ajax_url_lieu , {name: query}, function(data) {
            //console.log( data.lieu[0].country );

            var d = [];
            var din = null;

            for (var i = 0; i < data.lieu.length; i++) {

                var item = data.lieu[i];

                din = {};
                din.id = (typeof item != 'undefined' ? item.id : '');
                din.name = (typeof item != 'undefined' ? item.name : '');
                din.country = (typeof item != 'undefined' && typeof item.country != 'undefined' ? item.country.name : '');
                din.region = (typeof item != 'undefined' && typeof item.region != 'undefined' ? item.region.name : '');
                din.locality = (typeof item != 'undefined' && typeof item.locality != 'undefined' ? item.locality.name : '');
                din.quartier = (typeof item != 'undefined' && typeof item.quartier != 'undefined' ? item.quartier.name : '');

                d.push(din);
            }
            console.log(d);
            // console.log(data.lieu);


            callback( d );
            //  callback( data.lieu );
        });
    }

}

function cbAtRemoteArtists(query, callback)
{
    if(query.length>=2) {
        $.getJSON(ajax_url_Artiste , {name: query}, function(data) {
            callback(data.results);
        });
    }

}

function createElement(tagName, attributes)
{
    let d = document.createElement(tagName);

    for (var prop in attributes) {
        d.setAttribute(prop, attributes[prop]);
    }

    let html = null;

    if ( typeof arguments[2] != 'undefined' ) {
        html = arguments[2];
        d.innerHTML = html;
    }

    return d;
}

function addNewToOption()
{
    let d = createElement('option', {'value': 'new'}, 'Ajouter une option');
    return d;
}