document.addEventListener('DOMContentLoaded', function() {
     
    var alldata = document.querySelectorAll("[data-gaecategory]");
    [].forEach.call(alldata, function(el) {
          el.addEventListener('click', function() {
            
            var gaaction = window.location.pathname;
            gaaction = gaaction.replace(/\.html/,'');
            gaaction = gaaction.replace(/evenement_/,'');
            if ( gaaction == '/' )
                gaaction = 'accueil';
            else 
                gaaction = gaaction.substring(0,50);


            var gacategory = el.getAttribute('data-gaecategory');
            var gaelabel = el.getAttribute('data-gaelabel');

             _paq.push(['trackEvent', gacategory, gaaction, gaelabel]);

          });
    });
});