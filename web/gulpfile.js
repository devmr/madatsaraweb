var gulp = require('gulp');

// Import dependencies
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var plumber = require('gulp-plumber');
var minifycss = require("gulp-minify-css");
var concat = require("gulp-concat");
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');


gulp.task('imagemin', () => {
    return gulp.src('assets/flyers/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('assets/flyers'));
});

gulp.task('styles', function() {

    // On recupere les fichiers a traiter
    gulp.src([
        //'bower_components/Materialize/dist/css/materialize.min.css',
        'bower_components/bootstrap/dist/css/bootstrap.min.css',
        'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        'bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css',
        'bower_components/bootstrap-select/dist/css/bootstrap-select.min.css',
        'bower_components/Yamm3/yamm/yamm.css',
        'bower_components/bootstrap-social/bootstrap-social.css',
        'bower_components/font-awesome/css/font-awesome.min.css',
        'bower_components/map-icons/dist/css/map-icons.min.css',
        'dev/css/plugin/*.css',
        'dev/css/*.css'
    ])  
    // On lance Plumber pour eviter que ca pete
    .pipe(plumber())
    
    .pipe(concat('styles.css'))

    //minify
    .pipe(minifycss())

        // On balance tout ca dans notre dossier /css/
    .pipe(gulp.dest('assets/css/'))
})

gulp.task('javascripts', function() {

    // On recupere les fichiers a traiter
    gulp.src([
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/moment/min/moment-with-locales.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
        'bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
        'bower_components/jquery_lazyload/jquery.lazyload.js',
        'bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js',
        'bower_components/cookieconsent2/cookieconsent.js',
        'bower_components/gmaps/gmaps.min.js',
        'bower_components/lory/dist/lory.min.js',
        'dev/js/plugin/*.js',
        'dev/js/*.js'
    ])
    // On lance Plumber pour eviter que ca pete
    .pipe(plumber())
    
    .pipe(concat('scripts.js'))

    //minify
    .pipe(uglify())

        // On balance tout ca dans notre dossier /css/
    .pipe(gulp.dest('assets/js/'))
})

// Fonts
gulp.task('fontsmaterial', function() {
    return gulp.src([
                    'bower_components/Materialize/font/material-design-icons/*'])
            .pipe(gulp.dest('assets/font/material-design-icons/'));
});

gulp.task('fontsroboto', function() {
    return gulp.src([
                    'bower_components/Materialize/font/roboto/*'])
            .pipe(gulp.dest('assets/font/roboto/'));
});

gulp.task('fontsbootstrap', function() {
    return gulp.src([
                    'bower_components/bootstrap/dist/fonts/*'
                    ,'bower_components/font-awesome/fonts/*'
                ])
            .pipe(gulp.dest('assets/fonts/'));
});

gulp.task('fontsmapicons', function() {
    return gulp.src([
                    'bower_components/map-icons/dist/fonts/*' 
                ])
            .pipe(gulp.dest('assets/fonts/'));
});

// On construit notre tache par defaut
//gulp.task('default', ['styles', 'javascripts', 'fontsmaterial', 'fontsroboto' ], function() {})
gulp.task('default', ['styles', 'javascripts'  ], function() {})
gulp.task('js', [ 'javascripts'  ], function() {})
gulp.task('css', ['styles'  ], function() {})
gulp.task('image', ['imagemin'  ], function() {})
gulp.task('fonts', ['fontsbootstrap', 'fontsmapicons'  ], function() {})
gulp.task('defaultfont', ['styles', 'javascripts', 'fontsbootstrap', 'fontsmapicons'  ], function() {})
gulp.task('all', ['styles', 'javascripts', 'fontsbootstrap', 'imagemin' ], function() {})

//gulp.task('watch', ['styles', 'javascripts', 'fontsmaterial', 'fontsroboto'], function() {
gulp.task('watch', ['styles', 'javascripts'  ], function() {
    // On lance nos watch pour surveiller nos dossiers
    gulp.watch('dev/css/*', ['styles']) 
    gulp.watch([
        'dev/js/plugin/*.js',
        'dev/js/*.js'        
    ], ['javascripts']) 
    
})

gulp.task('watchall', ['styles', 'javascripts', 'fontsbootstrap', 'imagemin'], function() {
    // On lance nos watch pour surveiller nos dossiers
    gulp.watch('dev/css/*', ['styles']) 
    gulp.watch([
        'dev/js/plugin/*.js',
        'dev/js/*.js'        
    ], ['javascripts'])  
    gulp.watch('media/cache/my_thumb/assets/flyers/*', ['imagemin'])      
})