<?php
/**
 * Created by PhpStorm.
 * User: Miary
 * Date: 30/12/2018
 * Time: 00:30
 */

require __DIR__ . '/../../vendor/autoload.php';

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

$process = new Process(array('whereis', 'git'));
$process->run();

if (!$process->isSuccessful()) {
    throw new ProcessFailedException($process);
}

echo $process->getOutput();