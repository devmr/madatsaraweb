<?php
error_reporting(0);
setlocale(LC_ALL, 'en_US.UTF8');

require __DIR__.'/ElasticSearch.php';

use Elasticsearch\ClientBuilder;
$client = ClientBuilder::create()->build();

Header('Content-Type: text/html; charset=utf-8');

$bdd = 'mts_w4';
$elastic_index = 'madatsara';
$elastic_type = 'event';
if ('dev.madatsara.com' == $_SERVER['HTTP_HOST']) {
    $bdd = 'mts_devw4';
    $elastic_index = 'madatsara_dev';
}
/*var_dump($bdd);
var_dump($elastic_index);exit;*/
$dns = 'mysql:host=localhost;charset=utf8';
$user = 'madatsara';
$mdp = '!4xntmj60!';

$elasticS = new ElasticS\Es\ElasticSearch($elastic_index, $elastic_type);

try {
     $connection = new PDO( $dns, $user, $mdp );
    //echo 'Connexion OK';
} catch ( Exception $e ) {
  echo "Connection à MySQL impossible : ", $e->getMessage();
  die();
}
    
    
    $sql = "SELECT id,heure_fin_aube,slug,flyer,name,description,contact_event,CASE WHEN date_unique = '0000-00-00' THEN date_debut ELSE date_unique END as date_unique,heure_debut,heure_fin,CASE WHEN date_debut = '0000-00-00' THEN date_unique ELSE date_debut END as date_debut,CASE WHEN date_fin = '0000-00-00' THEN date_unique ELSE date_fin END as date_fin 
    FROM `".$bdd."`.`event`  
WHERE hidden = 0 AND deletedAt IS NULL";
    
    $sql .= " ORDER BY  `id` DESC";

    
    $res = $connection->prepare($sql);
    $res->execute();
    $row = $res->fetchAll(PDO::FETCH_ASSOC);
    echo count($row)." lignes \n<br >";

    if ( count($row) <= 0 )
        exit;

    // Delete index
      $response = $elasticS->deleteIndex();


    // Creer index
      $response = $elasticS->createIndexAutocomplete($elastic_index);


    $params = ['body' => []];
    $i = 0;

    //$slugify = new Slugify();
    $date_array = array();
    foreach($row as $ligne ) {

        echo $i." - Indexation de ".$ligne['name']." (".$ligne['id'].")<br />";

        $params['body'][] = [
            'index' => [
                '_index' => $elastic_index,
                '_type' => $elastic_type,
                '_id' => $ligne['id']
            ]
        ];

        //Enregistrer les dates
        $ligne['dateliste'] = getDates($ligne['id']);
        $params['body'][] = $ligne;

        if ($i % 1000 == 0) {
//            $responses = $client->bulk($params);
            $responses = $elasticS->bulkIndex($params);
            // erase the old bulk request
            $params = ['body' => []];
            // unset the bulk response when you are done to save memory
            //echo print_r($responses, true);
            unset($responses);
        }





        $i++;
        

        
    }
    // Send the last batch if it exists
    if (!empty($params['body'])) {
//        $responses = $client->bulk($params);
        $responses = $elasticS->bulkIndex($params);
    }
    echo "<hr>".$i." elements indéxés \n";





function getDates($id)
{

    global $connection;
    global $bdd;

    $sql = "SELECT a.date FROM ".$bdd.".event_date a LEFT JOIN ".$bdd.".event_by_date b ON b.event_date_id = a.id WHERE b.event_id = ".$id;
    $res = $connection->prepare($sql);
    $res->execute();
    return array_value_recursive('date',$res->fetchAll(PDO::FETCH_ASSOC));

}
function array_value_recursive($key, array $arr){
    $val = array();
    array_walk_recursive($arr, function($v, $k) use($key, &$val){
        if($k == $key) array_push($val, $v);
    });
    return count($val) > 1 ? $val : array_pop($val);
}