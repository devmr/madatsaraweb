<?php
namespace ElasticS\Es;

require __DIR__.'/../vendor/autoload.php';


use Elasticsearch\ClientBuilder;
use Cocur\Slugify\Slugify;

class ElasticSearch
{
    var $client = null;
    var $elastic_index = null;
    var $elastic_type = null;

    public function __construct($elastic_index,$elastic_type)
    {
        $this->client = ClientBuilder::create()->build();
        $this->elastic_index = $elastic_index;
        $this->elastic_type = $elastic_type;
        $this->indices = $this->client->indices();

    }

    public function searchByParams($params)
    {
        $response = $this->client->search($params);
        return $response;
    }
    public function getIdsBysearch($field,$value)
    {
        $ids = [];
        $response = $this->getAllSearchTextinField($field,$value);
//        echo $this->prettyPrintR($response);
        foreach ($response as $hit) {
            array_push($ids, $hit['_id']);
        }

        return $ids;
    }
    public function searchTextinField($field,$value)
    {
        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'body' => [
                'query' => [
                    'match' => [
                        $field => $value
                    ]
                ]
            ]
        ];
        return $this->searchByParams($params);
    }
    public function getId($id)
    {
        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'id' => $id
        ];
        return $this->client->get($params);
    }

    public function getIndexSettings($indexes)
    {
        $params = ['index' => $indexes];
        $response = $this->indices->getSettings($params);
        return $response;
    }

    public function getMapping($index='', $type='')
    {
        $response = [];

        // Get mappings for all indexes and types
        if ($index=='' && $type=='')
            $response = $this->indices->getMapping();

        // Get mappings for all types in 'my_index'
        if ($index!='' && $type=='') {
            $params = ['index' => $index];
            $response = $this->indices->getMapping($params);
        }


        // Get mappings for all types of 'my_type', regardless of index
        if ($index=='' && $type!='') {
            $params = ['type' => $type ];
            $response = $this->indices->getMapping($params);
        }

        // Get mapping 'my_type' in 'my_index'
        if ($index!='' && $type!='') {
            $params = [
                'index' => $index,
                'type' => $type
            ];
            $response = $this->indices->getMapping($params);
        }
        return $response;
    }

    public function getAllQuery(){

        $indices = $this->getAllIndexCustomElastic();
        $data = array();
        if ( count($indices)<=0)
            return false;

        foreach($indices as $index ) {
            $response = $this->client->search(['index' => $index , 'type' => 'custom_elastic' ]);
            $data[] =  $this->getAllValuesByFields('query', $response) ;
        }
        $data_ = array();
        foreach($data as $k => $v) {
            foreach($v as $k=>$vv)
                $data_[] = $vv;
        }
        $data_ = array_unique($data_);
        sort($data_);
        return array_values( $data_ );
    }
    private function getAllIndexCustomElastic()
    {
        $data = array();
        $keys_data = $this->getAllIndices();
        if( count($keys_data)<=0)
            return false;
        foreach( $keys_data as $v ) {
            if (substr($v,0,15) == 'custom_elastic-')
                $data[] = $v;
        }
        return array_reverse( $data );
    }

    private function getAllValuesByFields($field, $response = array()) {
        if ( count($response )<=0 )
            return false;

        $hits = $response['hits'];
        if ( !is_array($hits) || count( $hits )<=0 )
            return false;

        $total = $hits['total'];
        $data = $hits['hits'];

        if ( $total <=0 || count($data)<=0)
            return false;

        $data_val = array();
        foreach($data as $row ) {
            foreach($row['_source'] as $k => $v ) {
                if ($k == $field) {
                    $data_val[] =  $v ;
                }
            }
        }

        return array_unique(array_values($data_val));
    }

    public function getAllIndices()
    {
        $keys_data = array_keys( $this->indices->stats()['indices'] );
        return $keys_data;
    }


    public function indexDocument($id,$arrFields)
    {
        $response = [];
        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'id' => $id,
            'body' => $arrFields
        ];

        // Document will be indexed to my_index/my_type/my_id
        $response = $this->client->index($params);
        return $response;
    }



    public function deleteIndex($indexName='')
    {
        $response = [];
        if ($indexName=='') {
            $indexName = $this->elastic_index;
        }

        $params = [
            'index' => $indexName
        ];
        /*var_dump($indexName);
        var_dump($this->isIndexExists($indexName));*/
        if ($this->isIndexExists($indexName))
            $response = $this->indices->delete($params);



        return $response;
    }

    public function isIndexExists($indexName='')
    {
        if ($indexName=='') {
            $indexName = $this->elastic_index;
        }

        $params = [
            'index' => $indexName
        ];
        return $this->indices->exists($params);
    }

    public function createIndexAutocomplete($indexName)
    {
        /*$arrSettings = [
            'analysis' => [
                'filter' => [
                    'title_ngram' => [
                        'type' => 'nGram',
                        'min_gram' => 3,
                        'max_gram' => 5
                    ]
                ],
                'analyzer' => [
                    'autocomplete_analyzer' => [
                        'filter' => [
                            'asciifolding',
                            'title_ngram'
                        ],
                        'type' => 'custom',
                        'tokenizer' => 'lowercase',
                    ]
                ] ,
            ],
            'number_of_shards' => 5,
            'number_of_replicas' => 1
        ];*/

        $arrSettings = [
            'analysis' => [

                'analyzer' => [
                    'custom_analyzer' => [
                        'filter' => [
                            'stopwords'
                            ,'asciifolding'
                            ,'lowercase'
                            ,'snowball'
                            ,'elision'
                            ,'worddelimiter'
                        ],
                        'type' => 'custom',
                        'tokenizer' => 'nGram',
                    ]
                    ,'custom_search_analyzer' => [
                        'filter' => [
                            'stopwords'
                            ,'asciifolding'
                            ,'lowercase'
                            ,'snowball'
                            ,'elision'
                            ,'worddelimiter'
                        ],
                        'type' => 'custom',
                        'tokenizer' => 'standard',
                    ]
                ]
                ,'tokenizer' => [
                    'nGram' => [
                        'type' => 'nGram',
                        'min_gram' => 2,
                        'max_gram' => 20,
                    ]
                ]
                ,'filter' => [
                    'snowball' => [
                        'type' => 'snowball',
                        'language' => 'French'
                    ]
                    ,'elision' => [
                        'type' => 'elision',
                        'articles' => [
                            'l'
                            ,'m'
                            ,'t'
                            ,'qu'
                            ,'n'
                            ,'s'
                            ,'j'
                            ,'d'
                        ]
                    ]
                    ,'stopwords' => [
                        'type' => 'stop',
                        'stopwords' => [
                            '_french_'
                        ]
                        ,'ignore_case' => true
                    ]
                    ,'worddelimiter' => [
                        'type' => 'word_delimiter'
                    ]
                ],
            ],
            'number_of_shards' => 5,
            'number_of_replicas' => 1
        ];


        /*$arrMappings = [
            $this->elastic_type => [
                'properties' => [
                    'name' => [
                        'type' => 'string',
                        'index_analyzer' => 'autocomplete_analyzer',
                        'boost' => 10
                    ]
                ]
            ]
        ];*/

        $arrMappings = [
            $this->elastic_type => [
                'properties' => [
                    'name' => [
                        'type' => 'string',
                        'index_analyzer' => 'custom_analyzer',
                        'search_analyzer' => 'custom_search_analyzer',
                        'boost' => 6
                    ]
                ]
            ]
        ];

        $params = [
            'index' => $indexName,
            'body' => [
                'settings' => $arrSettings
                ,'mappings' => $arrMappings
                ]
            ];

        $response = $this->indices->create($params);
        return $response;
    }

    public function prettyPrintR($arr)
    {
        $html = '<pre>';
        $html .= print_r($arr,true);
        $html .= '</pre>';

        echo $html;
    }

    public function bulkIndex($params)
    {
        $this->client->bulk($params);
    }

    public function getAllSearchTextinField($field, $value, $fieldReturn=[])
    {
        $params =  [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'body' => [
                'query' => [
                    'match' => [
                        $field => $value
                    ]
                ],
                'sort' => [
                    'date_unique' => [
                        'order' => 'asc',
                    ],
                ]
            ]
        ];

        $response = $this->searchTextinField($field,$value);

        $total = $response['hits']['total'];

        if ($total>10) {
            $params['from'] = 0;
            $params['size'] = $total;
        }


        $response = $this->searchByParams($params);

        $ids = [];
        $hits = $response['hits'];
        $response = $hits['hits'];
        //echo $this->prettyPrintR($response);
        if (count($fieldReturn)<=0)
            return $response;

        $field = [];
        foreach ($response as $row) {
            if (array_key_exists('_source', $row) ) {
                $field[] = $this->arrayPush($field, $row['_source'], $fieldReturn);
            }
        }
        return $field;

    }

    private function arrayPush($fieldOrig, $rowSrc, $fieldReturn)
    {
        $data = [];
        foreach ($fieldReturn as $field ) {
            if (array_key_exists($field, $rowSrc)) {
                $data[$field] =  $rowSrc[$field] ;
            }

        }
        /*var_dump($fieldReturn);
        print_r($rowSrc);exit;*/
        return $data;
    }
}