ALTER TABLE countries ADD slug VARCHAR(255) NOT NULL;
CREATE UNIQUE INDEX UNIQ_5D66EBAD989D9B62 ON countries (slug);
ALTER TABLE locality ADD slug VARCHAR(255) NOT NULL;
CREATE UNIQUE INDEX UNIQ_E1D6B8E6989D9B62 ON locality (slug);
ALTER TABLE quartier ADD slug VARCHAR(255) NOT NULL;
CREATE UNIQUE INDEX UNIQ_FEE8962D989D9B62 ON quartier (slug);
ALTER TABLE region ADD slug VARCHAR(255) NOT NULL;
CREATE UNIQUE INDEX UNIQ_F62F176989D9B62 ON region (slug);
-- UPDATE event_lieu
UPDATE event_lieu SET country_id = 1 WHERE country_id IS NULL AND (
  name LIKE '%fjkm%'
  OR name LIKE '%antan%'
  OR name LIKE '%kianj%'
  OR name LIKE '%anos%'
  OR name LIKE '%amb%'
  OR name LIKE '%ivat%'
  OR name LIKE '%kely%'
  OR name LIKE '%andra%'
  OR name LIKE '%andri%'
  OR name LIKE '%ank%'
  OR name LIKE '%masin%'
  OR name LIKE '%jang%'
  OR name LIKE '%diego%'
  OR name LIKE '%fianar%'
  OR name LIKE '%toliar%'
  OR name LIKE '%zaza%'
  OR name LIKE '%junga%'
  OR name LIKE '%67%'
  OR name LIKE '%maro%'
  OR name LIKE '%loha%'
  OR name LIKE '%fara%'
  OR name LIKE '%rano%'
  OR name LIKE '%itsy%'
  OR name LIKE '%rika%'
  OR name LIKE '%osy%'
  OR name LIKE '%tave%'
  OR name LIKE '%terrain%'
  OR name LIKE '%tala%'
  OR name LIKE '%fene%'
  OR name LIKE 'ant%'
  OR name LIKE 'anj%'
  OR name LIKE '%apa%'
  OR name LIKE '%aza%'
  OR name LIKE '%kara%'
);
