<?php

namespace mdts\homeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
//use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('query',     TextType::class, array('required' => false))
            ->add('date_from',     TextType::class, array('required' => false))
            ->add('date_to',     TextType::class, array('required' => false))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
//    public function setDefaultOptions(OptionsResolverInterface $resolver)
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'mdts\homeBundle\Entity\Search', 'csrf_protection' => false,
        ));
    }


    public function getBlockPrefix()
    {
        return 'mdts_homebundle_search';
    }
}
