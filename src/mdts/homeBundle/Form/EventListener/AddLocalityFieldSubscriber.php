<?php

namespace mdts\homeBundle\Form\EventListener;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use mdts\homeBundle\Entity\locality;
use mdts\homeBundle\Entity\localityRepository;
use mdts\homeBundle\Entity\region;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class AddLocalityFieldSubscriber implements EventSubscriberInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SET_DATA => 'preSetData',
            FormEvents::PRE_SUBMIT => 'preSubmit',
        );
    }
    public function preSetData(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        $region = $data->getRegion() ? $data->getRegion() : null;
        $this->addElements($form, $region);
    }
    public function preSubmit(FormEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if (!$data || !array_key_exists('region', $data)) {
            return;
        }

        $region_id = $data['region'];
        $region = $this->em->getRepository('mdtshomeBundle:region')->find($region_id);

        $this->addElements($form, $region);
    }

    public function addElements(FormInterface $form, region $region = null)
    {
        $form->add('locality', EntityType::class, [

                'placeholder' => 'Choisir la ville',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\locality',
                'choice_label' => 'name', 'query_builder' => function (localityRepository $er) use ($region) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.region', 'c')
                        ->addSelect('c')
                        ->where('c.id = :id')
                        ->setParameter('id', $region)
                        ->orderBy('u.name', 'ASC');
                },
                ]);
    }
}
