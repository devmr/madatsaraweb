<?php

namespace mdts\homeBundle\Form;

use mdts\homeBundle\Form\Type\HiddenDateTimeType;
use mdts\homeBundle\Form\Type\HiddenHeureType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\Extension\Core\View\ChoiceView;
use Vich\UploaderBundle\Form\Type\VichImageType;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Symfony\Component\OptionsResolver\OptionsResolver;

class EventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $oldslug = '';

        $localids = $localartistes = $localprice = $multilieu = $related_event = array();
        $em = $options['er'];

        if ($entity->getId() !== null && $entity->getId() > 0) {
            if (method_exists($entity, 'getEventLieu') && method_exists($entity->getEventLieu(), 'getId')) {
                $multilieu[] = $entity->getEventLieu()->getId();
            }

            $encours = $em->getRepository('mdtshomeBundle:Event')->find($entity->getId());
            foreach ($encours->getEventlocal() as $local) {
                $localids[] = $local->getId();
            }
            foreach ($encours->getEventArtistesDjOrganisateurs() as $local) {
                $localartistes[] = $local->getId();
            }
            foreach ($encours->getEventmultilieu() as $local) {
                $multilieu[] = $local->getId();
            }
            $i = 0;
            foreach ($encours->getEventPrice() as $local) {
                $localprice[$i]['price'] = $local->getPrice();
                $localprice[$i]['detailprice'] = $local->getDetailPrice();
                //$localprice[$i]['devise'] = $local->getDevise()->getId();
            }
            $oldslug = $entity->getSlug();

            foreach ($encours->getEventRelated() as $local) {
                $related_event[] = $local->getId();
            }
             
        }

        //print_r($localprice);

        $builder
            ->add('name')
            ->add('flyerFile', VichImageType::class, array(
                'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_uri' => true, // not mandatory, default is true
            ))
            ->add('slug', TextType::class, array('required' => false))
            ->add('oldslug', HiddenType::class, ['mapped' => false, 'default' => $oldslug])
            //->add('deletedAt')
            //->add('position')
            //->add('category')
            ->add('prixenclair')

            ->add('description')
            ->add('dateUnique', HiddenDateTimeType::class)
             ->add('heureDebut', HiddenHeureType::class)
             ->add('heureFin', HiddenHeureType::class)
            //->add('heureFinAube')
            ->add('hidden', CheckboxType::class, array('required' => false))
            ->add('cancelledAt', CheckboxType::class, array('required' => false))
            ->add('dateDebut', HiddenDateTimeType::class)
            ->add('dateFin', HiddenDateTimeType::class)
            ->add('rrule', HiddenType::class)
            ->add('contactEvent')
            ->add('resaPrevente')
            ->add('accesTransport')
            ->add('filenameajax', HiddenType::class, ['mapped' => false])
            ->add('multiflyer', HiddenType::class, ['mapped' => false])
            ->add('dragandrop', HiddenType::class, ['mapped' => false])

            ->add('prixmultiple', CollectionType::class, [
                'entry_type' => prixmultipleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])

            ->add('flyermultiple', CollectionType::class, [
                'entry_type' => flyermultipleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])

            ->add('videomultiple', CollectionType::class, [
                'entry_type' => videomultipleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])

            ->add('entreetype', EntityType::class, [
                'placeholder' => 'Choisir',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\EntreeType',
                'choice_label' => 'name',
                ])
            ->add('eventtype', EntityType::class, [
                'placeholder' => 'Choisir',
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\EventType',
                'choice_label' => 'name',
                'mapped' => false, 'data' => ($entity->getId() !== null && $entity->getId() > 0 && null !== $entity->getEventType()[0] ? $em->getReference('mdtshomeBundle:EventType', $entity->getEventType()[0]->getId()) : null),
                ])

            /*->add('eventlieu', 'hidden', [
                  'mapped' => false
                  ,'empty_data' => 'ddd'
                ])*/
            /*->add('eventlieu', 'hidden_entity', [
                  'class' => 'mdts\homeBundle\Entity\EventLieu'
                ])*/

            ->add('eventlocal', HiddenType::class, [
                  'mapped' => false,
                  'default' => implode(',', $localids),

                ])

            ->add('artistesdj', HiddenType::class, [
                  'mapped' => false,
                  'default' => implode(',', $localartistes),

                ])

            ->add('eventmultilieu', HiddenType::class, [
                  'mapped' => false,
                  'default' => implode(',', $multilieu),

                ])

            ->add('articlesmultiple', CollectionType::class, [
                'entry_type' => articlesmultipleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])
            ->add('save', SubmitType::class)
            ->add('save_and_new', SubmitType::class)
            ->add('save_and_back', SubmitType::class)
            ->add('save_and_copy', SubmitType::class)

            ->add('related_event', HiddenType::class, [
                'mapped' => false,
                'default' => implode(',', $related_event),
            ])

            ->add('dateclair', HiddenType::class)
            ->add('optdateclair', HiddenType::class )

            ->add('eventbymember', HiddenType::class, [
                'mapped' => false
            ])
            ->add('event_type', HiddenType::class, [
                'mapped' => false,
                'default' => implode(',', $localids),

            ])

        ;
    }



    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'mdts\homeBundle\Entity\Event', 'er' => false,
        ));
    }

    /**
     * @return string
     */
//    public function getName()
    public function getBlockPrefix()
    {
        return 'mdts_homebundle_event';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('er');

    }
}
