<?php

namespace mdts\homeBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class DateTimeToStringTransformer implements DataTransformerInterface
{
    public function __construct()
    {
    }

    /**
     * @param \DateTime|null $datetime
     *
     * @return string
     */
    public function transform($datetime)
    {
        if (null === $datetime) {
            return '';
        }
        if (!\DateTime::createFromFormat('Y-m-d', $datetime->format('Y-m-d'))) {
            return '';
        }

        return $datetime->format('Y-m-d');
    }

    /**
     * @param string $datetimeString
     *
     * @return \DateTime
     */
    public function reverseTransform($datetimeString)
    {
        $datetime = new \DateTime($datetimeString);

        return $datetime;
    }
}
