<?php

namespace mdts\homeBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class HeureToStringTransformer implements DataTransformerInterface
{
    public function __construct()
    {
    }

    /**
     * @param \DateTime|null $datetime
     *
     * @return string
     */
    public function transform($datetime)
    {
        if (null === $datetime) {
            return '';
        }
        if (!\DateTime::createFromFormat('H:i:s', $datetime->format('H:i:s'))) {
            return '';
        }

        return $datetime->format('H:i:s');
    }

    /**
     * @param string $datetimeString
     *
     * @return \DateTime
     */
    public function reverseTransform($datetimeString)
    {
        $datetime = new \DateTime($datetimeString);

        return $datetime;
    }
}
