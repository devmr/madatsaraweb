<?php

namespace mdts\homeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class EventLocalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('required' => false))

            ->add('startdate', TextType::class, array('required' => false))
            ->add('enddate', TextType::class, array('required' => false))
            ->add('recurrence', HiddenType::class, array('required' => false))
            ->add('hidden', CheckboxType::class, array('required' => false))
            ->add('eventgrouped', CheckboxType::class, array('required' => false))

            ->add('startdateHome', TextType::class, array('required' => false))
            ->add('enddateHome', TextType::class, array('required' => false))
            ->add('parent', EntityType::class, [
                'placeholder' => 'Choisir',
                'required' => false,
                'class' => 'mdts\homeBundle\Entity\EventLocal',
                'choice_label' => 'name',
                'query_builder' => function(\mdts\homeBundle\Entity\EventLocalRepository $repository){
                    return $repository->createQueryBuilder('s')
                        ->where('s.parent IS NULL AND s.startdate !=\'\' AND s.enddate != \'\' ')
                        ->add('orderBy', 's.name ASC');
                }
            ])

        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'mdts\homeBundle\Entity\EventLocal',
        ));
    }

    /**
     * @return string
     */
//    public function getName()
    public function getBlockPrefix()
    {
        return 'mdts_homebundle_eventlocal';
    }
}
