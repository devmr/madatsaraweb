<?php

namespace mdts\homeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventArtistesDjOrganisateursType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $localevent = array();
        $parentartisteid = array();
        $enfantartisteid = array();
        $em = $options['er'];

        if ($entity->getId() !== null && $entity->getId() > 0) {
            $EventByArtisteId = $em->getRepository('mdtshomeBundle:Event')->getEventByArtisteId($entity->getId());
            foreach ($EventByArtisteId as $row) {
                $localevent[] = $row->getId();
            }

            $groupartistesByArtisteId = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getGroupartistesByArtisteId($entity->getId());
            foreach ($groupartistesByArtisteId as $row) {
                $parentartisteid[] = $row->getId();
            }

            $enfantsartistesByArtisteId = $entity->getParentArtiste();
            foreach ($enfantsartistesByArtisteId as $row) {
                $enfantartisteid[] = $row->getId();
            }
            // ld($enfantartisteid);
        }

        $builder
            ->add('name', TextType::class, array('required' => true))
            ->add('slug')
            ->add('type', ChoiceType::class, [
                'required' => true,
                'choices' => [
                    'Artiste' => 'artiste', 'DJ' => 'dj', 'Organisateur' => 'organisateur',
                ],
                'choices_as_values' => true,
                'placeholder' => 'Choisir',
                ])
            ->add('event', HiddenType::class, [
                  'mapped' => false,
                  'default' => implode(',', $localevent),

                ])
            ->add('articlesmultiple', CollectionType::class, [
                'entry_type' => articlesmultipleType::class,
                'allow_add' => true,
                'allow_delete' => true,
                'prototype' => true,

                'mapped' => false,
            ])
            ->add('photoFile', VichImageType::class, array(
                'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_link' => true, // not mandatory, default is true
            ))
            ->add('filenameajax', HiddenType::class, ['mapped' => false])
            ->add('dragandrop', HiddenType::class, ['mapped' => false])


            
            ->add('parentartisteid', HiddenType::class, [
                'mapped' => false,
                'default' => implode(',', $parentartisteid),
            ])

            ->add('enfantartisteid', HiddenType::class, [
                'mapped' => false,
                'default' => implode(',', $enfantartisteid),
            ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'mdts\homeBundle\Entity\EventArtistesDjOrganisateurs', 'er' => false,
        ));
    }

    /**
     * @return string
     */
//    public function getName()
    public function getBlockPrefix()
    {
        return 'mdts_homebundle_eventartistesdjorganisateurs';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('er');

    }
}
