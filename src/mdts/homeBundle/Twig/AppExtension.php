<?php

namespace mdts\homeBundle\Twig;

use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Symfony\Component\Routing\RouterInterface;
use Twig_Environment;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


// Request service
use Symfony\Component\HttpFoundation\RequestStack;

class AppExtension extends \Twig_Extension
{
    protected $doctrine;
    protected $router;
    protected $_container;
    protected $requestStack;

    public function __construct(RegistryInterface $doctrine, RouterInterface $router, Container $serviceContainer, RequestStack $requestStack)
    {
        $this->doctrine = $doctrine;
        $this->router = $router;
        $this->_container = $serviceContainer;
        $this->requestStack = $requestStack->getCurrentRequest();
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('isvowel', array($this, 'isVowel')),
            new \Twig_SimpleFilter('getlocalevent', array($this, 'getLocalEvent')),
            new \Twig_SimpleFilter('getlieuevent', array($this, 'getLieuEvent')),
            new \Twig_SimpleFilter('gettypeevent', array($this, 'getTypeEvent')),
            new \Twig_SimpleFilter('getartiste', array($this, 'getArtiste')),
            new \Twig_SimpleFilter('formatdaterule', array($this, 'formatdaterule')),
            new \Twig_SimpleFilter('json_decode', 'json_decode'),
            new \Twig_SimpleFilter('isDateUniqueDC', array($this, 'isDateUniqueDC')),
            new \Twig_SimpleFilter('isDateRangeDC', array($this, 'isDateRangeDC')),
            new \Twig_SimpleFilter('isDateMultiplesDC', array($this, 'isDateMultiplesDC')),

        );
    }
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction(
                'mtsOembed', array($this, 'mtsOembed'), array('is_safe' => array('html' => true))
                ),
            new \Twig_SimpleFunction(
                'asset_url', array($this, 'asset_url')
            ),
            new \Twig_SimpleFunction(
                'file_exists', array($this, 'file_exists')
            ),
            new \Twig_SimpleFunction(
                'getDateUniqueDebutByDateClair', array($this, 'getDateUniqueDebutByDateClair')
            ),
            new \Twig_SimpleFunction(
                'getDateUniqueFinByDateClair', array($this, 'getDateUniqueFinByDateClair')
            ),
            new \Twig_SimpleFunction(
                'getDateRangeByDateClair', array($this, 'getDateRangeByDateClair')
            ),
            new \Twig_SimpleFunction(
                'getDateMultiplesByDateClair', array($this, 'getDateMultiplesByDateClair')
            ),
            new \Twig_SimpleFunction(
                'showDateEvent', array($this, 'showDateEvent')
            ),
            new \Twig_SimpleFunction(
                'showDateEventComplete', array($this, 'showDateEventComplete')
            )
            ,new \Twig_SimpleFunction(
                'Paginate', array($this, 'Paginate')
            ),
        
            );
    }

    /**
     * Implement asset_url function
     * We get the router context. This will default to settings in
     * parameters.yml if there is no active request.
     */
    public function file_exists($path)
    {
        $path = $this->_container->get('kernel')->getRootDir().'/../web/'.$path;

        return file_exists($path);
    }

    /**
     * Implement asset_url function
     * We get the router context. This will default to settings in
     * parameters.yml if there is no active request.
     */
    public function assetUrl($path)
    {
        $context = $this->router->getContext();
        $host = $context->getScheme().'://'.$context->getHost().$context->getBaseUrl();

        return $host.$path;
    }

    public function mtsOembed($url, $w = 560, $h = 315)
    {
        if ($this->isYoutubeUrl($url)) {
            $id = $this->getYoutubevideoId($url);
            if ($id != '') {
                return sprintf('<iframe width="%s" height="%s" src="https://www.youtube.com/embed/%s" frameborder="0" allowfullscreen></iframe>', $w, $h, $id);
            }
        } elseif ($this->isDailymotionUrl($url)) {
            $id = $this->getDailymotionvideoId($url);
            if ($id != '') {
                return sprintf('<iframe frameborder="0" src="//www.dailymotion.com/embed/video/%s"  width="%s" height="%s" allowfullscreen></iframe>', $id, $w, $h);
            }
        } elseif ($this->isVimeoUrl($url)) {
            $id = $this->getVimeovideoId($url);
            if ($id != '') {
                return sprintf('<iframe frameborder="0"  src="https://player.vimeo.com/video/%s?title=0&byline=0" width="%s" height="%s" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>', $id, $w, $h);
            }
        } elseif ($this->isFacebookUrl($url)) {
            $html = '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>';

            $html .= sprintf('<div class="fb-video" data-href="%s" data-width="%s"></div>', $url, $w);

            return $html;
        }

        return $url;
    }
    /**
     * Recuperer ID video youtube.
     *
     * @param string $url
     *
     * @return string ID de la vidéo
     */
    public function getYoutubevideoId($url)
    {
        $id = '';


        //echo $url;

        $path = parse_url($url, PHP_URL_PATH);
        $query = parse_url($url, PHP_URL_QUERY);

        if ($path != '') {
            //var_dump($path);

            if ($query != '') {
                //var_dump($query);
                /*
                * Exemple http://www.youtube.com/watch?v=Gf02t9Dfs8w&feature=youtube_gdata
                */
                parse_str($query, $tab_qs);
                //print_r($tab_qs);
                $id = $tab_qs['v'];
            } else {
                /**
                 * Exemple : http://youtu.be/qJ9fh6W-njY.
                 */
                $id = substr($path, 1);
            }
        }

        return $id;
    }
    /**
     * Recuperer ID video Dailymotion.
     *
     *
     * @param string $url
     *
     * @return string ID de la vidéo
     */
    public function getDailymotionvideoId($url)
    {
        $id = '';

        //echo $url;

        $path = parse_url($url, PHP_URL_PATH);
        $query = parse_url($url, PHP_URL_QUERY);

        if ($path != '') {

                /**
                 * Exemple : /video/x3oe02w_georgina-meva-mahabo_music --> x3oe02w.
                 */
                $path = str_replace('/video/', '', $path);
            $id = substr($path, 0, strpos($path, '_'));
        }

        return $id;
    }
    /**
     * Recuperer ID video Vimeo.
     *
     *
     * @param string $url
     *
     * @return string ID de la vidéo
     */
    public function getVimeovideoId($url)
    {
        $id = '';

        $path = parse_url($url, PHP_URL_PATH);
        $query = parse_url($url, PHP_URL_QUERY);

        if ($path != '') {
            /**
                 * Exemple : https://vimeo.com/152839850.
                 */
                $id = substr($path, 1);
        }

        return $id;
    }
    /**
     * L'URL vient de Youtube.
     *
     * @param string $url
     *
     * @return bool
     */
    public function isFacebookUrl($url)
    {
        $host = parse_url($url, PHP_URL_HOST);

        if ($host != '' && preg_match('/facebook/i', $host)) {
            return true;
        }

        return false;
    }
    /**
     * L'URL vient de Youtube.
     *
     * @param string $url
     *
     * @return bool
     */
    public function isYoutubeUrl($url)
    {
        $host = parse_url($url, PHP_URL_HOST);

        if ($host != '' && preg_match('/youtu/i', $host)) {
            return true;
        }

        return false;
    }
    /**
     * L'URL vient de Dailymotion.
     *
     * @param string $url
     *
     * @return bool
     */
    public function isDailymotionUrl($url)
    {
        $host = parse_url($url, PHP_URL_HOST);

        if ($host != '' && preg_match('/dailymotion/i', $host)) {
            return true;
        }

        return false;
    }
    /**
     * L'URL vient de Dailymotion.
     *
     * @param string $url
     *
     * @return bool
     */
    public function isVimeoUrl($url)
    {
        $host = parse_url($url, PHP_URL_HOST);

        if ($host != '' && preg_match('/vimeo/i', $host)) {
            return true;
        }

        return false;
    }
    public function getTypeEvent($slug)
    {
        $local = $this->doctrine->getRepository('mdtshomeBundle:EventType')->findOneBy(array('origSlug' => $slug));

        return $local->getName();
    }

    public function getArtiste($slug)
    {
        $local = $this->doctrine->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findOneBy(array('origSlug' => $slug));

        return $local;
    }

    public function formatdaterule($str)
    {
        $date = \DateTime::createFromFormat('Ymd\THis\Z', $str);

        return $date->format('Y-m-d\TH:i');
    }
    public function getLocalEvent($slug)
    {
        $local = $this->doctrine->getRepository('mdtshomeBundle:EventLocal')->findOneBy(array('origSlug' => $slug));

        return $local->getName();
    }
    public function getLieuEvent($slug)
    {
        $local = $this->doctrine->getRepository('mdtshomeBundle:EventLieu')->findOneBy(array('origSlug' => $slug));

        return $local->getName();
    }
    public function isVowel($str)
    {
        $vowels = ['a', 'e', 'i', 'o', 'u', 'y', 'h'];

        foreach (array_values($vowels) as $v) {
            $haystack[$v] = 1;
        }

        $str = trim($str);

        $str = strtolower($str);

        if (isset($haystack[$str[0]])) {
            return true;
        }

        return false;
    }

    public function getName()
    {
        return 'app_extension';
    }

    public function getDateUniqueDebutByDateClair($str)
    {
        //return $str->getDateClair();
        if(!isset($str))
            return false;

        $str = trim($str);

        $arrDate = explode('---',$str);



        if (count($arrDate)<2)
            return false;


        return $arrDate[0];
    }
    public function getDateUniqueFinByDateClair($str)
    {
        if(!isset($str))
            return false;

        $str = trim($str);

        $arrDate = explode('---',$str);

        if (count($arrDate)<2)
            return false;


        return $arrDate[1];
    }
    public function getDateRangeByDateClair($str)
    {
        if(!isset($str))
            return false;

        $str = trim($str);

        $arrDate = explode('au',$str);

        if (count($arrDate)<2)
            return false;


        return $str;
    }
    public function getDateMultiplesByDateClair($str)
    {
        if(!isset($str))
            return false;

        $str = trim($str);

        $arrDate = explode(';',$str);

        if (count($arrDate)<2)
            return false;


        return $str;
    }
    public function isDateUniqueDC($str)
    {
        if(!isset($str))
            return false;

        $str = trim($str);

        $arrDate = explode('---',$str);

        if (count($arrDate)<2)
            return false;

        return true;
    }

    // 2017-02-13 12:00 au 2017-02-25 12:00
    public function isDateRangeDC($str)
    {
        if(!isset($str))
            return false;

        $str = trim($str);

        $arrDate = explode('au',$str);

        if (count($arrDate)<2)
            return false;

        return true;
    }

    // 2017-02-22 12:00; 2017-02-15 12:00; 2017-02-10 12:00; 2017-02-26 12:00
    public function isDateMultiplesDC($str)
    {
        if(!isset($str))
            return false;

        $str = trim($str);

        $arrDate = explode(';',$str);

        if (count($arrDate)<2)
            return false;

        return true;
    }

    public function showDateEvent($item)
    {
        // Si vient d'un event local
        if (is_array($item)) {
            return $this->showDateEventArray($item);
        }

        if( method_exists($item, 'getDateUnique' ) && !is_null($item->getDateUnique()) ) {
            return $this->showDateUnique($item);
        }

        if( method_exists($item, 'getDateDebut' ) &&  !is_null($item->getDateDebut() ) ) {
            return $this->showDateRange($item);
        }

        if(method_exists($item, 'getOptdateclair' ) &&   'multiplesdate' == $item->getOptdateclair()  ) {
            return $this->showDateMultiples($item);
        }

        return false;
    }

    public function showDateEventArray($item)
    {
        // Si vient d'un event normal
        if (!is_array($item)) {
            return false;
        }

        if( array_key_exists('dateUnique', $item  ) && !is_null($item['dateUnique']) ) {
            return $this->showDateUnique($item);
        }

        if( array_key_exists('dateDebut', $item  ) && !is_null($item['dateDebut']) ) {
            return $this->showDateRange($item);
        }

        if( array_key_exists('optdateclair', $item  ) && 'multiplesdate' == $item['optdateclair'] ) {
            return $this->showDateMultiples($item);
        }

        return false;
    }

    public function showDateEventComplete($item)
    {
        if( !is_null($item->getDateUnique() ) ) {
            return $this->showDateUnique($item, true);
        }

        if( !is_null($item->getDateDebut() ) ) {
            return $this->showDateRange($item, true);
        }

        if( 'multiplesdate' == $item->getOptdateclair()  ) {
            return $this->showDateMultiples($item);
        }

        return false;
    }

    private function showDateUnique($entity, $complete = false)
    {
        $ed = null;

        if (is_array($entity)) {
            $dU = $entity['dateUnique'];
            $hD = $entity['heureDebut'];
            $hF = $entity['heureFin'];
            $hFa = $entity['heureFinAube'];
        } else {
            $dU = $entity->getDateUnique();
            $hD = $entity->getHeureDebut();
            $hF = $entity->getHeureFin();
            $hFa = $entity->getHeureFinAube();
            $ed = $entity->getEventDate();
        }


        $dtnow = new \DateTime('now');
        $date = $dU->format('Y-m-d');
        $heuredebut = $hD->format('H:i:s');
        $dt = new \DateTime($date.' '.$heuredebut);

        // Si la date est < date courante
        if ($dt->format('U')<$dtnow->format('U')) {

            // Si EventDate exists
            if( !is_null( $ed ) ) {
                return $this->showEventDate($entity,$complete);
            }

        }


        return $this->showDateUniqueSimple($entity, $complete);
    }

    private function showDateRange($entity, $complete = false)
    {
        $arrDate = $this->getArrDateInfo($entity);
        $dU = $arrDate['dU'];
        $hD = $arrDate['hD'];
        $hF = $arrDate['hF'];
        $hFa = $arrDate['hFa'];
        $dateDebutTime = $arrDate['dD'];
        $dateFinTime = $arrDate['dF'];

        $path = $this->_container->get('kernel')->getRootDir()  ;
        $loader = new \Twig_Loader_Filesystem($path);
        $env = new Twig_Environment($loader);


        $dateCompactDm  = $dateDebutTime->format('d/m');
        $dateCompactDmy  = $dateDebutTime->format('d/m/y');
        $dateCompactFin  = $dateFinTime->format('d/m/y');

        $formatdMY = 'd MMM yyyy';
        $title = ' title="Afficher les événements pour la même date"';

        $strf = 'Du <a href="%s"'.$title.'>%s</a> au <a href="%s">%s</a>';
        $dateParam = $dateDebutTime->format('d/m/Y');
        $urlDateDebut = $this->getUrlDate($dateParam);
        $dateParam = $dateFinTime->format('d/m/Y');
        $urlDateFin = $this->getUrlDate($dateParam);



        if ($complete) {
            $dateFormatShort = $dateFormatMedium = 'long';
            $formatdMY = 'd MMMM yyyy';
            $dateCompactDm  = twig_localized_date_filter($env,$dateDebutTime,$dateFormatShort, 'none',null, null, "d MMMM");
            $dateCompactDmy = twig_localized_date_filter($env,$dateDebutTime,$dateFormatShort, 'none',null, null, $formatdMY);
            $dateCompactFin = twig_localized_date_filter($env,$dateFinTime,$dateFormatShort, 'none',null, null, $formatdMY);
        }

        $dateDebutHtml = $dateCompactDmy;
        $dateFinHtml = $dateCompactFin;

        $dateFormatShort = 'short';

        // cas1 - le mois et l'année sont identiques
        if ($dateDebutTime->format('m') == $dateFinTime->format('m') && $dateDebutTime->format('Y') == $dateFinTime->format('Y') ) {
            $datefin = twig_localized_date_filter($env,$dateFinTime,$dateFormatShort, 'none',null, null, $formatdMY);
//            return 'Du '.$dateDebutTime->format('d').' au '.$datefin ;

            $dateDebutHtml = $dateDebutTime->format('d');
            $dateFinHtml = $datefin;
        }
        // cas2 - le mois sont différents mais l'année est identique
        if ($dateDebutTime->format('m') != $dateFinTime->format('m') && $dateDebutTime->format('Y') == $dateFinTime->format('Y') ) {
//            return 'Du '.$dateCompactDm.' au '.$dateCompactFin ;
            $dateDebutHtml = $dateCompactDm;
            $dateFinHtml = $dateCompactFin;
        }

        // cas3 - l'année est différent - ex: cheval entre decembre et janvier
         return sprintf($strf,$urlDateDebut, $dateDebutHtml, $urlDateFin, $dateFinHtml);

    }

    // 2017-02-22 12:00; 2017-02-15 12:00; 2017-02-10 12:00; 2017-02-26 12:00
    private function showDateMultiples($entity)
    {
        $title = ' title="Afficher les événements pour la même date"';
        $strf = '<a href="%s"'.$title.'>%s</a>';

        $str = '';

        $date = null;

        if (!is_array($entity)) {
            $date = $entity->getDateclair();
        }
        if (is_array($entity)) {
            $date = $entity['dateclair'];
        }

        if (is_null($date))
            return false;

        // Combien de dates ?
        $arrDate = explode(';',$date);

        $i = 0;
        foreach ($arrDate as $val) {
            $val = trim($val);

            // Supprimer l'heure
            $val = substr($val,0,strpos($val,' '));
            $val = trim($val);

            //Transformer en dateTime
            if ($i<3)
                $format = 'd/m';


            if ($i>2)
                break;

            $dU = \DateTime::createFromFormat('Y-m-d', $val);
            $dt = $dU->format($format);
            $dateParam = $dU->format('d/m/Y');
            $urlDate = $this->getUrlDate($dateParam);
            $dt = sprintf($strf,$urlDate,$dt);

            // Enregistrer dans un Array
            $str .= $dt;

            if ($i<2)
                $str .= ', ';

            if ($i==2)
                $str .= '...';

            $i++;

        }

        return $str;
    }

    private function showEventDate($entity, $complete = false)
    {
        $dates = $entity->getEventDate();
        $nbItems = count($dates);

        if ($nbItems==1) {
            return $this->showDateUniqueSimple($entity,$complete);
        } 

        $str = '';

        $i = 0;
        foreach($dates as $date ) {



            // Si le nb items > 1 et index < 4
            if ($nbItems>1 && $i<3) {

                // Limiter l'affichage à 3 items
                 $str .=  $date->getDate()->format('d/m') ;

                if ($i<$nbItems && $i<2)
                    $str .=', ';


            }


            $i++;

        }

        if ($nbItems>2 )
            $str .= '...';

        return $str;
    }

    private function showDateUniqueSimple($entity, $complete = false)
    {



        $arrDate = $this->getArrDateInfo($entity);
        $dU = $arrDate['dU'];
        $hD = $arrDate['hD'];
        $hF = $arrDate['hF'];
        $hFa = $arrDate['hFa'];

        $path = $this->_container->get('kernel')->getRootDir()  ;
        $loader = new \Twig_Loader_Filesystem($path);
        $env = new Twig_Environment($loader);

        $dateCompact  = $dU->format('d/m');
        $dateFormatShort = 'short';
        $dateFormatMedium = 'medium';

        $dateParam = $dU->format('d/m/Y');
        $urlDate = $this->getUrlDate($dateParam);

        $title = ' title="Afficher les événements pour la même date"';


        $strf = 'Le <a href="%s"'.$title.'>%s</a>%s%s';
        $heureD = '';
        $heureF = '';

        if ($complete) {
            $dateFormatShort = $dateFormatMedium = 'long';
            $dateCompact  = twig_localized_date_filter($env,$dU,$dateFormatShort, 'none');
        }

        // Si l(heure fin n'est pas 00:00
        if ($hF->format('H:i') !== '00:00') {
            $strf = 'Le <a href="%s"'.$title.'>%s</a> de %s à %s';
            $heureD = $hD->format('H:i');
            $heureF = $hF->format('H:i');
//            return sprintf($strf1, $urlDate, $dateCompact, $hD->format('H:i'), $hF->format('H:i'));
        }

        if ($hFa == 1 ) {

            $date = twig_localized_date_filter($env,$dU,$dateFormatShort, 'none');
            // return 'Le '.$date. ' de '.$hD->format('H:i').' à l\'aube';
            $strf = 'Le <a href="%s"'.$title.'>%s</a> de %s à l\'aube%s';
            $heureD = $hD->format('H:i');
//            return sprintf($strf2, $urlDate, $date, $hD->format('H:i'), '');
        }

        if ($hD->format('H:i') !== '00:00') {

            $date = twig_localized_date_filter($env,$dU,$dateFormatMedium, 'none');
//            return 'Le '.$date. ' à '.$hD->format('H:i') ;
            $strf = 'Le <a href="%s"'.$title.'>%s</a> à %s%s';
            $heureD = $hD->format('H:i');
//            return sprintf($strf3, $urlDate, $date, $hD->format('H:i'), '');
        }

        $date = twig_localized_date_filter($env,$dU,'long', 'none');
//        return 'Le '.$date;
        return sprintf($strf, $urlDate, $date, $heureD, $heureF);
    }

    private function getArrDateInfo($entity)
    {

        if (is_array($entity)) {
            $dU = $entity['dateUnique'];
            $hD = $entity['heureDebut'];
            $hF = $entity['heureFin'];
            $hFa = $entity['heureFinAube'];
            $dD = $entity['dateDebut'];
            $dF = $entity['dateFin'];
        } else {
            $dU = $entity->getDateUnique();
            $hD = $entity->getHeureDebut();
            $hF = $entity->getHeureFin();
            $hFa = $entity->getHeureFinAube();
            $dD = $entity->getDateDebut();
            $dF = $entity->getDateFin();
        }
        return [
            'dU' => $dU,
            'hD' => $hD,
            'hF' => $hF,
            'dD' => $dD,
            'dF' => $dF,
            'hFa' => $hFa
        ];
    }

    private function getUrlDate($dateParam=[])
    {
        $url = $this->_container->get('router')->generate('mdts_searchevent', ['mdts_homebundle_search[date_from]'=>$dateParam, 'mdts_homebundle_search[query]'=>'']);
        return $url;
    }

    public function Paginate(String $url, int $limit, int $countRows, int $nbPages, int $maxPages, int $currentPage)
    {


        $htmlPagination = $this->getHtmlNav($url,$limit,$countRows,$nbPages,$currentPage );
        return $htmlPagination;

//        return $url.' - limit: '.$limit.' - maxPages: '.$maxPages.' - currentPage: '.$currentPage;

    }

    private function getHtmlNav($url, $limit, $countRows, $nbPages, $currentPage, $tag=[] )
    {
//        $url = $this->_container->get('router')->generate($url, ['page'=>11]);
//        var_dump($url);

        $arrQueryString = $this->requestStack->query->all();
        unset($arrQueryString['page']);

        $sprintfWrapperAll = array_key_exists('wrapper', $tag) ? $tag['wrapper'] : '<div class="pull-left"><ul class="pagination pagination-sm">%s</ul></div>';

        $sprintfWrapperItems = array_key_exists('wrapperItems', $tag) ? $tag['wrapperItems'] : '<li>%s</li>';
        $sprintfWrapperItemsCurrent = array_key_exists('wrapperItemsCurrent', $tag) ? $tag['wrapperItemsCurrent'] : '<li class="active">%s</li>';

        $txtLeftIcon = array_key_exists('txtLeftIcon', $tag) ? $tag['txtLeftIcon'] : '&laquo;';
        $txtRightIcon = array_key_exists('txtRightIcon', $tag) ? $tag['txtRightIcon'] : '&raquo;';

        $sprintfWrapperLeftIcon = array_key_exists('wrapperLeftIcon', $tag) ? $tag['wrapperLeftIcon'] : '<li>%s</li>';
        $sprintfWrapperLeftCurrentIcon = array_key_exists('wrapperLeftCurrentIcon', $tag) ? $tag['wrapperLeftCurrentIcon'] : '<li class="disabled">%s</li>';

        $sprintfWrapperRightIcon = array_key_exists('wrapperRightIcon', $tag) ? $tag['wrapperRightIcon'] : '<li>%s</li>';
        $sprintfWrapperRightCurrentIcon = array_key_exists('wrapperRightCurrentIcon', $tag) ? $tag['wrapperRightCurrentIcon'] : '<li class="disabled">%s</li>';

        $sprintfLinkFirst = array_key_exists('wrapperLinkFirst', $tag) ? $tag['wrapperLinkFirst'] : '<li>%s</li><li class="disabled"><span>...</span></li>';
        $sprintfLinkLast = array_key_exists('wrapperLinkLast', $tag) ? $tag['wrapperLinkLast'] : '<li class="disabled"><span>...</span></li><li>%s</li>';


        $isFirst = $isLast = false;
        // ne rien afficher si le nb total d'enreg est < nb de pages à afficher
        if ($countRows<=$nbPages)
            return '';

        // Derniere page
        $last       = ceil( $countRows / $limit );

        // Page courante <= nombre de page a afficher
        if ($currentPage<=$nbPages ) {
            $start = 1;
            $end = $nbPages;
            $isFirst = true;
        }

        // Page courante+nombre de page a afficher >= derniere page
        if ($last>$nbPages && $currentPage+$nbPages>$last) {
            $start = $last-$nbPages+1;
            $end = $last;
            $isLast = true;
        }

        if ($last<$nbPages && $currentPage+$nbPages>=$last) {
            $end = $last;
        }

        // On n'est pas sur les intervalles de la 1ere et de la derniere page
        if ($currentPage+$nbPages==$last || (!$isFirst && !$isLast)) {
            $start = $currentPage-floor($nbPages/2);
            $end = $currentPage+floor($nbPages/2);
        }

        if ($end>$start && ($end-$start)==$nbPages)
            $start += 1;


//        var_dump('countRows: '.$countRows.' - nbages: '.$nbPages.' - currentPage: '.$currentPage.' - start: '.$start.' - last: '.$last.' - end: '.$end.' - (currentPage +  nbPages):'.($currentPage + $nbPages));
//        var_dump($isFirst);
//        var_dump($isLast);
//        print_r($arrQueryString);

        $html = '';


        if ($currentPage == 1) {
            $sprintfLeft = $sprintfWrapperLeftCurrentIcon ;
        } else {
            $sprintfLeft = $sprintfWrapperLeftIcon;
        }

        $arrQ = $arrQueryString+['page'=>( $currentPage - 1 )];
        $href = $this->_container->get('router')->generate($url, $arrQ);
        $linkFirstStart = '<a href="' .$href . '">'.$txtLeftIcon.'</a>';
        $html .= sprintf($sprintfLeft,$linkFirstStart );

        if ( $start > 1 ) {
            $arrQ = $arrQueryString+['page'=>1];
            $href = $this->_container->get('router')->generate($url, $arrQ);
            $linkFirst = '<a href="' . $href . '">1</a>';
            $html .= sprintf($sprintfLinkFirst,$linkFirst );
        }

        $wrapperItems = '';
        for ( $i = $start ; $i <= $end; $i++ ) {

            if ( $currentPage == $i )
                $wrapperItems = $sprintfWrapperItemsCurrent;
            else
                $wrapperItems = $sprintfWrapperItems;

            $arrQ = $arrQueryString+['page'=>$i];
            $href = $this->_container->get('router')->generate($url, $arrQ);
            $linkItems = '<a href="' . $href . '">' . $i . '</a>';
            $html .= sprintf($wrapperItems,$linkItems );

        }

        if ( $end < $last ) {
            $arrQ = $arrQueryString+['page'=>$last];
            $href = $this->_container->get('router')->generate($url, $arrQ);
            $linkLast = '<a href="' . $href . '">' . $last . '</a>';
            $html .= sprintf($sprintfLinkLast,$linkLast );
        }

        $sprintfRight = $sprintfWrapperRightIcon;
        if ($currentPage == $last) {
            $sprintfRight = $sprintfWrapperRightCurrentIcon ;
        }


        $arrQ = $arrQueryString+['page'=>( $currentPage + 1 )];
        $href = $this->_container->get('router')->generate($url, $arrQ);
        $linkEndStart = '<a href="' . $href . '">'.$txtRightIcon.'</a>';
        $html .= sprintf($sprintfRight,$linkEndStart );

        // afficher le nombre de resultats affiches / nb total
        $htmlResults = $this->getHtmlNavResults($countRows,$currentPage,$limit);

        // Ne pas afficher la pagination si limit == nb de rows
        if ($limit==$countRows)
            $html = '';

       $html = $htmlResults.sprintf($sprintfWrapperAll,$html);

       // SELECT nb de pages a afficher
        $htmlSelect = $this->getHtmlNavSelectPages($countRows);

        $html .= $htmlSelect;

        return $html;
    }

    private function getHtmlNavSelectPages(int $countRows)
    {
        $qsFormExists = false;
        $limit = 5;

        $arrQueryString = $this->requestStack->query->all();

        if (count($arrQueryString)>0 && array_key_exists('form', $arrQueryString ))
            $qsFormExists = true;

        if ($qsFormExists && array_key_exists('limit', $arrQueryString['form']))
          $limit = (int) $arrQueryString['form']['limit'] ;

        $arrNb = [5];
        if ($countRows>10)
            array_push($arrNb, 10);
        if ($countRows>20)
            array_push($arrNb, 20);
        if ($countRows>30)
            array_push($arrNb, 30);
        if ($countRows>50)
            array_push($arrNb, 50);
        if ($countRows>100)
            array_push($arrNb, 100);
        if ($countRows>200)
            array_push($arrNb, 200);



        $html = '<div class="pull-left mtm mls">Lignes par page: ';
        $html .= '<select data-selectpagination><option></option>';

        foreach ($arrNb as $option) {
            $selected = $limit==$option?' selected="selected"':'';
            $html .= '<option value="'. $option .'"'. $selected .'>'. $option .'</option>';

        }

        $selectedAll = $limit==$countRows?' selected="selected"':'';
        if ($countRows<=50)
            $html .= '<option value="'. $countRows .'"'. $selectedAll .'>All</option>';

        $html .= '</select></div>';


        return $html;
    }

    private function getHtmlNavResults($countRows, $currentPage, $limit)
    {
        $start = ($currentPage*$limit)-$limit+1;
        $offset = ($currentPage*$limit);

        if ($offset>$countRows)
            $offset = $countRows;



        $html = '';
        $html .= '<div class="pull-left mtm mrs">Affichage de '.$start.' à '.$offset.' sur '.$countRows.'</div>';



        return $html;
    }

}
