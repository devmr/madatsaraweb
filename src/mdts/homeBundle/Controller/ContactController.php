<?php

namespace mdts\homeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use mdts\homeBundle\Entity\Contact;
use mdts\homeBundle\Form\ContactType;
use mdts\homeBundle\Entity\NewsletterAbonne;

class ContactController extends Controller
{
    protected $_contactHelper;

    /**
     * Set your services here (we need to override this method to initialize our services into class properties).
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->_contactHelper = $this->get('madatsara.service');
    }

    public function indexAction(Request $request)
    {
        $paramEmail = $this->get('madatsara.service')->getParameter('default_email');
        $defaultDestEmail = array_key_exists('to', $paramEmail) ? $paramEmail['to'] : '';
        $defaultFromEmail = array_key_exists('from', $paramEmail) ? $paramEmail['from'] : '';
        $ip = $this->get('madatsara.service')->getIpAddress();

        $contact = new Contact();
        $form = $this->get('form.factory')->create(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($contact->getSubscribe()) {
                $entity = new NewsletterAbonne();
                $entity->setEmail($contact->getEmail());
                $entity->setSource('contactpage');
                $this->get('madatsara.service')->createOrUpdateNewsletter($entity);
            }



            //Envoyer mail admin
            $this->get('madatsara.service')->sendMail(
                $defaultDestEmail,
                '[MADATSARA] - Formulaire de contact',
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'mdtshomeBundle:Email:contact.html.twig',
                    array(
                        'name' => $contact->getName(),
                        'email' => $contact->getEmail(),
                        'message' => $contact->getMessage(),
                        'ip' => $ip,
                        'subscribe' => $contact->getSubscribe(),
                    )
                ),
                $defaultFromEmail
            );


            //Mail de confirmation
            $this->get('madatsara.service')->sendMail(
                $contact->getEmail(),
                '[MADATSARA] - Formulaire de contact',
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'mdtshomeBundle:Email:contactconfirm.html.twig',
                    array(
                        'name' => $contact->getName(),
                        'email' => $contact->getEmail(),
                        'message' => $contact->getMessage(),
                        'subscribe' => $contact->getSubscribe(),
                    )
                ),
                $defaultFromEmail
            );


            $request->getSession()->getFlashBag()->add('notice', 'votre message a bien été transmis.');

            // On redirige vers la page contact
            return $this->redirect($this->generateUrl('mts_contact'));
        }


        return $this->render('mdtshomeBundle:Default:contact.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
