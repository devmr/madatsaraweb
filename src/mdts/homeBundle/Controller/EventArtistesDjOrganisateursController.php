<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\EventArtistesDjOrganisateurs;
use mdts\homeBundle\Form\EventArtistesDjOrganisateursType;
use mdts\homeBundle\Entity\searchBackendModel;
use mdts\homeBundle\Form\checkboxArtistesType;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * EventArtistesDjOrganisateurs controller.
 */
class EventArtistesDjOrganisateursController extends Controller
{
    /**
     * Lists all EventArtistesDjOrganisateurs entities.
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $session->remove('iframe');

        // Form de recherche
        $smodel = new searchBackendModel();
        $filter_form = $this->createFormBuilder($smodel, array(
            'csrf_protection' => false,
            ))
            //->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('GET')

            ->add('query', TextType::class, array('required' => false))

            ->add('limit', HiddenType::class, ['data' => 5] )
            ->add('submit', SubmitType::class, array('label' => 'Chercher'))
            ->getForm();
        $filter_form->handleRequest($request);

        // Form des checkbox
        $idsForm = $form = $this->get('madatsara.service')->createFormAllIds('admin_artistesdjorga_delete_all')->createView();

        // Param du form de recherche
        $formQuery = $request->query->get('form');
        $query = is_array($formQuery) ? $formQuery : [];
        $limit = is_array($request->query->get('form')) && array_key_exists('limit',$request->query->get('form'))?(int)$request->query->get('form')['limit'] :5;
        $currentPage = $request->query->getInt('page', 1);

        // Get entity doctrine
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findAllQuery($query,  $currentPage, $limit);
        $countRows = $entities->count() ;
        $maxPages = ceil($entities->count() / $limit);

        // Render view
        return $this->render(
            'mdtshomeBundle:EventArtistesDjOrganisateurs:index.html.twig',
            array_merge(
                [
                    'entities' => $entities,
                    'idsForm' =>$idsForm,
                    'filter_form' => $filter_form->createView(),
                ],
                compact('countRows','limit', 'maxPages', 'currentPage')
            )
        );
    }
    public function deleteallAction(Request $request)
    {
        // POST Data param
        $datapost = filter_input_array(INPUT_POST);
        $data = array_key_exists('mdts_homebundle_all', $datapost) ? $datapost['mdts_homebundle_all'] : [];
        $data['action'] = array_key_exists('action', $datapost) ? $datapost['action'] : '';
        $ids = array_key_exists('ids', $data) ? explode(',',$data['ids']) : [];

        if ($request->isMethod('DELETE') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->removeallEventArtistesDjOrganisateurs($ids);
        }
        if ($request->isMethod('POST') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->copyallEventArtistesDjOrganisateurs($ids);
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été supprimées.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression des entités.');
        }

        return $this->redirect($this->generateUrl('admin_artistesdjorga'));
    }
    public function duplicateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLieu entity.');
        }
        //Enregistrer dans BDD
        $srv = $this->get('madatsara.service')->duplicateEventArtistesDjOrganisateurs($entity);

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été dupliquée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la copie de l\'Entité «'.$entity->getName().'».');
        }

        return $this->redirect($this->generateUrl('admin_artistesdjorga'));
    }
    /**
     * Creates a new EventArtistesDjOrganisateurs entity.
     */
    public function createAction(Request $request)
    {
        $entity = new EventArtistesDjOrganisateurs();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            // Form 
            $arrForm = [];

            if ($form->get('filenameajax')->getData()!='')
                $arrForm['filenameajax'] = $form->get('filenameajax')->getData();

            if ($form->get('dragandrop')->getData()!='')
                $arrForm['dragandrop'] = $form->get('dragandrop')->getData();

            if ($form->get('event')->getData())
                $arrForm['event'] = $form->get('event')->getData();

            if ($form->get('parentartisteid')->getData())
                $arrForm['parentartisteid'] = $form->get('parentartisteid')->getData();

            if ($form->get('enfantartisteid')->getData())
                $arrForm['enfantartisteid'] = $form->get('enfantartisteid')->getData();

            if ($form->get('articlesmultiple')->getData())
                $arrForm['articlesmultiple'] = $form->get('articlesmultiple')->getData();


            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventArtistesDjOrganisateurs($entity, $arrForm);
            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été créé.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la création de l\'Entité «'.$entity->getName().'».');
            }

            return $this->redirect($this->generateUrl('admin_artistesdjorga'));
        }

        return $this->render('mdtshomeBundle:EventArtistesDjOrganisateurs:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EventArtistesDjOrganisateurs entity.
     *
     * @param EventArtistesDjOrganisateurs $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EventArtistesDjOrganisateurs $entity)
    {
        $form = $this->createForm(EventArtistesDjOrganisateursType::class, $entity, array(
            'action' => $this->generateUrl('admin_artistesdjorga_create'),
            'method' => 'POST',
            'er' => false
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EventArtistesDjOrganisateurs entity.
     */
    public function newAction()
    {
        $session = new Session();
        $session->remove('iframe');
        $entity = new EventArtistesDjOrganisateurs();
        $form = $this->createCreateForm($entity);

        return $this->render('mdtshomeBundle:EventArtistesDjOrganisateurs:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EventArtistesDjOrganisateurs entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventArtistesDjOrganisateurs entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventArtistesDjOrganisateurs:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EventArtistesDjOrganisateurs entity.
     */
    public function editAction($id)
    {
        $request = Request::createFromGlobals();
        //ld($request->query->get('iframe'));
        $getIframe = $request->query->get('iframe');
        if (!is_null($getIframe) && '1' === $getIframe) {
            $session = new Session();
            $session->set('iframe', '1');
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventArtistesDjOrganisateurs entity.');
        }

        $articlesevent = array();
        $i = 0;
        foreach ($entity->getArticlesEvent() as $local) {
            $articlesevent[$i]['url'] = $local->getUrl();
            ++$i;
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventArtistesDjOrganisateurs:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'articlesevent' => json_encode($articlesevent),
        ));
    }

    /**
     * Creates a form to edit a EventArtistesDjOrganisateurs entity.
     *
     * @param EventArtistesDjOrganisateurs $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EventArtistesDjOrganisateurs $entity)
    {
        $er = $this->getDoctrine()
                ->getEntityManager();

        $form = $this->createForm(EventArtistesDjOrganisateursType::class, $entity, array(
            'action' => $this->generateUrl('admin_artistesdjorga_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'er' => $er,
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EventArtistesDjOrganisateurs entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventArtistesDjOrganisateurs entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //$session = new Session();
            //ld($session->get('iframe'));

            // Form 
            $arrForm = [];

            $form = $editForm;

            if ($form->get('filenameajax')->getData()!='')
                $arrForm['filenameajax'] = $form->get('filenameajax')->getData();

            if ($form->get('dragandrop')->getData()!='')
                $arrForm['dragandrop'] = $form->get('dragandrop')->getData();

            if ($form->get('event')->getData())
                $arrForm['event'] = $form->get('event')->getData();

            if ($form->get('parentartisteid')->getData())
                $arrForm['parentartisteid'] = $form->get('parentartisteid')->getData();

            if ($form->get('enfantartisteid')->getData())
                $arrForm['enfantartisteid'] = $form->get('enfantartisteid')->getData();

            if ($form->get('articlesmultiple')->getData())
                $arrForm['articlesmultiple'] = $form->get('articlesmultiple')->getData();

            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventArtistesDjOrganisateurs($entity, $arrForm, $id);
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été mis à jour.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de l\'enregistrement de l\'Entité «'.$entity->getName().'».');
            }

            return $this->redirect($this->generateUrl('admin_artistesdjorga_edit', array('id' => $id)));
        }

        return $this->render('mdtshomeBundle:EventArtistesDjOrganisateurs:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a EventArtistesDjOrganisateurs entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($request->isMethod('GET')) {
            $srv = $this->get('madatsara.service')->removeEventArtistesDjOrganisateurs($id);
        }

        if (!$request->isMethod('GET') && $form->isValid()) {
            $srv = $this->get('madatsara.service')->removeEventArtistesDjOrganisateurs($id);
        }
        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'entite a bien été supprimée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression de l\'entite .');
        }

        return $this->redirect($this->generateUrl('admin_artistesdjorga'));
    }

    /**
     * Creates a form to delete a EventArtistesDjOrganisateurs entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_artistesdjorga_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
