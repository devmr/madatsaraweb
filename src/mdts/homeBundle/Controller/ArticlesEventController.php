<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\ArticlesEvent;
use mdts\homeBundle\Form\ArticlesEventType;
use mdts\homeBundle\Entity\searchBackendModel;
use mdts\homeBundle\Form\checkboxEvenementType;
use Symfony\Component\HttpFoundation\Session\Session;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

/**
 * ArticlesEvent controller.
 */
class ArticlesEventController extends Controller
{
    /**
     * Lists all ArticlesEvent entities.
     */
    public function indexAction(Request $request)
    {
        $session = new Session();
        $session->remove('iframe');
        $getIframe = $request->query->get('iframe');
        //ld($getIframe);
        if (!is_null($getIframe) && '1' === $getIframe) {
            $session->set('iframe', '1');
        }

        // Form de recherche
        $smodel = new searchBackendModel();
        $filter_form = $this->createFormBuilder($smodel, array(
            'csrf_protection' => false,
        ))
            //->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('GET')

            ->add('query', TextType::class, array('required' => false))

            ->add('limit', HiddenType::class, ['data' => 5] )
            ->add('submit', SubmitType::class, array('label' => 'Chercher'))
            ->getForm();
        $filter_form->handleRequest($request);

        // Form des checkbox
        $idsForm = $form = $this->get('madatsara.service')->createFormAllIds('admin_articlesevent_delete_all')->createView();


        // Param du form de recherche
        $formQuery = $request->query->get('form');
        $query = is_array($formQuery) ? $formQuery : [];
        $limit = is_array($request->query->get('form')) && array_key_exists('limit',$request->query->get('form'))?(int)$request->query->get('form')['limit'] :5;
        $currentPage = $request->query->getInt('page', 1);

        // Get entity doctrine
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('mdtshomeBundle:ArticlesEvent')->findAllQuery($query,  $currentPage, $limit);
        $countRows = $entities->count() ;
        $maxPages = ceil($entities->count() / $limit);

        // Render view
        return $this->render(
            'mdtshomeBundle:ArticlesEvent:index.html.twig',
            array_merge(
                [
                    'entities' => $entities,
                    'idsForm' =>$idsForm,
                    'filter_form' => $filter_form->createView(),
                ],
                compact('countRows','limit', 'maxPages', 'currentPage')
            )
        );
    }
    /**
     * Creates a new ArticlesEvent entity.
     */
    public function createAction(Request $request)
    {
        $session = new Session();
        $getIframe = $session->get('iframe');

        $entity = new ArticlesEvent();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateArticleEvent($entity, $form);
            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getUrl().'» a bien été créé.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la création de l\'Entité «'.$entity->getUrl().'».');
            }

            $params = array();
            if ('1' === $getIframe) {
                $params = array('iframe' => '1');
            }

            return $this->redirect($this->generateUrl('admin_articlesevent', $params));
        }

        return $this->render('mdtshomeBundle:ArticlesEvent:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a ArticlesEvent entity.
     *
     * @param ArticlesEvent $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ArticlesEvent $entity)
    {
        $form = $this->createForm(ArticlesEventType::class, $entity, array(
            'action' => $this->generateUrl('admin_articlesevent_create'),
            'method' => 'POST',
            'er' => false
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ArticlesEvent entity.
     */
    public function newAction()
    {
        $session = new Session();
        $session->remove('iframe');
        $request = Request::createFromGlobals();
        $getIframe = $request->query->get('iframe');
        if (!is_null($getIframe) && '1' === $getIframe) {
            $session->set('iframe', '1');
        }

        $entity = new ArticlesEvent();
        $form = $this->createCreateForm($entity);

        return $this->render('mdtshomeBundle:ArticlesEvent:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ArticlesEvent entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ArticlesEvent entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:ArticlesEvent:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ArticlesEvent entity.
     */
    public function editAction($id)
    {
        $request = Request::createFromGlobals();
        $getIframe = $request->query->get('iframe');
        if (!is_null($getIframe) && '1' === $getIframe) {
            $session = new Session();
            $session->set('iframe', '1');
        }
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ArticlesEvent entity.');
        }

        $event = array();
        $i = 0;
        foreach ($em->getRepository('mdtshomeBundle:Event')->getEventByArticleId($id) as $events) {
            $event[$i]['id'] = $events->getId();
            ++$i;
        }

        $artiste = array();
        $i = 0;
        foreach ($em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getArtisteByArticleId($id) as $events) {
            $artiste[$i]['id'] = $events->getId();
            ++$i;
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:ArticlesEvent:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'event' => json_encode($event),
            'artiste' => json_encode($artiste),
        ));
    }

    /**
     * Creates a form to edit a ArticlesEvent entity.
     *
     * @param ArticlesEvent $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(ArticlesEvent $entity)
    {
        $er = $this->getDoctrine()->getManager();
        $form = $this->createForm(ArticlesEventType::class, $entity, array(
            'action' => $this->generateUrl('admin_articlesevent_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'er' => $er,
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ArticlesEvent entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ArticlesEvent entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateArticleEvent($entity, $editForm, $id);

            return $this->redirect($this->generateUrl('admin_articlesevent_edit', array('id' => $id)));
        }

        return $this->render('mdtshomeBundle:ArticlesEvent:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a ArticlesEvent entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($request->isMethod('GET')) {
            $srv = $this->get('madatsara.service')->removeArticlesEvent(array($id));
        }

        if (!$request->isMethod('GET') && $form->isValid()) {
            $srv = $this->get('madatsara.service')->removeArticlesEvent($id);
        }
        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'entite a bien été supprimée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression de l\'entite .');
        }

        return $this->redirect($this->generateUrl('admin_articlesevent'));
    }

    public function deleteallAction(Request $request)
    {
        // POST Data param
        $datapost = filter_input_array(INPUT_POST);
        $data = array_key_exists('mdts_homebundle_all', $datapost) ? $datapost['mdts_homebundle_all'] : [];
        $data['action'] = array_key_exists('action', $datapost) ? $datapost['action'] : '';
        $ids = array_key_exists('ids', $data) ? explode(',',$data['ids']) : [];

        if ($request->isMethod('DELETE') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->removeArticlesEvent($data['id']);
        }

        //print_r($data);
        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été supprimées.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression des entités .');
        }

        return $this->redirect($this->generateUrl('admin_articlesevent'));
    }

    /**
     * Creates a form to delete a ArticlesEvent entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_articlesevent_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
