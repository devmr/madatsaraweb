<?php

namespace mdts\homeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Form\NewsletterAbonneType;
use mdts\homeBundle\Entity\NewsletterAbonne;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class NewsletterController extends Controller
{
    public function suscribeAction(Request $request)
    {

        $return = [];
        //Form Newsletter
        $nl = new NewsletterAbonne();
        $form_nl = $this->get('form.factory')->create(NewsletterAbonneType::class, $nl);

        if ($form_nl->handleRequest($request)->isValid()) {
            $this->get('logger')->info(__FUNCTION__.' - Line '.__LINE__.' - form : '.print_r($form_nl->getData(), true));

            //return new Response($nl->getEmail());
            if (!is_null($nl->getEmail())) {
                $return = $this->get('madatsara.service')->createOrUpdateNewsletter($nl);
            }

            if (isset($return['error'])) {
                $msg = 'Votre adresse email est déjà enregistrée.';
            } else {
                $msg = 'Inscription effectuée. Merci pour votre participation.';
            }

            if (is_null($nl->getEmail())) {
                $msg = 'Erreur lors de l\'inscription. Merci de ré-essayer.';
            }

            $request->getSession()->getFlashBag()->add('notice', $msg);

            //Si nouvel inscrit envoi mail
            if (isset($return['ok'])) {
                $mail = \Swift_Message::newInstance();

                $return['infos']['source'] = $nl->getSource();

                $to = '';
                $from = '';
                $default_email = $this->get('madatsara.service')->getParameter('default_email');
                if ( array_key_exists('to', $default_email)) {
                    $to = $default_email['to'];
                    $from = $default_email['from'];
                }
                //Envoyer mail admin
                $mail
                    ->setFrom($from)
                    ->setTo($to)
                    ->setSubject('[MADATSARA] - Nouvelle inscription Newsletter')
                    ->setReplyTo($nl->getEmail())
                    ->setBody($this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'mdtshomeBundle:Email:inscription_newsletter.html.twig',
                        array(

                            'email' => $nl->getEmail(),
                            'infos' => $this->get('madatsara.service')->array2table($return['infos']),
                        )
                    ))
                    ->setContentType('text/html');

                $this->get('mailer')->send($mail);

                //Mail de confirmation
                $mail
                    ->setFrom($from)
                    ->setTo($nl->getEmail())
                    ->setSubject('[MADATSARA] - Inscription newsletter confirmée.')
                    ->setReplyTo('madatsara@gmail.com')
                    ->setBody($this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        'mdtshomeBundle:Email:confirm_inscription_newsletter.html.twig',
                        array(
                            'email' => $nl->getEmail(),
                        )
                    ))
                    ->setContentType('text/html');
                $this->get('mailer')->send($mail);

                //Check mail.log.
                //$date = new \DateTime();
                //exec("cat /var/log/mail.log | grep '".$nl->getEmail()."' | grep '".$date->format('M d')."' | grep -v '@madatsara.com' | grep -v 'rabehasy@gmail.com' | grep -v 'root@ns201506.ip-91-121-132.eu' 2>&1", $output);
            }
        }
        //Cas ou on accède directement à l'URL
        return $this->render('mdtshomeBundle:Newsletter:suscribe.html.twig', array(
            'form_nl' => $form_nl->createView(),
            'return' => $return,
        ));
    }
}
