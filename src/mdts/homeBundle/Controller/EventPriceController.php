<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\EventPrice;
use mdts\homeBundle\Form\EventPriceType;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

/**
 * EventPrice controller.
 */
class EventPriceController extends Controller
{
    /**
     * Lists all EventPrice entities.
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('mdtshomeBundle:EventPrice')->findAll();

        return $this->render('mdtshomeBundle:EventPrice:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new EventPrice entity.
     */
    public function createAction(Request $request)
    {
        $entity = new EventPrice();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_eventprice_show', array('id' => $entity->getId())));
        }

        return $this->render('mdtshomeBundle:EventPrice:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EventPrice entity.
     *
     * @param EventPrice $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EventPrice $entity)
    {
        $form = $this->createForm(new EventPriceType(), $entity, array(
            'action' => $this->generateUrl('admin_eventprice_create'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EventPrice entity.
     */
    public function newAction()
    {
        $entity = new EventPrice();
        $form = $this->createCreateForm($entity);

        return $this->render('mdtshomeBundle:EventPrice:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EventPrice entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventPrice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventPrice entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventPrice:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EventPrice entity.
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventPrice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventPrice entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventPrice:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a EventPrice entity.
     *
     * @param EventPrice $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EventPrice $entity)
    {
        $form = $this->createForm(new EventPriceType(), $entity, array(
            'action' => $this->generateUrl('admin_eventprice_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EventPrice entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventPrice')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventPrice entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_eventprice_edit', array('id' => $id)));
        }

        return $this->render('mdtshomeBundle:EventPrice:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a EventPrice entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('mdtshomeBundle:EventPrice')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find EventPrice entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_eventprice'));
    }

    /**
     * Creates a form to delete a EventPrice entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_eventprice_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
