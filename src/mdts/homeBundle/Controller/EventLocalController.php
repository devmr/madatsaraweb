<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\EventLocal;
use mdts\homeBundle\Entity\searchBackendModel;
use mdts\homeBundle\Form\EventLocalType;
use mdts\homeBundle\Form\checkboxEventlocalType;
use Recurr\Rule;
use Recurr\Transformer\TextTransformer;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
/**
 * EventLocal controller.
 */
class EventLocalController extends Controller
{
    /**
     * Lists all EventLocal entities.
     */
    public function indexAction(Request $request)
    {
        $array_year = array();
        for ($i = 0; $i < 20; ++$i) {
            $array_year[(date('Y') - 5) + $i] = (date('Y') - 5) + $i;
        }
        // Form de recherche
        $smodel = new searchBackendModel();
        $filter_form = $this->createFormBuilder($smodel, array(
            'csrf_protection' => false,
            ))
            //->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('GET')

            ->add('query', TextType::class, array('required' => false))
            /*->add('startdate', 'date', array(
                'placeholder'  => array('year' => 'Année', 'month' => 'Mois', 'day' => 'Jour'),
        'years' => range(date('Y'), date('Y')+20)
        ,'label' => 'Month & year',
        'input' => 'array',
        'widget' => 'choice'
        ,'required' => false
     ))*/
            ->add('month', ChoiceType::class, [
                'placeholder' => 'Choisir',
                 'required' => false,
                'choices' => [
                    '1' => 'jan.',
                    '2' => 'fev.',
                    '3' => 'mar.',
                    '4' => 'avr.',
                    '5' => 'mai.',
                    '6' => 'jui.',
                    '7' => 'juil.',
                    '8' => 'aou.',
                    '9' => 'sep.',
                    '10' => 'oct.',
                    '11' => 'nov.',
                    '12' => 'déc.',
                ], ])
            ->add('year', ChoiceType::class, [
                'placeholder' => 'Choisir',
                 'required' => false,
                'choices' => $array_year, ])

            ->add('limit', HiddenType::class, ['data' => 5] )
            ->add('submit', SubmitType::class, array('label' => 'Chercher'))
            ->getForm();
        $filter_form->handleRequest($request);

        // Form des checkbox
        $idsForm = $form = $this->get('madatsara.service')->createFormAllIds('admin_eventlocal_delete_all')->createView();

        // Param du form de recherche
        $formQuery = $request->query->get('form');
        $query = is_array($formQuery) ? $formQuery : [];
        $limit = is_array($request->query->get('form')) && array_key_exists('limit',$request->query->get('form'))?(int)$request->query->get('form')['limit'] :5;
        $currentPage = $request->query->getInt('page', 1);

        // Get entity doctrine
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('mdtshomeBundle:EventLocal')->findAllQuery($query,  $currentPage, $limit);
        $countRows = $entities->count() ;
        $maxPages = ceil($entities->count() / $limit);

        // Render view
        return $this->render(
            'mdtshomeBundle:EventLocal:index.html.twig',
            array_merge(
                [
                    'entities' => $entities,
                    'idsForm' =>$idsForm,
                    'filter_form' => $filter_form->createView(),
                ],
                compact('countRows','limit', 'maxPages', 'currentPage')
            )
        );
    }
    /**
     * Creates a new EventLocal entity.
     */
    public function createAction(Request $request)
    {
        $entity = new EventLocal();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {

            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventlocal($entity);

            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été créé.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la création de l\'Entité «'.$entity->getName().'».');
            }

            return $this->redirect($this->generateUrl('admin_eventlocal'));
        }

        return $this->render('mdtshomeBundle:EventLocal:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EventLocal entity.
     *
     * @param EventLocal $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EventLocal $entity)
    {
        $form = $this->createForm(EventLocalType::class, $entity, array(
            'action' => $this->generateUrl('admin_eventlocal_create'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EventLocal entity.
     */
    public function newAction()
    {
        $entity = new EventLocal();
        $form = $this->createCreateForm($entity);

        return $this->render('mdtshomeBundle:EventLocal:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EventLocal entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLocal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLocal entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventLocal:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function duplicateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLocal')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLocal entity.');
        }
        //Enregistrer dans BDD
        $srv = $this->get('madatsara.service')->duplicateEventLocal($entity);

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été dupliquée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la copie de l\'Entité «'.$entity->getName().'».');
        }

        return $this->redirect($this->generateUrl('admin_eventlocal'));
    }
    /**
     * Displays a form to edit an existing EventLocal entity.
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLocal')->find($id);

        $rule = new Rule($entity->getRecurrence(), new \DateTime());
        $textTransformer = new TextTransformer(
            new \Recurr\Transformer\Translator('fr')
        );
        $transformerArray = new \Recurr\Transformer\ArrayTransformer();
        //echo $textTransformer->transform($rule);
        //print_r($transformerArray->transform($rule));

        if ($entity->getStartDate() != '') {
            $date = \DateTime::createFromFormat('Y-m-d', $entity->getStartDate());
            $entity->setStartdate($date->format('d/m/Y'));
        }

        if ($entity->getEnddate() != '') {
            $date = \DateTime::createFromFormat('Y-m-d', $entity->getEnddate());
            $entity->setEnddate($date->format('d/m/Y'));
        }

        if ($entity->getStartDateHome() != '') {
            $date = \DateTime::createFromFormat('Y-m-d', $entity->getStartDateHome());
            $entity->setStartDateHome($date->format('d/m/Y'));
        }

        if ($entity->getEnddateHome() != '') {
            $date = \DateTime::createFromFormat('Y-m-d', $entity->getEnddateHome());
            $entity->setEnddateHome($date->format('d/m/Y'));
        }

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLocal entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventLocal:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a EventLocal entity.
     *
     * @param EventLocal $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EventLocal $entity)
    {
        $form = $this->createForm(EventLocalType::class, $entity, array(
            'action' => $this->generateUrl('admin_eventlocal_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EventLocal entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLocal')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLocal entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {

            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventlocal($entity, $id);
            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été mise à jour.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la mise à jour de l\'Entité «'.$entity->getName().'».');
            }

            return $this->redirect($this->generateUrl('admin_eventlocal_edit', array('id' => $id)));
        }

        return $this->render('mdtshomeBundle:EventLocal:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    public function deleteallAction(Request $request)
    {
        // POST Data param
        $datapost = filter_input_array(INPUT_POST);
        $data = array_key_exists('mdts_homebundle_all', $datapost) ? $datapost['mdts_homebundle_all'] : [];
        $data['action'] = array_key_exists('action', $datapost) ? $datapost['action'] : '';
        $ids = array_key_exists('ids', $data) ? explode(',',$data['ids']) : [];

        if ($request->isMethod('DELETE') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->removeallEventlocal($ids);
        }
        if ($request->isMethod('POST') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->copyallEventlocal($ids);
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été supprimées.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression des entités.');
        }

        return $this->redirect($this->generateUrl('admin_eventlocal'));
    }
    /**
     * Deletes a EventLocal entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($request->isMethod('GET')) {
            $srv = $this->get('madatsara.service')->removeEventlocal($id);
        }

        if (!$request->isMethod('GET') && $form->isValid()) {
            $srv = $this->get('madatsara.service')->removeEventlocal($id);
        }

        //Enregistrer dans BDD

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité a bien été supprimée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression de l\'Entité .');
        }

        return $this->redirect($this->generateUrl('admin_eventlocal'));
    }

    /**
     * Creates a form to delete a EventLocal entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
