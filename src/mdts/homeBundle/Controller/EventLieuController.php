<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\EventLieu;
use mdts\homeBundle\Form\EventLieuType;
use mdts\homeBundle\Entity\searchBackendModel;
use mdts\homeBundle\Form\checkboxEventLieuType;
use mdts\homeBundle\Entity\countriesRepository;
use mdts\homeBundle\Entity\regionRepository;
use mdts\homeBundle\Entity\localityRepository;
use mdts\homeBundle\Entity\quartierRepository;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * EventLieu controller.
 */
class EventLieuController extends Controller
{
    /**
     * Lists all EventLieu entities.
     */
    public function indexAction(Request $request)
    {
        // Form de recherche
        $smodel = new searchBackendModel();
        $filter_form = $this->createFormBuilder($smodel, array(
            'csrf_protection' => false,
            ))
            //->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('GET')

            ->add('query', TextType::class, array('required' => false))
            ->add('country', EntityType::class, [
                 'placeholder' => 'Choisir le pays',
                 'multiple' => true,
                 'required' => false,
                 'class' => 'mdts\homeBundle\Entity\countries',
                'choice_label' => 'name',
                'query_builder' => function (countriesRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
            ->add('region', EntityType::class, [
                 'placeholder' => 'Choisir la région',
                 'required' => false,
                 'multiple' => true,
                 'class' => 'mdts\homeBundle\Entity\region',
                'choice_label' => 'name',
                'query_builder' => function (regionRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
             ->add('locality', EntityType::class, [
                 'placeholder' => 'Choisir la ville',
                 'required' => false,
                 'multiple' => true,
                 'class' => 'mdts\homeBundle\Entity\locality',
                'choice_label' => 'name',
                'query_builder' => function (localityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
                ->add('quartier', EntityType::class, [
                 'placeholder' => 'Choisir le quartier',
                 'required' => false,
                 'multiple' => true,
                 'class' => 'mdts\homeBundle\Entity\quartier',
                'choice_label' => 'name',
                'query_builder' => function (quartierRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
                ])
            ->add('limit', HiddenType::class, ['data' => 5] )
            ->add('submit', SubmitType::class, array('label' => 'Chercher'))
            ->getForm();
        $filter_form->handleRequest($request);

        // Form des checkbox
        $idsForm = $form = $this->get('madatsara.service')->createFormAllIds('admin_eventlieu_delete_all')->createView();

        // Param du form de recherche
        $formQuery = $request->query->get('form');
        $query = is_array($formQuery) ? $formQuery : [];
        $limit = is_array($request->query->get('form')) && array_key_exists('limit',$request->query->get('form'))?(int)$request->query->get('form')['limit'] :5;
        $currentPage = $request->query->getInt('page', 1);

        // Get entity doctrine
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('mdtshomeBundle:EventLieu')->findAllQuery($query,  $currentPage, $limit);
        $countRows = $entities->count() ;
        $maxPages = ceil($entities->count() / $limit);

        // Render view
        return $this->render(
            'mdtshomeBundle:EventLieu:index.html.twig',
            array_merge(
                [
                    'entities' => $entities,
                    'idsForm' =>$idsForm,
                    'filter_form' => $filter_form->createView(),
                ],
                compact('countRows','limit', 'maxPages', 'currentPage')
            )
        );
    }
    public function deleteallAction(Request $request)
    {
        // POST Data param
        $datapost = filter_input_array(INPUT_POST);
        $data = array_key_exists('mdts_homebundle_all', $datapost) ? $datapost['mdts_homebundle_all'] : [];
        $data['action'] = array_key_exists('action', $datapost) ? $datapost['action'] : '';
        $ids = array_key_exists('ids', $data) ? explode(',',$data['ids']) : [];

        if ($request->isMethod('DELETE') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->removeallEventLieu($ids);
        }
        if ($request->isMethod('POST') && count($ids) > 0) {
            $srv = $this->get('madatsara.service')->copyallEventLieu($ids);
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été supprimées.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression des entités.');
        }

        return $this->redirect($this->generateUrl('admin_eventlieu'));
    }

    public function duplicateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLieu entity.');
        }
        //Enregistrer dans BDD
        $srv = $this->get('madatsara.service')->duplicateEventLieu($entity);

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été dupliquée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la copie de l\'Entité «'.$entity->getName().'».');
        }

        return $this->redirect($this->generateUrl('admin_eventlieu'));
    }
    /**
     * Creates a new EventLieu entity.
     */
    public function createAction(Request $request)
    {
        $entity = new EventLieu();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventLieu($entity, 0, $form);

            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été créé.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la création de l\'Entité «'.$entity->getName().'».');
            }

            return $this->redirect($this->generateUrl('admin_eventlieu'));
        }

        return $this->render('mdtshomeBundle:EventLieu:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a EventLieu entity.
     *
     * @param EventLieu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(EventLieu $entity)
    {
        $er = $this->getDoctrine()->getManager();

        $form = $this->createForm(EventLieuType::class, $entity, array(
            'action' => $this->generateUrl('admin_eventlieu_create'),
            'method' => 'POST',
            'er' => $er
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new EventLieu entity.
     */
    public function newAction()
    {
        $entity = new EventLieu();
        $form = $this->createCreateForm($entity);

        return $this->render('mdtshomeBundle:EventLieu:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a EventLieu entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLieu entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventLieu:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing EventLieu entity.
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLieu entity.');
        }

        $entitytels = $em->getRepository('mdtshomeBundle:TelsLieu')->getTelsByLieuID($id);
        $telslieu = array();
        $i = 0;
        foreach ($entitytels as $local) {
            $telslieu[$i]['tel'] = $local->getTel();
            ++$i;
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:EventLieu:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(), 'telslieu' => json_encode($telslieu),
        ));
    }

    /**
     * Creates a form to edit a EventLieu entity.
     *
     * @param EventLieu $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EventLieu $entity)
    {
        $er = $this->getDoctrine()->getManager();

        $form = $this->createForm(EventLieuType::class , $entity, array(
            'action' => $this->generateUrl('admin_eventlieu_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'er' => $er,
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing EventLieu entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLieu entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        //print_r ( $editForm->getData() );
        //print_r ( $request-> );exit;

        if ($editForm->isValid()) {
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventLieu($entity, $id, $editForm);
            //Afficher message de confirmation
            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été mise à jour.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la mise à jour de l\'Entité «'.$entity->getName().'».');
            }

            return $this->redirect($this->generateUrl('admin_eventlieu_edit', array('id' => $id)));
        }

        return $this->render('mdtshomeBundle:EventLieu:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a EventLieu entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($request->isMethod('GET')) {
            $srv = $this->get('madatsara.service')->removeEventLieu($id);
        }

        if (!$request->isMethod('GET') && $form->isValid()) {
            $srv = $this->get('madatsara.service')->removeEventLieu($id);
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité a bien été supprimée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression de l\'Entité .');
        }

        return $this->redirect($this->generateUrl('admin_eventlieu'));
    }

    /**
     * Creates a form to delete a EventLieu entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_eventlieu_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
