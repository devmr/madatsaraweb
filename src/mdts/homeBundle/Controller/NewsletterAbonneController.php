<?php

namespace mdts\homeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use mdts\homeBundle\Entity\NewsletterAbonne;
use mdts\homeBundle\Form\NewsletterAbonneType;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

/**
 * NewsletterAbonne controller.
 */
class NewsletterAbonneController extends Controller
{
    /**
     * Lists all NewsletterAbonne entities.
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('mdtshomeBundle:NewsletterAbonne')->findAll();

        return $this->render('mdtshomeBundle:NewsletterAbonne:index.html.twig', array(
            'entities' => $entities,
        ));
    }
    /**
     * Creates a new NewsletterAbonne entity.
     */
    public function createAction(Request $request)
    {
        $entity = new NewsletterAbonne();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('newsletterabonne_show', array('id' => $entity->getId())));
        }

        return $this->render('mdtshomeBundle:NewsletterAbonne:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a NewsletterAbonne entity.
     *
     * @param NewsletterAbonne $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NewsletterAbonne $entity)
    {
        $form = $this->createForm(new NewsletterAbonneType(), $entity, array(
            'action' => $this->generateUrl('newsletterabonne_create'),
            'method' => 'POST',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new NewsletterAbonne entity.
     */
    public function newAction()
    {
        $entity = new NewsletterAbonne();
        $form = $this->createCreateForm($entity);

        return $this->render('mdtshomeBundle:NewsletterAbonne:new.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a NewsletterAbonne entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:NewsletterAbonne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterAbonne entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:NewsletterAbonne:show.html.twig', array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing NewsletterAbonne entity.
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:NewsletterAbonne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterAbonne entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtshomeBundle:NewsletterAbonne:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a NewsletterAbonne entity.
     *
     * @param NewsletterAbonne $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(NewsletterAbonne $entity)
    {
        $form = $this->createForm(new NewsletterAbonneType(), $entity, array(
            'action' => $this->generateUrl('newsletterabonne_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing NewsletterAbonne entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtshomeBundle:NewsletterAbonne')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NewsletterAbonne entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('newsletterabonne_edit', array('id' => $id)));
        }

        return $this->render('mdtshomeBundle:NewsletterAbonne:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Deletes a NewsletterAbonne entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('mdtshomeBundle:NewsletterAbonne')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NewsletterAbonne entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('newsletterabonne'));
    }

    /**
     * Creates a form to delete a NewsletterAbonne entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('newsletterabonne_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
