<?php

namespace mdts\homeBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
//use Symfony\Component\HttpFoundation\Session\Session;
use mdts\homeBundle\Form\NewsletterAbonneType;
use mdts\homeBundle\Entity\NewsletterAbonne;

/*use Recurr;
use Recurr\Rule;
use Recurr\Transformer\TextTransformer;*/

class DefaultController extends Controller
{
    public $repository;

    protected $_madatsaraView;
    protected $_resultSearchForm;

    private $limit = 15;

    /**
     * Set your services here (we need to override this method to initialize our services into class properties).
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->_madatsaraView = $this->get('madatsara.service')->getSearchForm()->createView();

        $this->_resultSearchForm = $this->get('madatsara.service')->searchForm($this->get('madatsara.service')->getSearchForm());
    }

    public function indexAction(Request $request)
    {
        $date = $request->query->get('date');

        $d1 = date('Y-m-d', strtotime('monday this week'));
        $d2 = date('Y-m-d', strtotime('sunday this week'));

        $repository = $this
            ->getDoctrine()
            ->getManager();

        $eventWeekId = $repository->getRepository('mdtshomeBundle:Event')->getAllEventsIDCurrentWeek();

        $countryArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'country',3);
        $regionArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'region',3);
        $localityArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'locality',3);
        $quartierArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'quartier',3);





        $list = $repository->getRepository('mdtshomeBundle:Event')->getCurrentWeekEvent(8);
        $list_nextevent = $repository->getRepository('mdtshomeBundle:Event')->getNextEvent(4, 1, $request);
        $MonthNextEvent = $repository->getRepository('mdtshomeBundle:Event')->getMonthNextEvent($request);

        $list_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(4)->getResult();

        $all_types = $repository->getRepository('mdtshomeBundle:EventType')->findAll();

        $list_local = $repository->getRepository('mdtshomeBundle:Event')->getLocalEvent(0);
        //print_r($list_local);

        $all_local = $repository->getRepository('mdtshomeBundle:EventLocal')->getAllCurrentLocal();

        $ids_arr = $this->get('madatsara.service')->getEventIdArticlesRecents();

        $articlesrecents = $repository->getRepository('mdtshomeBundle:Event')->getArticlesByEventID($ids_arr, 20);

        $lastarticlesrecents = $repository->getRepository('mdtshomeBundle:ArticlesEvent')->getLastArticles(10);
        $event_arr = array();
        foreach ($lastarticlesrecents as $article) {
            //echo $article->getTitle()."<br>";
            $event_arr[$article->getId()] = ['article' => $article, 'events' => $repository->getRepository('mdtshomeBundle:Event')->getEventByArticleId($article->getId())];
        }



        return $this->render('mdtshomeBundle:Default:index.html.twig', array(
            'allevents' => $list_all,
            'events' => $list,
            'nextevents' => $list_nextevent,
            'localevent' => $list_local,
            'limitlocal' => 4,
            'searchForm' => $this->_madatsaraView,
            'all_types' => $all_types,
            'd1' => $d1,
            'd2' => $d2,
            'all_local' => $all_local,
            'MonthNextEvent' => $MonthNextEvent,
            'articlesrecents' => $articlesrecents,
            'articlesrecentsall' => $event_arr

            ,'country' => $countryArr
            ,'region' => $regionArr
            ,'locality' => $localityArr
            ,'quartier' => $quartierArr
            ,'prefixpathurl' => 'weekevent_'
            ,'prefixgaelabel' => 'accueil semaine'
        ));
    }

    public function detaileventAction($slug)
    {

        //Form Newsletter
         $nl = new NewsletterAbonne();
//        $form_nl = $this->get('form.factory')->create(new NewsletterAbonneType(), $nl);
        $form_nl = $this->get('form.factory')->create(NewsletterAbonneType::class, $nl);

        $repository = $this
            ->getDoctrine()
            ->getManager();
        $countdetail = $repository->getRepository('mdtshomeBundle:Event')->getCountDetailEventBySlug($slug);
        if ($countdetail <= 0) {
            throw $this->createNotFoundException('The product does not exist');
        }
        $detail = $repository->getRepository('mdtshomeBundle:Event')->getDetailEventBySlug($slug);
        $event_related_week = $repository->getRepository('mdtshomeBundle:Event')->getRelatedEventWeekExcludeSlug($slug, 3);
        $event_related_month = $repository->getRepository('mdtshomeBundle:Event')->getRelatedEventMonthExcludeSlug($slug, 3);
        $event_related_place = $repository->getRepository('mdtshomeBundle:Event')->getRelatedEventPlaceExcludeSlug($slug, 3);
        $event_related_type = $repository->getRepository('mdtshomeBundle:Event')->getRelatedEventTypeExcludeSlug($slug, 3);

        //print_r($detailheure);

        return $this->render('mdtshomeBundle:Default:detailevent.html.twig', array(
            'related_type' => $event_related_type,
            'related_place' => $event_related_place,
            'related_month' => $event_related_month,
            'related_week' => $event_related_week,
            'event' => $detail,
            'slug' => $slug,
            'form_nl' => $form_nl->createView(),
        ));
    }
    public function alleventlocalAction($slug, $slugplace='', Request $request)
    {

        $prefixpathurl = 'localevent_';
        $list = [];

        // Route courant
        $currentRoute = $request->attributes->get('_route');


        $repository = $this
            ->getDoctrine()
            ->getManager();
        $entity = $repository->getRepository('mdtshomeBundle:EventLocal')->findByOrigSlug($slug);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLocal entity.');
        }

        $eventMonthId = $repository->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromLocal($slug);
// dump($eventMonthId);
        $countryArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'country',0,$slugplace, $prefixpathurl);
        $regionArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'region',0,$slugplace, $prefixpathurl);
        $localityArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'locality',0,$slugplace, $prefixpathurl);
        $quartierArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'quartier',0,$slugplace, $prefixpathurl);





        $query = $repository->getRepository('mdtshomeBundle:Event')->getLocalEventBySlug($slug, 0, $currentRoute,$slugplace);
//        dump($query);
        $paginator = $this->get('knp_paginator');

        if (false!==$query) {
            $list = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                $this->limit/*limit per page*/
            );
        }

        $query_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0);
        $list_all = $paginator->paginate(
            $query_all, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );
        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements.html.twig', array(
            'slug' => $slug,
            'type' => __FUNCTION__,
            'allevents' => array(),
            'events' => $list, 'searchForm' => $this->_madatsaraView,

             'country' => $countryArr
            ,'region' => $regionArr
            ,'locality' => $localityArr
            ,'quartier' => $quartierArr
            ,'prefixpathurl' => $prefixpathurl
            ,'prefixgaelabel' => $slug
            ));
    }

    public function searchAction(Request $request)
    {
        //$session = new Session();
        //$session = $request->getSession();

        //var_dump($page);

        // $this->get('ladybug')->log($request->query->get('mdts_homebundle_search'));

        $queryparam = $request->query->get('mdts_homebundle_search');

        //print_r($articleSearch );

        if (is_array($queryparam) && count($queryparam) > 0) {
            $results = $this->_resultSearchForm;
            // $this->get('ladybug')->log($results);
            $tabid = $results;

            $repository = $this
                ->getDoctrine()
                ->getManager();

            $thisPage = $request->query->getInt('page', 1);
            $limit = 15;
            $entities = $repository->getRepository('mdtshomeBundle:Event')->getAllEventInID($tabid, $limit, false, $thisPage, true);
            $maxPages = ceil($entities->count() / $limit);

            return $this->render('mdtshomeBundle:Default:recherche.html.twig', array_merge(array(
                'type' => __FUNCTION__,
                'events' => $entities,
                'searchForm' => $this->_madatsaraView, '_response' => $this->_resultSearchForm,

            ), compact('limit', 'maxPages', 'thisPage')));
        }

        return $this->render('mdtshomeBundle:Default:recherche.html.twig', array(
                'results' => array(), 'searchForm' => $this->_madatsaraView,
            ));
    }

    public function alleventAction(Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();

        $query = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0, 'now');

        $query->useQueryCache(true);
        $query->useResultCache(true);
        $query->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate(
                $query, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                15/*limit per page*/
            );

        $query_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0);
        $query_all->useQueryCache(true);
        $query_all->useResultCache(true);
        $query_all->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');

        $list_all = $paginator->paginate(
            $query_all, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements.html.twig', array(
            'type' => __FUNCTION__,
            'allevents' => $list_all,
            'events' => $list, 'searchForm' => $this->_madatsaraView, '_response' => $this->_resultSearchForm,
            ));
    }
    public function alleventtafolanitraAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();

        $list = $repository->getRepository('mdtshomeBundle:Event')->getCurrentWeekEvent(0)->getResult();

        $titre = '';

        return $this->render('mdtshomeBundle:Default:event_tafolanitra.html.twig', array('events' => $list));
    }
    public function alleventweekAction($slug='',Request $request)
    {

        $prefixpathurl = 'weekevent_';


        $currentRoute = $request->attributes->get('_route');
//        dump($currentRoute);
//        dump($slug);
//        dump(strstr($currentRoute,'weekevent_'));

        $repository = $this
            ->getDoctrine()
            ->getManager();

        $eventWeekId = $repository->getRepository('mdtshomeBundle:Event')->getAllEventsIDCurrentWeek();

        $countryArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'country',0,$slug, $prefixpathurl);
        $regionArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'region',0,$slug, $prefixpathurl);
        $localityArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'locality',0,$slug, $prefixpathurl);
        $quartierArr = $this->get('madatsara.service')->getListPlaceByEvents($eventWeekId,'quartier',0,$slug, $prefixpathurl);




        $query = $repository->getRepository('mdtshomeBundle:Event')->getCurrentWeekEvent(0, false, $currentRoute,$slug);
        $query->useQueryCache(true);
        $query->useResultCache(true);
        $query->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        $query_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0);
        $query_all->useQueryCache(true);
        $query_all->useResultCache(true);
        $query_all->setResultCacheLifetime(5);

        $list_all = $paginator->paginate(
            $query_all, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements.html.twig', array(
            'type' => __FUNCTION__,
            'allevents' => $list_all,
            'events' => $list, 'searchForm' => $this->_madatsaraView

            ,'country' => $countryArr
            ,'region' => $regionArr
            ,'locality' => $localityArr
            ,'quartier' => $quartierArr
            ,'prefixpathurl' => $prefixpathurl
            ,'prefixgaelabel' => 'semaine'
            ));
    }

    public function alleventmonthAction($slug='', Request $request)
    {
        $prefixpathurl = 'monthevent_';

        // Route courant
        $currentRoute = $request->attributes->get('_route');

        $repository = $this
            ->getDoctrine()
            ->getManager();

        $eventMonthId = $repository->getRepository('mdtshomeBundle:Event')->getAllEventsIDCurrentMonth();

        $countryArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'country',0,$slug, $prefixpathurl);
        $regionArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'region',0,$slug, $prefixpathurl);
        $localityArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'locality',0,$slug, $prefixpathurl);
        $quartierArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'quartier',0,$slug, $prefixpathurl);


        $list = $repository->getRepository('mdtshomeBundle:Event')->getNextEvent(0, 0, $request, 1, false, $currentRoute,$slug);
        $list->useQueryCache(true);
        $list->useResultCache(true);
        $list->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $list, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        $list_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0);
        $MonthNextEvent = $repository->getRepository('mdtshomeBundle:Event')->getMonthNextEvent($request);

        $pagination_all = $paginator->paginate(
            $list_all, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements.html.twig', array(
            'type' => __FUNCTION__,
            'allevents' => $pagination_all,
            'events' => $pagination,
            'searchForm' => $this->_madatsaraView,
            'MonthNextEvent' => $MonthNextEvent

            ,'country' => $countryArr
            ,'region' => $regionArr
            ,'locality' => $localityArr
            ,'quartier' => $quartierArr
            ,'prefixpathurl' => $prefixpathurl
            ,'prefixgaelabel' => 'mois'
            ));
    }
    public function alleventtypeAction($slug, $slugplace='', Request $request)
    {
//        dump($slugplace);
        $prefixpathurl = 'typeevent_';

        // Route courant
        $currentRoute = $request->attributes->get('_route');


        $repository = $this
            ->getDoctrine()
            ->getManager();

        $entity = $repository->getRepository('mdtshomeBundle:EventType')->findByOrigSlug($slug);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventType entity.');
        }

        $eventMonthId = $repository->getRepository('mdtshomeBundle:Event')->getAllEventsIDFromType($slug);
// dump($eventMonthId);
        $countryArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'country',0,$slugplace, $prefixpathurl);
        $regionArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'region',0,$slugplace, $prefixpathurl);
        $localityArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'locality',0,$slugplace, $prefixpathurl);
        $quartierArr = $this->get('madatsara.service')->getListPlaceByEvents($eventMonthId,'quartier',0,$slugplace, $prefixpathurl);


        $query = $repository->getRepository('mdtshomeBundle:Event')->getAllEventByTypeSlug($slug, 0, 0,false, $currentRoute,$slugplace);
        $query->useQueryCache(true);
        $query->useResultCache(true);
        $query->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );
        $query_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0);
        $query_all->useQueryCache(true);
        $query_all->useResultCache(true);
        $query_all->setResultCacheLifetime(5);

        $list_all = $paginator->paginate(
            $query_all, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        $archive_place = $repository->getRepository('mdtshomeBundle:Event')->getAllEventByTypeSlug($slug, 5, 1);

        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements.html.twig', array(
         'archive_place' => $archive_place,
         'slug' => $slug,
         'type' => __FUNCTION__,
         'allevents' => $list_all,
         'events' => $list, 'searchForm' => $this->_madatsaraView

        ,'country' => $countryArr
        ,'region' => $regionArr
        ,'locality' => $localityArr
        ,'quartier' => $quartierArr
        ,'prefixpathurl' => $prefixpathurl
        ,'prefixgaelabel' => $slug

         ));
    }
    public function alleventtypearchivesAction($slug, Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();

        $entity = $repository->getRepository('mdtshomeBundle:EventType')->findByOrigSlug($slug);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventType entity.');
        }

        $query = $repository->getRepository('mdtshomeBundle:Event')->getAllEventByTypeSlug($slug, 0, 1);
        $query->useQueryCache(true);
        $query->useResultCache(true);
        $query->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements_archives.html.twig', array(
            'type' => __FUNCTION__,
            'slug' => $slug,
            'events' => $list, 'searchForm' => $this->_madatsaraView,
            ));
    }
    public function alleventplacearchivesAction($slug, Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();
        $entity = $repository->getRepository('mdtshomeBundle:EventLieu')->findByOrigSlug($slug);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find EventLieu entity.');
        }

        $query = $repository->getRepository('mdtshomeBundle:Event')->getAllEventByPlaceSlug($slug, 0, 1);
        $query->useQueryCache(true);
        $query->useResultCache(true);
        $query->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );

        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements_archives.html.twig', array(
          'slug' => $slug,
           'events' => $list, 'searchForm' => $this->_madatsaraView,
           ));
    }
    public function alleventplaceAction($slug, Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();



        $entity = $repository->getRepository('mdtshomeBundle:EventLieu')->findByOrigSlug($slug);
        if (!$entity || $entity[0]->getId() == 1) {
            throw $this->createNotFoundException('Unable to find EventLieu entity.');
        }

        $query = $repository->getRepository('mdtshomeBundle:Event')->getAllEventByPlaceSlug($slug, 0);
        $query->useQueryCache(true);
        $query->useResultCache(true);
        $query->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );
        $query_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0);
        $query_all->useQueryCache(true);
        $query_all->useResultCache(true);
        $query_all->setResultCacheLifetime(5);

        $list_all = $paginator->paginate(
            $query_all, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $this->limit/*limit per page*/
        );

        //$archive_place = $repository->getRepository('mdtshomeBundle:Event')->getAllEventByPlaceSlug($slug,5, 1)->getResult();
        $archive_place = $repository->getRepository('mdtshomeBundle:Event')->getAllEventByPlaceSlug($slug, 5, 1);

        //print_r($list_local);

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements.html.twig', array(
         'archive_place' => $archive_place,
         'slug' => $slug,
         'type' => __FUNCTION__,
         'allevents' => $list_all,
          'events' => $list, 'searchForm' => $this->_madatsaraView,
          ));
    }
}
