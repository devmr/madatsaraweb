<?php

namespace mdts\homeBundle\Services;

use mdts\homeBundle\Entity\Event;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class EventPostPersistsListener
{
    private $_container;

    public function __construct(Container $serviceContainer)
    {
        $this->_container = $serviceContainer;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        // Si entite n'est pas Event
        if (!$entity instanceof Event) {
            return;
        }

        $id = $entity->getId();
        $slug = $entity->getSlug();
        $this->updateOrigSlugEvent($id, $slug);
    }

    private function updateOrigSlugEvent(int $id, String $slug)
    {

//        $this->_container->get('logger')->info( __FUNCTION__.' - id: '.$id.' - slug: '.$slug );
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);

        $entity->setOrigSlug($slug);

        $em->flush();
    }
}