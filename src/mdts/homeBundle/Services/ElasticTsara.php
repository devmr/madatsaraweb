<?php
/**
 * Created by PhpStorm.
 * User: Miary
 * Date: 20/05/2017
 * Time: 23:18
 */

namespace mdts\homeBundle\Services;

use Elasticsearch\ClientBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class ElasticTsara
{
    //
    var $client = null;
    var $elastic_index = null;
    var $elastic_type = null;
    var $_container = null;
    var $log = null;

    public function __construct(Container $serviceContainer)
    {
        $hosts = [
            'localhost:9200'   // SSL to IP + Port
        ];
        $this->_container = $serviceContainer;

        $this->client = ClientBuilder::create()->setHosts($hosts)->build();
        $this->elastic_index = $this->_container->getParameter('elastic_index');
        $this->elastic_type = $this->_container->getParameter('elastic_type');
        $this->indices = $this->client->indices();
        $this->log = $this->_container->get('logger');
    }

    public function searchByParams($params)
    {
        $response = $this->client->search($params);
        return $response;
    }
    public function getAllIdsBysearch($field,$value)
    {
        $ids = [];
        $response = $this->getAllSearchTextinField($field,$value);
//        echo $this->prettyPrintR($response);
        foreach ($response as $hit) {
            array_push($ids, $hit['_source']['id']);
        }

        return $ids;
    }
    public function searchTextinField($field,$value)
    {
        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'body' => [
                'query' => [
                    'match' => [
                        $field => $value
                    ]
                ]
            ]
        ];
        return $this->searchByParams($params);
    }
    public function getId($id)
    {
        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'id' => $id
        ];
        return $this->client->get($params);
    }

    public function getIndexSettings($indexes)
    {
        $params = ['index' => $indexes];
        $response = $this->indices->getSettings($params);
        return $response;
    }

    public function getMapping($index='', $type='')
    {
        $response = [];

        // Get mappings for all indexes and types
        if ($index=='' && $type=='')
            $response = $this->indices->getMapping();

        // Get mappings for all types in 'my_index'
        if ($index!='' && $type=='') {
            $params = ['index' => $index];
            $response = $this->indices->getMapping($params);
        }


        // Get mappings for all types of 'my_type', regardless of index
        if ($index=='' && $type!='') {
            $params = ['type' => $type ];
            $response = $this->indices->getMapping($params);
        }

        // Get mapping 'my_type' in 'my_index'
        if ($index!='' && $type!='') {
            $params = [
                'index' => $index,
                'type' => $type
            ];
            $response = $this->indices->getMapping($params);
        }
        return $response;
    }

    public function getAllQuery(){

        $indices = $this->getAllIndexCustomElastic();
        $data = array();
        if ( count($indices)<=0)
            return false;

        foreach($indices as $index ) {
            $response = $this->searchByParams(['index' => $index , 'type' => 'custom_elastic' ]);
            $data[] =  $this->getAllValuesByFields('query', $response) ;
        }
        $data_ = array();
        foreach($data as $k => $v) {
            foreach($v as $k=>$vv)
                $data_[] = $vv;
        }
        $data_ = array_unique($data_);
        sort($data_);
        return array_values( $data_ );
    }
    /**
     * Recupérer la valeur du array du type de noeud
     *
     * @param String $nodeName - Nom du noeud
     *
     * @return array|bool
     */
    private function getAllIndexCustomElastic()
    {
        $data = array();
        $keys_data = $this->getAllIndices();
        if( count($keys_data)<=0)
            return false;
        foreach( $keys_data as $v ) {
            if (substr($v,0,15) == 'custom_elastic-')
                $data[] = $v;
        }
        return array_reverse( $data );
    }

    private function getAllValuesByFields($field, $response = array()) {
        if ( count($response )<=0 )
            return false;

        $hits = $response['hits'];
        if ( !is_array($hits) || count( $hits )<=0 )
            return false;

        $total = $hits['total'];
        $data = $hits['hits'];

        if ( $total <=0 || count($data)<=0)
            return false;

        $data_val = array();
        foreach($data as $row ) {
            foreach($row['_source'] as $k => $v ) {
                if ($k == $field) {
                    $data_val[] =  $v ;
                }
            }
        }

        return array_unique(array_values($data_val));
    }

    public function getAllIndices()
    {
        $keys_data = array_keys( $this->indices->stats()['indices'] );
        return $keys_data;
    }


    public function updateDocument($id,$arrFields)
    {
        $response = [];
        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'id' => $id,
            'body' => $arrFields
        ];

        // Document will be indexed to my_index/my_type/my_id
        $response = $this->client->update($params);
        return $response;
    }

    public function indexDocument($id,$arrFields)
    {

        unset($arrFields['_articles_event']);
        $i = 0;
        foreach ($arrFields['_event_artistes_dj_organisateurs'] as $artistes) {
            // Remove _articles_event
            unset($arrFields['_event_artistes_dj_organisateurs'][$i]['_articles_event']);


            $j = 0;
            foreach ($artistes['parent_artiste'] as $parents) {
                unset($arrFields['_event_artistes_dj_organisateurs'][$i]['parent_artiste'][$j]['_articles_event']);
                $j++;
            }

            $i++;

        }


        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'id' => $id,
            'body' => $arrFields
        ];

        // Document will be indexed to my_index/my_type/my_id
        $response = $this->client->index($params);
        return $response;
    }



    public function deleteIndex($indexName='')
    {
        $response = [];
        if ($indexName=='') {
            $indexName = $this->elastic_index;
        }

        $params = [
            'index' => $indexName
        ];
        /*var_dump($indexName);
        var_dump($this->isIndexExists($indexName));*/
        if ($this->isIndexExists($indexName))
            $response = $this->indices->delete($params);



        return $response;
    }

    public function isIndexExists($indexName='')
    {
        if ($indexName=='') {
            $indexName = $this->elastic_index;
        }

        $params = [
            'index' => $indexName
        ];
        return $this->indices->exists($params);
    }

    public function createIndexAutocomplete($indexName)
    {
        $arrSettings = [
            'analysis' => [
                'filter' => [
                    'title_ngram' => [
                        'type' => 'nGram',
                        'min_gram' => 3,
                        'max_gram' => 5
                    ]
                ],
                'analyzer' => [
                    'autocomplete_analyzer' => [
                        'filter' => [
                            'asciifolding',
                            'title_ngram'
                        ],
                        'type' => 'custom',
                        'tokenizer' => 'lowercase',
                    ]
                ] ,
            ],
            'number_of_shards' => 5,
            'number_of_replicas' => 1

        ];


        $arrMappings = [
            $this->elastic_type => [
                'properties' => [
                    'name' => [
                        'type' => 'string',
                        'analyzer' => 'autocomplete_analyzer',
                        'boost' => 10
                    ]
                ]
            ]
        ];

        $params = [
            'index' => $indexName,
            'body' => [
                'settings' => $arrSettings
                ,'mappings' => $arrMappings
            ]
        ];

        $response = $this->indices->create($params);
        return $response;
    }

    public function prettyPrintR($arr)
    {
        $html = '<pre>';
        $html .= print_r($arr,true);
        $html .= '</pre>';

        echo $html;
    }

    public function bulkIndex($params)
    {
        $this->client->bulk($params);
    }

    public function getAllSearchTextinField($field, $value, $fieldReturn=[])
    {
        // Escape * char
        $value = str_replace('*','\*',$value);

        // Lower
        $value = mb_strtolower($value);

        $params =  [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'body' => [
                'query' => [
                    'wildcard' => [
                        $field => '*' . $value . '*'
                    ]
                ],
                'sort' => [
                    'date_unique' => [
                        'order' => 'asc',
                    ],
                ]
            ]
        ];

        $response = $this->searchTextinField($field,$value);

        $total = $response['hits']['total'];

        if ($total>10) {
            $params['from'] = 0;
            $params['size'] = $total;
        }


        $response = $this->searchByParams($params);

        $ids = [];
        $hits = $response['hits'];
        $response = $hits['hits'];
//        echo $this->prettyPrintR($response);
        if (count($fieldReturn)<=0)
            return $response;

        $field = [];
        foreach ($response as $row) {
            if (array_key_exists('_source', $row) ) {
                $field[] = $this->arrayPush($field, $row['_source'], $fieldReturn);
            }
        }
        return $field;

    }

    private function arrayPush($fieldOrig, $rowSrc, $fieldReturn)
    {
        $data = [];
        foreach ($fieldReturn as $field ) {
            if (array_key_exists($field, $rowSrc)) {
                $data[$field] =  $rowSrc[$field] ;
            }

        }
        /*var_dump($fieldReturn);
        print_r($rowSrc);exit;*/
        return $data;
    }
    public function documentExists($id)
    {
        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'id' => $id,
            'client' => [ 'ignore' => [400, 404] ]
        ];
        $response = $this->client->getSource($params);
        if (!$response)
            return false;

        $this->log->info(__FUNCTION__.' - '.print_r($response,true));
        
        return (array_key_exists('id', $response) && $response['id']==$id);
    }
    public function removeDocument($id)
    {
        $docExists = $this->documentExists($id);
        if (!$docExists)
            return false;

        $params = [
            'index' => $this->elastic_index,
            'type' => $this->elastic_type,
            'id' => $id,
        ];


        $response = $this->client->delete($params);
        return $response;
    }

    public function getQueryCustomElastic($indexName, $typeName)
    {

        $client = ClientBuilder::create()->build();

        // Recupérer la valeur du array du type de noeud
        $indices = $this->getAllIndexCustomElastic($indexName);

        $data = array();
        if (count($indices) <= 0) {
            return false;
        }



        foreach ($indices as $index) {
            $response = $this->getAllResponseByIndex($index, $typeName);
            $data[] = $this->getAllValuesByFields('query', $response);
        }
        $data_ = array();
        foreach ($data as $k => $v) {
            foreach ($v as $k => $vv) {
                $data_[] = $vv;
            }
        }
        $data_ = array_unique($data_);
        sort($data_);

        return array_values($data_);
    }

    public function getAllResponseByIndex($index, $type = 'custom_elastic')
    {
        $nb = $this->getHitsResponseByIndex($index, $type);
        $response = $this->searchByParams(['index' => $index, 'type' => $type, 'from' => 0, 'size' => $nb]);

        return  $response;
    }
    public function getHitsResponseByIndex($index, $type = 'custom_elastic')
    {

        $response = $this->getResponseByIndex($index, $type);
        return  $response['hits']['total'];
    }
    public function getResponseByIndex($index, $type = 'custom_elastic', $limit = 10, $offset = 0)
    {
        $response = $this->searchByParams(['index' => $index, 'type' => $type, 'from' => $offset, 'size' => $limit]);

        return  $response;
    }
}