<?php

namespace mdts\homeBundle\Services;

use mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType;
use mdts\FrontendBundle\Entity\HistoryEventbymemberSent;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use mdts\homeBundle\Form\SearchType;
use mdts\homeBundle\Entity\Search;
use mdts\homeBundle\Entity\EventLocal;
use mdts\homeBundle\Entity\Devises;
use mdts\homeBundle\Entity\EntreeType;
use mdts\homeBundle\Entity\EventType;
use mdts\homeBundle\Entity\EventLieu;
use mdts\homeBundle\Entity\Event;
use mdts\homeBundle\Entity\countries;
use mdts\homeBundle\Entity\quartier;
use mdts\homeBundle\Entity\locality;
use mdts\homeBundle\Entity\region;
use mdts\homeBundle\Entity\EventArtistesDjOrganisateurs;
use mdts\homeBundle\Entity\EventFlyers;
use mdts\homeBundle\Entity\EventVideos;
use mdts\homeBundle\Entity\EventPrice;
use mdts\homeBundle\Entity\EventDate;
use mdts\homeBundle\Entity\TelsLieu;
use mdts\homeBundle\Entity\ArticlesEvent;
use mdts\homeBundle\Entity\NewsletterAbonne;
use mdts\FrontendBundle\Entity\EventByMember;
use mdts\FrontendBundle\Entity\registrationid;
use Symfony\Component\Filesystem\Filesystem;
use Recurr\Rule;
use Recurr\Transformer\TextTransformer;
use GeoIp2\Database\Reader;
use Udger\ParserFactory;
use HTMLPurifier;
use mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoType;

// Request service
use Symfony\Component\HttpFoundation\RequestStack;

// Form
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

// Process
use Symfony\Component\Process\Process;

class Madatsara
{
    private $_container;
    private $logger;
    private $em;
    private $requestStack;
    private $ipAddr;

    // notice that we're injecting service-container to be able to get other symfony2 services
    public function __construct(Container $serviceContainer, RequestStack $requestStack)
    {
        $this->_container = $serviceContainer;
        $this->logger = $this->_container->get('logger');
        $this->em = $this->_container->get('doctrine.orm.entity_manager');
        $this->requestStack = $requestStack->getCurrentRequest();


    }

    public function getHeaders(String $str)
    {
        if (''==$str)
            return '';

        if (!$this->requestStack->headers->has($str))
            return '';

        $str = $this->requestStack->headers->get($str);
        return $str;
    }

    public function getIpAddress()
    {
        $ip = $this->requestStack->getClientIp();

        // Si on est en local
        if ('::1'==$ip)
            $ip = '82.230.218.34';

        $this->ipAddr = $ip;
        return $this->ipAddr;
    }

    public function isUrlVideoType($url)
    {
        $host = parse_url($url, PHP_URL_HOST);
            //var_dump(urldecode($value));exit();
            $list = implode(',', array(
                'www.youtube.com', 'youtube.com', 'm.youtube.com', 'youtu.be', 'www.dailymotion.com', 'dailymotion.com', 'm.dailymotion.com', 'facebook.com', 'www.facebook.com',
            ));
           //return true if field value is foo
           return strpos(','.$list.',', ','.$host.',') !== false;
    }
    public function old__date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d')
    {
        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {
            $dates[] = date($output_format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    public function date_range($fromdate, $todate)
    {
        $fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
        $todate = \DateTime::createFromFormat('Y-m-d', $todate);

        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }
    public function dateRange($fromdate, $todate)
    {
        $tab_dates = array();
        $tabrange = $this->date_range($fromdate, $todate);
        foreach ($tabrange    as $date) {
            $tab_dates[] = $date->format('Y-m-d');
        }

        return $tab_dates;
    }
    public function getSearchForm()
    {
        // Init ipaddr
        $this->getIpAddress();

//        $request = $this->_container->get('request');
        $queryparam = $this->requestStack->query->get('mdts_homebundle_search');

        //$session = new Session();

        /*$routeName = $request->get('_route');
        if ( $routeName != 'mdts_searchevent'  )
           $session->clear();     */

        $sData = new Search();
        if (!is_null($queryparam)) {
            if ($this->isEnvironnementDev()) {
                // $this->_container->get('ladybug')->log($queryparam);
            }
            $sData->setDateFrom($queryparam['date_from']);
            //$sData->setDateTo($queryparam['date_to']);
            $sData->setQuery($queryparam['query']);
        }

        $sType = new SearchType();
        // $sForm = $this->_container->get('form.factory')->create($sType, $sData);
        $sForm = $this->_container->get('form.factory')->create(SearchType::class, $sData);

        $sForm->handleRequest($this->requestStack);

        //print_r($sData);

        /*if ($sData->getDateFrom()!='' || $sData->getDateTo()!='' || $sData->getQuery()!='' )
            $session->clear();

        if ( $sData->getQuery() ) {
            $session->set('query', $sData->getQuery() );
        }


        if ( $sData->getDateFrom() ) {

            $session->set('date_from', $sData->getDateFrom() );
        }

        if ( $sData->getDateTo() ) {
            $session->set('date_to', $sData->getDateTo() );
        } */

        //echo __LINE__.__FUNCTION__.' - '.$sData->getDateFrom().' - '.$sData->getDateTo().' - '.$sData->getQuery()."\n ";
        //echo '-----------Session all------'."\n".print_r($session->all(),true)."\n-----/Session all------\n";

        if (!is_null($sData->getQuery()) && '' != $sData->getQuery() || '' != $sData->getDateFrom() || '' != $sData->getDateTo()) {
            $this->logger->info('SEARCHINFO -- IP='.$this->ipAddr.'|date='.date('d-m-Y H:i:s').'|query=::'.$sData->getQuery().'::|date_from='.$sData->getDateFrom().'|date_to='.$sData->getDateTo());
        }

        //return $sData;

         /*if ($sData->getDateFrom()=='' && $sData->getDateTo()=='' && $sData->getQuery()=='' && ($session->get('query')!='' || $session->get('date_from')!='' || $session->get('date_to')!='' ))
            $sForm->setData( $sData->setQuery($session->get('query'))->setDateFrom($session->get('date_from'))->setDateTo($session->get('date_to')) );
        */

        return $sForm;
    }
    /**
     * Sends email if form is valid.
     *
     * @return null|RedirectResponse
     */
    public function searchForm($sForm)
    {

            $data = $sForm->getData();

            //// $this->_container->get('ladybug')->log($data); //Ladybug dans profiler

            //  print_r($data);
            $d1_start = $d1_end = $d2_start = $d2_end = '';
            //Si date debut rempli
            if ($data->date_from != '' && $data->date_from != '__/__/____') {
                $d1_start = $this->convertDate($data->date_from);
                $d1_end = $this->convertDate($data->date_from, 1);
            }
            //Si date fin rempli
            if ($data->date_to != '' && $data->date_to != '__/__/____') {
                $d2_start = $this->convertDate($data->date_to);
                $d2_end = $this->convertDate($data->date_to, 1);
            }

            //Si date fin vide et date debut non vide
            if ('' !== $d1_start && '' !== $d1_end && '' === $d2_start && '' === $d2_end) {
                $d2_start = $d1_start;
                $d2_end = $d1_end;
                $d1_start = '';
                $d1_end = '';
            }

            //Si les dates sont identiques
            if ($d2_start === $d1_start && $d2_end === $d1_end) {
                $d2_start = $d1_start;
                $d2_end = $d1_end;
                $d1_start = '';
                $d1_end = '';
            }

            //Si la date debut > date de fin ou inversement
            /*var_dump( \DateTime::createFromFormat('d/m/Y', $data->date_from )->format('U') );
            var_dump( \DateTime::createFromFormat('d/m/Y', $data->date_to )->format('U') );*/

            if ($data->date_from != '' && $data->date_from != '__/__/____' && $data->date_to != '' && $data->date_to != '__/__/____' && \DateTime::createFromFormat('d/m/Y', $data->date_from)->format('U') > \DateTime::createFromFormat('d/m/Y', $data->date_to)->format('U')) {
                $d1_start = \DateTime::createFromFormat('d/m/Y', $data->date_to)->format('Y-m-d\T').'00:00:00Z';
                $d1_end = \DateTime::createFromFormat('d/m/Y', $data->date_to)->format('Y-m-d\T').'23:59:59Z';
                $d2_start = \DateTime::createFromFormat('d/m/Y', $data->date_from)->format('Y-m-d\T').'00:00:00Z';
                $d2_end = \DateTime::createFromFormat('d/m/Y', $data->date_from)->format('Y-m-d\T').'23:59:59Z';
            }

        $dates = array(
                'd1s' => $d1_start, 'd1e' => $d1_end, 'd2s' => $d2_start, 'd2e' => $d2_end,
                );
        if ($this->isEnvironnementDev()) {
            // $this->_container->get('ladybug')->log($dates);
            // $this->_container->get('ladybug')->log($data->query); //Ladybug dans profiler
        } //Ladybug dans profiler
            // print_r($dates);
            $tab_ids = $tab_ids_date = array();
        if (!is_null($data->query) && '' !== $data->query && strlen($data->query) <= 25) {
            $tab_ids = $this->searchEventByQuery($data->query);
            if ($this->isEnvironnementDev()) {
//                 $this->_container->get('ladybug')->log($tab_ids);
//                dump($tab_ids);
            }
            // $this->logger->info('line '.__LINE__.' : '.print_r($tab_ids, true));
        }
        $tab_dates = array();
        if ($dates['d1s'] != '' && $dates['d2s'] != '') {
            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d1s']);
            $date1 = new \DateTime($d->format('Y-m-d'));

            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d2e']);
            $date2 = new \DateTime($d->format('Y-m-d'));

            $tabrange = $this->date_range($date1->format('Y-m-d'), $date2->format('Y-m-d'));
            $tab_dates = array($date1->format('Y-m-d'), $date2->format('Y-m-d'));
        } elseif ($dates['d1s'] == '' && $dates['d2s'] != '') {
            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d2s']);
            $date = new \DateTime($d->format('Y-m-d'));
            $tab_dates = array($date->format('Y-m-d'), $date->format('Y-m-d'));
        }

        if (count($tab_dates) > 0) {
            $tab_ids_date = $this->_container->get('doctrine.orm.entity_manager')
                        ->getRepository('mdtshomeBundle:Event')
                        ->getAllEventsIDFromDates($tab_dates);
            if ($this->isEnvironnementDev()) {
                // $this->_container->get('ladybug')->log($tab_ids_date);
            }
            // $this->// info('line '.__LINE__.' : '.print_r($tab_ids_date, true));
        }
        if ($this->isEnvironnementDev()) {
            // $this->_container->get('ladybug')->log($tab_dates);
        }

        if (count($tab_ids) > 0 && count($tab_ids_date) > 0) {
            $tab_ids = array_intersect($tab_ids, $tab_ids_date);
        } elseif (count($tab_ids) <= 0 && count($tab_ids_date) > 0) {
            $tab_ids = $tab_ids_date;
        }
        // $this->  info('line '.__LINE__.' : '.print_r($tab_ids, true));

            //return $elasticaManager->getRepository('mdtshomeBundle:Eventindex')->findES($data->query, $dates );

            //print_r($results);

        //}

        return $tab_ids;
    }

    public function convertDate($date, $end = 0)
    {
        if ($date != '' && $this->isValidDateTimeString($date, 'd/m/Y', 'Europe/Paris')) {
            $d = \DateTime::createFromFormat('d/m/Y', $date);

            return  $d->format('Y-m-d\T').($end > 0 ? '23:59:59Z' : '00:00:00Z');
        }

        return '';
    }

    public function isValidDateTimeString($str_dt, $str_dateformat, $str_timezone)
    {
        if (!$this->isValidTimezone($str_timezone)) {
            return false;
        }

        $date = \DateTime::createFromFormat($str_dateformat, $str_dt, new \DateTimeZone($str_timezone));

        return $date && \DateTime::getLastErrors()['warning_count'] == 0 && \DateTime::getLastErrors()['error_count'] == 0;
    }

    public function isValidTimezone($timezone)
    {
        return in_array($timezone, timezone_identifiers_list());
    }
    public function removeallEventlocal($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventLocal')->find($id);
            $em->remove($entity);
        }
        $em->flush();

        return true;
    }
    public function removeEventlocal($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:EventLocal')->find($id);

        if (!$entity) {
            return false;
        }

        $this->removeAssocLocal($entity, $em);

        $em->remove($entity);
        $em->flush();

        return true;
    }
    public function createOrUpdateEventlocal(EventLocal $entity, $id = 0)
    {
        $regexpdateSaisie = '/^\d{2}\/\d{2}\/\d{4}$/'; //d/m/Y
        $regexpdate = '/^\d{4}-\d{2}-\d{2}$/'; //Y-m-d
        $defaultFormat = 'j-m-Y';

        $startdate = $enddate = '';
        $startdateHome = $enddateHome = '';
        /*ld($entity->getStartdate());
        ld($entity->getEnddate());*/

        if ($entity->getStartdate() != '' && preg_match($regexpdateSaisie, $entity->getStartdate())) {
            $startdate = $entity->getStartdate();
            $date = \DateTime::createFromFormat('d/m/Y', $startdate);
            $startdate = $date->format('Y-m-d');
            //$entity->setStartdate( $date->format('Y-m-d') );
        }
        if ($entity->getEnddate() != '' && preg_match($regexpdateSaisie, $entity->getEnddate())) {
            $enddate = $entity->getEnddate();
            $date = \DateTime::createFromFormat('d/m/Y', $enddate);
            $enddate = $date->format('Y-m-d');
            //$entity->setEnddate( $date->format('Y-m-d') );
        }

        if ($startdate != '' && $enddate == '' && preg_match($regexpdate, $startdate)) {
            $enddate = $startdate;
        } elseif ($startdate == '' && $enddate != '' && preg_match($regexpdate, $enddate)) {
            $startdate = $enddate;
        }

        /* ld($startdate);
        ld($enddate);
        exit;*/

        $entity->setStartdate(null);
        $entity->setEnddate(null);

        if ($startdate != '' && preg_match($regexpdate, $startdate)) {
            $entity->setStartdate($startdate);
        }
        if ($enddate != '' && preg_match($regexpdate, $enddate)) {
            $entity->setEnddate($enddate);
        }

        if ($entity->getStartdateHome() != '' && preg_match($regexpdateSaisie, $entity->getStartdateHome())) {
            $startdateHome = $entity->getStartdateHome();
            $date = \DateTime::createFromFormat('d/m/Y', $startdateHome);
            $startdateHome = $date->format('Y-m-d');
            //$entity->setStartdate( $date->format('Y-m-d') );
        }
        if ($entity->getEnddateHome() != '' && preg_match($regexpdateSaisie, $entity->getEnddateHome())) {
            $enddateHome = $entity->getEnddateHome();
            $date = \DateTime::createFromFormat('d/m/Y', $enddateHome);
            $enddateHome = $date->format('Y-m-d');
            //$entity->setEnddate( $date->format('Y-m-d') );
        }

        if ($startdateHome != '' && $enddateHome == '' && preg_match($regexpdate, $startdateHome)) {
            $enddateHome = $startdateHome;
        } elseif ($startdateHome == '' && $enddateHome != '' && preg_match($regexpdate, $enddateHome)) {
            $startdateHome = $enddateHome;
        }

        /* ld($startdate);
        ld($enddate);
        exit;*/

        $entity->setStartdateHome(null);
        $entity->setEnddateHome(null);

        if ($startdateHome != '' && preg_match($regexpdate, $startdateHome)) {
            $entity->setStartdateHome($startdateHome);
        }
        if ($enddate != '' && preg_match($regexpdate, $enddateHome)) {
            $entity->setEnddateHome($enddateHome);
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');

        if ($id > 0) {
            //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' removeAssocEvent - id > 0 : '.$id );
            $this->removeAssocLocal($entity, $em);
        }

        /*ld($entity->getStartdate());
        ld($entity->getEnddate());*/

        if ($startdate != '' && $enddate != '') {
            $tabrange = $this->date_range($startdate, $enddate);

            //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' tabrange : '.print_r($tabrange,true));

            foreach ($tabrange    as $date) {
                $tab_dates[] = $date->format('Y-m-d');
                //$date = $date->format('Y-m-d');
                $entitydate = $em->getRepository('mdtshomeBundle:EventDate')->findByDate($date);
                if (!$entitydate) {

                    //Creer
                    $entitydate = new EventDate();
                    $entitydate->setDate($date);
                    $em->persist($entitydate);

                    //echo 'ID non trouvé - créé avec ID : '.$entitydate->getId().' - '.$date->format('Y-m-d').PHP_EOL;
                } else {
                    $entitydate = $entitydate[0];
                }

                //Attacher entity à la date

                $entity->addLocalByDate($entitydate);
            }
        }

        //Si rrule existe
        if ($entity->getRecurrence()) {

            //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' RRULE : '.$entity->getRrule());

            //echo $entity->getRrule();

            $rule = new rule($entity->getRecurrence(), new \DateTime());
            $textTransformer = new TextTransformer(
                new \Recurr\Transformer\Translator('fr')
            );
            $transformer = new \Recurr\Transformer\ArrayTransformer();
            //echo $textTransformer->transform($rule);
            //print_r($transformer->transform($rule));
            foreach ($transformer->transform($rule) as $row) {
                if (!in_array($row->getStart()->format('Y-m-d'), $tab_dates)) {
                    $tab_dates[] = $row->getStart()->format('Y-m-d');
                    //echo $row->getStart()->format('Y-m-d')."\n";
                    //exit;
                    $entitydate = $em->getRepository('mdtshomeBundle:EventDate')->findByDate($row->getStart());
                    if (!$entitydate) {

                        //Creer
                        $entitydate = new EventDate();
                        $entitydate->setDate($row->getStart());
                        $em->persist($entitydate);

                        //echo 'ID non trouvé - créé avec ID : '.$entitydate->getId().' - '.$row->getStart()->format('Y-m-d').PHP_EOL;
                    } else {
                        $entitydate = $entitydate[0];
                    }

                    $entity->addLocalByDate($entitydate);
                }
            }
        }

        if ($startdateHome != '' && $enddateHome != '') {
            $tabrange = $this->date_range($startdateHome, $enddateHome);

            //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' tabrange : '.print_r($tabrange,true));

            foreach ($tabrange    as $date) {
                $tab_dates[] = $date->format('Y-m-d');
                //$date = $date->format('Y-m-d');
                $entitydate = $em->getRepository('mdtshomeBundle:EventDate')->findByDate($date);
                if (!$entitydate) {

                    //Creer
                    $entitydate = new EventDate();
                    $entitydate->setDate($date);
                    $em->persist($entitydate);

                    //echo 'ID non trouvé - créé avec ID : '.$entitydate->getId().' - '.$date->format('Y-m-d').PHP_EOL;
                } else {
                    $entitydate = $entitydate[0];
                }

                //Attacher entity à la date

                $entity->addLocalByDateHome($entitydate);
            }
        }

        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        //Si new
        if ($id <= 0) {
            $entity->setOrigSlug($entity->getSlug());
            $em->flush();
        }

        $this->enableHomeEvent();
        $this->enableHomeEventLocal();

        return true;
    }
    public function getNextEventTypeSlug($slug)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $em->getFilters()->disable('softdeleteable');
        $res = $em->getRepository('mdtshomeBundle:EventType')->createQueryBuilder('p')
       ->where('p.slug LIKE :slug')->setParameter('slug', '%'.$slug.'%')
       ->getQuery()->getResult();
        $em->getFilters()->enable('softdeleteable');

        return $slug.'-'.(count($res) + 1);
    }
    public function getNextEventLocalSlug($slug)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $em->getFilters()->disable('softdeleteable');
        $res = $em->getRepository('mdtshomeBundle:EventLocal')->createQueryBuilder('p')
       ->where('p.slug LIKE :slug')->setParameter('slug', '%'.$slug.'%')
       ->getQuery()->getResult();
        $em->getFilters()->enable('softdeleteable');

        return $slug.'-'.(count($res) + 1);
    }
    public function copyallEventlocal($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventLocal')->find($id);
            $this->duplicateEventLocal($entity);
            /*$new_entity = clone $entity;
            $new_entity->setSlug( $this->getNextEventLocalSlug( $new_entity->getSlug() ) );
            $em->persist($new_entity);*/
        }
        //$em->flush();
        return true;
    }
    public function duplicateEventLocal(EventLocal $entity)
    {
        $new_entity = clone $entity;

        $newSlug = $this->getNextEventLocalSlug($new_entity->getSlug());
        $new_entity->setSlug($newSlug);
        $new_entity->setOrigSlug($newSlug);

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($new_entity);
        $em->flush();

        return true;
    }

    public function removeallDevises($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:Devises')->find($id);
            $em->remove($entity);
        }
        $em->flush();

        return true;
    }
    public function removeDevises($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:Devises')->find($id);

        if (!$entity) {
            return false;
        }
        $em->remove($entity);
        $em->flush();

        return true;
    }
    public function createOrUpdateDevises(Devises $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    public function createOrUpdateCountry(countries $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    public function createOrUpdateRegion(region $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    public function createOrUpdateLocality(locality $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    public function createOrUpdateQuartier(quartier $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    public function copyallDevises($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:Devises')->find($id);
            $this->duplicateDevises($entity);
            /*$new_entity = new Devises();
            $new_entity->setInitiales( $entity->getInitiales() );

            $em->persist($new_entity);*/
        }
        $em->flush();

        return true;
    }
    public function duplicateDevises(Devises $entity)
    {
        /*$deepCopy = new DeepCopy();
        $new_entity = $deepCopy->copy($entity);*/

        //$new_entity = clone $entity;

        $new_entity = new Devises();
        $new_entity->setInitiales($entity->getInitiales());
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($new_entity);
        $em->flush();

        return true;
    }
    //Entreetype
    public function removeallEntreetype($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EntreeType')->find($id);
            $em->remove($entity);
        }
        $em->flush();

        return true;
    }
    public function removeEntreetype($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:EntreeType')->find($id);

        if (!$entity) {
            return false;
        }
        $em->remove($entity);
        $em->flush();

        return true;
    }
    public function createOrUpdateEntreetype(EntreeType $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    public function copyallEntreetype($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EntreeType')->find($id);
            $this->duplicateEntreetype($entity);
            /*$new_entity = new EntreeType();
            $new_entity->setName( $entity->getName() );

            $em->persist($new_entity);*/
        }
        $em->flush();

        return true;
    }
    public function duplicateEntreetype(EntreeType $entity)
    {
        /*$deepCopy = new DeepCopy();
        $new_entity = $deepCopy->copy($entity);*/

        //$new_entity = clone $entity;

        $new_entity = new EntreeType();
        $new_entity->setName($entity->getName());
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($new_entity);
        $em->flush();

        return true;
    }
    //EventType
    public function removeallEventType($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventType')->find($id);
            $em->remove($entity);
        }
        $em->flush();

        return true;
    }
    public function removeEventType($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:EventType')->find($id);
        // echo $id;exit;echo' - '.$entity->getName();exit;

        if (!$entity) {
            return false;
        }
        $em->remove($entity);
        $em->flush();

        return true;
    }
    public function createOrUpdateEventType(EventType $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        //Si new
        if ($id <= 0) {
            $entity->setOrigSlug($entity->getSlug());
            $em->flush();
        }

        return true;
    }

    public function copyallEventType($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventType')->find($id);
            $this->duplicateEventType($entity);

           /* $new_entity = clone $entity;
            $new_entity->setSlug( $this->getNextEventTypeSlug( $new_entity->getSlug() ) );
            $em->persist($new_entity); */
        }
        //$em->flush();
        return true;
    }
    public function duplicateEventType(EventType $entity)
    {
        /*$deepCopy = new DeepCopy();
        $new_entity = $deepCopy->copy($entity);*/

        //$new_entity = clone $entity;

        $new_entity = clone $entity;
        $newSlug = $this->getNextEventTypeSlug($new_entity->getSlug());
        $new_entity->setSlug($newSlug);
        $new_entity->setOrigSlug($newSlug);

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($new_entity);
        $em->flush();

        return true;
    }
    public function getNextEventSlug($slug)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $em->getFilters()->disable('softdeleteable');
        $res = $em->getRepository('mdtshomeBundle:Event')->createQueryBuilder('p')
       ->where('p.slug LIKE :slug')->setParameter('slug', '%'.$slug.'%')
       ->getQuery()->getResult();
        $em->getFilters()->enable('softdeleteable');

        return $slug.'-'.(count($res) + 1);
    }
    //EventLieu
    public function getNextEventLieuSlug($slug)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $em->getFilters()->disable('softdeleteable');
        $res = $em->getRepository('mdtshomeBundle:EventLieu')->createQueryBuilder('p')
       ->where('p.slug LIKE :slug')->setParameter('slug', '%'.$slug.'%')
       ->getQuery()->getResult();
        $em->getFilters()->enable('softdeleteable');

        return $slug.'-'.(count($res) + 1);
    }
    //EventLieu
    public function getNextEventArtistesDjOrganisateursSlug($slug)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $em->getFilters()->disable('softdeleteable');
        $res = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->createQueryBuilder('p')
       ->where('p.slug LIKE :slug')->setParameter('slug', '%'.$slug.'%')
       ->getQuery()->getResult();
        $em->getFilters()->enable('softdeleteable');

        return $slug.'-'.(count($res) + 1);
    }

    public function removeallEventLieu($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);
            $em->remove($entity);
        }
        $em->flush();

        return true;
    }

    public function removeallEventByMember($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }



        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $this->removeHistoryEventbymemberSent([ 'eventbymember_id' => $id ]);
            $entity = $em->getRepository('mdtsFrontendBundle:EventByMember')->find($id);
            $em->remove($entity);
        }
        $em->flush();

        return true;
    }
    public function removeArticlesEvent($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        // $this->logger->info('IDs : '.print_r($ids, true));

         //var_dump($ids);exit;

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($id);
            $this->removeRelationByArticleID($id);
            $em->remove($entity);
            $em->flush();
        }

        return true;
    }
    public function removeallEventArtistesDjOrganisateurs($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);
            $em->remove($entity);
        }
        $em->flush();

        return true;
    }
    public function removeEventLieu($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);
        //echo $entity->getName();exit;

        if (!$entity) {
            return false;
        }
        $em->remove($entity);
        $em->flush();

        return true;
    }
    public function createOrUpdateEventLieu(EventLieu $entity, $id = 0, $form = [])
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        //Enregistrer videomultiple
        if (isset($form['telmultiple']) && count($form['telmultiple']) > 0) {

            //Tels multiples
            foreach ($entity->getTelsLieu() as $types) {
                $entitytypeold = $em->getRepository('mdtshomeBundle:TelsLieu')->find($types->getId());
                $em->remove($entitytypeold);
            }

            //Save new tels
            $i = 0;
            foreach ($form['telmultiple'] as $row) {
                $tel = $row['tel']->getData();

                if ($tel != '') {
                    $entitytel = new TelsLieu();
                    $entitytel->setTel($tel);

                    $entity->addTelsLieu($entitytel);
                    $entitytel->setEventLieu($entity);

                    $em->persist($entitytel);
                }
                ++$i;
            }
        }

        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        //Si new
        if ($id <= 0) {
            $entity->setOrigSlug($entity->getSlug());
            $em->flush();
        }

        return true;
    }

    public function createOrUpdateEventArtistesDjOrganisateurs(EventArtistesDjOrganisateurs  $entity, $form, $id = 0)
    {
        $filenameajax = (array_key_exists('filenameajax', $form) && $form['filenameajax'] != ''?$form['filenameajax']:'');
        $dragandrop = (array_key_exists('dragandrop', $form) && $form['dragandrop'] != ''?$form['dragandrop']:'');
        $events = (array_key_exists('event', $form) && $form['event'] != ''?$form['event']:'');
        $parentartisteid = (array_key_exists('parentartisteid', $form) && $form['parentartisteid'] != ''?$form['parentartisteid']:'');
        $enfantartisteid = (array_key_exists('enfantartisteid', $form) && $form['enfantartisteid'] != ''?$form['enfantartisteid']:'');
        $articlesmultiple = (array_key_exists('articlesmultiple', $form) && is_array($form['articlesmultiple']) ?$form['articlesmultiple']:[]);
         

        $em = $this->_container->get('doctrine.orm.entity_manager');

        if ($id > 0) {
            //Articles
            foreach ($entity->getArticlesEvent() as $types) {
                $entitytypeold = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($types->getId());
                $entity->removeArticlesEvent($entitytypeold);
            }
        }


        //fichier envoyé via ajax
         if ( $filenameajax != '') {
            $fajax = $filenameajax;

            $fdragandrop = $dragandrop;

            $fs = new Filesystem();
             $path = $this->_container->get('kernel')->getRootDir().'/../web/tmp/';
             if ($fs->exists($path.$fajax)) {

                $info = new \SplFileInfo($fajax);
                 $fname = $this->_container->get('slugify')->slugify($entity->getName()).'_'.uniqid();
                 $fext = $info->getExtension();
                 $filenamecopy = $fname.'.'.$fext;

                 if ($fdragandrop == '1') {
                     $fs->copy($path.$fajax, $this->_container->get('kernel')->getRootDir().'/../web/assets/artistes_orig/'.$filenamecopy);

                     $entity->setPhoto($filenamecopy);

                     $source = $this->_container->get('kernel')->getRootDir().'/../web/assets/artistes_orig/'.$filenamecopy;
                     $source_resize = $this->_container->get('kernel')->getRootDir().'/../web/assets/artistes/'.$fname.'_resize.'.$fext;
                     $dest = $this->_container->get('kernel')->getRootDir().'/../web/assets/artistes/'.$fname.'.png';

                     $cmd_resize = ['magick',$source,'-resize','75x75!',$source_resize];
                     $process = new Process($cmd_resize);
                     $process->run();

                     $cmd_circle = ['magick','-size','75x75','xc:none','-fill',$source_resize,'-draw','"circle 36,36,36,1"',$dest];
                     $process = new Process($cmd_circle);
                     $process->run();
                     $process = new Process(['rm',$source_resize]);
                     $process->run();
                 }

                 $fs->remove($path.$fajax);

             }
         }

        if ($id <= 0) {
            $em->persist($entity);
        }

        $em->flush();

        //PLusieurs event
        if ($events != '') {
            //Combien
            $tab = explode(',', $events);

            if (count($tab) > 0) {
                foreach ($tab as $row) {
                    $this->addArtisteInEventID($row, $entity->getId());
                }
            }
        }

        // Parent artiste
        if ($parentartisteid!='') {
             
            //Combien
            $tab = explode(',', $parentartisteid);

            if (count($tab) > 0) {
                foreach ($tab as $row) {
                    $this->addArtisteInGroupartists($row, $entity->getId());
                }
            }
        }

        // Enfants artiste
        if ($enfantartisteid!='') {

            //Recuperer les artistes de l'event et si existe - supprimer avant d'ajouter
            if ($entity->getParentArtiste()) {
                foreach ($entity->getParentArtiste() as $artiste) {
                    //Supprimer
                    $entity->removeParentArtiste($artiste);
                }
            }
 
            //Combien
            $tab = explode(',', $enfantartisteid);

            if (count($tab) > 0) {
                foreach ($tab as $row) {
                    $entityRow = $this->em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($row);
                    $entity->addParentArtiste($entityRow);
                }
            }
        }

        //Si new
        if ($id <= 0) {
            $entity->setOrigSlug($entity->getSlug());
        }

        // Ecrire en BDD
        $em->flush();

        return true;
    }

    public function createOrUpdateArticleEvent(ArticlesEvent  $entity, $form, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        if ($id <= 0) {
            $em->persist($entity);
        }

        $em->flush();

        //PLusieurs event
        if (method_exists($form, 'get') && $form->get('event')->getData() && $form->get('event')->getData() != '') {
            //Combien
            $tab = explode(',', $form->get('event')->getData());
            //Si on trouve des caractères non numériques dans le tableau
            if (!array_walk($tab, 'intval')) {
                return false;
            }

            if (count($tab) > 0) {
                $this->removeEventNotInListByArticleID($tab, $entity->getId());
                foreach ($tab as $row) {
                    $this->addEventsInArticleID($row, $entity->getId(), $entity->getAddartistesInevent());
                }
            }
        }

        //PLusieurs artistes
        if (method_exists($form, 'get') && $form->get('artiste')->getData() && $form->get('artiste')->getData() != '') {
            //Combien
            $tab = explode(',', $form->get('artiste')->getData());
            //Si on trouve des caractères non numériques dans le tableau
            if (!array_walk($tab, 'intval')) {
                return false;
            }
            $this->removeArtistesNotInListByArticleID($tab, $entity->getId());
            if (count($tab) > 0) {
                foreach ($tab as $row) {
                    $this->addArtistesInArticleID($row, $entity->getId());
                }
            }
        }
        $this->getInfoByTtrssAndUpdate();

        return true;
    }
    public function removeRelationByArticleID($article_id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entityarticle = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($article_id);

        //Tous les events dans la BDD
        foreach ($em->getRepository('mdtshomeBundle:Event')->getEventByArticleId($article_id) as $events) {
            //Supprimer la relation
                $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($events->getId());
            $entityevent->removeArticlesEvent($entityarticle);
        }

        //Tous les artistes
        foreach ($em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getArtisteByArticleId($article_id) as $artiste) {

            //Supprimer la relation
            $entityartiste = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($artiste->getId());
            $entityartiste->removeArticlesEvent($entityarticle);
        }
    }
    public function removeArtistesNotInListByArticleID($list, $article_id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entityarticle = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($article_id);

        //Tous les artistes
        foreach ($em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->getArtisteByArticleId($article_id) as $artiste) {
            if (!in_array($artiste->getId(), $list)) {
                //Supprimer la relation
                $entityartiste = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($artiste->getId());
                $entityartiste->removeArticlesEvent($entityarticle);
            }
        }
    }
    public function removeEventNotInListByArticleID($list, $article_id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entityarticle = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($article_id);

        //Tous les events dans la BDD
        foreach ($em->getRepository('mdtshomeBundle:Event')->getEventByArticleId($article_id) as $events) {
            if (!in_array($events->getId(), $list)) {

                //Supprimer la relation
                $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($events->getId());
                $entityevent->removeArticlesEvent($entityarticle);
            }
        }
    }

    /**
     * Ajouter un event à un article.
     */
    public function addEventsInArticleID($event_id, $article_id, $addartistesInevent = false)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entityarticle = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($article_id);
        $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($event_id);

        //Recuperer les articles de l'event et si existe - supprimer avant d'ajouter
        foreach ($entityevent->getArticlesEvent() as $article) {
            //Supprimer si trouvé

            if ($article_id == $article->getId()) {
                $entityevent->removeArticlesEvent($article);
            }
        }

        //get artistes by event
        if ($addartistesInevent && count($entityevent->getEventArtistesDjOrganisateurs()) > 0) {
            //Pour chaque artiste - ajouter ou non l'article
            foreach ($entityevent->getEventArtistesDjOrganisateurs() as $artiste) {
                $this->addArtistesInArticleID($artiste->getId(), $article_id);
            }
        }

        $entityevent->addArticlesEvent($entityarticle);
        $em->flush();
    }

    public function addArtistesInArticleID($artiste_id, $article_id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $logger = $this->_container->get('logger');

        $entityartiste = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($artiste_id);
        $entityarticle = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($article_id);

        //Recuperer les articles de l'artiste et si existe - supprimer avant d'ajouter
        foreach ($entityartiste->getArticlesEvent() as $article) {
            //Supprimer si trouvé

            if ($article_id == $article->getId()) {
                $entityartiste->removeArticlesEvent($article);
            }
        }

        $entityartiste->addArticlesEvent($entityarticle);
        $em->flush();
    }

    public function addArtisteInEventID($event_id, $artiste_id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entityartiste = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($artiste_id);
        $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($event_id);

        //Recuperer les artistes de l'event et si existe - supprimer avant d'ajouter
        foreach ($entityevent->getEventArtistesDjOrganisateurs() as $artiste) {
            //Supprimer si trouvé
            if ($artiste_id == $artiste->getId()) {
                $entityevent->removeEventArtistesDjOrganisateur($entityartiste);
            }
        }

        $entityevent->addEventArtistesDjOrganisateur($entityartiste);
        $em->flush();
    }

    public function removeAssocLocal($entity, $em)
    {
        //Dates
        foreach ($entity->getLocalByDate() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventDate')->find($types->getId());
            $entity->removeLocalByDate($entitytypeold);
        }

        foreach ($entity->getLocalByDateHome() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventDate')->find($types->getId());
            $entity->removeLocalByDateHome($entitytypeold);
        }

        return true;
    }
    public function removeAssocEvent($entity, $em)
    {
        //Suppr Types
        foreach ($entity->getEventtype() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:eventtype')->find($types->getId());
            $entity->removeEventtype($entitytypeold);
        }

        //Local
        foreach ($entity->getEventlocal() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventLocal')->find($types->getId());
            $entity->removeEventlocal($entitytypeold);
        }

        //Artistes
        foreach ($entity->getEventArtistesDjOrganisateurs() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($types->getId());
            $entity->removeEventArtistesDjOrganisateur($entitytypeold);
        }

        //Videos
        foreach ($entity->getEventvideos() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventVideos')->find($types->getId());
            $em->remove($entitytypeold);
        }

        //Prix
        foreach ($entity->getEventPrice() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventPrice')->find($types->getId());
            $entity->removeEventPrice($entitytypeold);
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' removeEventPrice - id  : '.$types->getId());
        }

        //Dates
        foreach ($entity->getEventDate() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventDate')->find($types->getId());
            $entity->removeEventDate($entitytypeold);
             $this->logger->info(__FUNCTION__.' - line '.__LINE__.' removeEventDate - id  : '.$types->getId());
        }

        //Multilieux
        foreach ($entity->getEventmultilieu() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventLieu')->find($types->getId());
            $entity->removeEventmultilieu($entitytypeold);
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' removeEventmultilieu - id  : '.$types->getId());
        }

        //Articles
        foreach ($entity->getArticlesEvent() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($types->getId());
            $entity->removeArticlesEvent($entitytypeold);
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' removeArticlesEvent - id  : '.$types->getId());
        }

        //Related event
        foreach ($entity->getEventRelated() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:Event')->find($types->getId());
            $entity->removeEventRelated($entitytypeold);
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' removeEventRelated - id  : '.$types->getId());
        }

        return true;
    }
    public function copyallEvent($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);

            $this->duplicateEvent($entity);
        }
        //$em->flush();
        return true;
    }
    public function removeallEvent($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        foreach ($ids as $id) {
            if (!$this->removeEvent($id)) {
                return false;
            }
        }

        return true;
    }
    public function duplicateEvent(Event $entity, $return = false)
    {

        $new_entity = clone $entity;

        $newSlug = $this->getNextEventSlug($new_entity->getSlug());

        $new_entity->setSlug($newSlug);
        $new_entity->setOrigSlug($newSlug);
        $new_entity->setName($new_entity->getName().' (copy)');
        $new_entity->setHidden(1);

        $fs = new Filesystem();
        $path = $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/';
        if ('' != $new_entity->getFlyer() && $fs->exists($path.$new_entity->getFlyer())) {
            $info = new \SplFileInfo($new_entity->getFlyer());
            $filenamecopy = substr($this->_container->get('slugify')->slugify($entity->getName()), 0, 50).'_'.uniqid().'.'.$info->getExtension();
            $fs->copy($path.$new_entity->getFlyer(), $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/'.$filenamecopy);
            $new_entity->setFlyer($filenamecopy);
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($new_entity);
        $em->flush();

        if (true === $return) {
            return $new_entity->getId();
        }

        return true;
    }
    public function duplicateEventArtistesDjOrganisateurs(EventArtistesDjOrganisateurs $entity)
    {
        $new_entity = clone $entity;
        $newSlug = $this->getNextEventArtistesDjOrganisateursSlug($new_entity->getSlug());
        $new_entity->setSlug($newSlug);
        $new_entity->setOrigSlug($newSlug);
        $new_entity->setName($new_entity->getName().' (copy)');

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($new_entity);
        $em->flush();

        return true;
    }
    public function removeEvent($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:Event')->find($id);

        if (!$entity) {
            return false;
        }

        $this->removeAssocEvent($entity, $em);

        //Flyers
        foreach ($entity->getEventFlyers() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:EventFlyers')->find($types->getId());
            $entity->removeEventFlyer($entitytypeold);
        }

        $em->remove($entity);
        $em->flush();

        //Delete index
        $response = $this->_container->get('madatsara.elasticsearch')->removeDocument($id);


        return true;
    }
    public function removeEventArtistesDjOrganisateurs($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);

        if (!$entity) {
            return false;
        }

        //Articles
        foreach ($entity->getArticlesEvent() as $types) {
            $entitytypeold = $em->getRepository('mdtshomeBundle:ArticlesEvent')->find($types->getId());
            $entity->removeArticlesEvent($entitytypeold);
        }

        $em->remove($entity);
        $em->flush();

        return true;
    }
    public function updateEventByMember(EventByMember $entity, $form, $id = 0)
    {
        $retour = true;
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $eventID = 0;

        //Date de mise à jour
        $date = new \DateTime('now');
        $entity->setUpdatedAt($date);

        // ld($form->get('eventunmapped')->getData() );

        if( $form->get('eventunmapped')->getData()>0) {

            // $this->logger->info(__LINE__.'-'.__FUNCTION__);
            $eventID = $form->get('eventunmapped')->getData();
            $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($eventID);
            //ld($entityevent);
            $entity->setEvent($entityevent);
        }

        $em->persist($entity);
        $em->flush();
        // $this->logger->info(__LINE__.'-'.__FUNCTION__);

        if ($eventID > 0) {
            $this->saveHistoryEventbymember($entity, $entityevent);
        }  
        
        return $retour;
    }
    public function createOrUpdateEventByMember(EventByMember $entity, $form, $id = 0)
    {
        // Init ipaddr
        $this->getIpAddress();

        $em = $this->_container->get('doctrine.orm.entity_manager');
        // ld( $this->getParameter('default_email'));exit;
        if ($id>0) {
            return $this->updateEventByMember($entity,$form,$id);
        }
        //Set user iD
        $user = $this->_container->get('security.token_storage')->getToken()->getUser();
        if ($user->getId() > 0) {
            $entity->setAuthor($user);
        }

        //Date
        $date = new \DateTime('now');
        $entity->setCreatedAt($date);

        //Infos
        $data = array(
            'ip' => $this->ipAddr,
        );

        $path_geoipmmdb_exists = $this->_container->hasParameter('path_geoipmmdb');
        if ($path_geoipmmdb_exists) {
            $path_geoipmmdb = $this->getPathParam('path_geoipmmdb');
            $reader = new Reader($path_geoipmmdb);
            $record = $reader->city($this->ipAddr);
            $data = array_merge($data,  ['geoinfo' => [
                'country_isoCode' => $record->country->isoCode, 'country_confidence' => $record->country->confidence, 'country_names' => $record->country->names['fr'], 'city_confidence' => $record->city->confidence, 'city_geonameId' => $record->city->geonameId, 'city_names' => $record->city->names['fr'], 'postal_confidence' => $record->postal->confidence, 'postal_code' => $record->postal->code, 'location_averageIncome' => $record->location->averageIncome, 'location_accuracyRadius' => $record->location->accuracyRadius, 'location_latitude' => $record->location->latitude, 'location_longitude' => $record->location->longitude, 'location_populationDensity' => $record->location->populationDensity, 'location_postalCode' => $record->location->postalCode, 'location_timeZone' => $record->location->timeZone,
            ]]);
        }
        if ($this->hasParameter('path_udgerdb')) {
            $data = $this->getDataUdgerDb($data);
        }
        $entity->setInfos(json_encode($data));

        //set Status
        if ($this->_container->hasParameter('eventbymember_defaultstatus_id')) {
            $entityStatus = $em->getRepository('mdtsFrontendBundle:EventByMemberStatus')->find($this->_container->getParameter('eventbymember_defaultstatus_id'));
            $entity->setStatus($entityStatus);
        }

        $em->persist($entity);
        $em->flush();

        return true;
    }
    private function setSaveDateEvent($entity)
    {


        $option = $entity->getOptdateclair();
        switch($option) {
            case 'rangedate' :
                // 2017-02-13 12:00 au 2017-02-25 12:00
                $entity->setDateUnique(null);
                $entity->setDateDebut($this->getDateDebutInDateClair($entity->getDateClair()));
                $entity->setDateFin($this->getDateFinInDateClair($entity->getDateClair()));
                break;

            case 'multiplesdate':
                // 2017-02-22 12:00; 2017-02-15 12:00; 2017-02-10 12:00; 2017-02-26 12:00
                $entity->setDateUnique(null);
                $entity->setDateDebut(null);
                $entity->setDateFin(null);
                break;

            default:
                // 2017-02-09 21:00---2017-02-03 23:59
                $entity->setDateDebut(null);
                $entity->setDateFin(null);

                $entity->setDateUnique( $this->getDateUniqueInDateClair($entity->getDateClair()) );
                $entity->setHeureDebut( $this->getHeureDebutInDateClair($entity->getDateClair()) );
                $entity->setHeureFin( $this->getHeureFinInDateClair($entity->getDateClair()));
                break;
        }

        return $entity;
    }

    private function getArrayRangeDateinDateClair($dateclair)
    {
        // 2017-02-22 12:00; 2017-02-15 12:00; 2017-02-10 12:00; 2017-02-26 12:00
        $arrDate = [];
        $arrDate_ = [];

        $dateclair = trim($dateclair);


        // explode et prendre le 1er array
        $arrDate = explode(';',$dateclair);

        // ld($arrDate);

        // Pour chaque date
        foreach ($arrDate as $dates) {

            $dates = trim($dates);

            // Supprimer l'heure
            $dates = substr($dates,0,strpos($dates,' '));
            $dates = trim($dates);



            // Enregistrer dans un new array
            $arrDate_[] = \DateTime::createFromFormat('Y-m-j', $dates);
        }

        return $arrDate_;


    }
    private function getHeureFinInDateClair($dateclair)
    {
        // transformer 2017-02-09 21:00---2017-02-03 23:59 en 23:59

        $dateclair = trim($dateclair);

        // explode et prendre le 1er array
        $arrDate = explode('---',$dateclair);

        $date = $arrDate[1];

        // Si la date est 23:59 -> changer en 00:00
        if ( $date == '23:59') {
            $date = '00:00';
        }

        // Transformer cette date en DateTime
        return \DateTime::createFromFormat('H:i', $date ) ;

    }
    private function getHeureDebutInDateClair($dateclair)
    {
        // transformer 2017-02-09 21:00---2017-02-03 23:59 en 21:00

        $dateclair = trim($dateclair);

        // explode et prendre le 1er array
        $arrDate = explode('---',$dateclair);

        $date = $arrDate[0];

        $arrDate = explode(' ',$date );

        $date = $arrDate[1];

        // Transformer cette date en DateTime
        return \DateTime::createFromFormat('H:i', $date ) ;

    }
    private function getDateUniqueInDateClair($dateclair)
    {
        // transformer 2017-02-09 21:00---2017-02-03 23:59 en 2017-02-09

        $dateclair = trim($dateclair);

        // explode et prendre le 1er array
        $arrDate = explode(' ',$dateclair);

        $date = $arrDate[0];

        // Transformer cette date en DateTime
        return \DateTime::createFromFormat('Y-m-j', $date );

    }
    private function getDateDebutInDateClair($dateclair)
    {
        // 2017-02-13 12:00 au 2017-02-25 12:00

        $dateclair = trim($dateclair);

        // explode et prendre le 1er array
        $arrDate = explode('au',$dateclair);

        $date = $arrDate[0];

        $arrDate = explode(' ',$date);

        $date = $arrDate[0];

        // Transformer cette date en DateTime
        return \DateTime::createFromFormat('Y-m-j', $date );

    }
    private function getDateFinInDateClair($dateclair)
    {
        // 2017-02-13 12:00 au 2017-02-25 12:00
        // ld($dateclair);
        $dateclair = trim($dateclair);

        // explode et prendre le 1er array
        $arrDate = explode('au',$dateclair);

        $date = $arrDate[1];



        $arrDate = explode(' ',$date);

        $date = $arrDate[1];



        // Transformer cette date en DateTime
        return \DateTime::createFromFormat('Y-m-j', $date );

    }
    public function createOrUpdateEvent(Event $entity, $form, $id = 0)
    {
        $this->logger->info(__LINE__.'-'.__FUNCTION__.' - form: '.print_r($form,1));

        $mainflyermultipleExist = false;
        $originalFlyer = '';
        $isNew = $id==0;

        $path = $this->_container->get('kernel')->getRootDir().'/../web/tmp/';
        $pathFlyers = $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/';

        // If updated -> remove assoc before update
        if (!$isNew) {
            $this->removeAssocEvent($entity, $this->em);
        }

        // FileSystem
        $fs = new Filesystem();

        // All fields form
        $filenameajax = (array_key_exists('filenameajax', $form) && $form['filenameajax'] != ''?$form['filenameajax']:'');
        $dragandrop = (array_key_exists('dragandrop', $form) && $form['dragandrop'] != ''?$form['dragandrop']:'');
        $eventmultilieu = (array_key_exists('eventmultilieu', $form) && $form['eventmultilieu'] != ''?$form['eventmultilieu']:'');
        $eventtype = (array_key_exists('eventtype', $form) && $form['eventtype'] != ''?$form['eventtype']:'');
        $eventlocal = (array_key_exists('eventlocal', $form) && $form['eventlocal'] != ''?$form['eventlocal']:'');
        $artistesdj = (array_key_exists('artistesdj', $form) && $form['artistesdj'] != ''?$form['artistesdj']:'');
        $related_event = (array_key_exists('related_event', $form) && $form['related_event'] != ''?$form['related_event']:'');
        $multiflyer = (array_key_exists('multiflyer', $form) && $form['multiflyer'] != ''?$form['multiflyer']:'');
        $eventbymember = (array_key_exists('eventbymember', $form) && $form['eventbymember'] != ''?$form['eventbymember']:'');
        $cancelledAt = (array_key_exists('cancelledAt', $form) && $form['cancelledAt'] != ''?$form['cancelledAt']:'');
        $api = (array_key_exists('api', $form) && $form['api'] != ''?$form['api']:'');
        $videomultiple = (array_key_exists('videomultiple', $form) && is_array($form['videomultiple'] )?$form['videomultiple']:[]);
        $prixmultiple = (array_key_exists('prixmultiple', $form) && is_array($form['prixmultiple'] )?$form['prixmultiple']:[]);
        $articlesmultiple = (array_key_exists('articlesmultiple', $form) && is_array($form['articlesmultiple'] )?$form['articlesmultiple']:[]);

        $multipleLieuxArr = ''!=$eventmultilieu ? explode(',', $eventmultilieu) : [];
        $artistsArr = ''!=$artistesdj ? explode(',', $artistesdj) : [];
        $localArr = ''!=$eventlocal ? explode(',', $eventlocal) : [];
        $relatedEventArr = ''!=$related_event ? explode(',', $related_event) : [];
        $multiflyer = ''!=$multiflyer ?json_decode($multiflyer, true) : [];

        if (!$isNew && $entity->getFlyer() != '') {
            $originalFlyer = $entity->getFlyer();
        }

        // D'ou vient l'enregistrement? api rest ou web?
        if ($api!='') {
            $entityApi = $this->em->getRepository('mdtshomeBundle:Api')->find($api);
            $entity->setApi($entityApi);
        }

        //fichier envoyé via ajax
        if ($filenameajax != '' && $fs->exists($path.$filenameajax) ) {

            $info = new \SplFileInfo($filenameajax);
            $originalFlyer = $this->_container->get('slugify')->slugify($entity->getName()).'_'.uniqid().'.'.$info->getExtension();
        }

        if (''!=$originalFlyer && $dragandrop == '1' && $fs->exists($path.$filenameajax)) {
            // copy temp file to flyers folder
            $fs->copy($path.$filenameajax, $pathFlyers.$originalFlyer);

            // remove temp file
            $fs->remove($path.$filenameajax);
        }

        // Eventlieu vide
        if (count($multipleLieuxArr)<=0) {
            $entity->setEventlieu(null);
        }

        // PLusieurs lieux
        $i = 0;
        foreach ($multipleLieuxArr as $row) {
            $entitylieu = $this->em->getRepository('mdtshomeBundle:EventLieu')->find($row);

            // Enregistrer la premiere valeur dans eventlieu
            if ($i==0) {
                $entity->setEventlieu($entitylieu);
            } else {
                $entity->addEventmultilieu($entitylieu);
            }
            ++$i;
        }



        //Enregistrer eventtype
        if ($eventtype > 0 ) {
            $entitytype = $this->em->getRepository('mdtshomeBundle:EventType')->find($eventtype);
            $entity->addEventtype($entitytype);
        }

        //Enregistrer eventlocal
        foreach ($localArr as $row) {
            $entitylocal = $this->em->getRepository('mdtshomeBundle:EventLocal')->find($row);
            $entity->addEventlocal($entitylocal);
        }

        //Enregistrer artistes
        foreach ($artistsArr as $row) {
            $entityartiste = $this->em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($row);
            $entity->addEventArtistesDjOrganisateur($entityartiste);
        }

        //Enregistrer Event related multiple
        foreach ($relatedEventArr as $row) {
            $entityevent = $this->em->getRepository('mdtshomeBundle:Event')->find($row);
            //Si ce n'est pas l'event en cours ou new event
            if ($row != $id) {
                $entity->addEventRelated($entityevent);
            }
        }

        //Enregistrer videomultiple
        $i = 0;
        foreach ($videomultiple as $row) {
            $url = $row['url'];

            if ($url != '' && filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                $entityvideo = new EventVideos();
                $entityvideo->setUrl($url);

                $entity->addEventvideo($entityvideo);
                $entityvideo->setEvent($entity);

                $this->em->persist($entityvideo);

            }
            ++$i;
        }

        //Enregistrer prix multiple
        $i = 0;
        foreach ($prixmultiple as $row) {
            if ($row['price'] != '' && filter_var($row['price'], FILTER_VALIDATE_INT) && $row['detailprice'] != '' ) {
                $price = $row['price'] ;
                $detailprice = $row['detailprice'] ;
                $devises = $row['devises'] ;

                $entityDevises = $this->em->getRepository('mdtshomeBundle:Devises')->findOneByInitiales($devises);



                $entityprice = new EventPrice();
                $entityprice->setPrice($price);
                $entityprice->setDetailPrice($detailprice);
                if ($devises!='' && $entityDevises)
                    $entityprice->setDevise($entityDevises);
                $this->em->persist($entityprice);

                $entity->addEventPrice($entityprice);
            }

            ++$i;
        }


        // Multiples flyers
        foreach ($multiflyer as $row) {
            $image = $row['image'];
            $ismain = $row['main'];

            if ($fs->exists($path.$image)) {
                $info = new \SplFileInfo($image);
                $filenamecopy = $this->_container->get('slugify')->slugify($entity->getName()).'_'.uniqid().'.'.$info->getExtension();
                $fs->copy($path.$image, $pathFlyers.$filenamecopy);
                $fs->remove($path.$image);



                //SI main
                if ($ismain) {
                    $entity->setFlyer($filenamecopy);
                    $mainflyermultipleExist = true;
                } else {
                    $entityflyer = $this->createEntityFlyer([
                        'ismain' => $ismain,
                        'filename' => $filenamecopy
                        ]);

                    $entity->addEventFlyer($entityflyer);
                }
            }
        }


//        var_dump($mainflyermultipleExist);exit;
        //Enregistrer articlesmultiple
        $i = 0;
        foreach ($articlesmultiple as $row) {
            $url = $row['url'] ;
            $addartiste = $row['addartiste'] ;


            if ($url != '' && filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
                $entityarticle = $this->em->getRepository('mdtshomeBundle:ArticlesEvent')->findOneByUrl($url);
                if (!$entityarticle) {
                    $entityarticle = new ArticlesEvent();
                    $entityarticle->setUrl($url);
                    $this->em->persist($entityarticle);
                }

                $entity->addArticlesEvent($entityarticle);
                if ($addartiste == 1) {
                    //Recuperer les artistes
                    foreach ($artistsArr as $row) {

                        //Ebtity de l'EventArtistesDjOrganisateurs
                        $entityartiste = $this->em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($row);

                        //Recuperer les articles de chaque artiste et si existe - supprimer avant d'ajouter
                        foreach ($entityartiste->getArticlesEvent() as $articles) {
                            //Supprimer si trouvé
                            if ($entityarticle->getId() == $articles->getId()) {
                                $entityartiste->removeArticlesEvent($entityarticle);
                            }
                        }
                        $entityartiste->addArticlesEvent($entityarticle);
                    }
                }
            }
            ++$i;
        }

        $tab_dates = array();


        // parser dateclair
        $entity = $this->setSaveDateEvent($entity);

        // ld( is_null( $entity->getDateDebut() ) );exit;
        //Si date unique
        $bDateUnique = ('' == $entity->getOptdateclair() || 'unique' == $entity->getOptdateclair());
        if ($bDateUnique &&   is_null( $entity->getDateDebut() ) ) {
            $entity->setDateDebut(null);
            $entity->setDateFin(null);

            $tab_dates[] = $entity->getDateUnique()->format('Y-m-d');


            $entitydate = $this->em->getRepository('mdtshomeBundle:EventDate')->findByDate($entity->getDateUnique());
            if (!$entitydate) {
                //Creer
                $entitydate = new EventDate();
                $entitydate->setDate($entity->getDateUnique());
                $this->em->persist($entitydate);
            } else {
                $entitydate = $entitydate[0];
            }

                //Attacher entity à la date

                $entity->addEventDate($entitydate);
        }
        // Si dates multiples
        elseif ('multiplesdate' == $entity->getOptdateclair()) {
            //  2017-02-22 12:00; 2017-02-15 12:00; 2017-02-10 12:00; 2017-02-26 12:00
            $tabrange = $this->getArrayRangeDateinDateClair($entity->getDateClair());
            // ld($tabrange);exit;

            // Si rrule existe --> ne rien faire
            if (!$entity->getRrule()) {
                foreach ($tabrange as $date) {
                    $tab_dates[] = $date->format('Y-m-d');
                    //$date = $date->format('Y-m-d');
                    $entitydate = $this->em->getRepository('mdtshomeBundle:EventDate')->findByDate($date);
                    if (!$entitydate) {

                        //Creer
                        $entitydate = new EventDate();
                        $entitydate->setDate($date);
                        $this->em->persist($entitydate);

                        //echo 'ID non trouvé - créé avec ID : '.$entitydate->getId().' - '.$date->format('Y-m-d').PHP_EOL;
                    } else {
                        $entitydate = $entitydate[0];
                    }

                    //Attacher entity à la date

                    $entity->addEventDate($entitydate);
                }
            }
        }
        else {
            $entity->setDateUnique(null);

            // intervalle entre 2 dates
            $tabrange = $this->date_range($entity->getDateDebut()->format('Y-m-d'), $entity->getDateFin()->format('Y-m-d'));


            // Si rrule existe --> ne rien faire
            if (!$entity->getRrule()) {
                foreach ($tabrange as $date) {
                    $tab_dates[] = $date->format('Y-m-d');
                    //$date = $date->format('Y-m-d');
                    $entitydate = $this->em->getRepository('mdtshomeBundle:EventDate')->findByDate($date);
                    if (!$entitydate) {

                        //Creer
                        $entitydate = new EventDate();
                        $entitydate->setDate($date);
                        $this->em->persist($entitydate);

                        //echo 'ID non trouvé - créé avec ID : '.$entitydate->getId().' - '.$date->format('Y-m-d').PHP_EOL;
                    } else {
                        $entitydate = $entitydate[0];
                    }

                    //Attacher entity à la date

                    $entity->addEventDate($entitydate);
                }
            }
        }

        //Si rrule existe
        if ($entity->getRrule()) {

            $rule = new rule($entity->getRrule(), new \DateTime());
            $transformer = new \Recurr\Transformer\ArrayTransformer();
            //echo $textTransformer->transform($rule);
            //print_r($transformer->transform($rule));
            foreach ($transformer->transform($rule) as $row) {
                if (!in_array($row->getStart()->format('Y-m-d'), $tab_dates)) {
                    $tab_dates[] = $row->getStart()->format('Y-m-d');
                    //echo $row->getStart()->format('Y-m-d')."\n";
                    //exit;
                    $entitydate = $this->em->getRepository('mdtshomeBundle:EventDate')->findByDate($row->getStart());
                    if (!$entitydate) {

                        //Creer
                        $entitydate = new EventDate();
                        $entitydate->setDate($row->getStart());
                        $this->em->persist($entitydate);

                        //echo 'ID non trouvé - créé avec ID : '.$entitydate->getId().' - '.$row->getStart()->format('Y-m-d').PHP_EOL;
                    } else {
                        $entitydate = $entitydate[0];
                    }

                    $entity->addEventDate($entitydate);
                }
            }
        }

        if ($isNew) {
            $entity->setFlyer($originalFlyer);
        }


        if (!$isNew) {
            // remove index doc from elasticsearch
            $this->_container->get('madatsara.elasticsearch')->removeDocument($id);
        }

        //Si flyer multiple
        if (!$isNew && $mainflyermultipleExist && $originalFlyer != '') {
            $this->addFlyerToEventFlyers($id, $originalFlyer);
        }


        //Si marquer comme annuler
        if (!$isNew && $cancelledAt && '' == $entity->getFlyercancelled()) {
            $flyercancelled = $this->createFlyerCancelled($entity);
            $entity->setFlyercancelled($flyercancelled);
        }


        // persist if new
        if ($isNew) {
            $this->em->persist($entity);
        }
            
        // Write in DB - INSERT Or UPDATE
        $this->em->flush();

        // Enregistrer dans eventbymember
        $this->saveEventByMember($entity, $eventbymember );

        //EnableHomeevent
        $this->setSortableEvent();
        $this->enableHomeEvent();
        $this->enableHomeEventLocal();

        //Generer miniatures images
        $this->createThumbFlyer($entity);

        // index in elasticsearch
//        $convert = $this->_container->get('jms_serializer');
//        $json = $convert->serialize($entity, 'json');
//        $data = json_decode($json,true);
//        $this->_container->get('madatsara.elasticsearch')->indexDocument($entity->getId(), $data);


        return true;
    }

    public function copyallEventLieu($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventLieu')->find($id);

            $this->duplicateEventLieu($entity);
            /*$new_entity = clone $entity;
            $new_entity->setSlug( $this->getNextEventLieuSlug( $new_entity->getSlug() ) );
            $em->persist($new_entity); */
        }
        //$em->flush();
        return true;
    }
    public function copyallEventArtistesDjOrganisateurs($ids)
    {
        if (count($ids) <= 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        foreach ($ids as $id) {
            $entity = $em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($id);
            $this->duplicateEventArtistesDjOrganisateurs($entity);

            /*$new_entity = clone $entity;
            $new_entity->setSlug( $this->getNextEventArtistesDjOrganisateursSlug( $new_entity->getSlug() ) );
            $em->persist($new_entity); */
        }
        $em->flush();

        return true;
    }
    public function duplicateEventLieu(EventLieu $entity)
    {
        $new_entity = clone $entity;
        $newSlug = $this->getNextEventLieuSlug($new_entity->getSlug());
        $new_entity->setSlug($newSlug);
        $new_entity->setOrigSlug($newSlug);

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($new_entity);
        $em->flush();

        return true;
    }

    public function deleteFlyer($id = 0)
    {
        if ($id == 0) {
            return false;
        }

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:EventFlyers')->find($id);
        $em->remove($entity);
        $em->flush();

        return true;
    }

    public function setFlyer($id = 0, $eventid = 0)
    {
        // $this->logger->info('Start --- '.__FUNCTION__.' - line '.__LINE__.' - id : '.$id.' - '.$eventid);
        if ($id == 0 || $eventid == 0) {
            return false;
        }

        //echo $id.'-'.$eventid;exit;

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtshomeBundle:EventFlyers')->find($id);

        if (!$entity) {
            return false;
        }

        $flyerimage = $entity->getImage();
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' -  '.$flyerimage);

        $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($eventid);
        $oldimage = $entityevent->getFlyer();
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' -  '.$oldimage);

        //Copier l'image avant d'etre supprime
        $fs = new Filesystem();
        $path = $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/';
        $info = new \SplFileInfo($flyerimage);
        $filenamecopy = $this->_container->get('slugify')->slugify($entityevent->getName()).'_'.uniqid().'.'.$info->getExtension();
        $fs->copy($path.$flyerimage, $path.$filenamecopy);
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' -  copie de '.$flyerimage.' vers '.$filenamecopy);
        //Delete eventflyers
        $this->deleteFlyer($id);
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__);

        $entityevent->setFlyer($filenamecopy);
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__);

        //Ajouter oldimage dans eventflyer
        $entityflyer = $em->getRepository('mdtshomeBundle:EventFlyers')->findByImage($oldimage);
        $this->logger->info(__FUNCTION__.' - line '.__LINE__); //$this->logger->info( __FUNCTION__.' - line '.__LINE__.' -  '.$entityflyer );
        if (!$entityflyer) {
            $this->logger->info(__FUNCTION__.' - line '.__LINE__);
            $entityflyer = new EventFlyers();
            $entityflyer->setImage($oldimage);
            $entityflyer->setCrdate(new \DateTime());
            $entityflyer->setHidden(0);
            $em->persist($entityflyer);
            $this->logger->info(__FUNCTION__.' - line '.__LINE__);
        } else {
            $entityflyer = $entityflyer[0];
            $this->logger->info(__FUNCTION__.' - line '.__LINE__.' - '.$entityflyer[0]->getImage());
        }
        if ($entityflyer) {
            $entityevent->addEventFlyer($entityflyer);
        }

        $em->flush();
        $this->logger->info(__FUNCTION__.' - line '.__LINE__);
        $this->createThumbFlyer($entityevent);
        $this->logger->info(__FUNCTION__.' - line '.__LINE__);

        return $entityflyer->getId();
    }
    public function getEvents($id, $selectfield = '*', $returnfield = '*')
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $sql = 'FROM `event` a
        LEFT JOIN event_event_local b ON b.event_id = a.id
        LEFT JOIN event_by_date c ON c.event_id = a.id
        LEFT JOIN event_date d ON d.id = c.event_date_id
         WHERE b.event_local_id = :eventlocalid AND a.hidden = 0 AND a.deletedAt IS NULL AND d.date >= :today';

        $res = $connection->prepare('SELECT COUNT(DISTINCT a.id) AS nb '.$sql);
        $res->bindValue(':eventlocalid', $id, \PDO::PARAM_INT);
        $res->bindValue(':today', date('Y-m-d'), \PDO::PARAM_STR);
        $res->execute();
        $row = $res->fetch(\PDO::FETCH_ASSOC);
        $nb = $row['nb'];

        if ($nb > 0) {
            $res = $connection->prepare('SELECT DISTINCT '.$selectfield.' '.$sql);
            $res->bindParam(':eventlocalid', $id, \PDO::PARAM_INT);
            $res->bindValue(':today', date('Y-m-d'), \PDO::PARAM_STR);
            $res->execute();
            if ($returnfield == '*' || preg_match('/,/', $returnfield)) {
                return $res->fetchAll(\PDO::FETCH_ASSOC);
            }

            $data = array();
            foreach ($res->fetchAll(\PDO::FETCH_ASSOC) as $k => $v) {
                $data[] = $v[$returnfield];
            }

            return $data;
        }

        return false;
    }
    public function getEventIdArticlesRecents()
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $sql = 'SELECT DISTINCT event_id FROM `articles_by_event` ORDER BY `article_id` DESC LIMIT 50';

        $res = $connection->prepare($sql);
        $res->execute();
        $row = $res->fetchAll(\PDO::FETCH_ASSOC);
        $ids_arr = array();
        if (count($row) > 0) {
            foreach ($row as $data) {
                $ids_arr[] = $data['event_id'];
            }
        }

        return $ids_arr;
    }
    public function enableHomeEventLocal()
    {
        $log_arr = array();

        $date_tmp = new \DateTime('now');
        $date_tmp->modify('this month');
        $mois = $date_tmp->format('m');

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        //$sql = "FROM `event_local` WHERE MONTH(startdate) = :mois AND startdate >= :aujourdhui AND YEAR(startdate) = :annee";
        $sql = 'FROM `event_local` a LEFT JOIN local_by_date_home b ON b.event_local_id = a.id LEFT JOIN event_date c ON c.id = b.event_date_id WHERE c.date = :aujourdhui';

        $res = $connection->prepare('SELECT COUNT(*) AS nb '.$sql);
        /*$res->bindValue(':mois', $mois  );
        $res->bindValue(':annee', date('Y')  );*/
        $res->bindValue(':aujourdhui', date('Y-m-d'));
        $res->execute();
        $row = $res->fetch(\PDO::FETCH_ASSOC);
        $nb = $row['nb'];

        $log_arr[] = 'NB event : <strong>'.$nb.'</strong>';

        //Si eventlocal exists?
        if ($nb > 0) {
            $this->deleteAllEventHomeLocal();

            $res = $connection->prepare('SELECT a.name,a.id,a.startdate_home,a.enddate_home '.$sql.' ORDER BY c.date ASC');
            /*$res->bindValue(':mois', $mois  );
            $res->bindValue(':annee', date('Y')  );*/
            $res->bindValue(':aujourdhui', date('Y-m-d'));
            $res->execute();
            $row = $res->fetchAll(\PDO::FETCH_ASSOC);

            $allid_arr = $alldates_arr = $allnames_arr = $event_iterator_name = array();

            //Pour chaque eventlocal
            foreach ($row as $local) {
                $startdate = $local['startdate_home'];
                $enddate = $local['enddate_home'];
                $eventlocal = $local['id'];
                $eventlocal_name = $local['name'];

                $log_arr[] = 'eventlocal_name : '.$eventlocal_name.' startdate : '.$startdate.' - enddate : '.$enddate;

                /**
                 * Si startdate = enddate => Date debut du mois a startdate eventlocal
                 * Sinon => Date debut du mois a enddate eventlocal.
                 */
                $date_array = $this->dateRange($startdate, $enddate);

                //Les events dans cet localevent
                $events_name = $this->getEvents($eventlocal, 'a.name', 'name');
                $events_id = $this->getEvents($eventlocal, 'a.id', 'id');
                if (is_array($events_name)) {
                    $arrayit = new \ArrayIterator($events_name);
                    $infinite = new \InfiniteIterator($arrayit);
                    $limit = new \LimitIterator($infinite, 0, count($date_array));

                    foreach ($limit as $value) {
                        $event_iterator_name[] = $value;
                    }
                }

                if (!is_bool($events_id)) {
                    $log_arr[] = 'NB events dans : '.$eventlocal_name.'  : '.count($events_id);
                    $arr_div = $events_id;
                    $arr_div = $this->array_divide($arr_div, count($arr_div) / 4);
                    $arrayit = new \ArrayIterator($arr_div);
                    $infinite = new \InfiniteIterator($arrayit);
                    $limit = new \LimitIterator($infinite, 0, count($date_array));

                    $event_iterator = array();
                    foreach ($limit as $value) {
                        $event_iterator[] = $value;
                    }
                    $i = 0;
                    $log_arr[] = 'NB iteration events : <strong>'.count($event_iterator).'</strong>';
                    foreach ($event_iterator as $eventid) {
                        $allid_arr[] = $eventid;
                        $alldates_arr[] = $date_array[$i];
                        $allnames_arr[] = $event_iterator_name[$i];
                        ++$i;
                    }
                }
            }

            //Supprimer avant insert
            if (is_array($allid_arr)) {
                //print_r($allid_arr);
                //$log_arr[] = '<hr />DELETE IDs : '.implode(', ', array_unique( $allid_arr) );
            }

            //$this->deleteEventHome( implode(', ', array_unique( $allid_arr) ) );

            $i = 0;
            foreach ($allid_arr as $eventid) {
                if (count($eventid) < 4) {
                    $nbfill = 4 - count($eventid);
                    /*var_dump($nbfill);
                    print_r($events_id);*/
                    for ($n = 0; $n < $nbfill; ++$n) {
                        if (isset($events_id[$n])) {
                            $eventid = array_merge($eventid, array($events_id[$n]));
                        }
                    }
                }
                //print_r($alldates_arr);
                $this->addEventHomeLocal($eventid, $alldates_arr[$i]);
                //$log_arr[] =  "INSERT " .$allnames_arr[$i]." (ID: ". $eventid . ") - " . $alldates_arr[$i] . " - (ID date : ".$this->getDateId($alldates_arr[$i]). ")";
                ++$i;
            }
        }
        //print_r($log_arr);
        return $log_arr;
    }
    public function array_divide($array, $segmentCount)
    {
        $dataCount = count($array);
        if ($dataCount == 0) {
            return false;
        }
        $segmentLimit = ceil($dataCount / $segmentCount);
        $outputArray = array_chunk($array, $segmentLimit, true);

        return $outputArray;
    }

    public function array2table($data) {

        $t = '<table border="0" cellpadding="0" cellspacing="0" width="100%" >';
        $t .= '<tbody>';

        foreach ($data as $k=>$v) {

            $v = (!is_array($v) ? $v : $this->array2table($v));
            $k = (!is_array($k) ? $k : $this->array2table($k));


            $t .= '<tr>';
            $t .= '<td style="padding:20px;"><strong>'.$k.'</strong></td>';
            $t .= '<td style="padding:20px;">'.$v.'</td>';
            $t .= '</tr>';

        }
        $t .= '</tbody>';
        $t .= '</table>';
        return $t;
    }

    public function enableHomeEvent()
    {
        // $this->logger->info(__FUNCTION__.'-'.__LINE__);
        $log_arr = array();

        $date_tmp = new \DateTime('now');
        $date_tmp->modify('this month');
        $mois = $date_tmp->format('m');

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        //$sql = "FROM `event_local` WHERE MONTH(startdate) = :mois AND startdate >= :aujourdhui AND YEAR(startdate) = :annee";
        $sql = 'FROM `event_local` a LEFT JOIN local_by_date_home b ON b.event_local_id = a.id LEFT JOIN event_date c ON c.id = b.event_date_id WHERE c.date = :aujourdhui';

        $res = $connection->prepare('SELECT COUNT(*) AS nb '.$sql);
        /*$res->bindValue(':mois', $mois  );
        $res->bindValue(':annee', date('Y')  );*/
        $res->bindValue(':aujourdhui', date('Y-m-d'));
        $res->execute();
        $row = $res->fetch(\PDO::FETCH_ASSOC);
        $nb = $row['nb'];

        $log_arr[] = 'NB event : <strong>'.$nb.'</strong>';
        // $this->logger->info(__FUNCTION__.'-'.__LINE__.' - NB event :  '.$nb);

        //Si eventlocal exists?
        if ($nb > 0) {
            $this->deleteAllEventHome();

            $res = $connection->prepare('SELECT a.name,a.id,a.startdate_home,a.enddate_home '.$sql.' ORDER BY c.date ASC');
            /*$res->bindValue(':mois', $mois  );
            $res->bindValue(':annee', date('Y')  );*/
            $res->bindValue(':aujourdhui', date('Y-m-d'));
            $res->execute();
            $row = $res->fetchAll(\PDO::FETCH_ASSOC);

            $allid_arr = $alldates_arr = $allnames_arr = $event_iterator_name = array();

            //Pour chaque eventlocal
            foreach ($row as $local) {
                $startdate = $local['startdate_home'];
                $enddate = $local['enddate_home'];
                $eventlocal = $local['id'];
                $eventlocal_name = $local['name'];

                $log_arr[] = 'eventlocal_name : '.$eventlocal_name.' startdate : '.$startdate.' - enddate : '.$enddate;
                // $this->logger->info(__FUNCTION__.'-'.__LINE__.' - eventlocal_name : '.$eventlocal_name.' startdate : '.$startdate.' - enddate : '.$enddate);

                //Date debut du mois a startdate eventlocal
                //$date_array = $this->dateRange(date('Y') . '-' . $mois . '-01', $date);
                $date_array = $this->dateRange($startdate, $enddate);
                // $this->logger->info(__FUNCTION__.'-'.__LINE__.' - date_array : '.implode(', ', $date_array));

                //Les events dans cet localevent
                $events_name = $this->getEvents($eventlocal, 'a.name', 'name');
                $events_id = $this->getEvents($eventlocal, 'a.id', 'id');
                if (is_array($events_name)) {
                    $arrayit = new \ArrayIterator($events_name);
                    $infinite = new \InfiniteIterator($arrayit);
                    $limit = new \LimitIterator($infinite, 0, count($date_array));
                    foreach ($limit as $value) {
                        $event_iterator_name[] = $value;
                    }
                }
                // $this->logger->info(__FUNCTION__.'-'.__LINE__);

                if (is_array($events_id)) {
                    $log_arr[] = 'NB events dans : '.$eventlocal_name.'  : <strong>'.count($events_id).'</strong>';
                    // $this->logger->info(__FUNCTION__.'-'.__LINE__.' - NB events dans : '.$eventlocal_name.'  :  '.count($events_id));
                    $arrayit = new \ArrayIterator($events_id);
                    $infinite = new \InfiniteIterator($arrayit);
                    $limit = new \LimitIterator($infinite, 0, count($date_array));

                    $event_iterator = array();
                    foreach ($limit as $value) {
                        $event_iterator[] = $value;
                    }
                    $i = 0;
                    $log_arr[] = 'NB iteration events : <strong>'.count($event_iterator).'</strong>';
                    // $this->logger->info(__FUNCTION__.'-'.__LINE__.' - NB iteration events : '.count($event_iterator));
                    foreach ($event_iterator as $eventid) {
                        $allid_arr[] = $eventid;
                        $alldates_arr[] = $date_array[$i];
                        $allnames_arr[] = $event_iterator_name[$i];
                        ++$i;
                    }
                }
            }

            //Supprimer avant insert
            $log_arr[] = '<hr />DELETE IDs : <strong>'.implode(', ', array_unique($allid_arr)).'</strong>';
            // $this->logger->info(__FUNCTION__.'-'.__LINE__);
            //$this->deleteEventHome( implode(', ', array_unique( $allid_arr) ) );

            $i = 0;
            foreach ($allid_arr as $eventid) {
                $this->addEventHome($eventid, $alldates_arr[$i]);
                $log_arr[] = 'INSERT '.$allnames_arr[$i].' (ID: '.$eventid.') - '.$alldates_arr[$i].' - (ID date : '.$this->getDateId($alldates_arr[$i]).')';
                ++$i;
            }
            // $this->logger->info(__FUNCTION__.'-'.__LINE__);
        }

        //print_r($log_arr);
        // $this->logger->info(__FUNCTION__.'-'.__LINE__);

        return $log_arr;
    }
    public function addEventHomeLocal($id, $date)
    {
        $date_id = $this->getDateId($date);
        /*print_r($id);
        print_r($date);
        print_r($date_id);
        echo "\n---------------\n";*/
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();
        $sql = 'INSERT INTO `event_date_home_local` SET `event_id` = :event_id, event_date_id = :event_date_id';
        if (count($id) <= 0) {
            return false;
        }
        foreach ($id as $uid) {
            if (!$this->relationDateEventExists($uid, $date_id)) {
                $res = $connection->prepare($sql);
                $res->bindValue(':event_id', $uid, \PDO::PARAM_INT);
                $res->bindValue(':event_date_id', $date_id, \PDO::PARAM_INT);
                $res->execute();
            }
        }
    }
    public function addEventHome($id, $date)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $res = $connection->prepare('SELECT COUNT(*) AS nb FROM `event_date_home` WHERE `event_id` = :event_id AND event_date_id = :event_date_id');

        $res->bindValue(':event_id', $id, \PDO::PARAM_INT);
        $res->bindValue(':event_date_id', $this->getDateId($date), \PDO::PARAM_INT);
        $res->execute();
        $row = $res->fetch(\PDO::FETCH_ASSOC);
        $nb = $row['nb'];
        if ($nb <= 0) {
            $sql = 'INSERT INTO `event_date_home` SET `event_id` = :event_id, event_date_id = :event_date_id';

            $res = $connection->prepare($sql);
            $res->bindValue(':event_id', $id, \PDO::PARAM_INT);
            $res->bindValue(':event_date_id', $this->getDateId($date), \PDO::PARAM_INT);
            $res->execute();
        }
    }

    /**
     * Relation entre une date et un event.
     *
     * @param $event_id
     * @param $date_id
     *
     * @return bool
     */
    public function relationDateEventExists($event_id, $date_id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();
        $sql = 'SELECT count(*) as nb FROM `event_date_home_local` WHERE `event_id` = :event_id AND `event_date_id` = :event_date_id ';

        $res = $connection->prepare($sql);
        $res->bindValue(':event_id', $event_id, \PDO::PARAM_INT);
        $res->bindValue(':event_date_id', $date_id, \PDO::PARAM_INT);
        $res->execute();

        $row = $res->fetch(\PDO::FETCH_ASSOC);

        return $row['nb'] > 0 ? true : false;
    }
    public function getDateId($date)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();
        $sql = 'SELECT id FROM `event_date` WHERE `date` = :date';

        $res = $connection->prepare($sql);
        $res->bindValue(':date', $date);
        $res->execute();

        $row = $res->fetch(\PDO::FETCH_ASSOC);

        return $row['id'];
    }
    public function deleteAllEventHomeLocal()
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $sql = 'DELETE FROM `event_date_home_local`';

        $res = $connection->prepare($sql);
        $res->execute();
    }
    public function deleteAllEventHome()
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $sql = 'DELETE FROM `event_date_home`';

        $res = $connection->prepare($sql);
        $res->execute();
    }
    public function deleteEventHome($id)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $sql = 'DELETE FROM `event_date_home` WHERE event_id IN(:event_id)';

        $res = $connection->prepare($sql);
        $res->bindValue(':event_id', $id);
        $res->execute();
    }
    public function setSortableEvent()
    {
        $other = $current = $all = $data = $other_all = $allid = array();

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $res = $connection->prepare('SELECT a.name,a.id, a.date_unique, a.date_debut, a.date_fin FROM event a LEFT JOIN `event_by_date` b ON b.event_id = a.id LEFT JOIN `event_date` c ON c.id = b.event_date_id WHERE b.event_id IS NOT NULL AND a.deletedAt IS NULL AND a.hidden = 0 AND c.`date` >= :date_now ORDER BY c.`date` ASC');
        $res->bindValue('date_now', date('Y-m-d'));
        $res->execute();
        foreach ($res->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $date_debut = $row['date_debut'];
            $date_unique = $row['date_unique'];

            $data[] = $row;

            if ($date_debut == date('Y-m-d') || $date_unique == date('Y-m-d')) {
                $current[] = $row['id'];
            } else {
                $other[] = $row['id'];
            }
        }

        $res = $connection->prepare('SELECT a.name,a.id, a.date_unique, a.date_debut, a.date_fin FROM event a LEFT JOIN `event_by_date` b ON b.event_id = a.id LEFT JOIN `event_date` c ON c.id = b.event_date_id WHERE b.event_id IS NOT NULL AND a.deletedAt IS NULL AND a.hidden = 0 AND c.`date` < :date_now ORDER BY c.`date` DESC');
        $res->bindValue('date_now', date('Y-m-d'));
        $res->execute();
        foreach ($res->fetchAll(\PDO::FETCH_ASSOC) as $row) {   
            $other_all[] = $row['id'];
        }

        $current = array_unique($current);
        $other = array_unique($other);
        $all = array_merge($current, $other);

        $other_all = array_unique($other_all);

        $allid = array_merge($all, $other_all);

        $sql = "UPDATE `event` SET `position` =  CASE `id` \n";
        $i = 1;
        foreach ($all as $ids) {
            $tsql[] = ' WHEN '.$ids.' THEN '.$i;
            ++$i;
        }
        foreach ($other_all as $ids) {
            $tsql[] = ' WHEN '.$ids.' THEN '.$i;
            ++$i;
        }
        $sql .= implode("\n", $tsql)."\n";
        $sql .= ' END ';
        $sql .= "\n WHERE id IN(".implode(',', $allid).')';

        // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' setSortable SQL : '.$sql);

        $res = $connection->prepare($sql);
        $res->execute();
        
        // Sync emails between mailtrain. and madatsara.newsletter_abonnes tables
        $this->syncSubscribersEmails();
        
        return array('current' => $current, 'other' => $other, 'other_all' => $other_all);
    }

    /**
     * Activer ou desactiver les events.
     *
     * @param array $ids_array
     *
     * @return bool
     */
    public function setEnableOrDisableEvent($ids_array = array())
    {
        if (count($ids_array) == 0) {
            return false;
        }

        //Pour chaque ID
        foreach ($ids_array as $id) {
            $entityevent = $this->em->getRepository('mdtshomeBundle:Event')->find($id);
            //echo $id.' - '.$entityevent->getHidden().' - '.$entityevent->getName()."\n";
            if (true === $entityevent->getHidden()) {
                $entityevent->setHidden(false);
            } else {
                $entityevent->setHidden(true);
            }
        }
        $this->em->flush();

        return true;
    }

    /**
     * Inscrire ou non à la NL.
     *
     * @param string $email
     */
    public function createOrUpdateNewsletter(NewsletterAbonne $entity)
    {
        // Init ipaddr
        $this->getIpAddress();

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity_nl = $em->getRepository('mdtshomeBundle:NewsletterAbonne')->findByEmail($entity->getEmail());

        // $this->logger->info(__FUNCTION__.' - Line '.__LINE__.' - email : '.$entity->getEmail());

        $path_geoipmmdb_exists = $this->_container->hasParameter('path_geoipmmdb');
        $country_isoCode = $country_name = $city_name = $location_timeZone = '';

        if (!$entity_nl && $path_geoipmmdb_exists) {
            $path_geoipmmdb = $this->getPathParam('path_geoipmmdb');
            $reader = new Reader($path_geoipmmdb);
            $record = $reader->city($this->ipAddr);
            $result = [
                'country_isoCode' => $record->country->isoCode, 'country_confidence' => $record->country->confidence, 'country_names' => $record->country->names['fr'], 'city_confidence' => $record->city->confidence, 'city_geonameId' => $record->city->geonameId, 'city_names' => $record->city->names['fr'], 'postal_confidence' => $record->postal->confidence, 'postal_code' => $record->postal->code, 'location_averageIncome' => $record->location->averageIncome, 'location_accuracyRadius' => $record->location->accuracyRadius, 'location_latitude' => $record->location->latitude, 'location_longitude' => $record->location->longitude, 'location_populationDensity' => $record->location->populationDensity, 'location_postalCode' => $record->location->postalCode, 'location_timeZone' => $record->location->timeZone,
            ];

            $country_isoCode = $record->country->isoCode;
            $country_name = $record->country->names['fr'];
            $city_name = $record->city->names['fr'];
            $location_timeZone = $record->location->timeZone;

            if ($this->isEnvironnementDev() || '82.230.218.34' == $this->ipAddr) {
                print_r($result);
            }
        }

        if (!$entity_nl) {

            $referer = $this->requestStack->headers->get('referer');
            $ipAddress = $this->ipAddr;
            $userAgent = $this->requestStack->headers->get('User-Agent');
            //Ajout
            $infos =
                array(
                    'ip' => $ipAddress, 'user_agent' => $userAgent, 'referer' => $referer, 'country_isoCode' => $country_isoCode, 'country_name' => $country_name, 'city_name' => $city_name, 'location_timeZone' => $location_timeZone,
                )
             ;

            // $this->logger->info(__FUNCTION__.' - Line '.__LINE__.' - infos : '.print_r($infos, true));

            $entity_nl = new NewsletterAbonne();
            $entity_nl->setEmail($entity->getEmail());
            $entity_nl->setSource($entity->getSource());
            $date = new \DateTime('now');
            $entity_nl->setInscritdate($date);
            $entity_nl->setInfos(json_encode($infos));
            $entity_nl->setCountryIsoCode($country_isoCode);
            $entity_nl->setCountryName($country_name);
            $entity_nl->setCityName($city_name);
            $entity_nl->setLocationTimeZone($location_timeZone);

            // Attacher l'email a un artiste, lieu, region, pays, commune, quartier
            // $this->logger->info(__LINE__.'----'.    $referer);
            $this->attachEmailAbonneByReferer($entity_nl,$referer);


            $em->persist($entity_nl);

            $em->flush();

            // Attacher l'email a un pays via IP address
            $this->attachPaysToAbonneIp($country_name, $entity_nl);

            return array('ok' => $entity_nl->getId(), 'infos' => $infos);
        }

        return array('error' => $entity_nl[0]->getId());
    }
    public function createFlyerCancelled(Event $entity)
    {
        $path = $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/';
        $filename = $entity->getFlyer();

        $info = new \SplFileInfo($filename);
        $extfile = $info->getExtension();
        $fname = str_replace('.'.$extfile, '', $filename);

        $filename_blur = uniqid().'.'.$extfile;
        $filename_traitement = $fname.'_'.uniqid().'.'.$extfile;

        list($width, $height, $type, $attr) = getimagesize($path.$filename);

        //$cmd = 'cd '.$path.'; ls -l | wc -l';
        $cmd = 'cd '.$path.' && ';
        $cmd .= 'magick "'.$filename.'" -blur 0x6 ../../tmp/'.$filename_blur.' && ';
        $cmd .= 'magick -background "rgba(255,255,255,0.7)" -fill black -gravity center -size '.$width.'x'.$height.' -font DejaVu-Sans caption:"Annulé / Reporté" ../../tmp/'.$filename_blur.' +swap -gravity south -composite "'.$filename_traitement.'"';

        $fs = new Filesystem();
        $fs->remove($this->_container->get('kernel')->getRootDir().'../web/tmp/'.$filename_blur);

        $process = new Process($cmd);
        $process->run();

        return $filename_traitement;

    }

    public function sendMail($to, $subject, $msg, $fromemail, $fromname = '')
    {
        $mail = \Swift_Message::newInstance();

        //Envoyer mail admin
        $mail
            ->setFrom($fromemail)
            ->setTo($to)
            ->setSubject($subject)
            ->setBody($msg)
            ->setContentType('text/html');

        $this->_container->get('mailer')->send($mail);
    }

    public function createOrUpdateRegistrationid($registrationid, $android_modelname, $android_version, $device_id)
    {
        // Init ipaddr
        $this->getIpAddress();

        $isNew = false;
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $entity = $em->getRepository('mdtsFrontendBundle:registrationid')->findBydeviceId($device_id);

        if (!$entity) {
            $isNew = true;

            $entity = new registrationid();
            $entity->setAndroidModelname($android_modelname);
            $entity->setAndroidVersion($android_version);
            $entity->setDeviceId($device_id);
            $entity->setIpAddress($this->ipAddr);

            $path_geoipmmdb_exists = $this->_container->hasParameter('path_geoipmmdb');
            if ($path_geoipmmdb_exists) {
                $path_geoipmmdb = $this->getPathParam('path_geoipmmdb');
                $reader = new Reader($path_geoipmmdb);
                $record = $reader->city($this->ipAddr);

                $entity->setCountryIsoCode($record->country->isoCode);
                $entity->setCountryName($record->country->names['fr']);
                $entity->setCityName($record->city->names['fr']);
                $entity->setLocationTimeZone($record->location->timeZone);
                $em->persist($entity);
                $em->flush();
            }
        } else {
            //Update or create registrationid
            $entity = $entity[0];
            $entity->setRegistrationId($registrationid);
            $em->flush();
        }

        return $isNew;
    }

    public function createOrUpdateUser($data = array())
    {
        // Init ipaddr
        $this->getIpAddress();

        $isNew = false;
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('mdtsUserBundle:user')->findByemail($data['email']);

        $email = $data['email'];
        $username = $data['email'];
        $nick_name = $data['nick_name'];
        $first_name = $data['first_name'];
        $last_name = $data['last_name'];
        $real_name = $data['real_name'];
        $middle_name = $data['middle_name'];
        $profile_picture = $data['profile_picture'];
        $expire_in = $data['expire_in'];
        $service = $data['service'];
        $token = $data['token'];
        $emailRS = $data['emailRS'];
        $deviceId = $data['device_id'];
        // $this->logger->info(__FUNCTION__.' - data : '.print_r($data, true));

        if (!$user) {
            $isNew = true;
            $user = $this->_container
                    ->get('fos_user.util.user_manipulator')
                    ->create(
                            $email,
                            $email,
                            $email,
                            1,
                            0);
        } else {
            $user = $this->_container
                    ->get('fos_user.user_manager')
                    ->findUserByUsername($data['email']);
        }
        //Infos
        $data = array(
            'service' => $service, 'ip' => $this->ipAddr, 'getUsername' => $username, 'getNickname' => $nick_name, 'getFirstName' => $first_name, 'getLastName' => $last_name, 'getRealName' => $real_name, 'getEmail' => $email, 'getProfilePicture' => $profile_picture, 'getExpiresIn' => $expire_in,
            );

            //Geo
            $path_geoipmmdb_exists = $this->hasParameter('path_geoipmmdb');
        if ($path_geoipmmdb_exists) {
            $path_geoipmmdb = $this->getPathParam('path_geoipmmdb');
            $reader = new Reader($path_geoipmmdb);
            $record = $reader->city($this->ipAddr);
            $data = array_merge($data,  ['geoinfo' => [
                    'country_isoCode' => $record->country->isoCode, 'country_confidence' => $record->country->confidence, 'country_names' => $record->country->names['fr'], 'city_confidence' => $record->city->confidence, 'city_geonameId' => $record->city->geonameId, 'city_names' => $record->city->names['fr'], 'postal_confidence' => $record->postal->confidence, 'postal_code' => $record->postal->code, 'location_averageIncome' => $record->location->averageIncome, 'location_accuracyRadius' => $record->location->accuracyRadius, 'location_latitude' => $record->location->latitude, 'location_longitude' => $record->location->longitude, 'location_populationDensity' => $record->location->populationDensity, 'location_postalCode' => $record->location->postalCode, 'location_timeZone' => $record->location->timeZone,
                ]]);

            $country_isoCode = $record->country->isoCode;
            $country_name = $record->country->names['fr'];
            $city_name = $record->city->names['fr'];
            $location_timeZone = $record->location->timeZone;

            if ($this->isEnvironnementDev() || '82.230.218.34' == $this->ipAddr) {
                //print_r($data);
            }
        }
        if ($this->_container->hasParameter('path_udgerdb')) {
            $data = $this->getDataUdgerDb($data);
        }

        // $this->logger->info(__FUNCTION__.' - username : #'.$username.'#');
            //Update some fields
            if ($isNew && 'facebook' === $service && !is_null($username)) {
                $user->setFacebookId($username);
                $user->setFacebookAccessToken($token);
            }
        if ($isNew && 'google' === $service && !is_null($username)) {
            $user->setGoogleId($username);
            $user->setGoogleAccessToken($token);
        }

            //Update only if new
            if ($isNew) {
                //Nickname...
                $user->setNickname($nick_name);
                $user->setFirstName($first_name);
                $user->setLastName($last_name);
                $user->setProfilePicture($profile_picture);

                $user->setInfos(json_encode($data));

                if ($path_geoipmmdb_exists) {
                    $user->setCountryIsoCode($record->country->isoCode);
                    $user->setCountryName($record->country->names['fr']);
                    $user->setCityName($record->city->names['fr']);
                    $user->setLocationTimeZone($record->location->timeZone);
                }
                //Browser
                if ($this->_container->hasParameter('path_udgerdb') && is_array($data['user_agent'])) {
                    $user->setBrowserUaName($data['user_agent']['ua_name']);
                    $user->setBrowserUaVer($data['user_agent']['ua_ver']);
                    $user->setBrowserUaFamily($data['user_agent']['ua_family']);
                    $user->setBrowserUaCompany($data['user_agent']['ua_company']);
                    $user->setBrowserUaEngine($data['user_agent']['ua_engine']);
                    $user->setBrowserOsName($data['user_agent']['os_name']);
                    $user->setBrowserOsFamily($data['user_agent']['os_family']);
                    $user->setBrowserDeviceName($data['user_agent']['device_name']);
                }
            }

            //Update quand meme email RS si new ou update
            $user->setEmailRs($emailRS != '' ? $emailRS : $email);

        $this->_container->get('fos_user.user_manager')->updateUser($user);

            //Si deviceId non vide
            if ($deviceId != '') {
                $devices = $em->getRepository('mdtsFrontendBundle:registrationid')->findBydeviceId($deviceId);
                //Si device existe
                if ($devices) {
                    foreach ($devices as $device) {
                        // $this->logger->info(__FUNCTION__.' - deviceID : #'.$device->getId().'#');
                        //Ajout
                        $entityDevice = $em->getRepository('mdtsFrontendBundle:registrationid')->find($device->getId());
                        // $this->logger->info(__FUNCTION__.' - getUser()->count() : #'.$entityDevice->getUser()->count().'#');
                        // $this->logger->info(__FUNCTION__.' - getUser()getUser()->contains(user)? : #'.$entityDevice->getUser()->contains($user).'#');
                        //Ajouter si non existant
                        if (!$entityDevice->getUser()->contains($user)) {
                            $entityDevice->addUser($user);
                        }
                    }

                    $em->flush();
                }
            }

            //Envoyer email si new
            $default_email = $this->getParameter('default_email');
            $to = '';
            $from = '';
            if ( array_key_exists('to', $default_email)) {
                $to = default_email['to'];
                $from = default_email['from'];
            }
            if ($isNew && '' !== $service) {
                $msg = $this->_container->get('templating')->render(
                    'mdtshomeBundle:Email:generique.html.twig',
                    array(
                        'content' => $this->_container->get('madatsara.service')->array2table($data),
                    )
                );

                $this->sendMail($to, '[MADATSARA] - Nouveau membre via '.$service, $msg, $from);
            }

        return $isNew;
    }

    public function hasParameter($param_name)
    {
        return $this->_container->hasParameter($param_name);
    }

    public function getParameter($param_name)
    {
        return $this->_container->getParameter($param_name);
    }



    private function getInfoByTtrssAndUpdate()
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $connection = $em->getConnection();

        $sql = 'UPDATE `articles_event` a LEFT JOIN `srm_ttrss`.`ttrss_entries` AS b ON b.`link` = a.`url` SET a.`title` = b.`title`, a.`author` = b.`author`, a.`content` = b.`content`,a.`created_at` = b.`date_entered` WHERE b.`title` IS NOT NULL';

        $statement = $connection->prepare($sql);
        $statement->execute();
        //echo $sql;exit;

        $this->purifyContent();
        //exit;
    }

    private function purifyContent()
    {
        $config = \HTMLPurifier_Config::createDefault();
        $config->set('HTML.Allowed', 'em,strong,i,b,p,h1,h2,h3,h4,h5,h6,font[style|size],ol,ul,li,br,div');
        $config->set('AutoFormat.RemoveEmpty',  true);
        $config->set('Cache.SerializerPath', $this->_container->get('kernel')->getRootDir().'/../web/tmp/'); // TODO: remove this later!
        $filter = new HTMLPurifier($config);

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $sql = 'SELECT id,title,content FROM articles_event WHERE content IS NOT NULL AND content_purify IS NULL';
        //echo $sql."\n";
        $res = $connection->prepare($sql);
        $res->execute();
        $row = $res->fetchAll(\PDO::FETCH_ASSOC);
        //echo count($row)." lignes \n";
        if (count($row) <= 0) {
            return false;
        }

        foreach ($row as $ligne) {
            $purify_html = $filter->purify($ligne['content']);
            //echo "Purification de ".$ligne['title']." (".$ligne['id'].") \n";

            $sql = 'UPDATE  articles_event SET content_purify = :content_purify WHERE id = :id';
            $res = $connection->prepare($sql);
            $res->bindParam(':content_purify', $purify_html, \PDO::PARAM_STR);
            $res->bindParam(':id', $ligne['id'], \PDO::PARAM_INT);
            try {
                $res->execute();
            } catch (Exception $e) {
                //echo "Requete  : ", $res->errorCode();
            }
        }

        return true;
    }

    private function createThumbFlyer($entity)
    {
        $pathFlyer  = $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/';
        $fs = new Filesystem();

        $flyer = $entity->getFlyer();
        if (!$fs->exists($pathFlyer.$flyer))
            return false;

        $multiflyers = $entity->getEventFlyers();

        $flyer_array = array();
        foreach ($multiflyers as $multiflyer) {
            $flyer_array[] = $multiflyer->getImage();
        }

        // $this->logger->info(__FUNCTION__.' - flyer : '.$flyer.' - flyer_array: '.print_r($flyer_array, true));
        if ('' === $flyer) {
            return false;
        }


        $paths = [
            'crop_100_100',
            'my_thumb_70',
            'resize',
            'my_thumb',
        ];

        foreach ($paths as $path) {
            $pathd = $this->_container->get('kernel')->getRootDir().'/../web/media/cache/'.$path.'/assets/flyers/';

            // $this->logger->info(__FUNCTION__.' - path : '.$path.' - '.$pathd);

            if ('' !== $flyer && !$fs->exists($pathd.$flyer)) {
                $this->_container
                    ->get('liip_imagine.controller')
                    ->filterAction(new \Symfony\Component\HttpFoundation\Request(), 'assets/flyers/'.$flyer, $path);
            }

            if (count($flyer_array) > 0) {
                foreach ($flyer_array as $flyer_) {
                    $this->_container
                    ->get('liip_imagine.controller')
                    ->filterAction(new \Symfony\Component\HttpFoundation\Request(), 'assets/flyers/'.$flyer_, $path);
                }
            }
        }
    }

    /**
     * This function extracts the non-tags string and returns a correctly formatted string
     * It can handle all html entities e.g. &amp;, &quot;, etc..
     *
     * @param string $s
     * @param int    $srt
     * @param int    $len
     * @param bool/integer  Strict if this is defined, then the last word will be complete. If this is set to 2 then the last sentence will be completed
     * @param string A string to suffix the value, only if it has been chopped
     */
    public function html_substr($s, $srt, $len = null, $strict = false, $suffix = null)
    {
        if (is_null($len)) {
            $len = strlen($s);
        }

        $f = 'static $strlen=0;
            if ( $strlen >= ' .$len.' ) { return "><"; }
            $html_str = html_entity_decode( $a[1] );
            $subsrt   = max(0, ('.$srt.'-$strlen));
            $sublen = ' .(empty($strict) ? '('.$len.'-$strlen)' : 'max(@strpos( $html_str, "'.($strict === 2 ? '.' : ' ').'", ('.$len.' - $strlen + $subsrt - 1 )), '.$len.' - $strlen)').';
            $new_str = substr( $html_str, $subsrt,$sublen);
            $strlen += $new_str_len = strlen( $new_str );
            $suffix = ' .(!empty($suffix) ? '($new_str_len===$sublen?"'.$suffix.'":"")' : '""').';
            return ">" . htmlentities($new_str, ENT_QUOTES, "UTF-8") . "$suffix<";';

        return preg_replace(array('#<[^/][^>]+>(?R)*</[^>]+>#', "#(<(b|h)r\s?/?>){2,}$#is"), '', trim(rtrim(ltrim(preg_replace_callback('#>([^<]+)<#', create_function(
            '$a',
            $f
        ), ">$s<"), '>'), '<')));
    }

    public function strip_tags_blacklist($html, $tags)
    {
        foreach ($tags as $tag) {
            $regex = '#<\s*'.$tag.'[^>]*>.*?<\s*/\s*'.$tag.'>#msi';
            $html = preg_replace($regex, '', $html);
        }

        return $html;
    }

    protected function isEnvironnementDev()
    {
        return 'dev' == $this->_container->get('kernel')->getEnvironment();
    }
    protected function isEnvironnementProd()
    {
        return !$this->isEnvironnementDev();
    }
    public function searchEventByQuery($q)
    {
        $ids = $this->_container->get('madatsara.elasticsearch')->getAllIdsBysearch('name', $q);
        return $ids;
    }

    public function addFlyerToEventFlyers($id, $originalFlyer,$tmpName=false)
    {
        // $this->logger->info('Start --- '.__FUNCTION__.' - line '.__LINE__.' - id : '.$id.' - $originalFlyer : '.$originalFlyer);
        $em = $this->_container->get('doctrine.orm.entity_manager');

        $entityevent = $em->getRepository('mdtshomeBundle:Event')->find($id);

        //Si fichier tmp
        if ($tmpName) {
            $originalFlyer = $this->getFlyerNameByTmp($tmpName,$entityevent);
        }

        //Ajouter oldimage dans eventflyer
        $entityflyer = $em->getRepository('mdtshomeBundle:EventFlyers')->findByImage($originalFlyer);
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__);
        if (!$entityflyer) {
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__);
            $entityflyer = new EventFlyers();
            $entityflyer->setImage($originalFlyer);
            $entityflyer->setCrdate(new \DateTime());
            $entityflyer->setHidden(0);
            $em->persist($entityflyer);
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__);
        } else {
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__);
            $entityflyer = $entityflyer[0];
        }

        if ($entityflyer) {

            $entityevent->addEventFlyer($entityflyer);
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__);
        }

        $em->flush();
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__);
        $this->createThumbFlyer($entityevent);
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__);

        return ['id'=>$entityflyer->getId(), 'filename' => $originalFlyer];
    }

    /**
     * Mettre a jour la table querylog
     *
     * @param string $query
     *
     * @return mixed
     */
    public function updateQueryLogByQuery($query)
    {

        // Recup l'entity
        $entity  = $this->em->getRepository('mdtshomeBundle:esquerylog')->findByQuery($query);


        if (!$entity) {
            return false;
        }

        $entity = $entity[0];

        // MAJ
        $hitsInDB = $entity->getHits();
        $entity->setHits($hitsInDB+1);

        // Ecrire dans la table
        $this->em->flush();

    }

    /**
     * Attacher un abonné par rapport au referer
     *
     * @param $entity_nl
     * @param $referer
     *
     */
    private function attachEmailAbonneByReferer($entityAbonneNewsletter, $referer)
    {

        $eventEntity = $this->getEventTypeOfReferer($referer);
        if (!$eventEntity)
            return false;

        $eventId = $eventEntity->getId();
        // $this->logger->info(__LINE__.'---'.$eventId);

        // Artistes de l'event
        $arrArtists = $this->em->getRepository('mdtshomeBundle:Event')->getAllArtistsIDOfEvent($eventId);
        $this->attachArtistsToAbonneNewsletter($arrArtists,$entityAbonneNewsletter);

        // Lieux de l'event
        $arrLieux = $this->em->getRepository('mdtshomeBundle:Event')->getAllPlacesIDOfEvent($eventId);
        // $this->logger->info(__LINE__.'---'.print_r($arrLieux,true));
        $this->attachLieuxToAbonneNewsletter($arrLieux,$entityAbonneNewsletter);

    }

    /**
     * @param $referer
     */
    private function getCleanedReferer($referer)
    {
        $scheme = $this->getParameter('router.request_context.scheme');
        $host = $this->getParameter('router.request_context.host');

        // $this->logger->info(__LINE__.'---'.$scheme.'---'.$host);

        // Dev ou Env
        $isDevEnv = $this->isEnvironnementDev();
        // $this->logger->info(__LINE__.'---'.$isDevEnv);

        // Supprimer URL
        $referer = str_replace($scheme . '://' . $host, '', $referer);

        // Supprimer si envDev
        if ($isDevEnv) {
            $referer = str_replace('/app_dev.php/', '', $referer);
        }

        if (!$isDevEnv) {
            $referer = str_replace('/', '', $referer);
        }

        // Supprimer .html
        $referer = str_replace('.html', '', $referer);

        $referer = trim($referer);

        return $referer;
    }

    private function getEventTypeOfReferer($referer)
    {
        $referer = $this->getCleanedReferer($referer);
        // $this->logger->info(__LINE__.'---'.$referer );
        if ($referer=='')
            return false;

        // Url du type "evenement_[slug}"
        $prefixe = substr($referer,0,10);
        $suffixe = substr($referer,10   );

        // $this->logger->info(__LINE__.'---'.$prefixe );
        // $this->logger->info(__LINE__.'---'.$suffixe );

        if ($prefixe!='evenement_')
            return false;

        // Chercher l'identifiant relatif au suffixe URL dans entity event
        $eventEntity = $this->em->getRepository('mdtshomeBundle:Event')->findOneByOrigSlug($suffixe);
        if (!$eventEntity)
            return false;

        return $eventEntity;

    }

    private function attachArtistsToAbonneNewsletter($arrArtists, $entityAbonneNewsletter)
    {
        // $this->logger->info(__LINE__.'---'.print_r($arrArtists, true) );
        if (count($arrArtists)<=0)
            return false;

        // Pour chaque artiste
        foreach ($arrArtists as $artisteId) {

            $artisteId = filter_var($artisteId, FILTER_VALIDATE_INT);
            if ($artisteId>0) {
                $entityArtiste = $this->em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findOneById($artisteId);
                $entityAbonneNewsletter->addEventArtistesDjOrganisateur($entityArtiste);
            }
        }
    }

    private function attachLieuxToAbonneNewsletter($arrLieux, $entityAbonneNewsletter)
    {
        // $this->logger->info(__LINE__.'---'.print_r($arrLieux,true));
        if (count($arrLieux)<=0)
            return false;

        // Pour chaque Lieux
        foreach ($arrLieux as $lieuxId) {


            $lieuxId = filter_var($lieuxId, FILTER_VALIDATE_INT);
            if ($lieuxId>0) {
                $entityLieu = $this->em->getRepository('mdtshomeBundle:EventLieu')->findOneById($lieuxId);
                $entityAbonneNewsletter->addEventLieu($entityLieu);

                // Ce lieu peut appartenir a un pays, une région, une commune ou un quartier ?
                $this->attachPaysRegionLocalityQuartierToAbonne($entityLieu, $entityAbonneNewsletter);

            }

        }
    }

    private function attachPaysRegionLocalityQuartierToAbonne($entityLieu, $entityAbonneNewsletter)
    {
        // Un pays
        if (!is_null($entityLieu->getCountry())) {
            $entityAbonneNewsletter->addCountry($entityLieu->getCountry());
        }

        // Une commune
        if (!is_null($entityLieu->getLocality())) {
            $entityAbonneNewsletter->addLocality($entityLieu->getLocality());
        }

        // Une region
        if (!is_null($entityLieu->getRegion())) {
            $entityAbonneNewsletter->addRegion($entityLieu->getRegion());
        }

        // Un quartier
        if (!is_null($entityLieu->getQuartier())) {
            $entityAbonneNewsletter->addQuartier($entityLieu->getQuartier());
        }
    }

    private function attachPaysToAbonneIp($country_name, $entityAbonneNewsletter)
    {
        // $country_name = 'France';
        // Chercher le pays
        $entityCountry = $this->em->getRepository('mdtshomeBundle:countries')->findOneByName($country_name);
        // $this->logger->info(__LINE__.'---'.$entityCountry );
        if (!$entityCountry)
            return false;

        // Recup les infos
        $name = $entityCountry->getName();
        $name = mb_strtolower($name);
        $countryID = $entityCountry->getId();

        // $this->logger->info(__LINE__.'---'.$name );

        // Si Madagascar -> forcer avec l'identifiant de Madagasikara
        if ('madagascar' == $name && $this->hasParameter('madagasikara_id')){
            $countryID = $this->getParameter('madagasikara_id');
        }

        // $this->logger->info(__LINE__.'--- $countryID : '.$countryID );

        if ($countryID!=$entityCountry->getId()) {
            $entityCountry = $this->em->getRepository('mdtshomeBundle:countries')->findOneById($countryID);
        }

        // Enregistrer si ce pays n'est pas deja dans la relation
        $this->addCountryToNewsletter($entityCountry,$entityAbonneNewsletter);



    }

    private function addCountryToNewsletter($entityCountry, $entityAbonneNewsletter)
    {
        // $this->logger->info(__FUNCTION__.' - line '.__LINE__.'----'.$entityAbonneNewsletter->getEmail());
        // Est-ce que ce pays fait partie de la liste des pays deja enregistres
        $countries = $entityAbonneNewsletter->getCountries();
        $arrCountryID = [];
        foreach ($countries as $country) {
            // $this->logger->info(__FUNCTION__.' - line '.__LINE__.' country getId '.$country->getId() );
            array_push($arrCountryID, $country->getId());
        }

        if (in_array($entityCountry->getId(), $arrCountryID))
            return false;


        $entityAbonneNewsletter->addCountry($entityCountry);

        // Ecrire dans la BDD
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $em->persist($entityAbonneNewsletter);
        $em->flush();
    }

    private function saveHistoryEventbymember(EventByMember $entity,Event $entityevent)
    {
        //  $this->logger->info(__LINE__.'-'.__FUNCTION__);
        $q = ['event_id' => $entityevent->getId(), 'eventbymember_id' => $entity->getId() , 'authorinfotype_id' => $entity->getAuthorinfotype()->getId()];
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.'-'.print_r($q,true));
        $rowCount = $this->em->getRepository('mdtsFrontendBundle:HistoryEventbymemberSent')->findAllQuery($q);

        $emailAuthorInfoType = ( $entity->getEmailauthorinfotype() ?$entity->getEmailauthorinfotype()->getId():0);
        $paramIdEmailAutorInfoType = ($this->hasParameter('paramidemailauthorinfotype')?$this->getParameter('paramidemailauthorinfotype'):0);
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.'-'.print_r(['rowC'=>count($rowCount),'emailAuthorInfoType'=>$emailAuthorInfoType,'paramIdEmailAutorInfoType'=>$paramIdEmailAutorInfoType],true));
        /*ld(count($rowCount));
        ld($emailAuthorInfoType);
        ld($paramIdEmailAutorInfoType);*/
        if(count($rowCount)>0 && $emailAuthorInfoType!=$paramIdEmailAutorInfoType)
            return  false;

        // $this->logger->info(__LINE__.'-'.__FUNCTION__);
        $entityHistory = new HistoryEventbymemberSent();
        $entityHistory->setEvent($entityevent);
        $entityHistory->setEventbymember($entity);
        $entityHistory->setAuthorinfotype($entity->getAuthorinfotype());

        $this->em->persist($entityHistory);
        $this->em->flush();

        // Envoyer email
        $this->sendMailHistoryEventbymember($entity,$entityevent);
    }

    private function sendMailHistoryEventbymember(EventByMember $entity,Event $entityevent)
    {
        // $this->logger->info(__LINE__.'-'.__FUNCTION__);
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.'- getDontsendEMail: '.$entity->getAuthorinfotype()->getDontsendEMail() );

        // Ne pas envoyer email
        if ($entity->getAuthorinfotype()->getDontsendEMail()==1 )
            return false;

        // Auteur du flyer
        $auteur = $entity->getAuthor();

        /*ld( $auteur->getEmailRs());
        ld( $auteur->getEmail());*/

        $arrEmail = [];

        $emailrs = $auteur->getEmailRs();
        $emailMember = $auteur->getEmail();

        // si ces emails sont valides
        if (filter_var($emailrs, FILTER_VALIDATE_EMAIL))
            $arrEmail[] = $emailrs;

        if (filter_var($emailMember, FILTER_VALIDATE_EMAIL))
            $arrEmail[] = $emailMember;

        // Supprimer les doublons
        $arrEmail = array_unique($arrEmail);
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.' emails : '.print_r($arrEmail,true));

        if (count($arrEmail)<=0)
            return false;

        // ld( $arrEmail );

        $default_email = $this->getParameter('default_email');
        // ld($default_email);
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.' - default_email: '.print_r($default_email,true));

        if (count($default_email)<=0)
            return false;



        if (!array_key_exists('from', $default_email))
            return false;


        $fromemail = $default_email['from'];

        $q = ['authorinfo_id' => $entity->getAuthorinfotype()->getId()];
        $entityEmailInfoType = $this->em->getRepository('mdtsFrontendBundle:EmailByInfoType')->findByQuery($q);
        if (!$entityEmailInfoType)
            return false;

        $bodyEmail = $entityEmailInfoType[0]->getContent();
        $objetEmail = $entityEmailInfoType[0]->getObjetmail();
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.'- bodyEmail : '.$bodyEmail );

        $titreflyer = $entity->getName();
        $created_at = $entity->getCreatedAt();
        $status_name = $entity->getStatus()->getName();
        $event_titre = $entityevent->getName();
        $event_slug = $entityevent->getOrigSlug();

        $arrEmails = [
            'titreflyer' => $titreflyer ,
            'created_at' => $created_at,
            'event_titre' => $event_titre,
            'event_slug' => $event_slug,
            'status_name' => $status_name
        ];
        /*$bodyEmail = $this->_container->get('templating')->render(
            $bodyEmail,
            $arrEmails
        );*/

        $template = $this->_container->get('twig')->createTemplate($bodyEmail);
        $bodyEmail = $template->render( $arrEmails );
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.'- bodyEmail : '.$bodyEmail );

        $template = $this->_container->get('twig')->createTemplate($objetEmail);
        $objetEmail = $template->render( $arrEmails );
        // $this->logger->info(__LINE__.'-'.__FUNCTION__.'- bodyEmail : '.$objetEmail );


        $msg = $this->_container->get('templating')->render(
            'mdtshomeBundle:Email:generique.html.twig',
            array(
                'content' => $bodyEmail,
            )
        );

        // $this->logger->info(__LINE__.'-'.__FUNCTION__.' msg : '.$msg);

        // $fromemail = str_replace('madatsara.com','localhost', $fromemail);
        foreach($arrEmail as $email) {
            // $this->logger->info(__LINE__.'-'.__FUNCTION__.' email : '.$email);
            // $email = str_replace('gmail.com','localhost', $email);
             $this->sendMail($email, $objetEmail, $msg, $fromemail);
        }
        return true;
    }

    private function saveEventByMember(Event $entity, $eventbymemberId)
    {
        // $this->logger->info(__LINE__.'-'.__FUNCTION__);
        if (!$entity)
            return false;

        if (!filter_var($eventbymemberId, FILTER_VALIDATE_INT))
            return false;

        // $this->logger->info(__LINE__.'-'.__FUNCTION__);

        $entitytype = $this->em->getRepository('mdtsFrontendBundle:EventByMember')->find($eventbymemberId);

        if (!$entitytype)
            return false;
        $this->logger->info(__LINE__.'-'.__FUNCTION__);

        $entitytype->setEvent($entity);
        // $this->logger->info(__LINE__.'-'.__FUNCTION__);

        // Save
        $this->em->flush();

    }

    /**
     * @param $groupartiste_id
     * @param $artiste_id
     */
    public function addArtisteInGroupartists($groupartiste_id, $artiste_id)
    {
        $entityartiste = $this->em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($artiste_id);
        $entityartistegroup = $this->em->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->find($groupartiste_id);

        //Recuperer les artistes de l'event et si existe - supprimer avant d'ajouter
        foreach ($entityartistegroup->getParentArtiste() as $artiste) {
            //Supprimer si trouvé
            if ($artiste_id == $artiste->getId()) {
                $entityartistegroup->removeParentArtiste($entityartiste);
            }
        }

        $entityartistegroup->addParentArtiste($entityartiste);
        $this->em->flush();
    }

    public function createOrUpdateEventByMemberAuthorinfoType(EventByMemberAuthorinfoType $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    public function createOrUpdateEventByMemberEmailAuthorinfoType(EventByMemberEmailAuthorinfoType $entity, $id = 0)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        if ($id <= 0) {
            $em->persist($entity);
        }
        $em->flush();

        return true;
    }

    private function createEntityFlyer($arrD=[])
    {
        $ismain = array_key_exists('ismain', $arrD) ? $arrD['ismain'] : '';
        $filename = array_key_exists('filename', $arrD) ? $arrD['filename'] : '';

        $entityflyer = new EventFlyers();
        $entityflyer->setIsmain($ismain);
        $entityflyer->setHidden(0);
        $entityflyer->setImage($filename);
        $entityflyer->setCrdate(new \Datetime());

        $this->em->persist($entityflyer);
        return $entityflyer;
    }

    private function getFlyerNameByTmp($tmpName,$entity)
    {
        $fs = new Filesystem();
        $path = $this->_container->get('kernel')->getRootDir().'/../web/tmp/';
        if( !$fs->exists($path.$tmpName) )
            return false;


        $info = new \SplFileInfo($tmpName);
        $filenamecopy = $this->_container->get('slugify')->slugify($entity->getName()).'_'.uniqid().'.'.$info->getExtension();
        $fs->copy($path.$tmpName, $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/'.$filenamecopy);
        $fs->remove($path.$tmpName);

        return $filenamecopy;
    }

    private function removeHistoryEventbymemberSent($q)
    {
        $em = $this->_container->get('doctrine.orm.entity_manager');
        $results = $em->getRepository('mdtsFrontendBundle:HistoryEventbymemberSent')->findAllQuery($q);
        foreach ($results as $row) {
            $em->remove($row);
        }
        $em->flush();

    }

    /**
     * @return \Symfony\Component\Form\FormView
     */
    public function createFormAllIds(String $url, String $name='mdts_homebundle_all', String $fieldHiddenIdName='ids')
    {
        $actionForm = $this->_container->get('router')->generate($url);

        $idsForm = $this->_container->get('form.factory')->createNamedBuilder($name)
            ->setAction($actionForm)
            ->setMethod('DELETE')
            ->add($fieldHiddenIdName, HiddenType::class)
            ->getForm();

        return $idsForm;
    }

    public function getPathParam($str)
    {
        $path = '';

        if ($this->hasParameter($str))
            $path = $this->_container->get('kernel')->getRootDir().'/../'.$this->getParameter($str);

        return $path;
    }

    private function getDataUdgerDb(array $data)
    {
        $path_udgerdb = $this->getPathParam('path_udgerdb');
        $browser = [];
        $factory = new \Udger\ParserFactory($path_udgerdb);
        $parser = $factory->getParser();
        try {
            $ua = $this->getHeaders('User-Agent');
            $ip = $this->getIpAddress();
//                $ua = $this->_container->get('request')->headers->get('User-Agent');
            $parser->setUA($ua);
            $parser->setIP($ip);
            $browser = $parser->parse();
        } catch (Exception $ex) {

        }
        /*$parser = new Parser();
        $parser->setDataDir($path_udgerdb);
        $parser->setAccessKey('XXXXXX');
        $browser = $parser->parse($this->_container->get('request')->headers->get('User-Agent'));*/
//            print_r($browser);exit;
        if (is_array($browser)) {
            $data = array_merge($data,  $browser );
        }

        return $data;
    }

    public function getListPlaceByEvents($id)
    {
        if (!$id) {
            return [];
        }

        $countryArr = [];
        $regionArr = [];
        $localityArr = [];
        $quartierArr = [];
        $countryIdsArr = [];
        $regionIdsArr = [];
        $localityIdsArr = [];
        $quartierIdsArr = [];
        $resultsArr = [];

        $slug = '';
        $type = 'country';
        $prefixpathurl = '';
        $typePlace = '';

        $limit = 0;

        $currentRoute = $this->requestStack->attributes->get('_route');

        // 2 eme argument de la funct - type
        if (func_num_args()>1) {
            $type = func_get_arg(1);
        }

        // 3 eme argument de la funct - limit
        if (func_num_args()>2 && filter_var(func_get_arg(2))) {
            $limit = func_get_arg(2);
        }

        // 4e argument  de la funct - slug
        if (func_num_args()>3) {
            $slug = func_get_arg(3);
        }

        // 5e argument  de la funct - prefix path
        if (func_num_args()>4) {
            $prefixpathurl = func_get_arg(4);
        }

        $slugIsPlace = (''!=$prefixpathurl && ''!=$currentRoute && strstr($currentRoute,$prefixpathurl));

        if ($slugIsPlace) {
            // Detecter
            $typePlace = str_replace($prefixpathurl,'',$currentRoute);
        }



        $cs = $this->em->getRepository('mdtshomeBundle:countries')->getByEvents($id);
        $rs = $this->em->getRepository('mdtshomeBundle:region')->getByEvents($id);
        $ls = $this->em->getRepository('mdtshomeBundle:locality')->getByEvents($id);
        $qs = $this->em->getRepository('mdtshomeBundle:quartier')->getByEvents($id);


        list($countryIdsArr,$regionIdsArr,$localityIdsArr,$quartierIdsArr) = $this->getIdsPlaces($typePlace,$slug);



        foreach ($cs as $c) {
            $cId = $c->getId();
            if (count($countryIdsArr)>0 && in_array($cId,$countryIdsArr)) {
                array_push($countryArr, $cId);
            }

            if (!$slugIsPlace) {
                array_push($countryArr, $cId);
            }

        }

        foreach ($rs as $c) {
            $cId = $c->getId();

            if (count($regionIdsArr)>0 && in_array($cId,$regionIdsArr)) {
                array_push($regionArr , $cId);
            }

            if (!$slugIsPlace) {
                array_push($regionArr, $cId);
            }
        }

        foreach ($ls as $c) {
            $cId = $c->getId();

            if (count($localityIdsArr)>0 && in_array($cId,$localityIdsArr)) {
                array_push($localityArr , $cId);
            }

            if (!$slugIsPlace) {
                array_push($localityArr, $cId);
            }
        }

        foreach ($qs as $c) {
            $cId = $c->getId();

            if (count($quartierIdsArr)>0 && in_array($cId,$quartierIdsArr)) {
                array_push($quartierArr , $cId);
            }

            if (!$slugIsPlace) {
                array_push($quartierArr, $cId);
            }
        }

        // remove duplicate
        $countryArr = array_unique($countryArr);
        $regionArr = array_unique($regionArr);
        $localityArr = array_unique($localityArr);
        $quartierArr = array_unique($quartierArr);

        // Get only $limit elements by array
        if ($limit>0) {
            $countryArr = array_slice($countryArr, 0, $limit);
            $regionArr = array_slice($regionArr, 0, $limit);
            $localityArr = array_slice($localityArr, 0, $limit);
            $quartierArr = array_slice($quartierArr, 0, $limit);
        }

//        print_r($countryArr);
//        print_r($regionArr);
//        print_r($localityArr);
//        print_r($quartierArr);

        // SI aucun pays, region, ville et quartier
        if (empty($countryArr) && empty($regionArr) && empty($localityArr) && empty($quartierArr)) {
            return [];
        }

        // which type?
        switch ($type) {
            case 'region':
                if (empty($regionArr)) {
                    return [];
                }
                $row = $this->em->getRepository('mdtshomeBundle:region')->findByIds($regionArr);
                break;
            case 'locality':
                if (empty($localityArr)) {
                    return [];
                }
                $row = $this->em->getRepository('mdtshomeBundle:locality')->findByIds($localityArr);
                break;
            case 'quartier':
                if (empty($quartierArr)) {
                    return [];
                }
                $row = $this->em->getRepository('mdtshomeBundle:quartier')->findByIds($quartierArr);
                break;
            case 'country':
            default:
                if (empty($countryArr)) {
                    return [];
                }
                $row = $this->em->getRepository('mdtshomeBundle:countries')->findByIds($countryArr);
                break;
        }


        $rows = $row->getResult();
        foreach ($rows as $row) {
            $name = $row->getName();
            $slugRow = $row->getSlug();
            $class = (''!=$slug && $slug==$slugRow ? 'active' : '');
            array_push($resultsArr, [
                'name' => $name,
                'slug' => $slugRow,
                'css' => $class
            ]);
        }
//        print_r($resultsArr);

        return $resultsArr;



    }

    private function getIdsPlaces($typePlace, $slug)
    {
        $countryIdsArr = [];
        $regionIdsArr = [];
        $localityIdsArr = [];
        $quartierIdsArr = [];

        if (''==$typePlace || ''==$slug) {
            return [$countryIdsArr,$regionIdsArr,$localityIdsArr,$quartierIdsArr];
        }

        switch ($typePlace) {
            case 'region':
                // Afficher le pays de cette region
                $repo = $this->em->getRepository('mdtshomeBundle:region')->findOneBySlug($slug);
                $countryId = $repo->getCountry()->getId();
                $regionId = $repo->getId();
                array_push($countryIdsArr, $countryId);

                // Region
                array_push($regionIdsArr, $regionId);

                // Afficher les villes dans cette region
                $regionid = $repo->getId();
                $rowV = $this->em->getRepository('mdtshomeBundle:locality')->findByRegion($regionid);
                foreach ($rowV as $ville) {
                    $vid = $ville->getId();
                    array_push($localityIdsArr, $vid);
                }

                // Quartier dans les villes
                foreach ($rowV as $ville) {
                    $vid = $ville->getId();
                    $rowQ = $this->em->getRepository('mdtshomeBundle:quartier')->findAllQuery(['ville_id'=>$vid]);
                    foreach ($rowQ as $quartier) {
                        $qid = $quartier->getId();
                        array_push($quartierIdsArr, $qid);
                    }
                }
                break;

            case 'locality':
                // Region de la ville
                $repo = $this->em->getRepository('mdtshomeBundle:locality')->findOneBySlug($slug);
                $regionId = $repo->getRegion()->getId();
                $localityId = $repo->getId();
                array_push($regionIdsArr, $regionId);

                // Pays de la region
                $repo = $this->em->getRepository('mdtshomeBundle:region')->find($regionId);
                $countryId = $repo->getCountry()->getId();
                array_push($countryIdsArr, $countryId);

                // Ville
                array_push($localityIdsArr, $localityId);

                // Quartier dans les villes
                $vid = $localityId;
                $rowQ = $this->em->getRepository('mdtshomeBundle:quartier')->findAllQuery(['ville_id'=>$vid]);
                foreach ($rowQ as $quartier) {
                    $qid = $quartier->getId();
                    array_push($quartierIdsArr, $qid);
                }

                break;

            case 'quartier':
                // Ville
                $repo = $this->em->getRepository('mdtshomeBundle:quartier')->findOneBySlug($slug);
                $localityId = $repo->getLocality()->getId();
                $quartierId = $repo->getId();
                array_push($localityIdsArr, $localityId);

                // Region de la ville
                $repo = $this->em->getRepository('mdtshomeBundle:locality')->find($localityId);
                $regionId = $repo->getRegion()->getId();
                array_push($regionIdsArr, $regionId);

                // Pays de la region
                $repo = $this->em->getRepository('mdtshomeBundle:region')->find($regionId);
                $countryId = $repo->getCountry()->getId();
                array_push($countryIdsArr, $countryId);

                // Quartier
//                    array_push($quartierIdsArr, $quartierId);
                // Quartier dans les villes
                $vid = $localityId;
                $rowQ = $this->em->getRepository('mdtshomeBundle:quartier')->findAllQuery(['ville_id'=>$vid]);
                foreach ($rowQ as $quartier) {
                    $qid = $quartier->getId();
                    array_push($quartierIdsArr, $qid);
                }
                break;

            case 'country':
                // Pays
                $repo = $this->em->getRepository('mdtshomeBundle:countries')->findOneBySlug($slug);
                $countryId = $repo->getId();
                array_push($countryIdsArr, $countryId);

                // Regions dans le pays
                $rowsR = $this->em->getRepository('mdtshomeBundle:region')->findAllQuery(['country_id'=>$countryId]);
                foreach ($rowsR as $row) {
                    $rid = $row->getId();
                    array_push($regionIdsArr, $rid);
                }


            default:
                break;
        }

        return [$countryIdsArr,$regionIdsArr,$localityIdsArr,$quartierIdsArr];

    }

    /**
     * Sync subscribers emails between 2 apps
     *
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     *
     */
    private function syncSubscribersEmails()
    {
        $sql = "INSERT INTO mailtrain.subscription__1 (cid,email,tz,created) 
                SELECT 
                  SUBSTR(conv(floor(rand() * 99999999999999), 20, 36),1,9), 
                  a.email,
                  'utc',
                  a.inscrit_date 
                FROM madatsara.newsletter_abonne a 
                  LEFT JOIN mailtrain.subscription__1 b ON b.email COLLATE utf8_unicode_ci  = a.email COLLATE utf8_unicode_ci 
                WHERE b.email IS NULL;
                UPDATE mailtrain.lists SET subscribers = (SELECT COUNT(*) FROM mailtrain.subscription__1 WHERE status = 1)  WHERE id = 1";

        $em = $this->_container->get('doctrine.orm.entity_manager');
        $connection = $em->getConnection();

        $res = $connection->prepare($sql);
        $res->execute();

        return true;
    }


}
