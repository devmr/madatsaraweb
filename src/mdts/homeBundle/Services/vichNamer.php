<?php

namespace mdts\homeBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use Vich\UploaderBundle\Mapping\PropertyMapping;
use Vich\UploaderBundle\Naming\NamerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;

class vichNamer implements NamerInterface
{
    private $_container;

    // notice that we're injecting service-container to be able to get other symfony2 services
    public function __construct(Container $serviceContainer)
    {
        $this->_container = $serviceContainer;
    }

    public function name($object, PropertyMapping $mapping): string
    {
        $file = $mapping->getFile($object);

        if (null !== $object && method_exists($object, 'getName')) {
            $name = $this->_container->get('slugify')->slugify($object->getName());
        } else {
            $name = $this->_container->get('slugify')->slugify($file->getClientOriginalName());
        }

        $path = $this->_container->get('kernel')->getRootDir().'/../web/assets/flyers/'.$name.'.'.$this->getExtension($file);
        $fs = new Filesystem();

        //Si le fichier existe avec le même nom
        if ($fs->exists($path)) {
            $name .= '_'.uniqid();
        }

        if ($extension = $this->getExtension($file)) {
            $name = sprintf('%s.%s', $name, $extension);
        }

        return $name;
    }

    /**
     * Guess the extension of the given file.
     *
     * @param UploadedFile $file
     *
     * @return string|null
     */
    private function getExtension(UploadedFile $file)
    {
        $originalName = $file->getClientOriginalName();
        if ($extension = pathinfo($originalName, PATHINFO_EXTENSION)) {
            return $extension;
        }
        if ($extension = $file->guessExtension()) {
            return $extension;
        }

        return null;
    }
}
