<?php

namespace mdts\homeBundle\Entity;

/**
 * eventlocalall.
 */
class eventlocalall
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $fooid;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fooid.
     *
     * @param string $fooid
     *
     * @return eventlocalall
     */
    public function setFooid($fooid)
    {
        $this->fooid = $fooid;

        return $this;
    }

    /**
     * Get fooid.
     *
     * @return string
     */
    public function getFooid()
    {
        return $this->fooid;
    }
}
