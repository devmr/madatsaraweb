<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * EventLieu.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\EventLieuRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Vich\Uploadable
 * @ORM\Table(indexes={
 *  @ORM\Index(name="name_idx", columns={"name", "orig_slug"}),
 *  @ORM\Index(name="name_ftx", columns={"name"}, flags={"fulltext"})  
 * })
 */
class EventLieu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @Gedmo\SortableGroup
     * @ORM\Column(name="category", type="string", length=128, nullable=true)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="gps", type="string", length=255, nullable=true)
     */
    private $gps;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="facebook", type="string", length=255, nullable=true)
     */
    private $facebook;

    /**
     * @var string
     *
     * @ORM\Column(name="www", type="string", length=255, nullable=true)
     */
    private $www;

    /**
     * @var int
     *
     * @ORM\Column(name="t3id", type="integer", nullable=true )
     */
    private $t3id;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventHeure", cascade={"persist"})
     */
    private $heure;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\countries" )
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\region" )
     */
    private $region;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\locality" )
     */
    private $locality;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\quartier" )
     */
    private $quartier;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="logolieux", fileNameProperty="logo")
     *
     * @var File
     */
    private $logoFile;

    /**
     * @var string
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="mdts\homeBundle\Entity\TelsLieu", mappedBy="eventlieu")
     */
    private $TelsLieu;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\Column(name="orig_slug", type="string",length=255, nullable=true)
     */
    private $origSlug;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->heure = new \Doctrine\Common\Collections\ArrayCollection();
        $this->TelsLieu = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return EventLieu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address.
     *
     * @param string $address
     *
     * @return EventLieu
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address.
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set gps.
     *
     * @param string $gps
     *
     * @return EventLieu
     */
    public function setGps($gps)
    {
        $this->gps = $gps;

        return $this;
    }

    /**
     * Get gps.
     *
     * @return string
     */
    public function getGps()
    {
        return $this->gps;
    }

    /**
     * Set tel.
     *
     * @param string $tel
     *
     * @return EventLieu
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel.
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return EventLieu
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set facebook.
     *
     * @param string $facebook
     *
     * @return EventLieu
     */
    public function setFacebook($facebook)
    {
        $this->facebook = $facebook;

        return $this;
    }

    /**
     * Get facebook.
     *
     * @return string
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * Set www.
     *
     * @param string $www
     *
     * @return EventLieu
     */
    public function setWww($www)
    {
        $this->www = $www;

        return $this;
    }

    /**
     * Get www.
     *
     * @return string
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Set logo.
     *
     * @param string $logo
     *
     * @return EventLieu
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return string
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set t3id.
     *
     * @param int $t3id
     *
     * @return EventLieu
     */
    public function setT3id($t3id)
    {
        $this->t3id = $t3id;

        return $this;
    }

    /**
     * Get t3id.
     *
     * @return int
     */
    public function getT3id()
    {
        return $this->t3id;
    }

    /**
     * Add eventlieu.
     *
     * @param \mdts\homeBundle\Entity\EventLieu $eventlieu
     *
     * @return EventLieu
     */
    public function addEventlieu(\mdts\homeBundle\Entity\EventLieu $eventlieu)
    {
        $this->eventlieu[] = $eventlieu;

        return $this;
    }

    /**
     * Remove eventlieu.
     *
     * @param \mdts\homeBundle\Entity\EventLieu $eventlieu
     */
    public function removeEventlieu(\mdts\homeBundle\Entity\EventLieu $eventlieu)
    {
        $this->eventlieu->removeElement($eventlieu);
    }

    /**
     * Get eventlieu.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventlieu()
    {
        return $this->eventlieu;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return EventLieu
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime $deletedAt
     *
     * @return EventLieu
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return EventLieu
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set category.
     *
     * @param string $category
     *
     * @return EventLieu
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add heure.
     *
     * @param \mdts\homeBundle\Entity\EventHeure $heure
     *
     * @return EventLieu
     */
    public function addHeure(\mdts\homeBundle\Entity\EventHeure $heure)
    {
        $this->heure[] = $heure;

        return $this;
    }

    /**
     * Remove heure.
     *
     * @param \mdts\homeBundle\Entity\EventHeure $heure
     */
    public function removeHeure(\mdts\homeBundle\Entity\EventHeure $heure)
    {
        $this->heure->removeElement($heure);
    }

    /**
     * Get heure.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * Set locality.
     *
     * @param \mdts\homeBundle\Entity\locality $locality
     *
     * @return EventLieu
     */
    public function setLocality(\mdts\homeBundle\Entity\locality $locality = null)
    {
        $this->locality = $locality;

        return $this;
    }

    /**
     * Get locality.
     *
     * @return \mdts\homeBundle\Entity\locality
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Set quartier.
     *
     * @param \mdts\homeBundle\Entity\quartier $quartier
     *
     * @return EventLieu
     */
    public function setQuartier(\mdts\homeBundle\Entity\quartier $quartier = null)
    {
        $this->quartier = $quartier;

        return $this;
    }

    /**
     * Get quartier.
     *
     * @return \mdts\homeBundle\Entity\quartier
     */
    public function getQuartier()
    {
        return $this->quartier;
    }

    /**
     * Set country.
     *
     * @param \mdts\homeBundle\Entity\countries $country
     *
     * @return EventLieu
     */
    public function setCountry(\mdts\homeBundle\Entity\countries $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \mdts\homeBundle\Entity\countries
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set region.
     *
     * @param \mdts\homeBundle\Entity\region $region
     *
     * @return EventLieu
     */
    public function setRegion(\mdts\homeBundle\Entity\region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \mdts\homeBundle\Entity\region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Event
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setLogoFile(File $image = null)
    {
        $this->logoFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getLogoFile()
    {
        return $this->logoFile;
    }

    /**
     * Add telsLieu.
     *
     * @param \mdts\homeBundle\Entity\TelsLieu $telsLieu
     *
     * @return EventLieu
     */
    public function addTelsLieu(\mdts\homeBundle\Entity\TelsLieu $telsLieu)
    {
        $this->TelsLieu[] = $telsLieu;

        return $this;
    }

    /**
     * Remove telsLieu.
     *
     * @param \mdts\homeBundle\Entity\TelsLieu $telsLieu
     */
    public function removeTelsLieu(\mdts\homeBundle\Entity\TelsLieu $telsLieu)
    {
        $this->TelsLieu->removeElement($telsLieu);
    }

    /**
     * Get telsLieu.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTelsLieu()
    {
        return $this->TelsLieu;
    }

    /**
     * Set origSlug.
     *
     * @param string $origSlug
     *
     * @return Event
     */
    public function setOrigSlug($origSlug)
    {
        $this->origSlug = $origSlug;

        return $this;
    }

    /**
     * Get origSlug.
     *
     * @return string
     */
    public function getOrigSlug()
    {
        return $this->origSlug;
    }
}
