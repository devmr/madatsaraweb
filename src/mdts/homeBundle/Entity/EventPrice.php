<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventPrice.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\EventPriceRepository")
 */
class EventPrice
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", nullable=true )
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="detailprice", type="string", length=255, nullable=true)
     */
    private $detailprice = null;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\Devises", inversedBy="devise")
     */
    private $devise;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price.
     *
     * @param float $price
     *
     * @return EventPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price.
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set detailprice.
     *
     * @param string $detailprice
     *
     * @return EventPrice
     */
    public function setDetailprice($detailprice)
    {
        $this->detailprice = $detailprice;

        return $this;
    }

    /**
     * Get detailprice.
     *
     * @return string
     */
    public function getDetailprice()
    {
        return $this->detailprice;
    }

    /**
     * Set devise.
     *
     * @param \mdts\homeBundle\Entity\Devises $devise
     *
     * @return EventPrice
     */
    public function setDevise(\mdts\homeBundle\Entity\Devises $devise = null)
    {
        $this->devise = $devise;

        return $this;
    }

    /**
     * Get devise.
     *
     * @return \mdts\homeBundle\Entity\Devises
     */
    public function getDevise()
    {
        return $this->devise;
    }

    /**
     * Set event.
     *
     * @param \mdts\homeBundle\Entity\Event $event
     *
     * @return EventPrice
     */
    public function setEvent(\mdts\homeBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event.
     *
     * @return \mdts\homeBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }
    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->devise = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add devise.
     *
     * @param \mdts\homeBundle\Entity\Devises $devise
     *
     * @return EventPrice
     */
    public function addDevise(\mdts\homeBundle\Entity\Devises $devise)
    {
        $this->devise[] = $devise;

        return $this;
    }

    /**
     * Remove devise.
     *
     * @param \mdts\homeBundle\Entity\Devises $devise
     */
    public function removeDevise(\mdts\homeBundle\Entity\Devises $devise)
    {
        $this->devise->removeElement($devise);
    }
}
