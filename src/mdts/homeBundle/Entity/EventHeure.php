<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventHeure.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\EventHeureRepository")
 */
class EventHeure
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="time")
     */
    private $heure;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set heure.
     *
     * @param \DateTime $heure
     *
     * @return EventHeure
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    /**
     * Get heure.
     *
     * @return \DateTime
     */
    public function getHeure()
    {
        return $this->heure;
    }
}
