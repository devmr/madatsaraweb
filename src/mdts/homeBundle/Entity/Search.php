<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Search.
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Search
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="query", type="string", length=255)
     */
    public $query;

    /**
     * @var date
     *
     * @ORM\Column(name="date_from", type="date")
     */
    public $date_from;

    /**
     * @var date
     *
     * @ORM\Column(name="date_to", type="date")
     */
    public $date_to;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set query.
     *
     * @param string $query
     *
     * @return Search
     */
    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * Get query.
     *
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Set dateFrom.
     *
     * @param \DateTime $dateFrom
     *
     * @return Search
     */
    public function setDateFrom($dateFrom)
    {
        $this->date_from = $dateFrom;

        return $this;
    }

    /**
     * Get dateFrom.
     *
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->date_from;
    }

    /**
     * Set dateTo.
     *
     * @param \DateTime $dateTo
     *
     * @return Search
     */
    public function setDateTo($dateTo)
    {
        $this->date_to = $dateTo;

        return $this;
    }

    /**
     * Get dateTo.
     *
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->date_to;
    }
}
