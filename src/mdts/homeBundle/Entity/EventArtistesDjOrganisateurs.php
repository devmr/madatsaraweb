<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * EventArtistesDjOrganisateurs.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\EventArtistesDjOrganisateursRepository")
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @Vich\Uploadable
 * @ORM\Table(indexes={
 *  @ORM\Index(name="name_idx", columns={"name", "orig_slug"}),  
 *  @ORM\Index(name="name_ftx", columns={"name"}, flags={"fulltext"})  
 * })
 */
class EventArtistesDjOrganisateurs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="vztid", type="integer", nullable=true)
     */
    private $vztid;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @Gedmo\SortableGroup
     * @ORM\Column(name="category", type="string", length=128, nullable=true)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100, nullable=true)
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="crdate", type="datetime", nullable=true)
     */
    private $crdate;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\Event")
     */
    private $event;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\ArticlesEvent", cascade={"persist"})
     * @ORM\JoinTable(name="articles_by_artistes",
     *    joinColumns={@ORM\JoinColumn(name="artiste_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")}
     *   )
     */
    private $ArticlesEvent;

    /**
     * @ORM\Column(name="orig_slug", type="string",length=255, nullable=true)
     */
    private $origSlug;

    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="photoartiste", fileNameProperty="photo")
     *
     * @var File
     */
    private $photoFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventArtistesDjOrganisateurs", cascade={"persist"})
     * @ORM\JoinTable(name="artiste_by_groupartists",
     *    joinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")}
     *     )
     */
    private $parentArtiste;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set vztid.
     *
     * @param int $vztid
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setVztid($vztid)
    {
        $this->vztid = $vztid;

        return $this;
    }

    /**
     * Get vztid.
     *
     * @return int
     */
    public function getVztid()
    {
        return $this->vztid;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime $deletedAt
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set category.
     *
     * @param string $category
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set crdate.
     *
     * @param \DateTime $crdate
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;

        return $this;
    }

    /**
     * Get crdate.
     *
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * Set event.
     *
     * @param \mdts\homeBundle\Entity\Event $event
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function setEvent(\mdts\homeBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event.
     *
     * @return \mdts\homeBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->ArticlesEvent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add articlesEvent.
     *
     * @param \mdts\homeBundle\Entity\ArticlesEvent $articlesEvent
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function addArticlesEvent(\mdts\homeBundle\Entity\ArticlesEvent $articlesEvent)
    {
        $this->ArticlesEvent[] = $articlesEvent;

        return $this;
    }

    /**
     * Remove articlesEvent.
     *
     * @param \mdts\homeBundle\Entity\ArticlesEvent $articlesEvent
     */
    public function removeArticlesEvent(\mdts\homeBundle\Entity\ArticlesEvent $articlesEvent)
    {
        $this->ArticlesEvent->removeElement($articlesEvent);
    }

    /**
     * Get articlesEvent.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticlesEvent()
    {
        return $this->ArticlesEvent;
    }

    /**
     * Set origSlug.
     *
     * @param string $origSlug
     *
     * @return Event
     */
    public function setOrigSlug($origSlug)
    {
        $this->origSlug = $origSlug;

        return $this;
    }

    /**
     * Get origSlug.
     *
     * @return string
     */
    public function getOrigSlug()
    {
        return $this->origSlug;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setPhotoFile(File $image = null)
    {
        $this->photoFile = $image;

        if ($image instanceof UploadedFile) {
            $this->setUpdatedAt(new \DateTime());
        }
    }

    /**
     * @return File
     */
    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    /**
     * Set logo.
     *
     * @param string $logo
     *
     * @return EventLieu
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get logo.
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Event
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }


    /**
     * Add parentArtiste
     *
     * @param \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $parentArtiste
     *
     * @return EventArtistesDjOrganisateurs
     */
    public function addParentArtiste(\mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $parentArtiste)
    {
        $this->parentArtiste[] = $parentArtiste;

        return $this;
    }

    /**
     * Remove parentArtiste
     *
     * @param \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $parentArtiste
     */
    public function removeParentArtiste(\mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $parentArtiste)
    {
        $this->parentArtiste->removeElement($parentArtiste);
    }

    /**
     * Get parentArtiste
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParentArtiste()
    {
        return $this->parentArtiste;
    }
}
