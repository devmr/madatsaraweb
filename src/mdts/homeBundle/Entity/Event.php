<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * Event.
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 * @ORM\Table(indexes={
 *   @ORM\Index(name="name_idx", columns={"name", "orig_slug"}),
 *   @ORM\Index(name="namedesc_ftx", columns={"name", "description"}, flags={"fulltext"})
 *   })
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\EventRepository")
 * @Vich\Uploadable
 */
class Event
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name = null;

    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="flyers", fileNameProperty="flyer")
     *
     * @var File
     */
    private $flyerFile;

    /**
     * @var string
     *
     * @ORM\Column(name="flyer", type="string", length=255, nullable=true)
     */
    private $flyer = null;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(length=255, unique=true)
     */
    private $slug;

    /**
     * @ORM\Column(name="deletedAt", type="datetime", nullable=true)
     */
    private $deletedAt;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="cancelledAt", type="boolean", nullable=true)
     */
    private $cancelledAt;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    private $position;

    /**
     * @Gedmo\SortableGroup
     * @ORM\Column(name="category", type="string", length=128, nullable=true)
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="prixenclair", type="string", length=255, nullable=true)
     */
    private $prixenclair = null;

    /**
     * @var string
     *
     * @ORM\Column(name="prixunique", type="integer", nullable=true )
     */
    private $prixunique;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description = null;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_unique", type="date", nullable=true)
     */
    private $dateUnique;

    /**
     * @var \Time
     *
     * @ORM\Column(name="heure_debut", type="time", nullable=true)
     */
    private $heureDebut;

    /**
     * @var \Time
     *
     * @ORM\Column(name="heure_fin", type="time", nullable=true)
     */
    private $heureFin;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="heure_fin_aube", type="boolean", nullable=true)
     */
    private $heureFinAube;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="hidden", type="boolean", nullable=true)
     */
    private $hidden;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date", nullable=true)
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date", nullable=true)
     */
    private $dateFin;

    /**
     * @var string
     *
     * @ORM\Column(name="rrule", type="string", length=255, nullable=true)
     */
    private $rrule = null;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_event", type="string", length=255, nullable=true)
     */
    private $contactEvent = null;

    /**
     * @var string
     *
     * @ORM\Column(name="resa_prevente", type="text", nullable=true)
     */
    private $resaPrevente = null;

    /**
     * @var string
     *
     * @ORM\Column(name="acces_transport", type="text", nullable=true)
     */
    private $accesTransport = null;

    /**
     * @var int
     *
     * @ORM\Column(name="t3id", type="integer", nullable=true )
     */
    private $t3id;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\EventLieu" )
     */
    private $eventlieu;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventLieu", cascade={"persist"})
     */
    private $eventmultilieu;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventHeure", cascade={"persist"})
     */
    private $eventmultiheure;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\EntreeType")
     */
    private $entreetype;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventLocal", cascade={"persist"})
     */
    private $eventlocal;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventType", cascade={"persist"})
     */
    private $eventtype;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventArtistesDjOrganisateurs", cascade={"persist"})
     */
    private $EventArtistesDjOrganisateurs;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventPrice", cascade={"persist"})
     */
    private $EventPrice;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventFlyers", cascade={"persist"})
     */
    private $EventFlyers;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventDate", cascade={"persist"})
     * @ORM\JoinTable(name="event_by_date")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $EventDate;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="mdts\homeBundle\Entity\EventVideos", mappedBy="event")
     */
    private $Eventvideos;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\ArticlesEvent", cascade={"persist"})
     * @ORM\JoinTable(name="articles_by_event",
     *    joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")}
     *   )
     */
    private $ArticlesEvent;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="indexed", type="boolean", nullable=true)
     */
    private $indexed;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\Event", cascade={"persist"})
     * @ORM\JoinTable(name="related_event")
     */
    private $EventRelated;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventDate", cascade={"persist"})
     * @ORM\JoinTable(name="event_date_home")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $EventDateHome;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventDate", cascade={"persist"})
     * @ORM\JoinTable(name="event_date_home_local")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $EventDateHomeLocal;

    /**
     * @var string
     *
     * @ORM\Column(name="flyercancelled", type="string", length=255, nullable=true)
     */
    private $flyercancelled = null;

    /**
     * @ORM\Column(name="orig_slug", type="string",length=255, nullable=true)
     */
    private $origSlug;

    /**
     * @var string
     *
     * @ORM\Column(name="dateclair", type="text", nullable=true)
     */
    private $dateclair = null;

    /**
     * @var string
     *
     * @ORM\Column(name="opt_dateclair", type="string",length=100, nullable=true)
     */
    private $optdateclair = null;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\Api")
     */
    private $api;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->eventmultilieu = new \Doctrine\Common\Collections\ArrayCollection();
        $this->eventmultiheure = new \Doctrine\Common\Collections\ArrayCollection();
        $this->eventlocal = new \Doctrine\Common\Collections\ArrayCollection();
        $this->eventtype = new \Doctrine\Common\Collections\ArrayCollection();
        $this->EventArtistesDjOrganisateurs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->EventPrice = new \Doctrine\Common\Collections\ArrayCollection();
        $this->EventFlyers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->Eventvideos = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ArticlesEvent = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Event
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set flyer.
     *
     * @param string $flyer
     *
     * @return Event
     */
    public function setFlyer($flyer)
    {
        $this->flyer = $flyer;

        return $this;
    }

    /**
     * Get flyer.
     *
     * @return string
     */
    public function getFlyer()
    {
        return $this->flyer;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Event
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set deletedAt.
     *
     * @param \DateTime $deletedAt
     *
     * @return Event
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt.
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set cancelledAt.
     *
     * @param bool $cancelledAt
     *
     * @return Event
     */
    public function setCancelledAt($cancelledAt)
    {
        $this->cancelledAt = $cancelledAt;

        return $this;
    }

    /**
     * Get cancelledAt.
     *
     * @return bool
     */
    public function getCancelledAt()
    {
        return $this->cancelledAt;
    }

    /**
     * Set position.
     *
     * @param int $position
     *
     * @return Event
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set category.
     *
     * @param string $category
     *
     * @return Event
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set prixenclair.
     *
     * @param string $prixenclair
     *
     * @return Event
     */
    public function setPrixenclair($prixenclair)
    {
        $this->prixenclair = $prixenclair;

        return $this;
    }

    /**
     * Get prixenclair.
     *
     * @return string
     */
    public function getPrixenclair()
    {
        return $this->prixenclair;
    }

    /**
     * Set prixunique.
     *
     * @param int $prixunique
     *
     * @return Event
     */
    public function setPrixunique($prixunique)
    {
        $this->prixunique = $prixunique;

        return $this;
    }

    /**
     * Get prixunique.
     *
     * @return int
     */
    public function getPrixunique()
    {
        return $this->prixunique;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set dateUnique.
     *
     * @param \DateTime $dateUnique
     *
     * @return Event
     */
    public function setDateUnique($dateUnique)
    {
        $this->dateUnique = $dateUnique;

        return $this;
    }

    /**
     * Get dateUnique.
     *
     * @return \DateTime
     */
    public function getDateUnique()
    {
        return $this->dateUnique;
    }

    /**
     * Set heureDebut.
     *
     * @param \DateTime $heureDebut
     *
     * @return Event
     */
    public function setHeureDebut($heureDebut)
    {
        //$heure = new \DateTime($heureDebut);
        $this->heureDebut = $heureDebut;

        return $this;
    }

    /**
     * Get heureDebut.
     *
     * @return \DateTime
     */
    public function getHeureDebut()
    {
        return $this->heureDebut;
    }

    /**
     * Set heureFin.
     *
     * @param \DateTime $heureFin
     *
     * @return Event
     */
    public function setHeureFin($heureFin)
    {
        //$heure = new \DateTime($heureFin);
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * Get heureFin.
     *
     * @return \DateTime
     */
    public function getHeureFin()
    {
        return $this->heureFin;
    }

    /**
     * Set heureFinAube.
     *
     * @param bool $heureFinAube
     *
     * @return Event
     */
    public function setHeureFinAube($heureFinAube)
    {
        $this->heureFinAube = $heureFinAube;

        return $this;
    }

    /**
     * Get heureFinAube.
     *
     * @return bool
     */
    public function getHeureFinAube()
    {
        return $this->heureFinAube;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Event
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Event
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set hidden.
     *
     * @param bool $hidden
     *
     * @return Event
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden.
     *
     * @return bool
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set dateDebut.
     *
     * @param \DateTime $dateDebut
     *
     * @return Event
     */
    public function setDateDebut($dateDebut)
    {
        //$this->dateDebut = new \DateTime($dateDebut);
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut.
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin.
     *
     * @param \DateTime $dateFin
     *
     * @return Event
     */
    public function setDateFin($dateFin)
    {
        //$this->dateFin = new \DateTime($dateFin);
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin.
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set rrule.
     *
     * @param string $rrule
     *
     * @return Event
     */
    public function setRrule($rrule)
    {
        $this->rrule = $rrule;

        return $this;
    }

    /**
     * Get rrule.
     *
     * @return string
     */
    public function getRrule()
    {
        return $this->rrule;
    }

    /**
     * Set contactEvent.
     *
     * @param string $contactEvent
     *
     * @return Event
     */
    public function setContactEvent($contactEvent)
    {
        $this->contactEvent = $contactEvent;

        return $this;
    }

    /**
     * Get contactEvent.
     *
     * @return string
     */
    public function getContactEvent()
    {
        return $this->contactEvent;
    }

    /**
     * Set resaPrevente.
     *
     * @param string $resaPrevente
     *
     * @return Event
     */
    public function setResaPrevente($resaPrevente)
    {
        $this->resaPrevente = $resaPrevente;

        return $this;
    }

    /**
     * Get resaPrevente.
     *
     * @return string
     */
    public function getResaPrevente()
    {
        return $this->resaPrevente;
    }

    /**
     * Set accesTransport.
     *
     * @param string $accesTransport
     *
     * @return Event
     */
    public function setAccesTransport($accesTransport)
    {
        $this->accesTransport = $accesTransport;

        return $this;
    }

    /**
     * Get accesTransport.
     *
     * @return string
     */
    public function getAccesTransport()
    {
        return $this->accesTransport;
    }

    /**
     * Set t3id.
     *
     * @param int $t3id
     *
     * @return Event
     */
    public function setT3id($t3id)
    {
        $this->t3id = $t3id;

        return $this;
    }

    /**
     * Get t3id.
     *
     * @return int
     */
    public function getT3id()
    {
        return $this->t3id;
    }

    /**
     * Set eventlieu.
     *
     * @param \mdts\homeBundle\Entity\EventLieu $eventlieu
     *
     * @return Event
     */
    public function setEventlieu(\mdts\homeBundle\Entity\EventLieu $eventlieu = null)
    {
        $this->eventlieu = $eventlieu;

        return $this;
    }

    /**
     * Get eventlieu.
     *
     * @return \mdts\homeBundle\Entity\EventLieu
     */
    public function getEventlieu()
    {
        return $this->eventlieu;
    }

    /**
     * Add eventmultilieu.
     *
     * @param \mdts\homeBundle\Entity\EventLieu $eventmultilieu
     *
     * @return Event
     */
    public function addEventmultilieu(\mdts\homeBundle\Entity\EventLieu $eventmultilieu)
    {
        $this->eventmultilieu[] = $eventmultilieu;

        return $this;
    }

    /**
     * Remove eventmultilieu.
     *
     * @param \mdts\homeBundle\Entity\EventLieu $eventmultilieu
     */
    public function removeEventmultilieu(\mdts\homeBundle\Entity\EventLieu $eventmultilieu)
    {
        $this->eventmultilieu->removeElement($eventmultilieu);
    }

    /**
     * Get eventmultilieu.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventmultilieu()
    {
        return $this->eventmultilieu;
    }

    /**
     * Add eventmultiheure.
     *
     * @param \mdts\homeBundle\Entity\EventHeure $eventmultiheure
     *
     * @return Event
     */
    public function addEventmultiheure(\mdts\homeBundle\Entity\EventHeure $eventmultiheure)
    {
        $this->eventmultiheure[] = $eventmultiheure;

        return $this;
    }

    /**
     * Remove eventmultiheure.
     *
     * @param \mdts\homeBundle\Entity\EventHeure $eventmultiheure
     */
    public function removeEventmultiheure(\mdts\homeBundle\Entity\EventHeure $eventmultiheure)
    {
        $this->eventmultiheure->removeElement($eventmultiheure);
    }

    /**
     * Get eventmultiheure.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventmultiheure()
    {
        return $this->eventmultiheure;
    }

    /**
     * Set entreetype.
     *
     * @param \mdts\homeBundle\Entity\EntreeType $entreetype
     *
     * @return Event
     */
    public function setEntreetype(\mdts\homeBundle\Entity\EntreeType $entreetype = null)
    {
        $this->entreetype = $entreetype;

        return $this;
    }

    /**
     * Get entreetype.
     *
     * @return \mdts\homeBundle\Entity\EntreeType
     */
    public function getEntreetype()
    {
        return $this->entreetype;
    }

    /**
     * Add eventlocal.
     *
     * @param \mdts\homeBundle\Entity\EventLocal $eventlocal
     *
     * @return Event
     */
    public function addEventlocal(\mdts\homeBundle\Entity\EventLocal $eventlocal)
    {
        $this->eventlocal[] = $eventlocal;

        return $this;
    }

    /**
     * Remove eventlocal.
     *
     * @param \mdts\homeBundle\Entity\EventLocal $eventlocal
     */
    public function removeEventlocal(\mdts\homeBundle\Entity\EventLocal $eventlocal)
    {
        $this->eventlocal->removeElement($eventlocal);
    }

    /**
     * Get eventlocal.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventlocal()
    {
        return $this->eventlocal;
    }

    /**
     * Add eventtype.
     *
     * @param \mdts\homeBundle\Entity\EventType $eventtype
     *
     * @return Event
     */
    public function addEventtype(\mdts\homeBundle\Entity\EventType $eventtype)
    {
        $this->eventtype[] = $eventtype;

        return $this;
    }

    /**
     * Remove eventtype.
     *
     * @param \mdts\homeBundle\Entity\EventType $eventtype
     */
    public function removeEventtype(\mdts\homeBundle\Entity\EventType $eventtype)
    {
        $this->eventtype->removeElement($eventtype);
    }

    /**
     * Get eventtype.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventtype()
    {
        return $this->eventtype;
    }

    /**
     * Add eventArtistesDjOrganisateur.
     *
     * @param \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur
     *
     * @return Event
     */
    public function addEventArtistesDjOrganisateur(\mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur)
    {
        $this->EventArtistesDjOrganisateurs[] = $eventArtistesDjOrganisateur;

        return $this;
    }

    /**
     * Remove eventArtistesDjOrganisateur.
     *
     * @param \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur
     */
    public function removeEventArtistesDjOrganisateur(\mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur)
    {
        $this->EventArtistesDjOrganisateurs->removeElement($eventArtistesDjOrganisateur);
    }

    /**
     * Get eventArtistesDjOrganisateurs.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventArtistesDjOrganisateurs()
    {
        return $this->EventArtistesDjOrganisateurs;
    }

    /**
     * Add eventPrice.
     *
     * @param \mdts\homeBundle\Entity\EventPrice $eventPrice
     *
     * @return Event
     */
    public function addEventPrice(\mdts\homeBundle\Entity\EventPrice $eventPrice)
    {
        $this->EventPrice[] = $eventPrice;

        return $this;
    }

    /**
     * Remove eventPrice.
     *
     * @param \mdts\homeBundle\Entity\EventPrice $eventPrice
     */
    public function removeEventPrice(\mdts\homeBundle\Entity\EventPrice $eventPrice)
    {
        $this->EventPrice->removeElement($eventPrice);
    }

    /**
     * Get eventPrice.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventPrice()
    {
        return $this->EventPrice;
    }

    /**
     * Add eventFlyer.
     *
     * @param \mdts\homeBundle\Entity\EventFlyers $eventFlyer
     *
     * @return Event
     */
    public function addEventFlyer(\mdts\homeBundle\Entity\EventFlyers $eventFlyer)
    {
        $this->EventFlyers[] = $eventFlyer;

        return $this;
    }

    /**
     * Remove eventFlyer.
     *
     * @param \mdts\homeBundle\Entity\EventFlyers $eventFlyer
     */
    public function removeEventFlyer(\mdts\homeBundle\Entity\EventFlyers $eventFlyer)
    {
        $this->EventFlyers->removeElement($eventFlyer);
    }

    /**
     * Get eventFlyers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventFlyers()
    {
        return $this->EventFlyers;
    }

    /**
     * Add eventDate.
     *
     * @param \mdts\homeBundle\Entity\EventDate $eventDate
     *
     * @return Event
     */
    public function addEventDate(\mdts\homeBundle\Entity\EventDate $eventDate)
    {
        $this->EventDate[] = $eventDate;

        return $this;
    }

    /**
     * Remove eventDate.
     *
     * @param \mdts\homeBundle\Entity\EventDate $eventDate
     */
    public function removeEventDate(\mdts\homeBundle\Entity\EventDate $eventDate)
    {
        $this->EventDate->removeElement($eventDate);
    }

    /**
     * Get eventDate.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventDate()
    {
        return $this->EventDate;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function setFlyerFile(File $image = null)
    {
        $this->flyerFile = $image;

        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTime('now');
        }
    }

    /**
     * @return File
     */
    public function getFlyerFile()
    {
        return $this->flyerFile;
    }

    /**
     * Add eventvideo.
     *
     * @param \mdts\homeBundle\Entity\EventVideos $eventvideo
     *
     * @return Event
     */
    public function addEventvideo(\mdts\homeBundle\Entity\EventVideos $eventvideo)
    {
        /*$eventvideo->setEvent($this);
        // Si l'objet fait déjà partie de la collection on ne l'ajoute pas
        if (!$this->Eventvideos->contains($eventvideo)) {
            $this->Eventvideos->add($eventvideo);
        }*/
        $this->Eventvideos[] = $eventvideo;

        return $this;
    }

    /**
     * Remove eventvideo.
     *
     * @param \mdts\homeBundle\Entity\EventVideos $eventvideo
     */
    public function removeEventvideo(\mdts\homeBundle\Entity\EventVideos $eventvideo)
    {
        $this->Eventvideos->removeElement($eventvideo);
    }

    /**
     * Get eventvideos.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventvideos()
    {
        return $this->Eventvideos;
    }

    /**
     * Add articlesEvent.
     *
     * @param \mdts\homeBundle\Entity\ArticlesEvent $articlesEvent
     *
     * @return Event
     */
    public function addArticlesEvent(\mdts\homeBundle\Entity\ArticlesEvent $articlesEvent)
    {
        $this->ArticlesEvent[] = $articlesEvent;

        return $this;
    }

    /**
     * Remove articlesEvent.
     *
     * @param \mdts\homeBundle\Entity\ArticlesEvent $articlesEvent
     */
    public function removeArticlesEvent(\mdts\homeBundle\Entity\ArticlesEvent $articlesEvent)
    {
        $this->ArticlesEvent->removeElement($articlesEvent);
    }

    /**
     * Get articlesEvent.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticlesEvent()
    {
        return $this->ArticlesEvent;
    }

    /**
     * Set indexed.
     *
     * @param bool $indexed
     *
     * @return Event
     */
    public function setIndexed($indexed)
    {
        $this->indexed = $indexed;

        return $this;
    }

    /**
     * Get indexed.
     *
     * @return bool
     */
    public function getIndexed()
    {
        return $this->indexed;
    }

    /**
     * Add eventRelated.
     *
     * @param \mdts\homeBundle\Entity\Event $eventRelated
     *
     * @return Event
     */
    public function addEventRelated(\mdts\homeBundle\Entity\Event $eventRelated)
    {
        $this->EventRelated[] = $eventRelated;

        return $this;
    }

    /**
     * Remove eventRelated.
     *
     * @param \mdts\homeBundle\Entity\Event $eventRelated
     */
    public function removeEventRelated(\mdts\homeBundle\Entity\Event $eventRelated)
    {
        $this->EventRelated->removeElement($eventRelated);
    }

    /**
     * Get eventRelated.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventRelated()
    {
        return $this->EventRelated;
    }

    /**
     * Add eventDateHome.
     *
     * @param \mdts\homeBundle\Entity\EventDate $eventDateHome
     *
     * @return Event
     */
    public function addEventDateHome(\mdts\homeBundle\Entity\EventDate $eventDateHome)
    {
        $this->EventDateHome[] = $eventDateHome;

        return $this;
    }

    /**
     * Remove eventDateHome.
     *
     * @param \mdts\homeBundle\Entity\EventDate $eventDateHome
     */
    public function removeEventDateHome(\mdts\homeBundle\Entity\EventDate $eventDateHome)
    {
        $this->EventDateHome->removeElement($eventDateHome);
    }

    /**
     * Get eventDateHome.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventDateHome()
    {
        return $this->EventDateHome;
    }

    /**
     * Set flyercancelled.
     *
     * @param string $flyercancelled
     *
     * @return Event
     */
    public function setFlyercancelled($flyercancelled)
    {
        $this->flyercancelled = $flyercancelled;

        return $this;
    }

    /**
     * Get flyercancelled.
     *
     * @return string
     */
    public function getFlyercancelled()
    {
        return $this->flyercancelled;
    }

    /**
     * Add eventDateHomeLocal.
     *
     * @param \mdts\homeBundle\Entity\EventDate $eventDateHomeLocal
     *
     * @return Event
     */
    public function addEventDateHomeLocal(\mdts\homeBundle\Entity\EventDate $eventDateHomeLocal)
    {
        $this->EventDateHomeLocal[] = $eventDateHomeLocal;

        return $this;
    }

    /**
     * Remove eventDateHomeLocal.
     *
     * @param \mdts\homeBundle\Entity\EventDate $eventDateHomeLocal
     */
    public function removeEventDateHomeLocal(\mdts\homeBundle\Entity\EventDate $eventDateHomeLocal)
    {
        $this->EventDateHomeLocal->removeElement($eventDateHomeLocal);
    }

    /**
     * Get eventDateHomeLocal.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventDateHomeLocal()
    {
        return $this->EventDateHomeLocal;
    }

    /**
     * Set origSlug.
     *
     * @param string $origSlug
     *
     * @return Event
     */
    public function setOrigSlug($origSlug)
    {
        $this->origSlug = $origSlug;

        return $this;
    }

    /**
     * Get origSlug.
     *
     * @return string
     */
    public function getOrigSlug()
    {
        return $this->origSlug;
    }

    /**
     * Set dateclair
     *
     * @param string $dateclair
     *
     * @return Event
     */
    public function setDateclair($dateclair)
    {
        $this->dateclair = $dateclair;

        return $this;
    }

    /**
     * Get dateclair
     *
     * @return string
     */
    public function getDateclair()
    {
        return $this->dateclair;
    }

    /**
     * Set optdateclair
     *
     * @param string $optdateclair
     *
     * @return Event
     */
    public function setOptdateclair($optdateclair)
    {
        $this->optdateclair = $optdateclair;

        return $this;
    }

    /**
     * Get optdateclair
     *
     * @return string
     */
    public function getOptdateclair()
    {
        return $this->optdateclair;
    }

    /**
     * Set api
     *
     * @param \mdts\homeBundle\Entity\Api $api
     *
     * @return Event
     */
    public function setApi(\mdts\homeBundle\Entity\Api $api = null)
    {
        $this->api = $api;

        return $this;
    }

    /**
     * Get api
     *
     * @return \mdts\homeBundle\Entity\Api
     */
    public function getApi()
    {
        return $this->api;
    }


}
