<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TelsLieu.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\TelsLieuRepository")
 */
class TelsLieu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255)
     */
    private $tel;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\EventLieu", inversedBy="TelsLieu")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="eventlieu_id", referencedColumnName="id")
     * })
     */
    private $eventlieu;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tel.
     *
     * @param string $tel
     *
     * @return TelsLieu
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel.
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set event.
     *
     * @param \mdts\homeBundle\Entity\EventLieu $event
     *
     * @return TelsLieu
     */
    public function setEventLieu(\mdts\homeBundle\Entity\EventLieu $eventlieu = null)
    {
        $this->eventlieu = $eventlieu;

        return $this;
    }

    /**
     * Get event.
     *
     * @return \mdts\homeBundle\Entity\EventLieu
     */
    public function getEventLieu()
    {
        return $this->eventlieu;
    }
}
