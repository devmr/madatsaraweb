<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * NewsletterAbonne.
 *
 * @ORM\Table(indexes={ @ORM\Index(name="name_idx", columns={"email"})  })
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\NewsletterAbonneRepository")
 */
class NewsletterAbonne
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\Email( message="L'adresse email saisie {{ value }} est incorrecte.", strict=true)
     * @Assert\Email( message="Aucun serveur n'a été trouvé pour ce domaine.", checkMX=true)
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inscrit_date", type="datetime", nullable=true)
     */
    private $inscritdate;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="desinscrit", type="boolean", nullable=true)
     */
    private $desinscrit;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="desinscrit_date", type="datetime", nullable=true)
     */
    private $desinscritdate;

    /**
     * @var string
     *
     * @ORM\Column(name="infos", type="text", nullable=true)
     */
    private $infos = null;

    /**
     * @var string
     *
     * @ORM\Column(name="country_isoCode", type="string", length=10, nullable=true)
     */
    private $country_isoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=255, nullable=true)
     */
    private $country_name;

    /**
     * @var string
     *
     * @ORM\Column(name="city_name", type="string", length=255, nullable=true)
     */
    private $city_name;

    /**
     * @var string
     *
     * @ORM\Column(name="location_timeZone", type="string", length=255, nullable=true)
     */
    private $location_timeZone;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=100, nullable=true)
     */
    private $source;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventLieu", cascade={"persist"})
     * @ORM\JoinTable(name="abonne_by_eventlieu",
     *    joinColumns={@ORM\JoinColumn(name="abonne_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="foreign_id", referencedColumnName="id")}
     *   )
     */
    private $EventLieu;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\countries", cascade={"persist"})
     * @ORM\JoinTable(name="abonne_by_countries",
     *    joinColumns={@ORM\JoinColumn(name="abonne_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="foreign_id", referencedColumnName="id")}
     *   )
     */
    private $countries;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\locality", cascade={"persist"})
     * @ORM\JoinTable(name="abonne_by_locality",
     *    joinColumns={@ORM\JoinColumn(name="abonne_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="foreign_id", referencedColumnName="id")}
     *   )
     */
    private $locality;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\region", cascade={"persist"})
     * @ORM\JoinTable(name="abonne_by_region",
     *    joinColumns={@ORM\JoinColumn(name="abonne_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="foreign_id", referencedColumnName="id")}
     *   )
     */
    private $region;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\quartier", cascade={"persist"})
     * @ORM\JoinTable(name="abonne_by_quartier",
     *    joinColumns={@ORM\JoinColumn(name="abonne_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="foreign_id", referencedColumnName="id")}
     *   )
     */
    private $quartier;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\homeBundle\Entity\EventArtistesDjOrganisateurs", cascade={"persist"})
     * @ORM\JoinTable(name="abonne_by_artistes",
     *    joinColumns={@ORM\JoinColumn(name="abonne_id", referencedColumnName="id")},
     *    inverseJoinColumns={@ORM\JoinColumn(name="foreign_id", referencedColumnName="id")}
     *   )
     */
    private $EventArtistesDjOrganisateurs;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email.
     *
     * @param string $email
     *
     * @return NewsletterAbonne
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set inscritdate.
     *
     * @param \DateTime $inscritdate
     *
     * @return NewsletterAbonne
     */
    public function setInscritdate($inscritdate)
    {
        $this->inscritdate = $inscritdate;

        return $this;
    }

    /**
     * Get inscritdate.
     *
     * @return \DateTime
     */
    public function getInscritdate()
    {
        return $this->inscritdate;
    }

    /**
     * Set desinscrit.
     *
     * @param bool $desinscrit
     *
     * @return NewsletterAbonne
     */
    public function setDesinscrit($desinscrit)
    {
        $this->desinscrit = $desinscrit;

        return $this;
    }

    /**
     * Get desinscrit.
     *
     * @return bool
     */
    public function getDesinscrit()
    {
        return $this->desinscrit;
    }

    /**
     * Set desinscritdate.
     *
     * @param \DateTime $desinscritdate
     *
     * @return NewsletterAbonne
     */
    public function setDesinscritdate($desinscritdate)
    {
        $this->desinscritdate = $desinscritdate;

        return $this;
    }

    /**
     * Get desinscritdate.
     *
     * @return \DateTime
     */
    public function getDesinscritdate()
    {
        return $this->desinscritdate;
    }

    /**
     * Set infos.
     *
     * @param string $infos
     *
     * @return NewsletterAbonne
     */
    public function setInfos($infos)
    {
        $this->infos = $infos;

        return $this;
    }

    /**
     * Get infos.
     *
     * @return string
     */
    public function getInfos()
    {
        return $this->infos;
    }

    /**
     * Set countryIsoCode.
     *
     * @param string $countryIsoCode
     *
     * @return NewsletterAbonne
     */
    public function setCountryIsoCode($countryIsoCode)
    {
        $this->country_isoCode = $countryIsoCode;

        return $this;
    }

    /**
     * Get countryIsoCode.
     *
     * @return string
     */
    public function getCountryIsoCode()
    {
        return $this->country_isoCode;
    }

    /**
     * Set countryName.
     *
     * @param string $countryName
     *
     * @return NewsletterAbonne
     */
    public function setCountryName($countryName)
    {
        $this->country_name = $countryName;

        return $this;
    }

    /**
     * Get countryName.
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->country_name;
    }

    /**
     * Set cityName.
     *
     * @param string $cityName
     *
     * @return NewsletterAbonne
     */
    public function setCityName($cityName)
    {
        $this->city_name = $cityName;

        return $this;
    }

    /**
     * Get cityName.
     *
     * @return string
     */
    public function getCityName()
    {
        return $this->city_name;
    }

    /**
     * Set locationTimeZone.
     *
     * @param string $locationTimeZone
     *
     * @return NewsletterAbonne
     */
    public function setLocationTimeZone($locationTimeZone)
    {
        $this->location_timeZone = $locationTimeZone;

        return $this;
    }

    /**
     * Get locationTimeZone.
     *
     * @return string
     */
    public function getLocationTimeZone()
    {
        return $this->location_timeZone;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return NewsletterAbonne
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->EventLieu = new \Doctrine\Common\Collections\ArrayCollection();
        $this->countries = new \Doctrine\Common\Collections\ArrayCollection();
        $this->locality = new \Doctrine\Common\Collections\ArrayCollection();
        $this->region = new \Doctrine\Common\Collections\ArrayCollection();
        $this->quartier = new \Doctrine\Common\Collections\ArrayCollection();
        $this->EventArtistesDjOrganisateurs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add eventLieu
     *
     * @param \mdts\homeBundle\Entity\EventLieu $eventLieu
     *
     * @return NewsletterAbonne
     */
    public function addEventLieu(\mdts\homeBundle\Entity\EventLieu $eventLieu)
    {
        $this->EventLieu[] = $eventLieu;

        return $this;
    }

    /**
     * Remove eventLieu
     *
     * @param \mdts\homeBundle\Entity\EventLieu $eventLieu
     */
    public function removeEventLieu(\mdts\homeBundle\Entity\EventLieu $eventLieu)
    {
        $this->EventLieu->removeElement($eventLieu);
    }

    /**
     * Get eventLieu
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventLieu()
    {
        return $this->EventLieu;
    }

    /**
     * Add country
     *
     * @param \mdts\homeBundle\Entity\countries $country
     *
     * @return NewsletterAbonne
     */
    public function addCountry(\mdts\homeBundle\Entity\countries $country)
    {
        $this->countries[] = $country;

        return $this;
    }

    /**
     * Remove country
     *
     * @param \mdts\homeBundle\Entity\countries $country
     */
    public function removeCountry(\mdts\homeBundle\Entity\countries $country)
    {
        $this->countries->removeElement($country);
    }

    /**
     * Get countries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * Add locality
     *
     * @param \mdts\homeBundle\Entity\locality $locality
     *
     * @return NewsletterAbonne
     */
    public function addLocality(\mdts\homeBundle\Entity\locality $locality)
    {
        $this->locality[] = $locality;

        return $this;
    }

    /**
     * Remove locality
     *
     * @param \mdts\homeBundle\Entity\locality $locality
     */
    public function removeLocality(\mdts\homeBundle\Entity\locality $locality)
    {
        $this->locality->removeElement($locality);
    }

    /**
     * Get locality
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocality()
    {
        return $this->locality;
    }

    /**
     * Add region
     *
     * @param \mdts\homeBundle\Entity\region $region
     *
     * @return NewsletterAbonne
     */
    public function addRegion(\mdts\homeBundle\Entity\region $region)
    {
        $this->region[] = $region;

        return $this;
    }

    /**
     * Remove region
     *
     * @param \mdts\homeBundle\Entity\region $region
     */
    public function removeRegion(\mdts\homeBundle\Entity\region $region)
    {
        $this->region->removeElement($region);
    }

    /**
     * Get region
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Add quartier
     *
     * @param \mdts\homeBundle\Entity\quartier $quartier
     *
     * @return NewsletterAbonne
     */
    public function addQuartier(\mdts\homeBundle\Entity\quartier $quartier)
    {
        $this->quartier[] = $quartier;

        return $this;
    }

    /**
     * Remove quartier
     *
     * @param \mdts\homeBundle\Entity\quartier $quartier
     */
    public function removeQuartier(\mdts\homeBundle\Entity\quartier $quartier)
    {
        $this->quartier->removeElement($quartier);
    }

    /**
     * Get quartier
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuartier()
    {
        return $this->quartier;
    }

    /**
     * Add eventArtistesDjOrganisateur
     *
     * @param \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur
     *
     * @return NewsletterAbonne
     */
    public function addEventArtistesDjOrganisateur(\mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur)
    {
        $this->EventArtistesDjOrganisateurs[] = $eventArtistesDjOrganisateur;

        return $this;
    }

    /**
     * Remove eventArtistesDjOrganisateur
     *
     * @param \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur
     */
    public function removeEventArtistesDjOrganisateur(\mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateur)
    {
        $this->EventArtistesDjOrganisateurs->removeElement($eventArtistesDjOrganisateur);
    }

    /**
     * Get eventArtistesDjOrganisateurs
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEventArtistesDjOrganisateurs()
    {
        return $this->EventArtistesDjOrganisateurs;
    }
}
