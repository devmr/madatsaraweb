<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * ArticlesEvent.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\ArticlesEventRepository")
 * @ORM\Table(indexes={ @ORM\Index(name="url_idx", columns={"url"})  })
 */
class ArticlesEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text" , nullable=true)
     */
    private $content;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\EventArtistesDjOrganisateurs")
     */
    private $EventArtistesDjOrganisateurs;

     /**
      * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\Event")
      */
     private $event;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="addartistes_inevent", type="boolean", nullable=true)
     */
    private $addartistes_inevent;

    /**
     * @var string
     *
     * @ORM\Column(name="content_purify", type="text" , nullable=true)
     */
    private $content_purify;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return ArticlesEvent
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set event.
     *
     * @param \mdts\homeBundle\Entity\Event $event
     *
     * @return ArticlesEvent
     */
    public function setEvent(\mdts\homeBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event.
     *
     * @return \mdts\homeBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set eventArtistesDjOrganisateurs.
     *
     * @param \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateurs
     *
     * @return ArticlesEvent
     */
    public function setEventArtistesDjOrganisateurs(\mdts\homeBundle\Entity\EventArtistesDjOrganisateurs $eventArtistesDjOrganisateurs = null)
    {
        $this->EventArtistesDjOrganisateurs = $eventArtistesDjOrganisateurs;

        return $this;
    }

    /**
     * Get eventArtistesDjOrganisateurs.
     *
     * @return \mdts\homeBundle\Entity\EventArtistesDjOrganisateurs
     */
    public function getEventArtistesDjOrganisateurs()
    {
        return $this->EventArtistesDjOrganisateurs;
    }

    /**
     * Set addartistesInevent.
     *
     * @param bool $addartistesInevent
     *
     * @return ArticlesEvent
     */
    public function setAddartistesInevent($addartistesInevent)
    {
        $this->addartistes_inevent = $addartistesInevent;

        return $this;
    }

    /**
     * Get addartistesInevent.
     *
     * @return bool
     */
    public function getAddartistesInevent()
    {
        return $this->addartistes_inevent;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return ArticlesEvent
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return ArticlesEvent
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return ArticlesEvent
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set author.
     *
     * @param string $author
     *
     * @return ArticlesEvent
     */
    public function setAuthor($author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author.
     *
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set contentPurify.
     *
     * @param string $contentPurify
     *
     * @return ArticlesEvent
     */
    public function setContentPurify($contentPurify)
    {
        $this->content_purify = $contentPurify;

        return $this;
    }

    /**
     * Get contentPurify.
     *
     * @return string
     */
    public function getContentPurify()
    {
        return $this->content_purify;
    }
}
