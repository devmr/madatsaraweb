<?php

namespace mdts\homeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Devises.
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\homeBundle\Entity\DevisesRepository")
 */
class Devises
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="initiales", type="string", length=10)
     */
    private $initiales;

    /**
     * @ORM\OneToMany(targetEntity="mdts\homeBundle\Entity\EventPrice", mappedBy="devise")
     */
    private $devise;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    public function __construct()
    {
        $this->devise = new ArrayCollection();
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set initiales.
     *
     * @param string $initiales
     *
     * @return Devises
     */
    public function setInitiales($initiales)
    {
        $this->initiales = $initiales;

        return $this;
    }

    /**
     * Get initiales.
     *
     * @return string
     */
    public function getInitiales()
    {
        return $this->initiales;
    }

    /**
     * Add devise.
     *
     * @param \mdts\homeBundle\Entity\Devises $devise
     *
     * @return Devises
     */
    public function addDevise(\mdts\homeBundle\Entity\Devises $devise)
    {
        $this->devise[] = $devise;

        return $this;
    }

    /**
     * Remove devise.
     *
     * @param \mdts\homeBundle\Entity\Devises $devise
     */
    public function removeDevise(\mdts\homeBundle\Entity\Devises $devise)
    {
        $this->devise->removeElement($devise);
    }

    /**
     * Get devise.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevise()
    {
        return $this->devise;
    }
}
