<?php

namespace mdts\homeBundle\SearchRepository;

use FOS\ElasticaBundle\Repository;
use Cocur\Slugify\Slugify;

class EventindexRepository extends Repository
{
    public function date_range($fromdate, $todate)
    {
        $fromdate = \DateTime::createFromFormat('Y-m-d', $fromdate);
        $todate = \DateTime::createFromFormat('Y-m-d', $todate);

        return new \DatePeriod(
            $fromdate,
            new \DateInterval('P1D'),
            $todate->modify('+1 day')
        );
    }

    public function findES($search, $dates = array())
    {
        $query = new \Elastica\Query();

        //Slugify
        $slugify = new Slugify();
        $search = $slugify->slugify($search, ' ');

        $query_part = new \Elastica\Query\Bool();

        /*$fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setFieldQuery('name', $search);*/

        if ($search != '') {
            $fieldQuery = new \Elastica\Query\QueryString();
            $fieldQuery->setFields(array('name', 'description'));
            $fieldQuery->setQuery('*'.$search.'*');

            $query_part->addShould(
                //new \Elastica\Query\Term(array('name' => array('value' => $search, 'boost' => 3)))
                $fieldQuery
            );
        }

        //echo $search;
        //echo __FUNCTION__. print_r($dates, true);

        $filters = new \Elastica\Filter\Bool();

        if ($dates['d1s'] != '' && $dates['d2s'] != '') {
            /*$filters->addMust(
                new \Elastica\Filter\NumericRange('date_unique', array(
                    'gt' => $dates['d1s'],
                    'lt' => $dates['d2e']
                ))
            );*/

            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d1s']);
            $date1 = new \DateTime($d->format('Y-m-d'));

            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d2e']);
            $date2 = new \DateTime($d->format('Y-m-d'));

            /*var_dump($date1->format('Y-m-d'));
            var_dump($date2->format('Y-m-d'));*/

            $tabrange = $this->date_range($date1->format('Y-m-d'), $date2->format('Y-m-d'));

            foreach ($tabrange    as $date) {
                //$tab_dates[] = $date->format('Y-m-d');

                $query_part->addShould(
                    new \Elastica\Query\Term(array('dateliste' => array('value' => $date->format('Y-m-d'), 'boost' => 3)))
                );
            }

            /*$query_part->addShould(
                new \Elastica\Query\Term(array('dateliste' => array('value' => $date->format('Y-m-d'), 'boost' => 3)))
            );*/
        } elseif ($dates['d1s'] == '' && $dates['d2s'] != '') {
            $d = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $dates['d2s']);
            $date = new \DateTime($d->format('Y-m-d'));
            /*$date->sub(new \DateInterval('P1D'));
            $d1 = $date->format('Y-m-d\T').'00:00:00Z';

            $filters->addMust(
                new \Elastica\Filter\NumericRange('date_unique', array(
                    'gt' => $d1,
                    'lt' => $dates['d2e']
                ))
            );*/

            $query_part->addShould(
                new \Elastica\Query\Term(array('dateliste' => array('value' => $date->format('Y-m-d'), 'boost' => 3)))
            );
        }

        $filteredQuery = new \Elastica\Query\Filtered($query_part, $filters);

        $query->setQuery($filteredQuery);

        //print_r($query);

        //$query->setFrom(10);

        //$row = $type->search($query);

        //print_r($this->find($query,30000));

        return $this->find($query, 30000);
    }
}
