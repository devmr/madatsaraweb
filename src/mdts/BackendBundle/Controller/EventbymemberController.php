<?php

namespace mdts\BackendBundle\Controller;

use mdts\homeBundle\Form\IdsType;
use mdts\homeBundle\Form\Ids;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use mdts\homeBundle\Entity\searchBackendModel;
use mdts\FrontendBundle\Form\EventByMemberType;
use mdts\FrontendBundle\Entity\EventByMember;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class EventbymemberController extends Controller
{
    public function indexAction(Request $request, $page = 0)
    {

        $smodel = new searchBackendModel();
        $filter_form = $this->createFormBuilder($smodel, array(
            'csrf_protection' => false,
        ))
            //->setAction($this->generateUrl('admin_eventlocal_delete', array('id' => $id)))
            ->setMethod('GET')

            ->add('query', TextType::class, array('required' => false))

            ->add('submit', SubmitType::class, array('label' => 'Chercher'))
            ->add('limit', HiddenType::class, ['data' => 5] )
            ->getForm();
        $filter_form->handleRequest($request);

        // Form des checkbox
        $idsForm = $form = $this->get('madatsara.service')->createFormAllIds('admin_eventbymember_delete_all')->createView();

        $em = $this->getDoctrine()->getManager();


        $limit = is_array($request->query->get('form')) && array_key_exists('limit',$request->query->get('form'))?(int)$request->query->get('form')['limit'] :5;
        $currentPage = $request->query->getInt('page', 1);


        $query = $request->query->get('form');
        // $this->get('ladybug')->log($query);
        $entities = $em->getRepository('mdtsFrontendBundle:EventByMember')->findAllQuery($query,  $currentPage, $limit);
        $countRows = $entities->count() ;
        $maxPages = ceil($entities->count() / $limit);

        return $this->render('mdtsBackendBundle:Eventbymember:index.html.twig', array_merge(array(
            'entities' => $entities, 'idsForm' =>$idsForm, 'filter_form' => $filter_form->createView(),
        ), compact('countRows','limit', 'maxPages', 'currentPage')));
    }

    /**
     * Displays a form to edit an existing ArticlesEvent entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtsFrontendBundle:EventByMember')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        return $this->render('mdtsBackendBundle:Eventbymember:show.html.twig', array(
            'entity' => $entity, 'infos_dump' => json_encode(json_decode($entity->getInfos(), true), JSON_PRETTY_PRINT),
            ));
    }

    /**
     * Displays a form to edit an existing ArticlesEvent entity.
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('mdtsFrontendBundle:EventByMember')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('mdtsBackendBundle:Eventbymember:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }
    /**
     * Creates a form to edit a ArticlesEvent entity.
     *
     * @param ArticlesEvent $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(EventByMember $entity)
    {
        $er = $this->getDoctrine()
            ->getEntityManager();
        $form = $this->createForm(EventByMemberType::class, $entity, array(
            'action' => $this->generateUrl('admin_eventbymember_update', array('id' => $entity->getId())),
            'method' => 'PUT',
            'er' => $er
        ));

        $form->add('submit', SubmitType::class, array('label' => 'Update'));

        return $form;
    }
    /**
     * Creates a form to delete a ArticlesEvent entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_eventbymember_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', SubmitType::class, array('label' => 'Delete'))
            ->getForm()
        ;
    }
    /**
     * Edits an existing Event entity.
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $this->get('logger')->info(__LINE__.'-'.__FUNCTION__);

        $entity = $em->getRepository('mdtsFrontendBundle:EventByMember')->find($id);

        if (!$entity) {
            $this->get('logger')->info(__LINE__.'-'.__FUNCTION__);
            throw $this->createNotFoundException('Unable to find Event entity.');
        }
        $this->get('logger')->info(__LINE__.'-'.__FUNCTION__);
        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $this->get('logger')->info(__LINE__.'-'.__FUNCTION__);
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventByMember($entity, $editForm, $id);

            if ($srv === true) {
                $request->getSession()->getFlashBag()->add('notice', 'L\'Entité «'.$entity->getName().'» a bien été enregistré.');
            } else {
                $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de l\'enregistrement de l\'Entité «'.$entity->getName().'».');
            }

            $session = new Session();
            $referer = (!empty($session->get('referer')) ? $session->get('referer') : '');
            //dump($referer);exit;

            //return $this->redirect($referer);

            if ($srv === true) {
                //Si on clique sur Save and retour
                if ($referer != '') {
                    return $this->redirect($referer);
                }
                return $this->redirect($this->generateUrl('admin_eventbymember_edit', array('id' => $id)));
            } else {
                return $this->redirect($this->generateUrl('admin_eventbymember_edit', array('id' => $id)));
            }
        }

        return $this->render('mdtsBackendBundle:Eventbymember:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function attacheventAction($id)
    {
        return new Response(__FUNCTION__);
    }

    /**
     * Deletes a EventLieu entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        $srv = false;
        if ($request->isMethod('GET')) {
//            $srv = $this->get('madatsara.service')->removeEventLieu($id);
        }

        if (!$request->isMethod('GET') && $form->isValid()) {
//            $srv = $this->get('madatsara.service')->removeEventLieu($id);
        }

        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'L\'Entité a bien été supprimée.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression de l\'Entité .');
        }

        return $this->redirect($this->generateUrl('admin_eventbymember_liste'));
    }

    public function deleteallAction(Request $request)
    {
        $form = $this->get('madatsara.service')->createFormAllIds('admin_eventbymember_delete_all');
        $form->handleRequest($request);
        $srv = false;
        $data = $form->getData();
        $ids = array_key_exists('ids', $data) ? explode(',',$data['ids']) : [];

//        print_r( $ids);exit;
        //echo $this->get('request')->getMethod();exit();
        if ($request->isMethod('DELETE')) {
                $srv = $this->get('madatsara.service')->removeallEventByMember($ids);
        }
        if ($request->isMethod('POST')) {
//                $srv = $this->get('madatsara.service')->copyallEventLieu($ids);
        }


        //Afficher message de confirmation
        if ($srv === true) {
            $request->getSession()->getFlashBag()->add('notice', 'Les entités ont bien été supprimées.');
        } else {
            $request->getSession()->getFlashBag()->add('warning', 'Erreur lors de la suppression des entités.');
        }

        return $this->redirect($this->generateUrl('admin_eventbymember_liste'));
    }


}
