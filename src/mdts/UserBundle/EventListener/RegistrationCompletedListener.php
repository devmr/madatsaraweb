<?php

namespace mdts\UserBundle\EventListener;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use GeoIp2\Database\Reader;
use Symfony\Component\HttpFoundation\RequestStack;

class RegistrationCompletedListener implements EventSubscriberInterface
{
    protected $userManager;
    protected $_container;
    public $to;
    public $from;
    protected $request;
    protected $madatsaraServicemadatsara;

    public function __construct(UserManagerInterface $userManager, Container $container, RequestStack $requestStack)
    {
        $this->userManager = $userManager;
        $this->_container = $container;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
        $this->madatsaraServicemadatsara = $this->_container->get('madatsara.service');

        $default_email = $this->madatsaraServicemadatsara->getParameter('default_email');
        if ( array_key_exists('to', $default_email)) {
            $this->to = $default_email['to'];
            $this->from = $default_email['from'];
        }


    }

    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompletedEvent',
            FOSUserEvents::REGISTRATION_CONFIRMED => 'onRegistrationComfirmedEvent',
        );
    }

    public function onRegistrationCompletedEvent(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();

        //Update user
        $data = array();
        $path_geoipmmdb_exists = $this->_container->hasParameter('path_geoipmmdb');
        if ($path_geoipmmdb_exists) {
            $path_geoipmmdb = $this->madatsaraServicemadatsara->getPathParam('path_geoipmmdb');
            $reader = new Reader($path_geoipmmdb);
            $record = $reader->city($this->_container->get('request_stack')->getCurrentRequest()->getClientIp());
            $data = array_merge($data,  ['geoinfo' => [
                'country_isoCode' => $record->country->isoCode, 'country_confidence' => $record->country->confidence, 'country_names' => $record->country->names['fr'], 'city_confidence' => $record->city->confidence, 'city_geonameId' => $record->city->geonameId, 'city_names' => $record->city->names['fr'], 'postal_confidence' => $record->postal->confidence, 'postal_code' => $record->postal->code, 'location_averageIncome' => $record->location->averageIncome, 'location_accuracyRadius' => $record->location->accuracyRadius, 'location_latitude' => $record->location->latitude, 'location_longitude' => $record->location->longitude, 'location_populationDensity' => $record->location->populationDensity, 'location_postalCode' => $record->location->postalCode, 'location_timeZone' => $record->location->timeZone,
            ]]);
        }

        if ($this->_container->hasParameter('path_udgerdb')) {
            $path_udgerdb = $this->madatsaraServicemadatsara->getPathParam('path_udgerdb');
            $browser = [];
            $factory = new \Udger\ParserFactory($path_udgerdb);
            $parser = $factory->getParser();
            try {
                $ua = $this->madatsaraServicemadatsara->getHeaders('User-Agent');
                $ip = $this->madatsaraServicemadatsara->getIpAddress();
//                $ua = $this->_container->get('request')->headers->get('User-Agent');
                $parser->setUA($ua);
                $parser->setIP($ip);
                $browser = $parser->parse();
            } catch (Exception $ex) {

            }
            /*$parser = new Parser();
            $parser->setDataDir($path_udgerdb);
            $parser->setAccessKey('XXXXXX');
            $browser = $parser->parse($this->_container->get('request')->headers->get('User-Agent'));*/
//            print_r($browser);exit;
            if (is_array($browser)) {
                $data = array_merge($data,  $browser );
            }
        }
        //Infos
        if (count($data) > 0) {
            $user->setInfos(json_encode($data));
        }

        //Geo
        if ($path_geoipmmdb_exists) {
            $user->setCountryIsoCode($record->country->isoCode);
            $user->setCountryName($record->country->names['fr']);
            $user->setCityName($record->city->names['fr']);
            $user->setLocationTimeZone($record->location->timeZone);
        }
        //Browser
        if ($this->_container->hasParameter('path_udgerdb') && is_array($browser['info'])) {
            $user->setBrowserUaName($browser['info']['ua_name']);
            $user->setBrowserUaVer($browser['info']['ua_ver']);
            $user->setBrowserUaFamily($browser['info']['ua_family']);
            $user->setBrowserUaCompany($browser['info']['ua_company']);
            $user->setBrowserUaEngine($browser['info']['ua_engine']);
            $user->setBrowserOsName($browser['info']['os_name']);
            $user->setBrowserOsFamily($browser['info']['os_family']);
            $user->setBrowserDeviceName($browser['info']['device_name']);
        }
        $this->userManager->updateUser($user);
        $msg = $this->_container->get('templating')->render(

            'mdtshomeBundle:Email:generique.html.twig',
            array(
                'content' => '<p><strong>'.$user->getUsername().'</strong> vient de s\'inscrire sur le site avec l\'adresse email suivant : ('.$user->getEmail().')</p>'.$this->_container->get('madatsara.service')->array2table($data),
            )
        );
        $this->madatsaraServicemadatsara->sendMail($this->to, '[MADATSARA] - Nouveau membre inscrit', $msg, $this->from);
    }

    public function onRegistrationComfirmedEvent(FilterUserResponseEvent $event)
    {
        $user = $event->getUser();
        $msg = $this->_container->get('templating')->render(

            'mdtshomeBundle:Email:generique.html.twig',
            array(
                'content' => '<p><strong>'.$user->getUsername().'</strong> vient de confirmer son inscription en tant que membres sur le site</p>',
            )
        );
        $this->madatsaraServicemadatsara->sendMail($this->to, '[MADATSARA] - Confirmation inscription de '.$user->getUsername(), $msg, $this->from);
    }
}
