<?php

namespace mdts\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @ORM\Entity(repositoryClass="mdts\UserBundle\Entity\UserRepository")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true )
     */
    protected $facebook_id;

    /**
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true )
     */
    protected $facebook_access_token;

    /**
     * @ORM\Column(name="google_id", type="string", length=255, nullable=true )
     */
    protected $google_id;

    /**
     * @ORM\Column(name="google_access_token", type="string", length=255, nullable=true )
     */
    protected $google_access_token;

    /**
     * @ORM\Column(name="twitterid", type="string", length=255, nullable=true )
     */
    protected $twitterid;

    /**
     * @ORM\Column(name="twitter_token", type="string", length=255, nullable=true )
     */
    protected $twittertoken;

    /**
     * @ORM\Column(name="nickname", type="string", length=255, nullable=true )
     */
    protected $nickname;

    /**
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true )
     */
    protected $first_name;

    /**
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true )
     */
    protected $last_name;

    /**
     * @ORM\Column(name="profile_picture", type="text" , nullable=true )
     */
    protected $profile_picture;

    /**
     * @var string
     *
     * @ORM\Column(name="email_rs", type="string", length=255, nullable=true)
     */
    private $email_rs;

    /**
     * @var string
     *
     * @ORM\Column(name="infos", type="text", nullable=true)
     */
    private $infos = null;

    /**
     * @var string
     *
     * @ORM\Column(name="country_isoCode", type="string", length=10, nullable=true)
     */
    private $country_isoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=255, nullable=true)
     */
    private $country_name;

    /**
     * @var string
     *
     * @ORM\Column(name="city_name", type="string", length=255, nullable=true)
     */
    private $city_name;

    /**
     * @var string
     *
     * @ORM\Column(name="location_timeZone", type="string", length=255, nullable=true)
     */
    private $location_timeZone;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_ua_name", type="string", length=255, nullable=true)
     */
    private $browser_ua_name;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_ua_ver", type="string", length=255, nullable=true)
     */
    private $browser_ua_ver;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_ua_family", type="string", length=255, nullable=true)
     */
    private $browser_ua_family;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_ua_company", type="string", length=255, nullable=true)
     */
    private $browser_ua_company;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_ua_engine", type="string", length=255, nullable=true)
     */
    private $browser_ua_engine;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_os_name", type="string", length=255, nullable=true)
     */
    private $browser_os_name;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_os_family", type="string", length=255, nullable=true)
     */
    private $browser_os_family;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_device_name", type="string", length=255, nullable=true)
     */
    private $browser_device_name;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\FrontendBundle\Entity\registrationid", mappedBy="user", cascade={"all"}, orphanRemoval=true)
     */
    private $device;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * Set facebookId.
     *
     * @param string $facebookId
     *
     * @return User
     */
    public function setFacebookId($facebookId)
    {
        $this->facebook_id = $facebookId;

        return $this;
    }

    /**
     * Get facebookId.
     *
     * @return string
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set facebookAccessToken.
     *
     * @param string $facebookAccessToken
     *
     * @return User
     */
    public function setFacebookAccessToken($facebookAccessToken)
    {
        $this->facebook_access_token = $facebookAccessToken;

        return $this;
    }

    /**
     * Get facebookAccessToken.
     *
     * @return string
     */
    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }

    /**
     * Set googleId.
     *
     * @param string $googleId
     *
     * @return User
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;

        return $this;
    }

    /**
     * Get googleId.
     *
     * @return string
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }

    /**
     * Set googleAccessToken.
     *
     * @param string $googleAccessToken
     *
     * @return User
     */
    public function setGoogleAccessToken($googleAccessToken)
    {
        $this->google_access_token = $googleAccessToken;

        return $this;
    }

    /**
     * Get googleAccessToken.
     *
     * @return string
     */
    public function getGoogleAccessToken()
    {
        return $this->google_access_token;
    }

    /**
     * Set twitterid.
     *
     * @param string $twitterid
     *
     * @return User
     */
    public function setTwitterid($twitterid)
    {
        $this->twitterid = $twitterid;

        return $this;
    }

    /**
     * Get twitterid.
     *
     * @return string
     */
    public function getTwitterid()
    {
        return $this->twitterid;
    }

    /**
     * Set twittertoken.
     *
     * @param string $twittertoken
     *
     * @return User
     */
    public function setTwittertoken($twittertoken)
    {
        $this->twittertoken = $twittertoken;

        return $this;
    }

    /**
     * Get twittertoken.
     *
     * @return string
     */
    public function getTwittertoken()
    {
        return $this->twittertoken;
    }

    /**
     * Set nickname.
     *
     * @param string $nickname
     *
     * @return User
     */
    public function setNickname($nickname)
    {
        $this->nickname = $nickname;

        return $this;
    }

    /**
     * Get nickname.
     *
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }

    /**
     * Set firstName.
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName.
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set profilePicture.
     *
     * @param string $profilePicture
     *
     * @return User
     */
    public function setProfilePicture($profilePicture)
    {
        $this->profile_picture = $profilePicture;

        return $this;
    }

    /**
     * Get profilePicture.
     *
     * @return string
     */
    public function getProfilePicture()
    {
        return $this->profile_picture;
    }

    /**
     * Set infos.
     *
     * @param string $infos
     *
     * @return User
     */
    public function setInfos($infos)
    {
        $this->infos = $infos;

        return $this;
    }

    /**
     * Get infos.
     *
     * @return string
     */
    public function getInfos()
    {
        return $this->infos;
    }

    /**
     * Set countryIsoCode.
     *
     * @param string $countryIsoCode
     *
     * @return User
     */
    public function setCountryIsoCode($countryIsoCode)
    {
        $this->country_isoCode = $countryIsoCode;

        return $this;
    }

    /**
     * Get countryIsoCode.
     *
     * @return string
     */
    public function getCountryIsoCode()
    {
        return $this->country_isoCode;
    }

    /**
     * Set countryName.
     *
     * @param string $countryName
     *
     * @return User
     */
    public function setCountryName($countryName)
    {
        $this->country_name = $countryName;

        return $this;
    }

    /**
     * Get countryName.
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->country_name;
    }

    /**
     * Set cityName.
     *
     * @param string $cityName
     *
     * @return User
     */
    public function setCityName($cityName)
    {
        $this->city_name = $cityName;

        return $this;
    }

    /**
     * Get cityName.
     *
     * @return string
     */
    public function getCityName()
    {
        return $this->city_name;
    }

    /**
     * Set locationTimeZone.
     *
     * @param string $locationTimeZone
     *
     * @return User
     */
    public function setLocationTimeZone($locationTimeZone)
    {
        $this->location_timeZone = $locationTimeZone;

        return $this;
    }

    /**
     * Get locationTimeZone.
     *
     * @return string
     */
    public function getLocationTimeZone()
    {
        return $this->location_timeZone;
    }

    /**
     * Set browserUaName.
     *
     * @param string $browserUaName
     *
     * @return User
     */
    public function setBrowserUaName($browserUaName)
    {
        $this->browser_ua_name = $browserUaName;

        return $this;
    }

    /**
     * Get browserUaName.
     *
     * @return string
     */
    public function getBrowserUaName()
    {
        return $this->browser_ua_name;
    }

    /**
     * Set browserUaVer.
     *
     * @param string $browserUaVer
     *
     * @return User
     */
    public function setBrowserUaVer($browserUaVer)
    {
        $this->browser_ua_ver = $browserUaVer;

        return $this;
    }

    /**
     * Get browserUaVer.
     *
     * @return string
     */
    public function getBrowserUaVer()
    {
        return $this->browser_ua_ver;
    }

    /**
     * Set browserUaFamily.
     *
     * @param string $browserUaFamily
     *
     * @return User
     */
    public function setBrowserUaFamily($browserUaFamily)
    {
        $this->browser_ua_family = $browserUaFamily;

        return $this;
    }

    /**
     * Get browserUaFamily.
     *
     * @return string
     */
    public function getBrowserUaFamily()
    {
        return $this->browser_ua_family;
    }

    /**
     * Set browserUaCompany.
     *
     * @param string $browserUaCompany
     *
     * @return User
     */
    public function setBrowserUaCompany($browserUaCompany)
    {
        $this->browser_ua_company = $browserUaCompany;

        return $this;
    }

    /**
     * Get browserUaCompany.
     *
     * @return string
     */
    public function getBrowserUaCompany()
    {
        return $this->browser_ua_company;
    }

    /**
     * Set browserUaEngine.
     *
     * @param string $browserUaEngine
     *
     * @return User
     */
    public function setBrowserUaEngine($browserUaEngine)
    {
        $this->browser_ua_engine = $browserUaEngine;

        return $this;
    }

    /**
     * Get browserUaEngine.
     *
     * @return string
     */
    public function getBrowserUaEngine()
    {
        return $this->browser_ua_engine;
    }

    /**
     * Set browserOsName.
     *
     * @param string $browserOsName
     *
     * @return User
     */
    public function setBrowserOsName($browserOsName)
    {
        $this->browser_os_name = $browserOsName;

        return $this;
    }

    /**
     * Get browserOsName.
     *
     * @return string
     */
    public function getBrowserOsName()
    {
        return $this->browser_os_name;
    }

    /**
     * Set browserOsFamily.
     *
     * @param string $browserOsFamily
     *
     * @return User
     */
    public function setBrowserOsFamily($browserOsFamily)
    {
        $this->browser_os_family = $browserOsFamily;

        return $this;
    }

    /**
     * Get browserOsFamily.
     *
     * @return string
     */
    public function getBrowserOsFamily()
    {
        return $this->browser_os_family;
    }

    /**
     * Set browserDeviceName.
     *
     * @param string $browserDeviceName
     *
     * @return User
     */
    public function setBrowserDeviceName($browserDeviceName)
    {
        $this->browser_device_name = $browserDeviceName;

        return $this;
    }

    /**
     * Get browserDeviceName.
     *
     * @return string
     */
    public function getBrowserDeviceName()
    {
        return $this->browser_device_name;
    }

    /**
     * Set emailRs.
     *
     * @param string $emailRs
     *
     * @return User
     */
    public function setEmailRs($emailRs)
    {
        $this->email_rs = $emailRs;

        return $this;
    }

    /**
     * Get emailRs.
     *
     * @return string
     */
    public function getEmailRs()
    {
        return $this->email_rs;
    }

    /**
     * Add device.
     *
     * @param \mdts\FrontendBundle\Entity\registrationid $device
     *
     * @return User
     */
    public function addDevice(\mdts\FrontendBundle\Entity\registrationid $device)
    {
        $this->device[] = $device;

        return $this;
    }

    /**
     * Remove device.
     *
     * @param \mdts\FrontendBundle\Entity\registrationid $device
     */
    public function removeDevice(\mdts\FrontendBundle\Entity\registrationid $device)
    {
        $this->device->removeElement($device);
    }

    /**
     * Get device.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDevice()
    {
        return $this->device;
    }
}
