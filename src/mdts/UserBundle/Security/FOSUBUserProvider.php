<?php

namespace mdts\UserBundle\Security;

use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\FOSUBUserProvider as BaseClass;
use mdts\homeBundle\Services\Madatsara;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use GeoIp2\Database\Reader;
// Request service
use Symfony\Component\HttpFoundation\RequestStack;

class FOSUBUserProvider extends BaseClass
{
    protected $logger;
    protected $madatsara;
    protected $_container;
    protected $requestStack;
    protected $request;

    public function __construct(UserManagerInterface $userManager, array $properties, Container $container,RequestStack $requestStack)
    {
        $this->_container = $container;
        $this->logger = $this->_container->get('logger');
        $this->madatsara = $this->_container->get('madatsara.service');

        $this->userManager = $userManager;
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();

        parent::__construct($userManager, $properties);
    }

    /**
     * {@inheritdoc}
     */
    public function connect(UserInterface $user, UserResponseInterface $response)
    {
        $property = $this->getProperty($response);
        $username = $response->getUsername();

        //on connect - get the access token and the user ID
        $service = $response->getResourceOwner()->getName();

        $this->logger->info('FOSUBUserProvider - Info : '.print_r(array(
             'service' => $service, 'getUsername' => $response->getUsername(), 'getNickname' => $response->getNickname(), 'getFirstName' => $response->getFirstName(), 'getLastName' => $response->getLastName(), 'getRealName' => $response->getRealName(), 'getEmail' => $response->getEmail(), 'getProfilePicture' => $response->getProfilePicture(), 'getExpiresIn' => $response->getExpiresIn(),
            ), true));

        $setter = 'set'.ucfirst($service);
        $setter_id = $setter.'Id';
        $setter_token = $setter.'AccessToken';

        //we "disconnect" previously connected users
        if (null !== $previousUser = $this->userManager->findUserBy(array($property => $username))) {
            $previousUser->$setter_id(null);
            $previousUser->$setter_token(null);
            $this->userManager->updateUser($previousUser);
        }

        //we connect current user
        $user->$setter_id($username);
        $user->$setter_token($response->getAccessToken());

        $this->userManager->updateUser($user);
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        $data = array(
        'service' => $response->getResourceOwner()->getName(), 'ip' => $this->_container->get('request_stack')->getCurrentRequest()->getClientIp(), 'getUsername' => $response->getUsername(), 'getNickname' => $response->getNickname(), 'getFirstName' => $response->getFirstName(), 'getLastName' => $response->getLastName(), 'getRealName' => $response->getRealName(), 'getEmail' => $response->getEmail(), 'getProfilePicture' => $response->getProfilePicture(), 'getExpiresIn' => $response->getExpiresIn(),
        );

        $path_geoipmmdb_exists = $this->_container->hasParameter('path_geoipmmdb');
        if ($path_geoipmmdb_exists) {
            $path_geoipmmdb = $this->madatsara->getPathParam('path_geoipmmdb');
            $reader = new Reader($path_geoipmmdb);
            $record = $reader->city($this->_container->get('request_stack')->getCurrentRequest()->getClientIp());
            $data = array_merge($data,  ['geoinfo' => [
                'country_isoCode' => $record->country->isoCode, 'country_confidence' => $record->country->confidence, 'country_names' => $record->country->names['fr'], 'city_confidence' => $record->city->confidence, 'city_geonameId' => $record->city->geonameId, 'city_names' => $record->city->names['fr'], 'postal_confidence' => $record->postal->confidence, 'postal_code' => $record->postal->code, 'location_averageIncome' => $record->location->averageIncome, 'location_accuracyRadius' => $record->location->accuracyRadius, 'location_latitude' => $record->location->latitude, 'location_longitude' => $record->location->longitude, 'location_populationDensity' => $record->location->populationDensity, 'location_postalCode' => $record->location->postalCode, 'location_timeZone' => $record->location->timeZone,
            ]]);

            $country_isoCode = $record->country->isoCode;
            $country_name = $record->country->names['fr'];
            $city_name = $record->city->names['fr'];
            $location_timeZone = $record->location->timeZone;

            if ('dev' == $this->_container->get('kernel')->getEnvironment() || '82.230.218.34' == $this->_container->get('request_stack')->getCurrentRequest()->getClientIp()) {
                //print_r($data);
            }
        }

        if ($this->_container->hasParameter('path_udgerdb')) {
            $path_udgerdb = $this->madatsara->getPathParam('path_udgerdb');
            $browser = [];
            $factory = new \Udger\ParserFactory($path_udgerdb);
            $parser = $factory->getParser();
            try {
                $ua = $this->madatsara->getHeaders('User-Agent');
                $ip = $this->madatsara->getIpAddress();
//                $ua = $this->_container->get('request')->headers->get('User-Agent');
                $parser->setUA($ua);
                $parser->setIP($ip);
                $browser = $parser->parse();
            } catch (Exception $ex) {

            }
            /*$parser = new Parser();
            $parser->setDataDir($path_udgerdb);
            $parser->setAccessKey('XXXXXX');
            $browser = $parser->parse($this->_container->get('request')->headers->get('User-Agent'));*/
//            print_r($browser);exit;
            if (is_array($browser)) {
                $data = array_merge($data,  $browser );
            }
        }

        $this->logger->info('FOSUBUserProvider - Info : '.print_r($data, true));
        $username = $response->getUsername();
        $user = $this->userManager->findUserBy(array($this->getProperty($response) => $username));
        //when the user is registrating
         //var_dump($user->getId());exit;
        if (null === $user) {
            $service = $response->getResourceOwner()->getName();
            $setter = 'set'.ucfirst($service);
            $setter_id = $setter.'Id';
            $setter_token = $setter.'AccessToken';
            // create new user here
            $user = $this->userManager->createUser();
            $user->$setter_id($username);
            $user->$setter_token($response->getAccessToken());
            //I have set all requested data with the user's username
            //modify here with relevant data
            $user->setUsername($username);
            $user->setEmail($username);
            $user->setPassword($username);
            $user->setEnabled(true);
            //Nickname...
            $user->setNickname($response->getNickname());
            $user->setFirstName($response->getFirstName());
            $user->setLastName($response->getLastName());
            $user->setProfilePicture($response->getProfilePicture());
            $user->setEmailRs($response->getEmail());
            //Infos
            $user->setInfos(json_encode($data));
            // Salt
            $user->setSalt(base_convert(sha1(uniqid(mt_rand(), true)), 16, 36));
            //Geo
            if ($path_geoipmmdb_exists) {
                $user->setCountryIsoCode($record->country->isoCode);
                $user->setCountryName($record->country->names['fr']);
                $user->setCityName($record->city->names['fr']);
                $user->setLocationTimeZone($record->location->timeZone);
            }
            //Browser
            if ($this->_container->hasParameter('path_udgerdb') && is_array($browser['info'])) {
                $user->setBrowserUaName($browser['info']['ua_name']);
                $user->setBrowserUaVer($browser['info']['ua_ver']);
                $user->setBrowserUaFamily($browser['info']['ua_family']);
                $user->setBrowserUaCompany($browser['info']['ua_company']);
                $user->setBrowserUaEngine($browser['info']['ua_engine']);
                $user->setBrowserOsName($browser['info']['os_name']);
                $user->setBrowserOsFamily($browser['info']['os_family']);
                $user->setBrowserDeviceName($browser['info']['device_name']);
            }

            $this->userManager->updateUser($user);

            //Envoyer mail

            $content =

            $msg = $this->_container->get('templating')->render(

                'mdtshomeBundle:Email:generique.html.twig',
                array(
                    'content' => $this->madatsara->array2table($data),
                )
            );
            $default_email = $this->madatsara->getParameter('default_email');
            $to = '';
            if ( array_key_exists('to', $default_email)) {
                $to = $default_email['to'];
                $from = $default_email['from'];
                $this->madatsara->sendMail($to, '[MADATSARA] - Nouveau membre via ' . $service, $msg, $from);
            }
            return $user;
        }

        //if user exists - go with the HWIOAuth way
        $user = parent::loadUserByOAuthUserResponse($response);

        $serviceName = $response->getResourceOwner()->getName();
        $setter = 'set'.ucfirst($serviceName).'AccessToken';

        //update access token
        $user->$setter($response->getAccessToken());

        return $user;
    }
}
