<?php

namespace mdts\noupdateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('mdtsnoupdateBundle:Default:index.html.twig', array('name' => $name));
    }
}
