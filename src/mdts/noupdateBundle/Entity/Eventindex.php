<?php

namespace mdts\noupdateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Eventindex.
 *
 * @ORM\Table(name="eventin")
 * @ORM\Entity
 */
class Eventindex
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @var \Boolean
     *
     * @ORM\Column(name="heure_fin_aube", type="boolean")
     */
    private $heureFinAube;

    /**
     * @ORM\Column(name="slug", length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="flyer", type="string", length=255, nullable=true)
     */
    private $flyer = null;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_event", type="string", length=255)
     */
    private $contactEvent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_unique", type="date")
     */
    private $dateUnique;

    /**
     * @var \Time
     *
     * @ORM\Column(name="heure_debut", type="time")
     */
    private $heureDebut;

    /**
     * @var \Time
     *
     * @ORM\Column(name="heure_fin", type="time")
     */
    private $heureFin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_debut", type="date")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_fin", type="date")
     */
    private $dateFin;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Eventindex
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Eventindex
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contact�Event.
     *
     * @param string $contact�Event
     *
     * @return Eventindex
     */
    public function setContact�Event($contact�Event)
    {
        $this->contact�Event = $contact�Event;

        return $this;
    }

    /**
     * Get contact�Event.
     *
     * @return string
     */
    public function getContact�Event()
    {
        return $this->contact�Event;
    }

    /**
     * Set heureFinAube.
     *
     * @param bool $heureFinAube
     *
     * @return Eventindex
     */
    public function setHeureFinAube($heureFinAube)
    {
        $this->heureFinAube = $heureFinAube;

        return $this;
    }

    /**
     * Get heureFinAube.
     *
     * @return bool
     */
    public function getHeureFinAube()
    {
        return $this->heureFinAube;
    }

    /**
     * Set slug.
     *
     * @param string $slug
     *
     * @return Eventindex
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set contactEvent.
     *
     * @param string $contactEvent
     *
     * @return Eventindex
     */
    public function setContactEvent($contactEvent)
    {
        $this->contactEvent = $contactEvent;

        return $this;
    }

    /**
     * Get contactEvent.
     *
     * @return string
     */
    public function getContactEvent()
    {
        return $this->contactEvent;
    }

    /**
     * Set dateUnique.
     *
     * @param \DateTime $dateUnique
     *
     * @return Eventindex
     */
    public function setDateUnique($dateUnique)
    {
        $this->dateUnique = $dateUnique;

        return $this;
    }

    /**
     * Get dateUnique.
     *
     * @return \DateTime
     */
    public function getDateUnique()
    {
        return $this->dateUnique;
    }

    /**
     * Set heureDebut.
     *
     * @param \DateTime $heureDebut
     *
     * @return Eventindex
     */
    public function setHeureDebut($heureDebut)
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    /**
     * Get heureDebut.
     *
     * @return \DateTime
     */
    public function getHeureDebut()
    {
        return $this->heureDebut;
    }

    /**
     * Set heureFin.
     *
     * @param \DateTime $heureFin
     *
     * @return Eventindex
     */
    public function setHeureFin($heureFin)
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * Get heureFin.
     *
     * @return \DateTime
     */
    public function getHeureFin()
    {
        return $this->heureFin;
    }

    /**
     * Set dateDebut.
     *
     * @param \DateTime $dateDebut
     *
     * @return Eventindex
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut.
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin.
     *
     * @param \DateTime $dateFin
     *
     * @return Eventindex
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin.
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set flyer.
     *
     * @param string $flyer
     *
     * @return Eventindex
     */
    public function setFlyer($flyer)
    {
        $this->flyer = $flyer;

        return $this;
    }

    /**
     * Get flyer.
     *
     * @return string
     */
    public function getFlyer()
    {
        return $this->flyer;
    }
}
