<?php

namespace mdts\FrontendBundle\Controller;

use Elastica\Bulk\Response as elasticaResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    protected $_madatsaraView;
    private $limit = 15;

    /**
     * Set your services here (we need to override this method to initialize our services into class properties).
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
    public function setContainer(\Symfony\Component\DependencyInjection\ContainerInterface $container = null)
    {
        parent::setContainer($container);
        $this->_madatsaraView = $this->get('madatsara.service')->getSearchForm()->createView();
    }

    public function eventbyartisteAction($slug, Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager();

        $entity = $repository->getRepository('mdtshomeBundle:EventArtistesDjOrganisateurs')->findByOrigSlug($slug);
        if (!$entity || $entity[0]->getId() == 1) {
            throw $this->createNotFoundException('Unable to find EventArtistesDjOrganisateurs entity.');
        }

        $event_id = array();

        $EventByArtisteId = $repository->getRepository('mdtshomeBundle:Event')->getEventByArtisteId($entity[0]->getId());
        foreach ($EventByArtisteId as $row) {
            $event_id[] = $row->getId();
        }

        $query = $repository->getRepository('mdtshomeBundle:Event')->getAllEventInID($event_id);
        $query->useQueryCache(true);
        $query->useResultCache(true);
        $query->setResultCacheLifetime(5);

        $paginator = $this->get('knp_paginator');
        $list = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            15/*limit per page*/
        );
        $query_all = $repository->getRepository('mdtshomeBundle:Event')->getAllEvent(0);
        $query_all->useQueryCache(true);
        $query_all->useResultCache(true);
        $query_all->setResultCacheLifetime(5);

        $list_all = $paginator->paginate(
            $query_all, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            $this->limit/*limit per page*/
        );

        $titre = '';

        return $this->render('mdtshomeBundle:Default:evenements.html.twig', array(
            'archive_place' => '',
            'slug' => $slug,
            'type' => __FUNCTION__,
            'allevents' => $list_all,
            'events' => $list, 'searchForm' => $this->_madatsaraView,
        ));
    }

    public function mobileAction()
    {
        return new Response('OK');
    }

    public function gmapAction()
    {
        // return new Response(__FUNCTION__);
        return $this->render('mdtsFrontendBundle:Default:gmap.html.twig');
    }
}
