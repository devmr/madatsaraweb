<?php

namespace mdts\FrontendBundle\Controller;

use mdts\FrontendBundle\Entity\EventByMember;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Validator\Constraints\Email;
use mdts\FrontendBundle\Form\EventByMemberTypeFe;
use GeoIp2\Database\Reader;

class EventController extends Controller
{
    public function addeventAction(Request $request)
    {
        $entity = new EventByMember();
        $form = $this->get('form.factory')->create(
            EventByMemberTypeFe::class,
            $entity,
            array(
            'er' => false
        ));
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            //Enregistrer dans BDD
            $srv = $this->get('madatsara.service')->createOrUpdateEventByMember(
                $entity,
                $form
            );

            if (true === $srv) {
                $data = array();
                $path_geoipmmdb_exists = $this->get('madatsara.service')->hasParameter('path_geoipmmdb');
                if ($path_geoipmmdb_exists) {
                    $path_geoipmmdb = $this->get('madatsara.service')->getPathParam('path_geoipmmdb');
                    $reader = new Reader($path_geoipmmdb);
                    $record = $reader->city($this->get('request_stack')->getCurrentRequest()->getClientIp());
                    $data = [
                        'country_isoCode' => $record->country->isoCode,
                        'country_names' => $record->country->names['fr'],
                        'city_names' => $record->city->names['fr'],
                        'location_timeZone' => $record->location->timeZone,
                    ];
                }

                $user = $this->get('security.token_storage')->getToken()->getUser();
                if ($user->getId() > 0) {
                    $data = array_merge($data, array(
                            'Username' => $user->getUsername(),
                            'Last Login' => $user->getLastLogin(), //dd
                            'Email' => $user->getEmail(),
                            'Email rs' => $user->getEmailRs(),
                            'Nick name' => $user->getNickname(),
                        )
                     );
                }

                $infos = '<pre>';
                $infos .= print_r(array_merge($data, array(
                    'ip' => $this->get('request_stack')->getCurrentRequest()->getClientIp(),
                    'user' => $this->get('request_stack')->getCurrentRequest()->getClientIp(),
                    'flyer' => 'https://www.madatsara.com/assets/flyerseventbyuser/'.$entity->getFlyer(),
                    )), true);
                $infos .= '</pre>';

                $mail = \Swift_Message::newInstance();

                //Envoyer mail admin
                $to = '';
                $from = '';
                $default_email = $this->get('madatsara.service')->getParameter('default_email');
                if ( array_key_exists('to', $default_email)) {
                    $to = $default_email['to'];
                    $from = $default_email['from'];
                }
                 $mail
                    ->setFrom($from)
                    ->setTo('madatsara@gmail.com')
                    ->setBcc($to)
                    ->setSubject('[MADATSARA] - Nouveau flyer ajouté')
                    ->setBody($this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'mdtshomeBundle:Email:ajout_evenement_admin.html.twig',
                    array(
                        'nom' => $entity->getName(),
                        'rubrique' => $entity->getThematique()->getName(),
                        'description' => $entity->getDescription(),
                        'infos' => $infos,
                    )
                ))
                    ->setContentType('text/html');

                    //Si flyer?
                    if ('' !== $entity->getFlyer()) {
                        $mail->attach(\Swift_Attachment::fromPath($this->get('kernel')->getRootDir().'/../web/assets/flyerseventbyuser/'.$entity->getFlyer()));
                    }

                $this->get('mailer')->send($mail);

                //Envoyer mail confirmation
                if ($user->getId() > 0) {
                    $email = $user->getEmail();
                    $emailrs = $user->getEmailRs();

                    $emailConstraint = new Email();
                    $emailConstraint->message = 'Invalid email address';
                    /*$errorList = $this->get('validator')->validateValue($email, $emailConstraint);
                    $errorListrs = $this->get('validator')->validateValue($emailrs, $emailConstraint);
                    if (count($errorList) > 0 && count($errorListrs) == 0) {
                        $email = $emailrs;
                    }*/

                    if (filter_var($emailrs, FILTER_VALIDATE_EMAIL)) {
                        $email = $emailrs;
                    }

                    $contentHtml = '<p>Bonjour,<br /><br />Ceci est un email qui confirme que l\'événement que vous avez ajouté <strong>'.$entity->getName().'</strong> a bien été enregistré.<br /><br />Le contenu sera affiché sur le site madatsara.com dès validation d\'un administrateur.<br /><br />Cordialement.</p>';
                    $mail = \Swift_Message::newInstance();
                    $mail
                        ->setFrom($from)
                        ->setTo($email)
                        ->setSubject('[MADATSARA] - Flyer/affiche ajouté')
                        ->setReplyTo('madatsara@gmail.com')
                        ->setBody($this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'mdtshomeBundle:Email:generique.html.twig',
                            array(
                                'content' => $contentHtml,
                            )
                        ))
                        ->setContentType('text/html');
                    $this->get('mailer')->send($mail);
                }

                $request->getSession()->getFlashBag()->add('notice', 'Le flyer a bien été enregistré. Un email de confirmation vous a été envoyé.');

                // On redirige vers la page contact
                return $this->redirect($this->generateUrl('mdts_frontendB_eventC_addeventA'));
            }
        }

        return $this->render('mdtsFrontendBundle:Default:addevent.html.twig', array('form' => $form->createView()));
    }
}
