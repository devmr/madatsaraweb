<?php

namespace mdts\FrontendBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

use Symfony\Component\OptionsResolver\OptionsResolver;

class EventByMemberType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entity = $builder->getData();
        $em = $options['er'];

        $events = array();

        if ($entity->getId() !== null && $entity->getId() > 0) {
            $encours = $em->getRepository('mdtsFrontendBundle:EventByMember')->find($entity->getId());
            if ( !is_null($entity->getEvent()) ) {
                $events[] = $entity->getEvent()->getId();
            }
        }

        $builder
            ->add('name')
            ->add('description')

            ->add('thematique', EntityType::class, [
                'placeholder' => 'Choisir',
                'required' => false,
                'class' => 'mdts\homeBundle\Entity\EventType',
                'choice_label' => 'name',
            ])
            ->add('save', SubmitType::class)
            ->add('status', EntityType::class, [
                'placeholder' => 'Choisir',
                'required' => false,
                'class' => 'mdts\FrontendBundle\Entity\EventByMemberStatus',
                'choice_label' => 'name',
                'query_builder' => function(\mdts\FrontendBundle\Entity\EventByMemberStatusRepository $repository){
                    return $repository->createQueryBuilder('s')
                        ->where('s.enable = 1 ')
                        ->add('orderBy', 's.name ASC');
                }
            ])
            ->add('eventunmapped', HiddenType::class, [
                'mapped' => false,
                'default' => implode(',', $events),

            ])
            ->add('sentauthor', HiddenType::class, [
                'mapped' => false,
                'default' => '0',

            ])

            ->add('authorinfotype', EntityType::class, [
                'placeholder' => 'Choisir',
                'required' => false,
                'class' => 'mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType',
                'choice_label' => 'name',
                'query_builder' => function(\mdts\FrontendBundle\Entity\EventByMemberAuthorinfoTypeRepository $repository){
                    return $repository->createQueryBuilder('s')
                        ->where('s.enable = 1 ')
                        ->add('orderBy', 's.name ASC');
                }
            ])

            ->add('emailauthorinfotype', EntityType::class, [
                'placeholder' => 'Choisir',
                'required' => false,
                'class' => 'mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoType',
                'choice_label' => 'name',
                'query_builder' => function(\mdts\FrontendBundle\Entity\EventByMemberEmailAuthorinfoTypeRepository $repository){
                    return $repository->createQueryBuilder('s')
                        ->where('s.enable = 1 ')
                        ->add('orderBy', 's.name ASC');
                }
            ])

            ->add('flyerFile', VichImageType::class, array(
                'required' => false,
                'allow_delete' => true, // not mandatory, default is true
                'download_uri' => true, // not mandatory, default is true
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('er');
         
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'mdts\FrontendBundle\Entity\EventByMember', 'er' => false,
        ));
    }

    /**
     * @return string
     */
//    public function getName()
    public function getBlockPrefix()
    {
        return 'mdts_frontendbundle_eventbymember';
    }
}
