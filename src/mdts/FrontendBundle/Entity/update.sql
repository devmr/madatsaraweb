--
-- 16 juillet 2018
--
CREATE TABLE chatbot_user (id INT AUTO_INCREMENT NOT NULL, user_id INT NULL, fbid VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_21D522B4A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE chatbot_user ADD CONSTRAINT FK_21D522B4A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id);
--
-- 17 juillet 2018
--
CREATE TABLE chatbot_session (id INT AUTO_INCREMENT NOT NULL, chatbot_user_id INT DEFAULT NULL, hears LONGTEXT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_764526E8FC220B6E (chatbot_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE chatbot_session ADD CONSTRAINT FK_764526E8FC220B6E FOREIGN KEY (chatbot_user_id) REFERENCES chatbot_user (id);
ALTER TABLE chatbot_user DROP FOREIGN KEY FK_21D522B4A76ED395;
DROP INDEX uniq_21d522b4a76ed395 ON chatbot_user;
CREATE UNIQUE INDEX UNIQ_3473772FA76ED395 ON chatbot_user (user_id);
ALTER TABLE chatbot_user ADD CONSTRAINT FK_21D522B4A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id);
CREATE TABLE chatbot_hears (id INT AUTO_INCREMENT NOT NULL, cmd VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
--
--
--
CREATE TABLE chatbot_fb_sticker (id INT AUTO_INCREMENT NOT NULL, url VARCHAR(255) NOT NULL, sticker VARCHAR(100) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX urlsticker_idx (url, sticker), FULLTEXT INDEX urlsticker_ftx (url, sticker), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
CREATE TABLE chatbot_images (id INT AUTO_INCREMENT NOT NULL, chatbot_user_id INT DEFAULT NULL, sticker_id INT DEFAULT NULL, url VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_726958D5FC220B6E (chatbot_user_id), INDEX IDX_726958D54D965A4D (sticker_id), INDEX url_idx (url), FULLTEXT INDEX url_ftx (url), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
ALTER TABLE chatbot_images ADD CONSTRAINT FK_726958D5FC220B6E FOREIGN KEY (chatbot_user_id) REFERENCES chatbot_user (id);
ALTER TABLE chatbot_images ADD CONSTRAINT FK_726958D54D965A4D FOREIGN KEY (sticker_id) REFERENCES chatbot_fb_sticker (id);