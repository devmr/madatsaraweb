<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * HistoryEventbymemberSent
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="mdts\FrontendBundle\Entity\HistoryEventbymemberSentRepository")
 */
class HistoryEventbymemberSent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\EventByMember")
     */
    private $eventbymember;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\homeBundle\Entity\Event")
     */
    private $event;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType")
     */
    private $authorinfotype;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set eventbymember
     *
     * @param \mdts\FrontendBundle\Entity\EventByMember $eventbymember
     *
     * @return HistoryEventbymemberSent
     */
    public function setEventbymember(\mdts\FrontendBundle\Entity\EventByMember $eventbymember = null)
    {
        $this->eventbymember = $eventbymember;

        return $this;
    }

    /**
     * Get eventbymember
     *
     * @return \mdts\FrontendBundle\Entity\EventByMember
     */
    public function getEventbymember()
    {
        return $this->eventbymember;
    }

    /**
     * Set event
     *
     * @param \mdts\homeBundle\Entity\Event $event
     *
     * @return HistoryEventbymemberSent
     */
    public function setEvent(\mdts\homeBundle\Entity\Event $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \mdts\homeBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set authorinfotype
     *
     * @param \mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType $authorinfotype
     *
     * @return HistoryEventbymemberSent
     */
    public function setAuthorinfotype(\mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType $authorinfotype = null)
    {
        $this->authorinfotype = $authorinfotype;

        return $this;
    }

    /**
     * Get authorinfotype
     *
     * @return \mdts\FrontendBundle\Entity\EventByMemberAuthorinfoType
     */
    public function getAuthorinfotype()
    {
        return $this->authorinfotype;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return HistoryEventbymemberSent
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
