<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Tools\Pagination\Paginator;

use mdts\homeBundle\Entity\SharedRepository;

/**
 * EventByMemberRepository.
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class EventByMemberRepository extends \Doctrine\ORM\EntityRepository
{
    public function findAllQuery($query = array(), $page = 0, $limit = 5)
    {
        $qb = $this->createQueryBuilder('a');
        $andw = false;
        if (isset($query['query']) && $query['query'] != '') {
            $qb->where('a.name LIKE :name')
                ->setParameter('name', '%'.$query['query'].'%');
            $andw = true;
        }

        $qb->orderBy('a.id', 'DESC');

        $query = $qb->getQuery();

//        $paginator = $this->paginate($query, $page, $limit);
        $paginator = SharedRepository::paginate($query, $page, $limit);

        return $paginator;
    }

    public function paginate($dql, $page, $limit)
    {
        $paginator = new Paginator($dql);

        // avoid msg: Not all identifier properties can be found in the ResultSetMapping: id
        $paginator->setUseOutputWalkers(false);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }
}
