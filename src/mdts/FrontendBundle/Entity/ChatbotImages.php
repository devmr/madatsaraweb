<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * ChatbotImages rrrd
 *
 * @ORM\Table(name="chatbot_images", indexes={
 *   @ORM\Index(name="url_idx", columns={"url"}),
 *   @ORM\Index(name="url_ftx", columns={"url"}, flags={"fulltext"})
 *   })
 * @ORM\Entity(repositoryClass="mdts\FrontendBundle\Repository\ChatbotImagesRepository")
 */
class ChatbotImages
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\ChatbotUser")
     */
    private $chatbotUser;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string")
     */
    private $url;

    /**
     * @ORM\ManyToOne(targetEntity="mdts\FrontendBundle\Entity\ChatbotFbSticker")
     */
    private $sticker;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chatbotUser
     *
     * @param mdts\FrontendBundle\Entity\ChatbotUser $chatbotUser
     *
     * @return ChatbotSession
     */
    public function setChatbotUser($chatbotUser)
    {
        $this->chatbotUser = $chatbotUser;

        return $this;
    }

    /**
     * Get chatbotUser
     *
     * @return mdts\FrontendBundle\Entity\ChatbotUser
     */
    public function getChatbotUser()
    {
        return $this->chatbotUser;
    }

    /**
     * Set hears
     *
     * @param string $hears
     *
     * @return ChatbotSession
     */
    public function setSticker($sticker)
    {
        $this->sticker = $sticker;

        return $this;
    }

    /**
     * Get hears
     *
     * @return string
     */
    public function getSticker()
    {
        return $this->sticker;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return ChatbotFbSticker
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return EventByMember
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return EventByMember
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

