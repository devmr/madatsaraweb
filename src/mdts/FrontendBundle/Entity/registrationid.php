<?php

namespace mdts\FrontendBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * registrationid.
 *
 * @ORM\Table(indexes={
 *      @ORM\Index(name="name_idx", columns={"registration_id"}),
 *      @ORM\Index(name="deviceid_idx", columns={"device_id"})
 *  })
 * @ORM\Entity(repositoryClass="mdts\FrontendBundle\Entity\registrationidRepository")
 */
class registrationid
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="registration_id", type="string", length=255)
     */
    private $registrationId;

    /**
     * @var string
     *
     * @ORM\Column(name="ip_address", type="string", length=255)
     */
    private $ipAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="android_modelname", type="string", length=255)
     */
    private $android_modelname;

    /**
     * @var string
     *
     * @ORM\Column(name="android_version", type="string", length=255)
     */
    private $android_version;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="country_isoCode", type="string", length=10, nullable=true)
     */
    private $country_isoCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country_name", type="string", length=255, nullable=true)
     */
    private $country_name;

    /**
     * @var string
     *
     * @ORM\Column(name="city_name", type="string", length=255, nullable=true)
     */
    private $city_name;

    /**
     * @var string
     *
     * @ORM\Column(name="location_timeZone", type="string", length=255, nullable=true)
     */
    private $location_timeZone;

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", type="string", length=255, nullable=true)
     */
    private $deviceId;

    /**
     * @ORM\ManyToMany(targetEntity="mdts\UserBundle\Entity\User", inversedBy="device", orphanRemoval=true)
     */
    private $user;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set registrationId.
     *
     * @param string $registrationId
     *
     * @return registrationid
     */
    public function setRegistrationId($registrationId)
    {
        $this->registrationId = $registrationId;

        return $this;
    }

    /**
     * Get registrationId.
     *
     * @return string
     */
    public function getRegistrationId()
    {
        return $this->registrationId;
    }

    /**
     * Set ipAddress.
     *
     * @param string $ipAddress
     *
     * @return registrationid
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    /**
     * Get ipAddress.
     *
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return registrationid
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return registrationid
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set androidModelname.
     *
     * @param string $androidModelname
     *
     * @return registrationid
     */
    public function setAndroidModelname($androidModelname)
    {
        $this->android_modelname = $androidModelname;

        return $this;
    }

    /**
     * Get androidModelname.
     *
     * @return string
     */
    public function getAndroidModelname()
    {
        return $this->android_modelname;
    }

    /**
     * Set androidVersion.
     *
     * @param string $androidVersion
     *
     * @return registrationid
     */
    public function setAndroidVersion($androidVersion)
    {
        $this->android_version = $androidVersion;

        return $this;
    }

    /**
     * Get androidVersion.
     *
     * @return string
     */
    public function getAndroidVersion()
    {
        return $this->android_version;
    }

    /**
     * Set countryIsoCode.
     *
     * @param string $countryIsoCode
     *
     * @return NewsletterAbonne
     */
    public function setCountryIsoCode($countryIsoCode)
    {
        $this->country_isoCode = $countryIsoCode;

        return $this;
    }

    /**
     * Get countryIsoCode.
     *
     * @return string
     */
    public function getCountryIsoCode()
    {
        return $this->country_isoCode;
    }

    /**
     * Set countryName.
     *
     * @param string $countryName
     *
     * @return NewsletterAbonne
     */
    public function setCountryName($countryName)
    {
        $this->country_name = $countryName;

        return $this;
    }

    /**
     * Get countryName.
     *
     * @return string
     */
    public function getCountryName()
    {
        return $this->country_name;
    }

    /**
     * Set cityName.
     *
     * @param string $cityName
     *
     * @return NewsletterAbonne
     */
    public function setCityName($cityName)
    {
        $this->city_name = $cityName;

        return $this;
    }

    /**
     * Get cityName.
     *
     * @return string
     */
    public function getCityName()
    {
        return $this->city_name;
    }

    /**
     * Set locationTimeZone.
     *
     * @param string $locationTimeZone
     *
     * @return NewsletterAbonne
     */
    public function setLocationTimeZone($locationTimeZone)
    {
        $this->location_timeZone = $locationTimeZone;

        return $this;
    }

    /**
     * Get locationTimeZone.
     *
     * @return string
     */
    public function getLocationTimeZone()
    {
        return $this->location_timeZone;
    }

    /**
     * Set deviceId.
     *
     * @param string $deviceId
     *
     * @return registrationid
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;

        return $this;
    }

    /**
     * Get deviceId.
     *
     * @return string
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user.
     *
     * @param \mdts\UserBundle\Entity\User $user
     *
     * @return registrationid
     */
    public function addUser(\mdts\UserBundle\Entity\User $user)
    {
        $this->user[] = $user;

        return $this;
    }

    /**
     * Remove user.
     *
     * @param \mdts\UserBundle\Entity\User $user
     */
    public function removeUser(\mdts\UserBundle\Entity\User $user)
    {
        $this->user->removeElement($user);
    }

    /**
     * Get user.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUser()
    {
        return $this->user;
    }
}
