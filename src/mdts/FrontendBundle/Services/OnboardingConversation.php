<?php
/**
 * Created by PhpStorm.
 * User: Miary
 * Date: 21/07/2018
 * Time: 23:10
 */

namespace mdts\FrontendBundle\Services;

use BotMan\BotMan\Messages\Incoming\Answer;
use BotMan\BotMan\Messages\Conversations\Conversation;
use BotMan\BotMan\Messages\Outgoing\Question;
use BotMan\BotMan\Messages\Outgoing\Actions\Button;
use BotMan\Drivers\Facebook\Extensions\QuickReplyButton;

class OnboardingConversation extends Conversation
{
    protected $sSlug;

    public function __construct($sSlug)
    {
        $this->sSlug = $sSlug;
    }

    public function run()
    {
        $this->start();
    }

    private function start()
    {
        /*if ($this->sSlug === 'arts-culture') {
            return $this->askLocalization();
        }*/
        $this->askLocalisationOrPays();
    }


    private function askLocalization()
    {
        $question = Question::create('Great. Can you give me your location?')
            ->addAction(QuickReplyButton::create('test')->type('location'));

        $this->ask($question, function (Answer $answer) {
            $this->bot->reply('Latitude: '.$answer->getMessage()->getLocation()
                    ->getLatitude().' Longitude: '.$answer->getMessage()->getLocation()->getLongitude());
        });
    }

    private function askLocalisationOrPays()
    {
        $question = Question::create('Veux-tu me donner ta localisation ?')->addButtons([
            Button::create('Oui')->value('localisation_y'),
            Button::create('Je vais chercher par pays')->value('localisation_n'),
        ]);

        $this->ask($question, function (Answer $answer) {
            if($answer->getValue() === 'yes') {
                $this->bot->reply('Awesome   ');
            }
        });
    }
}