<?php
/**
 * Created by PhpStorm.
 * User: Miary
 * Date: 20/05/2017
 * Time: 23:18
 */

namespace mdts\FrontendBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use BotMan\BotMan\BotMan;
use mdts\FrontendBundle\Entity\ChatbotUser;
use mdts\FrontendBundle\Entity\ChatbotSession;
use mdts\FrontendBundle\Entity\ChatbotFbSticker;
use mdts\FrontendBundle\Entity\ChatbotImages;

class FacebookBot
{

    private $_container;
    private $logger;
    private $em;

    public function __construct(Container $serviceContainer)
    {
        $this->_container = $serviceContainer;
        $this->logger = $this->_container->get('logger');
        $this->em = $this->_container->get('doctrine.orm.entity_manager');
    }
    public function saveCmd ($strHears, BotMan $bot)
    {

      // L'identifiant facebook de l'utilisateur
      $user = $bot->getUser();
      $iIdfb = $user->getId();

      // Enregistrer l'utilisateur
      $chatbotuser = $this->saveChatbotUser($iIdfb);

      // Enregistrer la session
      $chatbotsession = $this->saveChatbotSession($strHears, $bot, $chatbotuser);

      return $chatbotsession;

    }

    public function saveChatbotUser ($iFacebookId)
    {
      if (filter_var($iFacebookId, FILTER_VALIDATE_INT) <= 0) {
        return false;
      }

      // Find row in ChatbotUser
      $chatbotuser = $this->em->getRepository('mdtsFrontendBundle:ChatbotUser')->findOneByFbid($iFacebookId);
      $iChatbotuserId = method_exists($chatbotuser, 'getId') ? $chatbotuser->getId() : 0;

      // Find row in user
      $user = $this->em->getRepository('mdtsUserBundle:User')->findOneBy(array('facebook_id' => $iFacebookId));
      $iUserId = method_exists($user, 'getId') ? $user->getId() : 0;

      if ($iChatbotuserId === 0) {
        // Save user
        $chatbotuser = new ChatbotUser();
        $chatbotuser->setFbid($iFacebookId);
      }
        
      if ($iChatbotuserId === 0 && $iUserId > 0) {
          $chatbotuser->setUser($iUserId);
          // $this->logger->info( __FUNCTION__.' - line '.__LINE__ . ' user: ' . $firstname . ' - iRowUserid: ' . $iRowUserid);
      }

      if ($iChatbotuserId === 0) {
        // Étape 1 : On « persiste » l'entité
        $this->em->persist($chatbotuser);
        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $this->em->flush();
        // Attribuer id
        $iChatbotuserId = $chatbotuser->getId();
      }

      return $chatbotuser;

        
    }

    public function saveChatbotSession ($strHears, BotMan $bot, $chatbotuser)
    {
      if ($strHears === '') {
        return false;
      }

      // Ce que l'internaute a réellement tapé
      $arrPayload = $bot->getMessage()->getPayload();
      $arrMessage = array_key_exists('message', $arrPayload) ? $arrPayload['message'] : [];
      $strMessage = array_key_exists('text', $arrMessage) ? $arrMessage['text'] : '';

      // Enregistrer dans la session
      $chatbothears = $this->em->getRepository('mdtsFrontendBundle:ChatbotHears')->findOneByCmd($strHears);
      $iRowHearsdid = method_exists($chatbothears, 'getId') ? $chatbothears->getId() : 0;
      

      if ($iRowHearsdid > 0) {
        // Save user
        $chatbotsession = new ChatbotSession();
        $chatbotsession->setHears($chatbothears);
        $chatbotsession->setChatbotUser($chatbotuser);
        $chatbotsession->setHearsText($strMessage);

        // Étape 1 : On « persiste » l'entité
        $this->em->persist($chatbotsession);
        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $this->em->flush();

        return $chatbotsession;
      }

      return false;


    }

    /**
     *
     * Enregistrer images de bot
     *
     */
    
    public function savebotImages ($bot, $sStickerid, $sUrl)
    {
      if ($sUrl === '' && $iStickerid === '') {
        return false;
      }

      // L'identifiant facebook de l'utilisateur
      $user = $bot->getUser();
      $iIdfb = $user->getId();

      // Enregistrer l'utilisateur
      $chatbotuser = $this->saveChatbotUser($iIdfb);

      // Enregistrer la session
      $this->saveImage($bot, $chatbotuser, $sStickerid, $sUrl);
      
    }

    protected function saveImage ($bot, $chatbotuser, $sStickerid, $sUrl)
    {
      $iRowId = 0;
      if ($sStickerid !== '') {
        // Enregistrer dans la session
        $chatbotfbsticker = $this->em->getRepository('mdtsFrontendBundle:ChatbotFbSticker')->findOneBySticker($sStickerid);
        $iRowId = method_exists($chatbotfbsticker, 'getId') ? $chatbotfbsticker->getId() : 0;
      }


      // Si sticker non enregistré
      if ($sStickerid !== '' && $iRowId === 0) {
        // Save fbsticker
        $chatbotfbsticker = new ChatbotFbSticker();
        $chatbotfbsticker->setUrl($sUrl);
        $chatbotfbsticker->setSticker($sStickerid); 

        // Étape 1 : On « persiste » l'entité
        $this->em->persist($chatbotfbsticker);
        // Étape 2 : On « flush » tout ce qui a été persisté avant
        $this->em->flush();

        $iRowId = $chatbotfbsticker->getId();
      }

      // Save Images
      $chatbotimage = new ChatbotImages();
      $chatbotimage->setChatbotUser($chatbotuser);

      // Si sticker
      if ($sStickerid !== '' && $iRowId > 0) {
        $chatbotimage->setSticker($chatbotfbsticker); 
      }

      // Si images
      if ($sStickerid === '' && $sUrl !== '') {
        $chatbotimage->setUrl($sUrl); 
      }

      // Étape 1 : On « persiste » l'entité
      $this->em->persist($chatbotimage);
      // Étape 2 : On « flush » tout ce qui a été persisté avant
      $this->em->flush();
    }

}